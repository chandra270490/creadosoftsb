<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='MoveInStage_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(ob, pendingOrders, despStageQty)
	{
		// alert(JSON.stringify(despStageQty));
		var productRowId = $("#cboProduct").val();
          $("#tbl1").find("tr:gt(1)").remove();
          if(ob != null) 
          {
          	var table = document.getElementById("tbl1");
	        newRowIndex = table.rows.length;
	        row = table.insertRow(newRowIndex);

	        var cell = row.insertCell(0);
	        cell.innerHTML = productRowId;
	          cell.style.display="none";

	        var cell = row.insertCell(1);
	        cell.innerHTML = $("#cboProduct option:selected").text(); 
	        // cell.style.display="none";

          	var tot = 0;
            for(var i=0; i<ob.length; i++)
            {
		        var cell = row.insertCell(i+2);
		        cell.innerHTML = ob[i].avlQty;
		        if(i>0)  ///excluding no stage
		        {
		        	tot += parseInt(ob[i].avlQty);
		        }
            }

            /////////28 jun 18
            var cell = row.insertCell(i+2);
		    cell.innerHTML = despStageQty[0].avlQty;
		    tot += parseInt(despStageQty[0].avlQty);
            ///////END - 28 jun 18

            var cell = row.insertCell(i+3);
	        cell.innerHTML = tot; 
	        cell.style.color="red";


            var cell = row.insertCell(i+4);
            if(pendingOrders != null) 
          	{
	        	cell.innerHTML = pendingOrders[0].pendingQty; 
	        }
	        else
	        {
	        	cell.innerHTML = '0'; 
	        }
	        cell.style.color="blue";

	        $("#cboFromStage").prop("disabled", false);
          	$("#cboToStage").prop("disabled", false);
          }
          
          if( ob.length == 0 )
          {
          	alert("Set Opening bal first... even if it is 0...");
          	$("#cboFromStage").prop("disabled", true);
          	$("#cboToStage").prop("disabled", true);
          }

        
			
	}

	function moveData()
	{	
		var dt = $("#dt").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dt");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dt").focus();
				return;
			}
		// }

		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}

		productRowId = $("#cboProduct").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			$("#cboProduct").focus();
			return;
		}

		

		fromStage = $("#cboFromStage").val();
		if(fromStage == "-1")
		{
			alertPopup("Select form stage...", 8000);
			$("#cboFromStage").focus();
			return;
		}


		qty = parseInt($("#txtMove").val());
		if( qty<=0 )
		{
			alertPopup("QTy can not be zero...", 8000);
			$("#txtMove").focus();
			return;
		}
		///////avlQty must be more than moving qty
		// var x = $("#trStageRowId td:first").text(fromStage).text();
		var idx = 0;
		$('#trStageRowId td').each(function($index){
		    if($(this).html() == fromStage)
		        idx = $index;
		})

		var avlQty =parseInt($(tbl1).find('tr:eq(2) td:eq(' + idx + ')').text());
		
		// alert(qty + "  " + avlQty);
			// return;
		if(avlQty < qty && fromStage > 13) /// At noStage qty can be moved more than available...
		{
			alertPopup("Qty more than available...", 8000);
			$("#txtMove").focus();
			return;	
		}


		toStage = $("#cboToStage").val();
		if(toStage == "-1")
		{
			alertPopup("Select to stage...", 8000);
			$("#cboToStage").focus();
			return;
		}

		colourRowId = $("#cboColour").val();
		if( colourRowId == "-1" && (toStage == "26" || fromStage == "26") )  ///if desp stage and color not selected
		{
			msgBoxError("Error", "Select Colour...");
			return;
		}
		if(toStage < 26)  ///if desp stage and color not selected
		{
			// colourRowId = -1;
		}


		if(fromStage > toStage)
		{
			direction = 'B';
		}
		else
		{
			direction = 'F';
		}	

		weightage = $("#txtStageWeightage").val();
		remarks = $("#txtRemarks").val();

		$.ajax({
				'url': base_url + '/' + controller + '/moveData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productRowId': productRowId
							, 'dt': dt
							, 'empRowId': empRowId
							, 'fromStage': fromStage
							, 'toStage': toStage
							, 'qty': qty
							, 'weightage': weightage
							, 'direction': direction
							, 'remarks': remarks
							, 'colourRowId': colourRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
						setTable(data['ob'],  data['pendingOrders'],  data['despStageQty']);
						$("#cboFromStage").val("-1");
						$("#cboToStage").val("-1");
						$("#cboColour").val("-1");
						$("#txtMove").val("");
						$("#txtStageWeightage").val("");
							// alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Move in Stage</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
		              	?>
		              	<script>
							$( "#dt" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dt").val(dateFormat(new Date()));
						</script>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Employee Name:</label>";
							echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
		              	?>
		          	</div>
		          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Name:</label>";
							echo form_dropdown('cboProduct',$products, '-1',"class='form-control' id='cboProduct'");
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:15px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From Stage:</label>";
							// echo form_dropdown('cboFromStage',$stagesFrom, '-1',"class='form-control' id='cboFromStage'");
		              	?>
		              	<select id="cboFromStage" class="form-control">
			              <option value="-1" weightage="">--- Select ---</option>
			              <?php
			                foreach ($stagesFrom as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['stageRowId']; ?>  weightage="<?php echo $row['weightage']; ?>" ><?php echo $row['stageName']; ?></option>
			              <?php
			                }
			              ?>
		            	</select>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To Stage:</label>";
							echo form_dropdown('cboToStage',$stagesTo, '-1',"class='form-control' id='cboToStage'");
		              	?>
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Move Qty.:</label>";
							echo '<input type="number" step="1" name="txtMove" value="" class="form-control" maxlength="10" id="txtMove" />';
		              	?>
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Stage Weightage:</label>";
							echo '<input type="number" step="1" name="txtStageWeightage" value="" class="form-control" maxlength="10" id="txtStageWeightage" />';
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:10px;">					
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
							echo '<input type="text" name="txtRemarks" value="" class="form-control" maxlength="250" id="txtRemarks" />';
		              	?>
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" id="divColour">
		          		<?php
						echo "<label style='color: red;'>Colour:</label>";
						echo form_dropdown('cboColour',$colours, '-1', "class='form-control' id='cboColour'");
	              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='moveData();' value='Move Now' id='btnMove' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:200px; overflow:auto;box-shadow:5px 5px #d3d3d3;border-radius:25px">
				<table class='table table-bordered' id='tbl1'>
					 <tr>
						<th style='display:none;'>ProductRowId</th>
						<th style='display:none1;'>Product</th>
						<?php
							foreach ($stages4table as $row) 
							{
								echo "<th>".$row['stageName']."</th>";
							}
						?>
						<th style='display:none1;'>Total</th>
						<th style='display:none1;'>Orders</th>
					 </tr>
					 <tr id="trStageRowId" style='display:none;'>
						<td style='display:none;'></td>
						<td style='display:none1;'></td>
						<?php
							foreach ($stages4table as $row) 
							{
								echo "<td>".$row['stageRowId']."</td>";
							}
						?>
						<td style='display:none1;'></td>
						<td style='display:none1;'></td>
					 </tr>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>





<script type="text/javascript">
	$(document).ready(function(){
        $("#cboProduct").change(function(){
          var productRowId = $("#cboProduct").val();
          if(productRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductStatus',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productRowId': productRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                	setTable(data['ob'], data['pendingOrders'], data['despStageQty'])
                }
              }
          });
        });
      });

$(document).ready(function(){
        $("#cboFromStage").change(function(){
        	fromStage = parseInt($("#cboFromStage").val());
        	$("#cboToStage").val(fromStage+1);
        	// toStage = parseInt($("#cboToStage").val());
        	// if(toStage == 26) ////// if Despatch stage selected
        	// {
        	// 	visibleOnDespStage();
        	// }
        	$("#txtStageWeightage").val($('option:selected', '#cboFromStage').attr('weightage'));
        	visibleOnDespStage();
        });

        $("#divColour").css({visibility: "hidden"});
        $("#cboToStage").on('change', visibleOnDespStage);

        function visibleOnDespStage()
        {
        	fromStage = parseInt($("#cboFromStage").val());
        	toStage = parseInt($("#cboToStage").val());
        	if(toStage == 26 || fromStage == 26 ) ////// if Despatch stage selected
        	{
        		// alert();
        		$("#divColour").css({visibility: "visible"});
        	}
        	else
        	{
        		$("#divColour").css({visibility: "hidden"});
        	}
        	// $("#cboToStage").val(fromStage+1);
        	// $("#txtStageWeightage").val($('option:selected', '#cboFromStage').attr('weightage'));
        }
    });
</script>