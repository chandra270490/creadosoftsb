<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Products_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 var burl = '<?php echo base_url();?>';
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
			  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].productRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].productRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productCategoryRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].productCategory;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].productLength;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].productHeight;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].productWidth;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].uom;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].productRate;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].imagePath;
	          cell.style.display="none";
	          var cell = row.insertCell(12);
	          if( records[i].imagePathThumb != "N" )
	          {
				  var pic = burl + "bootstrap/images/products/" + records[i].imagePathThumb;
		          cell.innerHTML = "<img id=\'" + records[i].productRowId + "\' src=\'" + pic + "\' width='60px' alt='' />";
	      	  }
	      	  else
	      	  {
	      	  	  cell.innerHTML = "<img src='' width='60px' alt='' />";
	      	  }
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].imagePathThumb;
	          cell.style.display="none";
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].weightage;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].roLevel;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].hsn;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId, 'globalImageSrc': globalImageSrc}, 
				'success': function(data){
					if(data)
					{
						blankControls();
						setTable(data['records'])
						alertPopup('Record deleted...', 4000);
						
						$("#txtProductName").focus();
					}
				},
				'error': function(data){
					alert(data);
				}
			});
	}
	

	$(document).ready(function(e)
	{
		$("#frm").on("submit", (function(e)
		{
			e.preventDefault();
			productName = $("#txtProductName").val().trim();
			if(productName == "")
			{
				alertPopup("Town name can not be blank...", 8000);
				$("#txtProductName").focus();
				return;
			}
			productCategoryRowId = $("#cboProductCategories").val();
			if(productCategoryRowId == "-1")
			{
				alertPopup("Select product category...", 8000);
				$("#cboProductCategories").focus();
				return;
			}
			var uom = $("#cboUom").val();
			if(uom == "-1")
			{
				alertPopup("Select UOM...", 8000);
				$("#cboUom").focus();
				return;
			}
			var btn = $("#btnSave").val();
			// alert($("#txtRoLevel").val());
			if(btn == "Save")
			{
				// alert();
				// return;
				$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'data': new FormData(this),
					'contentType':false,
					'cache': false,
					'processData': false,
					// 'dataType': 'json',
					'success': function(data)
					{
						// alert( (data)  );
							if(data == "Duplicate...")
							{
								alertPopup("Duplicate record...", 6000);
							}
							else if(data == "Done...")
							{
								alertPopup('Record saved...', 6000);
								blankControls();
								///////////
								$.ajax({
									'url': base_url + '/' + controller + '/loadRecords',
									'type': 'POST',
									'data': 'data',
									'contentType':false,
									'cache': false,
									'processData': false,
									'dataType': 'json',
									'success': function(data)
									{
										setTable(data['records']) ///loading records in tbl1
										$("#txtProductName").focus();
									},
									'error': function(data)
									{
										alert(data);
									}
								});

								/////////

							}
							else
							{
								alert(data);
								// blankControls();
							}
						// }
					},
					'error': function(data)
					{
						alert(data.responseText);
					}
				});
			}
			else if(btn == "Update")
			{
				// var fd = new FormData(this);
				$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'data': new FormData(this),
					'contentType':false,
					'cache': false,
					'processData': false,
					// 'dataType': 'json',
					'success': function(data)
					{
						// alert( (data)  );
							if(data == "Duplicate...")
							{
								alertPopup("Duplicate record1...", 6000);
							}
							else if(data == "Done...")
							{
								alertPopup('Record updated...', 6000);
								blankControls();
								///////////
								$.ajax({
									'url': base_url + '/' + controller + '/loadRecords',
									'type': 'POST',
									'data': 'data',
									'contentType':false,
									'cache': false,
									'processData': false,
									'dataType': 'json',
									'success': function(data)
									{
										setTable(data['records']); ///loading records in tbl1
										$("#btnSave").val("Save");
										$("#txtProductName").focus();
									},
									'error': function(data)
									{
										alert(data);
									}
								});

								/////////

							}
							else
							{
								alert(data);
								// blankControls();
							}
						// }
					},
					'error': function(data)
					{
						alert(data.responseText);
					}
				});
			}

		}
		))
		
	});
	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecordsDel',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						blankControls();
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						$("#txtProductName").focus();
					}
				}
			});
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px;font-size:3vw'>Deleted Products</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Name: <span style='color: red;'>*</span></label>";
							echo form_input('txtProductName', '', "class='form-control' autofocus id='txtProductName' style='text-transform: capitalize;' maxlength=30 autocomplete='off'");
		              	?>
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category: <span style='color: red;'>*</span></label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1',"class='form-control' id='cboProductCategories'");
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:15px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Length:</label>";
							echo '<input type="number" step="0.1" name="txtLength" value="" placeholder="" class="form-control" maxlength="10" id="txtLength" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Height:</label>";
							echo '<input type="number" step="0.1" name="txtHeight" value="" placeholder="" class="form-control" maxlength="10" id="txtHeight" />';
		              	?>
		          	</div>

					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Width:</label>";
							echo '<input type="number" step="0.1" name="txtWidth" value="" placeholder="" class="form-control" maxlength="10" id="txtWidth" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Rate:</label>";
							echo '<input type="number" step="0.1" name="txtRate" value="" placeholder="" class="form-control" maxlength="10" id="txtRate" />';
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:15px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							$types = array();
							$types['-1'] = '--- Select ---';
							$types['in'] = "in";
							$types['cm'] = "cm";
							$types['mm'] = "mm";
							$types['ft'] = "ft";
							$types['Kg'] = "Kg";
							$types['Ltr'] = "Ltr";
							$types['Pc'] = "Pc";
							echo "<label style='color: black; font-weight: normal;'>UOM: <span style='color: red;'>*</span></label>";
							//echo form_dropdown('cboUom', $types, '-1', "class='form-control' id='cboUom'");
						?> 
						<select id="cboUom" name="cboUom" class="form-control" required>
							<option value="-1">--Select--</option>
							<?php
								$sql_sr = "SELECT * FROM `prod_uom_mst` ";
								$qry_sr = $this->db->query($sql_sr);
								foreach($qry_sr->result() as $row){
									$prod_uom_name = $row->prod_uom_name;
							?>
							<option value="<?=$prod_uom_name;?>"><?=$prod_uom_name;?></option>
							<?php 
								} 
							?>
						</select>
		          	</div>

		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Weightage:</label>";
							echo '<input type="number" pattern=" 0+\.[0-9]*[1-9][0-9]*$" step="0.01" name="txtWeightage" value="" placeholder="" class="form-control" maxlength="10" id="txtWeightage" />';
		              	?>
		          	</div>

		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>RO Level:</label>";
							echo '<input type="number" step="0.01" name="txtRoLevel" value="0" class="form-control" maxlength="10" id="txtRoLevel" />';
		              	?>
		          	</div>

		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>HSN:</label>";
							echo '<input type="text" name="txtHsn" class="form-control" maxlength="20" id="txtHsn" />';
		              	?>
		          	</div>

					<!-- Added By CNS 202012180003 -->
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Delete Item: <span style='color: red;'>*</span></label>";
							echo '<select id="deleted" name="deleted" class="form-control"><option value="N">No</option><option value="Y">Yes</option></select>';
		              	?>
		          	</div>
					<!-- End CNS -->

					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<label style='color: black; font-weight: normal;'>&nbsp; </span></label>
						<input type="file" id="imageFile" name="imageFile" class='form-control' style="" />
					</div>

					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					</div>

					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
		              	?>
		              	<input type="submit" value="Save" id="btnSave"  class='btn btn-primary btn-block' />
		          	</div>
				</div>

				<div class="row" style="margin-top:15px;">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<img src="" id="img"  alt="." />
						<label id="btnImageHide" class="" onclick="hideImage();"  title="Remove Photo"><span id="spanHideImage" class="glyphicon glyphicon-remove-circle" style="text-shadow: 0px 0px 9px #000000;position:absolute; cursor: pointer;cursor: hand;"  onmouseover="this.style.textShadow='0px 0px 9px red';" onmouseout="this.style.textShadow='0px 0px 9px #000000';"></span></label>
						<script type="text/javascript">
							function setRemoveImagePosition()
							{
								// myAlert("SS");
							    var left = Math.floor($('#img').offset().left);
							    var width = Math.floor($('#img').width());
							    var a = left + width - 35;
							    var a1 = left + 10;
							    var top1 = Math.floor($('#img').offset().top);
							    var height = Math.floor($('#img').height());
							    var b = top1 + height - 20;
								// $('#spanHeaderCamera').offset({top:b,left:a});
								$('#spanHideImage').offset({top:b,left:a1});
							}
							setRemoveImagePosition();
							function hideImage()
							{
								$('#img').attr("src", "");
								$("#imageFile").val("");
								$("#txtPhotoChange").val("yes");
							}	
						</script>
					</div>
				</div>

				<!-- <div class="row" style="margin-top:15px;">
					<div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
		              	?>
		              	<input type="submit" value="Save" id="btnSave"  class='btn btn-primary form-control' />
		          	</div>
				</div> -->

				<div class="row" style="margin-top:15px; display:none;">
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo form_input('txtHiddenRowId', '', 'id="txtHiddenRowId" placeholder="" maxlength="200"  autocomplete="off" readonly class="form-control"');
						?>
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo form_input('txtPhotoChange', '', 'id="txtPhotoChange" placeholder="" maxlength="200"  autocomplete="off" readonly class="form-control"');
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='display:none;'>productRowId</th>
					 	<th>Product Name</th>
						<th style='display:none;'>productCategoryRowId</th>
					 	<th>P.Category Name</th>
					 	<th>Length</th>
					 	<th>Height</th>
					 	<th>Width</th>
					 	<th>UOM</th>
					 	<th>Rate</th>
					 	<th style='display:none;'>imagePath</th>
					 	<th>image</th>
					 	<th style='display:none;'>imagePathThumb</th>
					 	<th>Weightage</th>
					 	<th>RO Level</th>
					 	<th>HSN</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 

						foreach ($records as $row) 
						{
						 	$rowId = $row['productRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['productRowId']."</td>";
						 	echo "<td>".$row['productName']."</td>";
						 	echo "<td style='display:none;'>".$row['productCategoryRowId']."</td>";
						 	echo "<td>".$row['productCategory']."</td>";
						 	echo "<td>".$row['productLength']."</td>";
						 	echo "<td>".$row['productHeight']."</td>";
						 	echo "<td>".$row['productWidth']."</td>";
						 	echo "<td>".$row['uom']."</td>";
						 	echo "<td>".$row['productRate']."</td>";
						 	echo "<td style='display:none;'>".$row['imagePath']."</td>";
						 	if( $row['imagePathThumb'] != "N")
						 	{
								$pic = base_url();
			 					$pic = $pic."bootstrap/images/products/".$row['imagePathThumb'];
							 	echo "<td class='text-center'><img id='".$rowId."' src='".$pic."' width='60px' /></td>";
						 	}
						 	else
						 	{
						 		echo "<td class='text-center'><img src='' width='60px' /></td>";
						 	}
						 	echo "<td style='display:none;'>".$row['imagePathThumb']."</td>";
						 	echo "<td>".$row['weightage']."</td>";
						 	echo "<td>".$row['roLevel']."</td>";
						 	echo "<td>".$row['hsn']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 20px;" >
		<div class="col-lg-7 col-sm-7 col-md-7 col-xs-0"></div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0"></div>

	</div>
</div>



<div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">WSS</h4>
        </div>
        <div class="modal-body">
            <p>Are you sure <br /> Delete this record..?</p>
        </div>
        <div class="modal-footer">
            <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	var globalrowid;
	var globalImageSrc;
	function delrowid(rowid)
	{
		globalrowid = rowid;
		globalImageSrc = $("#"+globalrowid).attr('src');
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();
		productName = $(this).closest('tr').children('td:eq(3)').text();
		productCategoryRowId = $(this).closest('tr').children('td:eq(4)').text();
		productLength = $(this).closest('tr').children('td:eq(6)').text();
		productHeight = $(this).closest('tr').children('td:eq(7)').text();
		productWidth = $(this).closest('tr').children('td:eq(8)').text();
		uom = $(this).closest('tr').children('td:eq(9)').text();
		roLevel = $(this).closest('tr').children('td:eq(15)').text();
		hsn = $(this).closest('tr').children('td:eq(16)').text();
		productRate = $(this).closest('tr').children('td:eq(10)').text();
		weightage = $(this).closest('tr').children('td:eq(14)').text();
		imageSrc = $("#"+globalrowid).attr('src');

		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#txtProductName").val(productName);
		$("#txtWeightage").val(weightage);
		$("#cboProductCategories").val(productCategoryRowId);
		$("#cboUom").val(uom);
		$("#txtRoLevel").val(roLevel);
		$("#txtHsn").val(hsn);
		$("#txtLength").val(productLength);
		$("#txtHeight").val(productHeight);
		$("#txtWidth").val(productWidth);
		$("#txtRate").val(productRate);
		$("#img").attr("src", imageSrc)
		$('#imageFile').val("");
		$("#txtHiddenRowId").val(globalrowid);
		$("#txtPhotoChange").val("no");
		$("#txtProductName").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );




	$(document).ready(function() {
	    $("#imageFile").on('change', function() 
	    {
		      //Get count of selected files
			var src = document.getElementById("imageFile");
			var target = document.getElementById("img");
			// var target1 = document.getElementById("imgPreview");
			var countFiles = $(this)[0].files.length;
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		  	if (extn == "jpg" || extn == "jpeg") 
		  	{
		        if (typeof(FileReader) != "undefined") 
		        {
		          //loop for each file selected for uploaded.
		          for (var i = 0; i < countFiles; i++) 
		          {
		            var reader = new FileReader();
		            reader.onload = function(e) {
		            	target.src = e.target.result; 
		            	target.height=200;
		            	// target1.src = e.target.result; 
		            }
		            reader.readAsDataURL($(this)[0].files[i]);
		          }
		          $("#txtPhotoChange").val("yes");
		        } 
		        else 
		        {
		          alert("This browser does not support FileReader.");
		        }
		        // setCoordinates();
		        // showCropper();
		        // updatePreview();
		    } 
		    else 
		    {
		    	alert("Pls select only images");
		    }
	    });
	  });
</script>