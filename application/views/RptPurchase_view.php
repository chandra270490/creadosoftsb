<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptPurchase_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 // alert(records.length);
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].poRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].poRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vNo;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].billDt));
	          // cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.innerHTML = parseFloat(records[i].totalDiscountAmt) + parseFloat(records[i].totalAmtAfterDiscount) ;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].avgDiscountPer;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].totalDiscountAmt;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].totalAmtAfterDiscount;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].gst5TaxAmt;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].gst12TaxAmt;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].gst18TaxAmt;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].gst28TaxAmt;
	          var cell = row.insertCell(16);
	          // var taxTotal = parseFloat(records[i].gst5TaxAmt)+parseFloat(records[i].gst12TaxAmt)+parseFloat(records[i].gst18TaxAmt)+parseFloat(records[i].gst28TaxAmt);
	          cell.innerHTML = records[i].totalNetAmt;//parseFloat(records[i].totalAmtAfterDiscount)+taxTotal;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].totalQty;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo + " [" + dateFormat(new Date(records[i].vDt)) + "]";	
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].purchaseOrExpense;	
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].billNo;
	  	  }

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    fixedHeader: true
		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		$("#tbl1 tr").on('click', showDetail);
			
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		// alert(partyRowId);
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'partyRowId': partyRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					}
				}
		});
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "poRowId" : $(tr).find('td:eq(2)').text()
		            , "vType" : $(tr).find('td:eq(3)').text()
		            , "vNo" :$(tr).find('td:eq(4)').text()
		            , "vDt" :$(tr).find('td:eq(5)').text()
		            , "partyRowId" :$(tr).find('td:eq(6)').text()
		            , "partyName" :$(tr).find('td:eq(7)').text()
		            , "totalAmt" :$(tr).find('td:eq(8)').text()
		            , "avgDiscountPer" :$(tr).find('td:eq(9)').text()
		            , "totalDiscountAmt" :$(tr).find('td:eq(10)').text()
		            , "totalAmtAfterDiscount" :$(tr).find('td:eq(11)').text()
		            , "gst5TaxAmt" :$(tr).find('td:eq(12)').text()
		            , "gst12TaxAmt" :$(tr).find('td:eq(13)').text()
		            , "gst18TaxAmt" :$(tr).find('td:eq(14)').text()
		            , "gst28TaxAmt" :$(tr).find('td:eq(15)').text()
		            , "net" :$(tr).find('td:eq(16)').text()
		            , "totalQty" :$(tr).find('td:eq(17)').text()
		            , "vNo2" :$(tr).find('td:eq(18)').text()
		            , "purchaseOrExpense" :$(tr).find('td:eq(19)').text()
		            , "refBillNoDt" :$(tr).find('td:eq(20)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();
		// var status = $("#cboStatus option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
							, 'status': 'status'
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
	function storeTblValuesDetail()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(0)').text()
		            , "productRowId" : $(tr).find('td:eq(1)').text()
		            , "productName" : $(tr).find('td:eq(2)').text()
		            , "rQty" :$(tr).find('td:eq(3)').text()
		            , "rate" :$(tr).find('td:eq(4)').text()
		            , "amt" :$(tr).find('td:eq(5)').text()
		            , "billNo" :$(tr).find('td:eq(6)').text()
		            , "billDt" :$(tr).find('td:eq(7)').text()
		            , "discount" :$(tr).find('td:eq(8)').text()
		            , "amtAfterDiscount" :$(tr).find('td:eq(9)').text()
		            , "gstPer" :$(tr).find('td:eq(10)').text()
		            , "gstAmt" :$(tr).find('td:eq(11)').text()
		            , "net" :$(tr).find('td:eq(12)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportDetailData()
	{	
		var TableData;
		TableData = storeTblValuesDetail();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No detail to print...", 8000);
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportDataDetail',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
							, 'globalVno': globalVno
							, 'globalVdt': globalVdt
							, 'globalPartyName': globalPartyName
							, 'globalOrderType': globalOrderType
							, 'globalCommitDt': globalCommitDt
							, 'globalStatus': globalStatus
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container-fluid" style="width: 95%">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Purchase Report</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From (Ref Bill Dt.):</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});

						    // Set the Current Date-50 as Default
						    // dt=new Date();
     					//     dt.setDate(dt.getDate() - 50);
   		 				// 	$("#dtFrom").val(dateFormat(dt));

   		 					// Set the Current Date as Default
							$("#dtFrom").val(dateFormat(new Date()));
						</script>					
		          	</div>
		          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To (Ref Bill Dt.):</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Party:</label>";
							echo form_dropdown('cboParty',$parties, '-1',"class='form-control' id='cboParty'");
		              	?>
		          	</div>
					
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>poRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Ref.Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none1;'>Total Amt</th>
					 	<th style='display:none1;'>Dis Per.</th>
					 	<th style='display:none1;'>Dis Amt.</th>
					 	<th style='display:none1;'>Amt After Dis.</th>
					 	<th style='display:none1;'>GST 5%</th>
					 	<th style='display:none1;'>GST 12%</th>
					 	<th style='display:none1;'>GST 18%</th>
					 	<th style='display:none1;'>GST 28%</th>
					 	<th style='display:none1;'>Net Amt</th>
					 	<th style='display:none1;'>Total Qty</th>
					 	<th>V.No. [vDt]</th>
					 	<th style='display:none1;'>Purchase/Exp</th>
					 	<th style='display:none1;'>Ref Bill#</th>

					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tblProducts'>
				 <thead>
					 <tr>
					 	<th style='display:none;'>poDetailRowid</th>
					 	<th style='display:none;'>ProductRowId</th>
					 	<th>Product</th>
					 	<th>R.Qty</th>
					 	<th>Rate</th>
					 	<th>Amt</th>
					 	<th style='display:none;'>Bill#</th>
					 	<th style='display:none;'>B.Dt</th>
					 	<th>Dis%</th>
					 	<th>Amt After Dis.</th>
					 	<th>GST%</th>
					 	<th>GST Amt</th>
					 	<th>Net</th>				 
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportDetailData();' value='Export Detail' id='btnLoadDetail' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>
</div>





<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );


	// $(document).ready(function()
	// {
	//     $("#tbl1 tr").on('click', showDetail);
	// });
	var globalVno, globalVdt, globalPartyName, globalOrderType,globalCommitDt, globalStatus 
	function showDetail()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		poRowId = $(this).closest('tr').children('td:eq(2)').text();
		globalVno = $(this).closest('tr').children('td:eq(4)').text();
		globalVdt = $(this).closest('tr').children('td:eq(5)').text();
		globalPartyName = $(this).closest('tr').children('td:eq(7)').text();
		// alert(poRowId);
		$.ajax({
			'url': base_url + '/RptPurchase_Controller/getProducts',
			'type': 'POST', 
			'data':{'poRowId':poRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data));
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['purchaseDetail'].length; i++)
		        {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = data['purchaseDetail'][i].poDetailRowId;
		          cell.style.display="none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['purchaseDetail'][i].productRowId;
		          cell.style.display="none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['purchaseDetail'][i].productName;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['purchaseDetail'][i].receivedQty;
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['purchaseDetail'][i].rate;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['purchaseDetail'][i].receivedQty * data['purchaseDetail'][i].rate;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['purchaseDetail'][i].billNo;
		          cell.style.display="none";
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['purchaseDetail'][i].billDt;
		          cell.style.display="none";
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['purchaseDetail'][i].discount;
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['purchaseDetail'][i].amtAfterDiscount;
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['purchaseDetail'][i].gstRate;
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['purchaseDetail'][i].gstAmt;
		          var cell = row.insertCell(12);
		          cell.innerHTML = data['purchaseDetail'][i].netAmt;
		        }
				
		        $("#tblProducts tr").on("click", highlightRow);	
			}
		});

	}
</script>