<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var sessionCap = "";
	// sessionCap = '<?php echo($this->session->userdata('captchaWord'));?>';
	sessionCap = '<?php echo $word;?>';
	// alert(sessionCap);
	function refreshCaptcha()
	{
		var controller='Home_Controller';
		var base_url='<?php echo site_url();?>';
		$.ajax({
					'url': base_url + '/' + controller + '/refreshCaptcha',
					'type': 'POST',
					'data': {'name': 'name'},
					'dataType': 'json',
					'success': function(data){
					if(data){
						// alert(data['word']);
						
						// sessionCap = '<?php echo json_encode($this->session->all_userdata());?>';
						// sessionCap = data['word'];
						// alert(sessionCapNew);
						sessionCap = data['word'];
						$("#captcha").html(data['image']);
						// myAlert('Thanks for registering with us.\nVery soon our executive will contact you... :)');
						}
				}
			});
	}
	function login()
	{	
		var controller='Login_controller';
		var base_url='<?php echo site_url();?>';
		location.href=base_url + '/' + controller;
	}
	function search()
	{	
		// alert("inside Search");
		var errNumber=0;

		var val = $("#cboSearchBloodGroup").val();
		var bloodGroup = $("#cboSearchBloodGroup option:selected").text();
		if(val==-1)
		{
			// alert(val);
			$("#cboSearchBloodGroup").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
			errNumber++;
		}
		else
		{
			$("#cboSearchBloodGroup").css("box-shadow","none");
		}
		
		if(errNumber>0)
		{
			myAlert("Please Fill All the compulsory details");
			return;
		}

		// alert("dd");
		var controller='Home_Controller';
		var base_url='<?php echo site_url();?>';
		$.ajax({
				'url': base_url + '/' + controller + '/search',
				'type': 'POST',
				'data': {'bloodGroup': bloodGroup},
				'dataType': 'json',
				'success': function(data){
					if(data){
						// alert(data);
						var table = document.getElementById("tblSearchResult");

						// Making table empty and appending heading rows
						$("#tblSearchResult").empty();
						var row = table.insertRow(0);
						var cell = row.insertCell(0);
						cell.innerHTML = "<b>S.no</b>";
						var cell = row.insertCell(1);
						cell.innerHTML = "<b>Name</b>";
						var cell = row.insertCell(2);
						cell.innerHTML = "<b>Locality</b>";
						var cell = row.insertCell(3);
						cell.innerHTML = "<b>Contact</b>";

						// Filling table with search result.
						$.each(data, function( index, value ) {
							var newRowIndex = table.rows.length;
							var row = table.insertRow(newRowIndex);
							var cell = row.insertCell(0);
							cell.innerHTML = index+1;
							// alert(index+1);
							var cell = row.insertCell(1);
							cell.innerHTML = value.name;
							var cell = row.insertCell(2);
							cell.innerHTML = value.locality;
							var cell = row.insertCell(3);
							// cell.innerHTML = "<a href='<?php  echo base_url();  ?>index.php/Homepage_Controller/index/B-'>Contact</a>";
							// var bloodGroup='B+';
							cell.innerHTML = "<a href='<?php  echo base_url();  ?>index.php/Homepage_Controller/index/"+bloodGroup+"'>Contact</a>";
						});
					}
					else
					{
						alert('failure');
					}
				}
		});
		
	}

	var controller='Home_Controller';
	var base_url='<?php echo site_url();?>';

	function loaddata()
	{	
		// alert("FF");
		var errNumber=0;

		var cap=$("#txtCaptcha").val().trim();
		// sessionCap = '<?php echo($this->session->userdata('captchaWord'));?>';
		// alert(sessionCap);
		if(cap.toUpperCase() != sessionCap.toUpperCase())
		{
			myAlert("Captcha mismatch...");
			return;
			// $("#txtCaptcha").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
			// errNumber++;
		}
		else
		{
			$("#txtCaptcha").css("box-shadow","none");
		}


		var name=$("#txtName").val().trim();
		if(name=="")
		{
			$("#txtName").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
			errNumber++;
		}
		var mobile1=$("#txtMobile1").val().trim();
		if(mobile1=="")
		{
			$("#txtMobile1").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
			errNumber++;
		}
		var bloodGroupId = $("#cboDonorBloodGroup").val();
		var bloodGroup = $("#cboDonorBloodGroup option:selected").text();
		if(bloodGroupId==-1)
		{
			// alert(bloodGroupId);
			$("#cboDonorBloodGroup").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
			errNumber++;
		}
		
		if(errNumber>0)
		{
			myAlert("Please Fill All the compulsory details");
			return;
		}
		if(mobile1.length<10 || mobile1.length>10)///mobile no. must be 10 digit
		{
			alert("Enter valid mobile no.");
			$("#txtMobile1").focus();
			return;
		}
		if(isNaN(mobile1)==true)///mobile no. must be numeric
		{
			alert("Enter valid mobile no.");
			$("#txtMobile1").focus();
			return;
		}

		var dob = $("#dtDob").val().trim();
		if(dob !="")
		{
			dtOk = testDate("dtDob");
			if(dtOk == false)
			{
				myAlert("Pls enter valid date of birth...")
				return;
			}
		}

		var fname=$("#txtfName").val().trim();
		var mobile2=$("#txtMobile2").val().trim();
		var email=$("#txtEmail").val().trim();
		if(email !="")
		{
			var resultEmail = validateEmail(email);
			if(resultEmail == false)
			{
				myAlert("Pls. enter valid email address...");
				return;
			}
		}
		var houseNo=$("#txtHno").val().trim();
		var street=$("#txtStreet").val().trim();
		var locality=$("#txtLocality").val().trim();
		var city=$("#txtCity").val().trim();
		// var already=$("#txtAlready").val().trim();
		var already="0";
		var gender = $("#cboGender option:selected").text();
		// var dataSource=$("#txtDataSource").val().trim();
		var dataSource='ajmerblooddonors.com';
		// var remarks=$("#txtRemarks").val().trim();
		var remarks=$("#txtLastDonationDate").val().trim();;

		if(document.getElementById("btnSave").value == "Register")
		{
			// alert("dd");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'data': {'name': name, 'fname': fname, 'mobile1': mobile1, 'mobile2': mobile2, 'email': email, 'houseNo': houseNo, 'street': street, 'locality': locality, 'city': city, 'dob': dob, 'bloodGroup': bloodGroup, 'already': already, 'gender': gender, 'dataSource': dataSource, 'remarks': remarks},
					'dataType': 'json',
					'success': function(data){
					// var container = $('#container');
					// alert("DD");
					if(data){
						if(data['zero']==0)
						{
							myAlert("Already registered...");
						}
						else
						{
							// alert(data);
							sessionCap = data['word'];
							$("#captcha").html(data['image']);
							// alert(sessionCap);
							myAlert('Thanks for registering with us.\nVery soon our executive will contact you... :)');
						}
					}
					else{
						alert('failure');
					}
				}
			});
			// alert("DD");
			blankcontrol();
		}
	}

	function blankcontrol()
	{
	    $('input[type=text]').each(function(){ $(this).val(''); });
	    $('input[type=number]').each(function(){ $(this).val(''); });
	    $("select").prop('selectedIndex', 0);
	}

</script>

<div class="row">
	<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
	</div>

	<!-- Search for Blood -->
	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style='border:1px solid gray; border-radius:10px; padding: 40px;box-shadow: 0px 2px 8px #888888; margin:2px; margin-top: 60px; max-height:590px;height:590px;'>
		<h2 class="text-center" style='margin-top: -20px; margin-bottom: 30px;'>Search</h2>
		<div class="row" style="margin-top:10px;">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php
					$this->load->helper('form');
					$attributes = array('onsubmit' => 'return validate_form(this)');//onsubmit="return validate_form(this);			
					echo form_open('Home_Controller/search', $attributes);

					$bgroup = array();
					$bgroup['-1'] = '--- Select Blood Group  ---';
					$bgroup['1'] = "A+";
					$bgroup['2'] = "A-";
					$bgroup['3'] = "B+";
					$bgroup['4'] = "B-";
					$bgroup['5'] = "O+";
					$bgroup['6'] = "O-";
					$bgroup['7'] = "AB+";
					$bgroup['8'] = "AB-";
					$bgroup['9'] = "A1+";
					$bgroup['10'] = "A1-";
					$bgroup['11'] = "A2+";
					$bgroup['12'] = "A2-";
					$bgroup['13'] = "A1B+";
					$bgroup['14'] = "A1B-";
					$bgroup['15'] = "A2B+";
					$bgroup['16'] = "A2B-";
					$bgroup['17'] = "Unknown";
					echo "<label style='color: black; font-weight: normal;'>Blood Group:</label>";
					echo form_dropdown('cboSearchBloodGroup', $bgroup, '--- Select Blood Group ---', "class='form-control' id='cboSearchBloodGroup'");
				?>
          	</div>
		</div>
		
		<div class="row" style="margin-top:10px;">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo '<input type="button" onclick="search();" value="Search" class="btn btn-danger col-lg-12 col-sm-12 col-md-12 col-xs-12" id="btnSearch"/>';
					echo form_close();
              	?>
          	</div>
		</div>

		<!-- Search Result -->
				
	 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style='margin:2px; max-height:330px;height:330px;overflow:auto; margin-top:10px;'>
	 		<div class="row">
	 			<table id='tblSearchResult' class="table">
	 			</table>
			</div>
		</div>
	</div>




	<!-- Register as a Donor -->
 	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style='border:1px solid gray; border-radius:10px; padding: 40px;box-shadow: 0px 2px 8px #888888; margin:2px; margin-top: 60px; max-height:590px;height:590px;'>
		<h2 class="text-center" style='margin-top: -20px; margin-bottom: 10px;'><span ondblclick="login();">Register</span> as a Blood Donor</h2>
		<?php
			$this->load->helper('form');
			$attributes = array('onsubmit' => 'return validate_form(this)');//onsubmit="return validate_form(this);			
			echo form_open('Home_Controller/registerDonor', $attributes);
		?>

		<!-- New row -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Name of Donor:</label>";
					echo form_input('txtName', '', 'id="txtName" placeholder="" maxlength="30" class="form-control"');
				?>
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Name of Father:</label>";
					echo form_input('txtfName', '', 'id="txtfName" placeholder="" maxlength="30" class="form-control"');
				?>
          	</div>
		</div>

		<!-- New row -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Mobile #1:</label>";
					echo form_input('txtMobile1', '', 'id="txtMobile1" placeholder="" pattern=" maxlength="10"([0-9]).{9,9}" title="Type valid mobile number." class="form-control"');
				?>
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Mobile #2:</label>";
					echo form_input('txtMobile2', '', 'id="txtMobile2" placeholder="" maxlength="10" pattern="([0-9]).{9,9}" title="Type valid mobile number." class="form-control"');
				?>
          	</div>
		</div>

		<!-- New row -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Email:</label>";
					echo form_input('txtEmail', '', 'id="txtEmail" placeholder="" maxlength="50" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Type valid email id" class="form-control"');
				?>
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Date of Birth:</label>";
					echo "<input type='text' id='dtDob' name='dtDob'  placeholder='' class='form-control dtp'>";
				?>
				<script>
				  	$(function() 
				  	{
					    $( ".dtp" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "1960:2020"
						});
					});
				</script>
          	</div>
		</div>

		<!-- New row -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>House No.:</label>";
					echo form_input('txtHno', '', 'id="txtHno" placeholder="" maxlength="10" class="form-control"');
				?>
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Street:</label>";
					echo form_input('txtStreet', '', 'id="txtStreet" placeholder="" maxlength="40" class="form-control"');
				?>
          	</div>
		</div>
		<!-- New row -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Locality:</label>";
					echo form_input('txtLocality', '', 'id="txtLocality" placeholder="" maxlength="40" class="form-control"');
				?>
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>City:</label>";
					echo form_input('txtCity', '', 'id="txtCity" placeholder="" maxlength="20" class="form-control"');
				?>
          	</div>
		</div>
		<!-- New row -->
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Blood Group:</label>";
					echo form_dropdown('cboDonorBloodGroup', $bgroup, '', 'id="cboDonorBloodGroup" placeholder="" class="form-control"');
				?>
          	</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Last Donation Date:</label>";
					echo "<input type='text' id='txtLastDonationDate' name='txtLastDonationDate'  placeholder='' class='form-control dtp'>";
				?>
				<script>
				  	$(function() 
				  	{
					    $( ".dtp" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "1990:2020"
						});
					});
				</script>

          	</div>
		</div>
		
		<!-- New row -->
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<?php
					$attributes[] = array("class"=>"form-control" );

					$arr = array();
					$arr['1'] = "Male";
					$arr['2'] = "Female";
					echo "<label style='color: black; font-weight: normal;'>Gender:</label>";
					echo form_dropdown('cboGender', $arr, '', "class='form-control' id='cboGender'");
				?>
          	</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<?php
					//echo "<label style='color: black; font-weight: normal;'>Captcha<span class='caret'></span></label>";
					echo "<div align='right' id='captcha' style='margin-top:10px;' onclick='refreshCaptcha();'>".$image."</div>";
              	?>
          	</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Type as Captcha:</label>";
					echo form_input('txtCaptcha', '', 'id="txtCaptcha" placeholder="" class="form-control"');
              	?>
          	</div>
		</div>

		<!-- New row -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;">
				<?php
					echo "<input type='button' onclick='loaddata();' value='Register' id='btnSave' class='btn btn-danger form-control'>";
					// echo form_submit('btnRegister', 'Register',"class='btn btn-danger col-lg-12'");
					echo form_close();
				?>
          	</div>
		</div>

	</div>

	<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
	</div>
</div>










<!-- <div align="center">
	Supported By:
</div> -->
<div align="center" class="row" style="margin-top:5px">
	<div class="col-lg-1">
	</div>

	<div id="carousel" class="carousel slide carousel-fade col-lg-10" data-ride="carousel">
	    <ol class="carousel-indicators" style="display:none;">
	        <li data-target="#carousel" data-slide-to="0" class="active"></li>
	        <li data-target="#carousel" data-slide-to="1"></li>
	        <li data-target="#carousel" data-slide-to="2"></li>
	        <li data-target="#carousel" data-slide-to="3"></li>
	        <li data-target="#carousel" data-slide-to="4"></li>
	        <li data-target="#carousel" data-slide-to="5"></li>
	    </ol>
	    <!-- Carousel items -->
	    <div class="carousel-inner">
	        <div class="item">
	        	<img src="<?php echo(base_url().'/bootstrap/images/mi.jpg'); ?>" />
	        </div>
	        <div class="active item">
	        	<img src="<?php echo(base_url().'/bootstrap/images/s1.jpg'); ?>" />
	        </div>
	        <div class="item">
	        	<img src="<?php echo(base_url().'/bootstrap/images/mi.jpg'); ?>" />
	        </div>
	        <div class="item">
	        	<img src="<?php echo(base_url().'/bootstrap/images/s2.jpg'); ?>" />
	        </div>
	        <div class="item">
	        	<img src="<?php echo(base_url().'/bootstrap/images/mi.jpg'); ?>" />
	        </div>
	        <div class="item">
	        	<img src="<?php echo(base_url().'/bootstrap/images/s3.jpg'); ?>" />
	        </div>
	    </div>
	    <!-- Carousel nav -->
	    <a class="carousel-control left" style="background-color:white;" href="#carousel" data-slide="prev">&lsaquo;</a>
	    <a class="carousel-control right"style="background-color:white;" href="#carousel" data-slide="next">&rsaquo;</a>
	</div>

	<div class="col-lg-1">
	</div>
</div>





<script type="text/javascript">
	$("#cboDonorBloodGroup").change(function(){
		var bloodGroup = $("#cboDonorBloodGroup").val();
		if(bloodGroup==-1)
		{
			$("#cboDonorBloodGroup").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
		}
		else
		{
			$("#cboDonorBloodGroup").css("box-shadow","lightgray 1px 1px 4px,lightgray -1px -1px 4px");
		}
	});

	$("#txtName").keyup(function(){
		var name = $("#name").val();
		if(name=="")
		{
			$("#txtName").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
		}
		else
		{
			$("#txtName").css("box-shadow","lightgray 1px 1px 4px,lightgray -1px -1px 4px");
		}
	});

	$("#txtMobile1").keyup(function(){
		var mobil1 = $("#txtMobile1").val();
		if(mobil1=="")
		{
			$("#txtMobile1").css("box-shadow","red 1px 1px 3px,red -1px -1px 3px");
		}
		else
		{
			$("#txtMobile1").css("box-shadow","lightgray 1px 1px 4px,lightgray -1px -1px 4px");
		}
	});



	</script>