<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
	var controller='User_Controller';
	var base_url='<?php echo site_url();?>';

	function loaddata()
	{	
		var uid = $("#txtUID").val().trim();
		if(uid == "")
		{
			alertPopup("Enter User name  ...", 8000);
			$("#txtUID").focus();
			return false;
		}
		var abAccess = $("#cboAccess").val();
		if(abAccess == "-1")
		{
			alertPopup("Select access to address book...", 8000);
			$("#cboAccess").focus();
			return false;
		}
		var pwdCredo = $("#txtPasswordCredo").val().trim();
		if(pwdCredo == "")
		{
			alertPopup("Enter password...", 8000);
			$("#txtPasswordCredo").focus();
			return false;
		}
		var pwdKinny = $("#txtPasswordKinny").val().trim();
		if(pwdKinny == "")
		{
			alertPopup("Enter password...", 8000);
			$("#txtPasswordKinny").focus();
			return false;
		}
		if(document.getElementById("btnSave").value == "Save")
		{
			// alert("DD");
			$.ajax({
					'url': base_url + '/' + controller + '/insertUser',
					'type': 'POST',
					'data': {'uid':uid, 
					// 'mobile':mobile,
					'passwordCredo':pwdCredo,
					'passwordKinny':pwdKinny,
					 'abAccess':abAccess
					},
					'success': function(data){
						var container = $('#container');
						if(data){
							container.html(data);
						}
					}
			});
		}
		else if(document.getElementById("btnSave").value == "Update")
		{
			// alert("ajax");
			// var uid = document.getElementById("txtUID").value;
			// var password = document.getElementById("txtPassword").value;
			$.ajax({
					'url': base_url + '/' + controller + '/updateUser',
					'type': 'POST',
					'data': {'rowid' : globalrowid, 
					'uid':uid, 
					// 'mobile':mobile,
					'passwordCredo':pwdCredo,
					'passwordKinny':pwdKinny,
					 'abAccess':abAccess},
					'success': function(data){
						var container = $('#container');
						if(data){
							container.html(data);
						}
					}
			});
		}	
	}
	function blankcontrol()
	{
		document.getElementById('txtUID').value="";
		// document.getElementById('txtMobile').value="";
		$("#txtPasswordCredo").attr("disabled", false);
		$("#txtPasswordKinny").attr("disabled", false);

		document.getElementById('txtPasswordCredo').value="";
		document.getElementById('txtPasswordKinny').value="";
		document.getElementById('btnSave').value="Save";
		document.getElementById('txtUID').focus();

	}
</script>
<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style='border:1px solid lightgray; border-radius:10px; padding: 10px;'>
		<h1 class="text-center" style='margin-top:0px'>Users</h1>
		<?php

			$attributes[] = array("class"=>"form-control" );

			$this->load->helper('form');		// it will load 'form' so that we will be able to use form_open() etc.
			echo validation_errors(); 
			echo form_open('User_Controller/insertUser', "onsubmit='return(false);'");
			echo form_input('uid', '', "placeholder='User Name' required class='form-control' maxlength='10' autofocus id='txtUID' style='margin-bottom:15px;' autocomplete='off'");
			// echo form_input('mobile', '', "placeholder='Mobile#' class='form-control' maxlength='10' autofocus id='txtMobile' style='margin-bottom:15px;' autocomplete='off'");
			// echo "<br>";
			echo form_password('passwordCredo', '', "placeholder='Password for CREDO' required class='form-control' maxlength='20' autofocus id='txtPasswordCredo' style='margin-bottom:15px;'");

			echo form_password('passwordKinny', '', "placeholder='Password for KINNY' required class='form-control' maxlength='20' autofocus id='txtPasswordKinny' style='margin-bottom:15px;'");
			$types = array();
			$types['-1'] = '--- Select ---';
			$types['C'] = "Complete Address book";
			$types['L'] = "Limited Address book";
			echo "<label style='color: black; font-weight: normal;'>Access to: <span style='color: red;'>*</span></label>";
			echo form_dropdown('cboAccess', $types, '-1', "class='form-control' id='cboAccess'");
			echo "<br />";
			echo "<input type='button' onclick='loaddata();' value='Save' id='btnSave' class='btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
			echo form_close();
		?>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0" >
	</div>
</div>
	<hr>
	<div id="containerTT"></div>