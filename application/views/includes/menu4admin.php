<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
        var base_url='<?php echo site_url();?>'; 


        $(document).ready( function () {
          <?php
            $php_array = $mr;
            $js_array = json_encode($php_array);
            echo "var javascript_array = ". $js_array . ";\n";
          ?>

          $('a.menu1').each(function(){
              for(i=0;i<javascript_array.length;i++)
              {
                  if(javascript_array[i]['menuoption'] === $(this).text())
                  {
                      $(this).css("display","block");
                      break;
                  }
                  else
                  {
                      $(this).css("display","none");
                  }
              }
          });
          // alert(javascript_array[0]['menuoption']);
          $.unblockUI();
        }); 


        // $(document).ready( function () {
        // var arr = new Array();
        //   $.ajax
        //   (
        //     {
        //       'url': base_url + '/Menu_Controller/getRights1',
        //       'type': 'POST', 
        //       'success': function(data)
        //       {
        //         arr = data.split(",");
        //         arr = arr.slice(0,arr.length-1);
        //           $('a.menu1').each(function(){
        //               for(i=0;i<arr.length;i++)
        //               {
        //                   if(arr[i] === $(this).text())
        //                   {
        //                       $(this).css("display","block");
        //                       break;
        //                   }
        //                   else
        //                   {
        //                       $(this).css("display","none");
        //                   }
        //               }
        //           });
        //       }
        //     }
        //   );
        // });
</script>
    <div class="container">
        <nav class="nav navbar navbar-inverse navbar-fixed-top">
          <div class="navbar-header"> <!-- It is responsible to Touch Menu for small devices -->
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SB-ERP</a>
          </div>

          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

              <li class="dropdown">
                <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Masters<span class="caret"></span></a>
                <!-- role="menu": fix moved by arrows (Bootstrap dropdown) -->
                <ul class="dropdown-menu" role="menu" style="background-color:orange;border-radius:25px">

                  <li class="dropdown-submenu">
                      <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Salary Masters</a>
                      <ul class="dropdown-menu" style="background-color:orange;border-radius:25px">
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/DepartmentTypes_Controller">Department Types</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/DesignationTypes_Controller">Designation Types</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Holidays_Controller">Holidays</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Employees_Controller">Employees</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/EmployeeContract_Controller">Employee Contract Detail</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/BaseLimits_Controller">Base Limits</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Allowances_Controller">Allowances</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/MeetingRemarks_Controller">Meeting Remarks</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/IncomeTaxSlabs_Controller">Income Tax Slabs</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OpeningBalLeaves_Controller">Opening Balance (Leaves)</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/EmployeeDiscontinue_Controller">Employee Discontinue</a></li>
                      </ul>
                  </li>

                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PrefixTypes_Controller">Prefix Types</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/ContactTypes_Controller">Contact Types</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/MailTypes_Controller">Mail Types</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Countries_Controller">Countries</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/States_Controller">States</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Districts_Controller">Districts</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Towns_Controller">Towns</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/AddressBook_Controller">Address Book</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Parties_Controller">Parties</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OpeningBalParties_Controller">Opening Balance (Parties)</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Transporters_Controller">Transporters</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Colours_Controller">Colours</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Placements_Controller">Placements</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OrderTypes_Controller">Order Types</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/ProductCategories_Controller">Product Categories</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Products_Controller">Products</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OpeningBalPurchase_Controller">Opening Balance (Purchase)</a></li>                            
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OurProduct_Controller">Our Products</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Productmcc">Product Master Category</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Products_Controller/del_prod">Deleted Products</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Products_Controller/prod_uom?id=">UOM</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Dsr_Controller/dsr_srm?id=">DSR Short Remarks</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Dsr_Controller/dsr_ls?id=">DSR Lead Stage</a></li>
                  
                  <li class="divider"></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Masterc">Budget Master</a></li>
                </ul>
              </li>

              <li class="dropdown">
                <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Transactions<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu" style=background-color:orange;border-radius:25px>

                  <li class="dropdown-submenu">
                      <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Salary Transactions</a>
                      <ul class="dropdown-menu" style=background-color:orange;border-radius:25px>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/LeaveApplication_Controller">Leave Application</a></li>                            
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/LeaveApproval_Controller">Leave Approval</a></li>                            
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Payments_Controller">Payments</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/SalaryOtherInc_Controller">Salary Incentive (Others)</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Attendance_Controller">Mark Attendance</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/AttendanceExecutive_Controller">Mark Attendance (Executives)</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/SalaryMonthly_Controller">Calculate Salary (Monthly)</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/SalaryContract_Controller">Calculate Salary (Contract)</a></li>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/SalaryExecutives_Controller">Calculate Salary (Executives)</a></li>                            
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OpeningBalLeaves_Controller">Opening Balance (Leaves)</a></li>                            
                      </ul>
                  </li>

                  <li class="dropdown-submenu">
                      <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Purchase Transactions</a>
                      <ul class="dropdown-menu" style=background-color:orange;border-radius:25px>
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PurchaseOrder_Controller">Purchase Order</a></li>                    
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PurchaseOrderApprove_Controller">Purchase Order Approve</a></li>                    
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PurchaseOrderSaveAndPrint_Controller">Purchase Order Save and Print</a></li>                    
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Purchase_Controller">Purchase</a></li>                    
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PurchaseNew_Controller">Purchase New</a></li>                    
                            <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PurchaseChallan_Controller">Purchase Challan</a></li>                    
                      </ul>
                  </li>

                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Quotations_Controller">Quotations</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Proforma_Controller">Proforma Invoice</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Orders_Controller">Orders</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Despatch_Controller">Despatch</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Challan_Controller">Challan</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Invoice_Controller">Invoice</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/SalesReturn_Controller">Sales Return</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Dsr_Controller">Daily Sales Report (DSR)</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/ScheduledAppointments_Controller">Scheduled Appointments</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/IssueReceive_Controller">Issue / Receive</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/MailLog_Controller">Mail Log</a></li>
                </ul>
              </li>

              <li class="dropdown">
                <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Production<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu" style=background-color:orange;border-radius:25px>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Stages_Controller">Stages Master</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/OpeningBal_Controller">Opening Balance</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/MoveInStage_Controller">Move In Stage</a></li>
                   <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/MoveInGodown_Controller">Move In Godown</a></li>
                   <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/MoveInGodownApprove_Controller">Move in Godown Approve</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/ProductStatus_Controller">Product Status</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/ProductStatusAtDesp_Controller">Product Status (Desp. Stage)</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptMovement_Controller">Stage Movement Log</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/PointsToEmployee_Controller">Points To Employee</a></li>
                  <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptPerformance_Controller">Employee Performance</a></li>
                </ul>
              </li>

              <li class="dropdown">
                <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" style=background-color:orange;border-radius:25px>
                      <li class="dropdown-submenu">
                          <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Salary Reports</a>
                          <ul class="dropdown-menu" style=background-color:orange;border-radius:25px>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptAttendance_Controller">Attendance Register</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptAttendanceExecutive_Controller">Attendance Register (Executives)</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptSalaryMonthly_Controller">Salary Register (Monthly)</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptSalaryExecutives_Controller">Salary Register (Executives)</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptEsi_Controller">ESI Report</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptLeaveApplication_Controller">Leave Applications</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptPayments_Controller">Payments Log</a></li>
                                <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptEmployeeLedgerMonthly_Controller">Employee Ledger (Monthly)</a></li>
                          </ul>
                      </li>

                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptProducts_Controller">Products List</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptQuotations_Controller">Quotations Log</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptProforma_Controller">Proforma Invoice Log</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptOrders_Controller">Orders Status</a></li>  
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptPurchase_Controller">Purchase Report</a></li>  
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptPurchaseProductWise_Controller">Purchase Report (Product Wise)</a></li>                 
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptDespatch_Controller">Despatch Report</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptChallan_Controller">Challan Report</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptInvoice_Controller">Invoice Report</a></li>     
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptInvoice2018_Controller">Invoice Report 2018</a></li>                  
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptColour_Controller">Colour-Wise Report</a></li>  
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptColourDespatch_Controller">Colour-Wise Report (Despatch)</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptTransporter_Controller">Transporter-Wise Report</a></li>                      
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptAddressbook_Controller">Address Book Report</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptStock_Controller">Stock Report</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptRoLevel_Controller">RO Level Report</a></li>                    
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptProductInOutLog_Controller">Product In/Out Log</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/RptDsr_Controller">DSR Report</a></li>
                    </ul>
              </li>

              <li class="dropdown">
                <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">Tools<span class="caret"></span></a>

                <!-- role="menu": fix moved by arrows (Bootstrap dropdown) -->
                    <ul class="dropdown-menu" role="menu" style=background-color:orange;border-radius:25px>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/User_Controller">Create Users</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Right_Controller">User Rights</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Changepwdadmin_Controller">Reset Password</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Backupdata_Controller">Backup Data</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/AdminRights_Controller">Admin Rights</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/NotificationHierarchy_Controller">Notification Hierarchy</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Query_Controller">Query Analyzer</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/Emailer_Controller">Email</a></li>
                    </ul>
              </li>

              <li class="dropdown">
                <a tabindex="0" class="menu1" style="display:none;" data-toggle="dropdown">CRM<span class="caret"></span></a>

                <!-- role="menu": fix moved by arrows (Bootstrap dropdown) -->
                    <ul class="dropdown-menu" role="menu" style=background-color:orange;border-radius:25px>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/franchiseesc">Dashboard</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/franchiseesc/franchisees_inq_list">Inquires List</a></li>
                      <li><a tabindex="0" class="menu1" style="display:none;" href="<?php  echo base_url();  ?>index.php/franchiseesc//franchisees_inq_steps">Inquiries Steps</a></li>
                    </ul>
              </li> 
            </ul>

            <ul class="nav navbar-nav navbar-right" style="padding-right:25px;">
              <li class="dropdown">
              <a href="#" tabindex="0" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" onclick="checkOrg();"> <?php echo $this->session->userid . $this->session->orgChar ?> <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a  tabindex="0" href="<?php  echo base_url();  ?>index.php/Login_controller/logout">Logout <span class="glyphicon glyphicon-log-out"></span></a></li>
                  <li class="divider"></li>
                  <li><a  tabindex="0" href="<?php  echo base_url();  ?>index.php/Changepwd_Controller">Change Password</a></li>
                </ul>
              </li>
            </ul>

            <ul class="nav navbar-nav navbar-right" style="padding-right:25px;">
              <li class="dropdown">
              <a href="#"><span id="spanNotification" class="label label-default" onclick="loadNotifications();">0</span></a>
              </li>
            </ul>
          </div>
        </nav>
        <script>  
          var notificationCount = 0;
          function loadNotifications()
          {
            var controller='Login_controller';
            var base_url='<?php echo site_url();?>';
            $.ajax({
                    'url': base_url + '/' + controller + '/loadNotifications4table',
                    'type': 'POST',
                     // 'global': false, /// not calling hourGlass function
                    'dataType': 'json',
                    'data': {
                                'dtFrom': 'ff'
                                , 'dtTo': 'gg'
                                , 'userRowId': 'tt'
                            },
                    'success': function(data)
                    {
                        if(data)
                        {
                            // alert(JSON.stringify(data));
                            $("#tblNotification").find("tr:gt(0)").remove();
                            var table = document.getElementById("tblNotification");
                            // alert(data.length);
                            for(i=0; i<data['notificationCount'].length; i++)
                            {
                                newRowIndex = table.rows.length;
                                row = table.insertRow(newRowIndex);

                                var cell = row.insertCell(0);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].notificationRowId;
                                var cell = row.insertCell(1);
                                // cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].notification;
                                var cell = row.insertCell(2);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].createdBy;
                                var cell = row.insertCell(3);
                                // cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].uid;
                                var cell = row.insertCell(4);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].vNo;
                                var cell = row.insertCell(5);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].padhLiya;
                                var cell = row.insertCell(6);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].createdStamp;
                                var cell = row.insertCell(7);
                                // cell.style.display="none";
                                cell.innerHTML = '<span class="label label-danger">Delete</span>';
                                cell.className = "padhLiya";
                            }
                            $("#spanNotification").removeClass("label-danger");
                            $("#spanNotification").addClass("label-default");
                            $("#spanNotification").text("0");
                            $('.padhLiya').bind('click', notificationPadhLiya);
                        }
                    }
                });
                $("#modalNotification").css("display","block");
          }

          function notificationPadhLiya()
          {
            rowIndex = $(this).parent().parent().index();
            colIndex = $(this).parent().index();
            notificationRowId = $(this).closest('tr').children('td:eq(0)').text();
            // alert(notificationRowId);
            var controller='Login_controller';
            var base_url='<?php echo site_url();?>';
            $.ajax({
                    'url': base_url + '/' + controller + '/notificationPadhLiya',
                    'type': 'POST',
                    'dataType': 'json',
                    'data': {
                                'dtFrom': 'ff'
                                , 'dtTo': 'gg'
                                , 'notificationRowId': notificationRowId
                            },
                    'success': function(data)
                    {
                        if(data)
                        {
                            // alert(JSON.stringify(data));
                            $("#tblNotification").find("tr:gt(0)").remove();
                            var table = document.getElementById("tblNotification");
                            // alert(data.length);
                            for(i=0; i<data['notificationCount'].length; i++)
                            {
                                newRowIndex = table.rows.length;
                                row = table.insertRow(newRowIndex);

                                var cell = row.insertCell(0);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].notificationRowId;
                                var cell = row.insertCell(1);
                                // cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].notification;
                                var cell = row.insertCell(2);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].createdBy;
                                var cell = row.insertCell(3);
                                // cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].uid;
                                var cell = row.insertCell(4);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].vNo;
                                var cell = row.insertCell(5);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].padhLiya;
                                var cell = row.insertCell(6);
                                cell.style.display="none";
                                cell.innerHTML = data['notificationCount'][i].createdStamp;
                                var cell = row.insertCell(7);
                                // cell.style.display="none";
                                cell.innerHTML = '<span class="label label-danger">Delete</span>';
                                cell.className = "padhLiya";
                            }
                            $("#spanNotification").removeClass("label-danger");
                            $("#spanNotification").addClass("label-default");
                            $("#spanNotification").text("0");
                            $('.padhLiya').bind('click', notificationPadhLiya);
                        }
                    }
                });
                // $("#modalNotification").css("display","block");
          }

          function checkOrg()
          {
            // var jsOrgRowId = '<?php echo $this->session->orgRowId; ?>';
            // alert(jsOrgRowId);
          }
           /* Remove following comments to enable hover effect on menu and sub-menu.
           */
           // $('li.dropdown,li.dropdown-submenu').hover(function() {
           //    $(this).find('>ul.dropdown-menu').stop(true, true).delay(100).fadeIn('slow');
           //    }, function() {
           //      $(this).find('>ul.dropdown-menu').stop(true, true).delay(100).fadeOut('slow');
           //  });

           $(document).ready(function(){
              // $("#spanNotification").trigger("click");
              loadNotificationCounter();
           });
         </script>
    </div> <!-- end of 'container' div   -->
<div class="container-fluid">