<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Employees_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='green';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='red';
	          cell.setAttribute("onmouseover", "this.style.color='lightgray'");
	          cell.setAttribute("onmouseout", "this.style.color='red'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].empRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].empRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].abRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].fName;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].fOccupation;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].gender;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].referencerRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].reference;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].departmentRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].departmentType;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].designationRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].designationType;
	          var cell = row.insertCell(14);
	          cell.innerHTML = dateFormat(new Date(records[i].doj));
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].basicSal;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].salType;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].mobileProfessional;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].emailProfessional;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].correspondenceAddress;
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].aadharNo;
	          var cell = row.insertCell(21);
	          cell.innerHTML = records[i].jobFunction;
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].reportingManager;
	          cell.style.display="none";
	          var cell = row.insertCell(23);
	          cell.innerHTML = records[i].reportingManagerName;
	          var cell = row.insertCell(24);
	          cell.innerHTML = records[i].emergencyContactName;
	          var cell = row.insertCell(25);
	          cell.innerHTML = records[i].emergencyContactNumber;
	          var cell = row.insertCell(26);
	          cell.innerHTML = records[i].emergencyContactAddress;
	          var cell = row.insertCell(27);
	          cell.innerHTML = records[i].deductPf;
	          var cell = row.insertCell(28);
	          cell.innerHTML = records[i].deductEsi;
	          var cell = row.insertCell(29);
	          cell.innerHTML = records[i].leavesOn;
	          var cell = row.insertCell(30);
	          cell.innerHTML = records[i].userRowId;
	          var cell = row.insertCell(31);
	          cell.innerHTML = records[i].hra;
	          var cell = row.insertCell(32);
	          cell.innerHTML = records[i].ta;
	          var cell = row.insertCell(33);
	          cell.innerHTML = records[i].ca;
	          var cell = row.insertCell(34);
	          cell.innerHTML = records[i].ma;
	          var cell = row.insertCell(35);
	          cell.innerHTML = records[i].showInAttendance;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		// $('#tbl1 tr').each(function(){
		// 	$(this).bind('click', highlightRow);
		// });
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						setTable(data['records'])
						alertPopup('Record deleted...', 4000);
						blankControls();
						$('#image-list').empty(); 
						$("#cboAbList").focus();
					}
				}
			});
	}
	
	$(document).ready(function(e)
	{
		$("#frm").on("submit", (function(e)
		{
			e.preventDefault();
			abRowId = $("#cboAbList").val();
			if(abRowId == "-1")
			{
				alertPopup("Select name...", 8000);
				$("#cboAbList").focus();
				return;
			}
			gender = $("#cboGender").val();
			if(gender == "-1")
			{
				alertPopup("Select gender...", 8000);
				$("#cboGender").focus();
				return;
			}
			
			ref = $("#cboReference").val();
			if(ref == "-1")
			{
				alertPopup("Select reference...", 8000);
				$("#cboReference").focus();
				return;
			}

			rm = $("#cboReportingManager").val();
			if(rm == "-1")
			{
				alertPopup("Select Reporting Manager...", 8000);
				$("#cboReportingManager").focus();
				return;
			}
			
			dept = $("#cboDepartment").val();
			if(dept == "-1")
			{
				alertPopup("Select department...", 8000);
				$("#cboDepartment").focus();
				return;
			}
			
			desig = $("#cboDesignation").val();
			if(desig == "-1")
			{
				alertPopup("Select designation...", 8000);
				$("#cboDesignation").focus();
				return;
			}

			var vDt = $("#txtDoj").val().trim();
			dtOk = testDate("txtDoj");
			if(dtOk == false)
			{
				alertPopup("Invalid date of joining...", 5000);
				$("#txtDoj").focus();
				return;
			}

			sal = $("#txtBasicSalary").val();
			// if(sal <= 0)
			// {
			// 	alertPopup("Invalid salary...", 8000);
			// 	$("#txtBasicSalary").focus();
			// 	return;
			// }

			deductPf = $("#cboPf").val();
			if(deductPf == "-1")
			{
				alertPopup("Select PF deduction...", 8000);
				$("#cboPf").focus();
				return;
			}
			deductEsi = $("#cboEsi").val();
			if(deductEsi == "-1")
			{
				alertPopup("Select ESI deduction...", 8000);
				$("#cboEsi").focus();
				return;
			}

			salType = $("#cboSalaryType").val();
			if(salType == "-1")
			{
				alertPopup("Select salary type...", 8000);
				$("#cboSalaryType").focus();
				return;
			}

			leavesOn = $("#txtLeavesOn").val();

			showInAttendance = $("#cboShowInAttendance").val();
			if(showInAttendance == "-1")
			{
				alertPopup("Select Show In Attendance...", 9000);
				$("#cboShowInAttendance").focus();
				return;
			}

			if($("#btnSave").val() == "Save")
			{
				$.ajax({
						'url': base_url + '/' + controller + '/insert',
						'type': 'POST',
						// 'dataType': 'json',
						'data': new FormData(this),
						'contentType':false,
						'cache': false,
						'processData': false,
						'success': function(data)
						{
							if(data == "Duplicate...")
							{
								alertPopup("Duplicate record...", 6000);
							}
							else if(data == "Duplicate User...")
							{
								alertPopup("This user already assigned to an Employee...", 6000);
							}
							else if(data == "Done...")
							{
								alertPopup('Record saved...', 6000);
								blankControls();
								$('#image-list').empty(); 
								///////////
								$.ajax({
									'url': base_url + '/' + controller + '/loadRecords',
									'type': 'POST',
									'data': 'data',
									'dataType': 'json',
									'success': function(data)
									{
										setTable(data['records']); ///loading records in tbl1
										$("#btnSave").val("Save");
										// $("#txtProductName").focus();
									},
									'error': function(jqXHR, exception)
									{
										document.write(jqXHR.responseText);
									}
								});

								/////////

							}
							else
							{
								alert(data);
								// blankControls();
							}
						}
				});
			}
			else if($("#btnSave").val() == "Update")
			{
				// var fd = new FormData(this);
				$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'data': new FormData(this),
					'contentType':false,
					'cache': false,
					'processData': false,
					// 'dataType': 'json',
					'success': function(data)
					{
						// alert(data);
						if(data == "Duplicate...")
						{
							alertPopup("Duplicate record...", 6000);
						}
						else if(data == "Duplicate User...")
						{
							alertPopup("This user already assigned to an Employee...", 8000);
						}
						else if(data == "Done...")
						{
							alertPopup('Record updated, Reloading Page... Wait...', 9000);
							location.reload();
						}
						else
						{
							alert(data);
							// blankControls();
						}
					},
					'error': function(jqXHR, exception)
					{
						document.write(jqXHR.responseText);
					}
				});
			}
		}
		))
	});


	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						$('#image-list').empty(); 
						// $("#cboAbList").focus();
					}
				}
			});
	}
</script>
<div class="container-fluid" style="width: 95%">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px;font-size:3vw'>Employees Registration</h3>
			<form id="frm" method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:10px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Employee Name:<span style='color: red;'>*</span></label>";
		              	?>
		              	<select name="cboAbList" id="cboAbList" class="form-control">
			              <option value="-1" addr="" mobile1="" mobile2="" townName="" >--- Select ---</option>
			              <?php
			                foreach ($abList as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['abRowId']; ?> addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
			              <?php
			                }
			              ?>
			            </select>

			            <script type="text/javascript">
				            $(document).ready(function()
				            {
				              $("#cboAbList").change(function()
				              {
							        $("#lblDetails").html($('option:selected', '#cboAbList').attr('addr'));
							        $("#lblDetails").html( $("#lblDetails").html() + "<br/>" + $('option:selected', '#cboAbList').attr('mobile1'));
							        $("#lblDetails").html( $("#lblDetails").html() + ", " + $('option:selected', '#cboAbList').attr('mobile2'));
							        $("#lblDetails").html( $("#lblDetails").html() + "<br/>" + $('option:selected', '#cboAbList').attr('townName'));
				              });
				            });
				        </script>	              	
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label id='lblDetails' class='blank' style='color: grey; font-weight: normal;margin-top:20px;'>.</label>";
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Father's Name:</label>";
							echo form_input('txtFather', '', "class='form-control' id='txtFather' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
		              	?>
		          	</div>
		        </div>
				<div class="row" style="margin-top:10px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Father's Occupation:</label>";
							echo form_input('txtFatherOccupation', '', "class='form-control' id='txtFatherOccupation' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Mobile # (Professional):</label>";
							echo form_input('txtMobileProfessional', '', "class='form-control' id='txtMobileProfessional' style='text-transform: capitalize;' maxlength=10 autocomplete='off'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Email (Professional):</label>";
							echo form_input('txtEmailProfessional', '', "class='form-control' id='txtEmailProfessional' style='' maxlength=50 autocomplete='off'");
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:10px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Aadhar #:</label>";
							echo form_input('txtAadhar', '', "class='form-control' id='txtAadhar' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Correspondence Address:</label>";
							echo form_textarea('txtCorrespondenceAddress', '', "class='form-control' id='txtCorrespondenceAddress' style='text-transform: capitalize;resize:none;height:50px;' maxlength=255 autocomplete='off'");
		              	?>
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							$gender = array();
							$gender['-1'] = '--- Select ---';
							$gender['Male'] = "Male";
							$gender['Female'] = "Female";
							echo "<label style='color: black; font-weight: normal;'>Gender: <span style='color: red;'>*</span></label>";
							echo form_dropdown('cboGender', $gender, 'Male', "class='form-control' id='cboGender'");
						?> 
		          	</div>
				</div>

				<div class="row" style="margin-top:10px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Reference:<span style='color: red;'>*</span></label>";
		              	?>
		              	<select id="cboReference" name="cboReference" class="form-control">
			              <option value="-1" addr="" mobile1="" mobile2="" townName="" >--- Select ---</option>
			              <?php
			                foreach ($abList as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['abRowId']; ?> addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
			              <?php
			                }
			              ?>
			            </select>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Department:<span style='color: red;'>*</span></label>";
							echo form_dropdown('cboDepartment',$departments, '-1',"class='form-control' id='cboDepartment'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Designation:<span style='color: red;'>*</span></label>";
							echo form_dropdown('cboDesignation',$designations, '-1',"class='form-control' id='cboDesignation'");
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:10px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Job Function:</label>";
							echo form_input('txtJobFunction', '', "class='form-control' id='txtJobFunction' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Reporting Manager:<span style='color: red;'>*</span></label>";
							// echo form_input('txtFatherOccupation', '', "class='form-control' id='txtFatherOccupation' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
		              	?>
		              	<select id="cboReportingManager" name="cboReportingManager" class="form-control">
			              <option value="-1" addr="" mobile1="" mobile2="" townName="" >--- Select ---</option>
			              <?php
			                foreach ($abList as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['abRowId']; ?> addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
			              <?php
			                }
			              ?>
			            </select>
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Date of Joining:<span style='color: red;'>*</span></label>";
							echo form_input('txtDoj', '', "class='form-control' placeholder='' id='txtDoj' maxlength='10'");
		              	?>
		              	<script>
							$( "#txtDoj" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "1990:2030"
							});
						</script>
		          	</div>
				</div>

				<div class="row" style="margin-top:10px;">
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Leave Earn On (Days):</label>";
							echo '<input type="number" step="1" name="txtLeavesOn" value="0" placeholder="" class="form-control" maxlength="3" id="txtLeavesOn" />';
		              	?>
		          	</div>
				</div>

				<div class="panel panel-default" style="margin-top:10px;">
					<div class="panel-heading"><b>Salary Detail</b></div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Basic Salary:<span style='color: red;'>*</span></label>";
									echo '<input type="number" step="1" name="txtBasicSalary" value="0" placeholder="" class="form-control" maxlength="10" id="txtBasicSalary" />';
				              	?>
		          			</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>HRA:</label>";
									echo '<input type="number" step="1" name="txtHra" value="0" placeholder="" class="form-control" maxlength="10" id="txtHra" />';
				              	?>
		          			</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Travelling Allow.:</label>";
									echo '<input type="number" step="1" name="txtTa" value="0" placeholder="" class="form-control" maxlength="10" id="txtTa" />';
				              	?>
		          			</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Conveyance Allow.:</label>";
									echo '<input type="number" step="1" name="txtCa" value="0" placeholder="" class="form-control" maxlength="10" id="txtCa" />';
				              	?>
		          			</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Medical Allow.:</label>";
									echo '<input type="number" step="1" name="txtMa" value="0" placeholder="" class="form-control" maxlength="10" id="txtMa" />';
				              	?>
		          			</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
								<?php
									$showInAttendance = array();
									$showInAttendance['-1'] = '--- Select ---';
									$showInAttendance['Y'] = "YES";
									$showInAttendance['N'] = "NO";
									echo "<label style='color: black; font-weight: normal;'>Show in Attendance:<span style='color: red;'>*</span></label>";
									echo form_dropdown('cboShowInAttendance',$showInAttendance, '-1',"class='form-control' id='cboShowInAttendance'");
				              	?>
				          	</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="margin-top:10px;">
								<?php
									$pf = array();
									$pf['-1'] = '--- Select ---';
									$pf['Y'] = "YES";
									$pf['N'] = "NO";
									echo "<label style='color: black; font-weight: normal;'>Deduct PF:<span style='color: red;'>*</span></label>";
									echo form_dropdown('cboPf',$pf, '-1',"class='form-control' id='cboPf'");
				              	?>
				          	</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="margin-top:10px;">
								<?php
									$esi = array();
									$esi['-1'] = '--- Select ---';
									$esi['Y'] = "YES";
									$esi['N'] = "NO";
									echo "<label style='color: black; font-weight: normal;'>Deduct ESI:<span style='color: red;'>*</span></label>";
									echo form_dropdown('cboEsi',$esi, '-1',"class='form-control' id='cboEsi'");
				              	?>
				          	</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="margin-top:10px;">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Link with user:</label>";
									echo form_dropdown('cboUser', $users, '-1',"class='form-control' id='cboUser'");
				              	?>
				          	</div>
							<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="margin-top:10px;">
								<?php
									$salType = array();
									$salType['-1'] = '--- Select ---';
									$salType['M'] = "Monthly";
									$salType['C'] = "Contract";
									echo "<label style='color: black; font-weight: normal;'>Salary Type:<span style='color: red;'>*</span></label>";
									echo form_dropdown('cboSalaryType',$salType, 'Monthly',"class='form-control' id='cboSalaryType'");
				              	?>
				          	</div>
						</div>
					</div>
				</div>

				<div class="row" style="margin-top:10px;">
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Emergency Contact Name:</label>";
							echo form_input('txtEmergencyContactName', '', "class='form-control' id='txtEmergencyContactName' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
		              	?>
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Emergency Contact Number:</label>";
							echo form_input('txtEmergencyContactNumber', '', "class='form-control' id='txtEmergencyContactNumber' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
		              	?>
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Emergency Contact Address:</label>";
							echo form_input('txtEmergencyContactAddress', '', "class='form-control' id='txtEmergencyContactAddress' style='text-transform: capitalize;' maxlength=255 autocomplete='off'");
		              	?>
		          	</div>
				</div>

				

				<div class="row" style="margin-top:10px;">
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label id='response' style='color: black; font-weight: normal;'></label>";
						?>
						<input type="file" name="images[]" id="images" multiple class="form-control">
		          		<br /><br />
						<?php
							echo "<label id='response' style='color: black; font-weight: normal;'></label>";
						?>
						<br><input type="button" name="removeImages"  class="btn btn-block btn-danger" id="removeImages" value="Remove files (Images)" onclick="$('#image-list').empty(); $('#images').val('');$('#txtPhotoChange').val('yes');">
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="overflow:auto;  text-align: center;">
		          		<div class="row"  style="overflow:auto;background: #f5f5f5; width: 96%; display: inline-block;border: 1px lightgrey solid;height:200px;">
							<ol id="image-list"></ol>
						</div>
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
		          		<br />
		          		<br /><br /><br />
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='submit'  value='Save' id='btnSave' class='btn btn-primary form-control' style='height:90px;'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:15px; display:none;">
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo form_input('txtHiddenRowId', '', 'id="txtHiddenRowId" placeholder="" maxlength="200"  autocomplete="off" readonly class="form-control"');
						?>
			      	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo form_input('txtPhotoChange', '', 'id="txtPhotoChange" placeholder="" maxlength="200"  autocomplete="off" readonly class="form-control"');
			          	?>
			      	</div>
				</div>
			</form>
			

		</div>
		
	</div>

	<script type="text/javascript">
	var input = document.getElementById("images"), formdata = false;
	formdata = new FormData();
		// (function () {
		// 	  if (window.FormData) {
		// 	  }
		// 	})();

		function showUploadedItem (source) 
		{
			var list = document.getElementById("image-list"),
			li   = document.createElement("li"),
			img  = document.createElement("img");
			img.src = source;
			img.width = "150";
			img.height = "150";
			li.appendChild(img);
			list.appendChild(li);
			// li.style.display = "inline";
			$("#txtPhotoChange").val("yes");
		}

		if (input.addEventListener) 
		{
			input.addEventListener("change", function (evt) 
			{
				var i = 0, len = this.files.length, img, reader, file;
				    
				document.getElementById("response").innerHTML = "Loading . . .";
				    // alert(len);
				for ( ; i < len; i++ ) 
				{
					file = this.files[i];
				  
					if (!!file.type.match(/image.*/)) 
					{

				    } 


					if ( window.FileReader ) 
					{
						$('#image-list').empty(); 
						  reader = new FileReader();
						  reader.onloadend = function (e) 
						  { 
						    showUploadedItem(e.target.result);
						  };
						  reader.readAsDataURL(file);
					}
					if (formdata) 
					{
						  formdata.append("images[]", file);
						  // alert(JSON.stringify(formdata));
					}

				}
			    document.getElementById("response").innerHTML = "";
		  	}, false);
		}

		

	</script>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='display:none;'>partyrowid</th>
						<th style='display:none;'>abRowId</th>
					 	<th>Name</th>
					 	<th>FName</th>
					 	<th>F_Occupation</th>
					 	<th>Gender</th>
					 	<th style='display:none;'>RefRowId</th>
					 	<th>Ref.</th>
					 	<th style='display:none;'>DeptRowId</th>
					 	<th>Dept</th>
					 	<th style='display:none;'>DesigRowId</th>
					 	<th>Desig</th>
					 	<th>DOJ</th>
					 	<th>Basic Sal.</th>
					 	<th>salType</th>
					 	<th>Mobile (P)</th>
					 	<th>Email (P)</th>
					 	<th>C. Address</th>
					 	<th>Aadhar</th>
					 	<th>Job Function</th>
					 	<th style='display:none;'>Repo.Mgr.RowId</th>
					 	<th>Repo.Mgr.</th>
					 	<th>Em. Contact</th>
					 	<th>Em. Contact#</th>
					 	<th>Em. Contact Add.</th>
					 	<th>PF</th>
					 	<th>ESI</th>
					 	<th>Leaves On</th>
					 	<th>UserRowId</th>
					 	<th>HRA</th>
					 	<th>TA</th>
					 	<th>CA</th>
					 	<th>MA</th>
					 	<th>Show In Attendance</th>
					 </tr>
				 </thead>
				 <tbody>
				 	<?php 
						foreach ($records as $row) 
						{
						 	$rowId = $row['empRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['empRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['abRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['fName']."</td>";
						 	echo "<td>".$row['fOccupation']."</td>";
						 	echo "<td>".$row['gender']."</td>";
						 	echo "<td style='display:none;'>".$row['referencerRowId']."</td>";
						 	echo "<td>".$row['reference']."</td>";
						 	echo "<td style='display:none;'>".$row['departmentRowId']."</td>";
						 	echo "<td>".$row['departmentType']."</td>";
						 	echo "<td style='display:none;'>".$row['designationRowId']."</td>";
						 	echo "<td>".$row['designationType']."</td>";
						 	$dt = strtotime($row['doj']);
							$dt = date('d-M-Y', $dt);
						 	echo "<td>".$dt."</td>";
						 	echo "<td>".$row['basicSal']."</td>";
						 	echo "<td>".$row['salType']."</td>";
						 	echo "<td>".$row['mobileProfessional']."</td>";
						 	echo "<td>".$row['emailProfessional']."</td>";
						 	echo "<td>".$row['correspondenceAddress']."</td>";
						 	echo "<td>".$row['aadharNo']."</td>";
						 	echo "<td>".$row['jobFunction']."</td>";
						 	echo "<td style='display:none;'>".$row['reportingManager']."</td>";
						 	echo "<td>".$row['reportingManagerName']."</td>";
						 	echo "<td>".$row['emergencyContactName']."</td>";
						 	echo "<td>".$row['emergencyContactNumber']."</td>";
						 	echo "<td>".$row['emergencyContactAddress']."</td>";
						 	echo "<td>".$row['deductPf']."</td>";
						 	echo "<td>".$row['deductEsi']."</td>";
						 	echo "<td>".$row['leavesOn']."</td>";
						 	echo "<td>".$row['userRowId']."</td>";
						 	echo "<td>".$row['hra']."</td>";
						 	echo "<td>".$row['ta']."</td>";
						 	echo "<td>".$row['ca']."</td>";
						 	echo "<td>".$row['ma']."</td>";
						 	echo "<td>".$row['showInAttendance']."</td>";
							echo "</tr>";
						}
					?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 10px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;margin-bottom:10%'>";
	      	?>
		</div>

	</div>

	

</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		abRowId = $(this).closest('tr').children('td:eq(3)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#cboAbList").val(abRowId);
		$("#txtFather").val($(this).closest('tr').children('td:eq(5)').text());
		$("#txtFatherOccupation").val($(this).closest('tr').children('td:eq(6)').text());
		$("#cboGender").val($(this).closest('tr').children('td:eq(7)').text());
		$("#cboReference").val($(this).closest('tr').children('td:eq(8)').text());
		$("#cboDepartment").val($(this).closest('tr').children('td:eq(10)').text());
		$("#cboDesignation").val($(this).closest('tr').children('td:eq(12)').text());
		$("#txtDoj").val($(this).closest('tr').children('td:eq(14)').text());
		$("#txtBasicSalary").val($(this).closest('tr').children('td:eq(15)').text());
		$("#cboPf").val($(this).closest('tr').children('td:eq(27)').text());
		$("#cboEsi").val($(this).closest('tr').children('td:eq(28)').text());
		$("#cboSalaryType").val($(this).closest('tr').children('td:eq(16)').text());
		$("#txtMobileProfessional").val($(this).closest('tr').children('td:eq(17)').text());
		$("#txtEmailProfessional").val($(this).closest('tr').children('td:eq(18)').text());
		$("#txtCorrespondenceAddress").val($(this).closest('tr').children('td:eq(19)').text());
		$("#txtAadhar").val($(this).closest('tr').children('td:eq(20)').text());
		$("#txtJobFunction").val($(this).closest('tr').children('td:eq(21)').text());
		$("#cboReportingManager").val($(this).closest('tr').children('td:eq(22)').text());
		// $("#cboReportingManager").val($(this).closest('tr').children('td:eq(23)').text());
		$("#txtEmergencyContactName").val($(this).closest('tr').children('td:eq(24)').text());
		$("#txtEmergencyContactNumber").val($(this).closest('tr').children('td:eq(25)').text());
		$("#txtEmergencyContactAddress").val($(this).closest('tr').children('td:eq(26)').text());
		$("#txtLeaveseOn").val($(this).closest('tr').children('td:eq(29)').text());
		$("#cboUser").val($(this).closest('tr').children('td:eq(30)').text());
		$("#txtHra").val($(this).closest('tr').children('td:eq(31)').text());
		$("#txtTa").val($(this).closest('tr').children('td:eq(32)').text());
		$("#txtCa").val($(this).closest('tr').children('td:eq(33)').text());
		$("#txtMa").val($(this).closest('tr').children('td:eq(34)').text());
		$("#cboShowInAttendance").val($(this).closest('tr').children('td:eq(35)').text());
		$("#txtHiddenRowId").val(globalrowid);
		$("#txtPhotoChange").val("no");
		// alert(globalrowid);
		$.ajax({
				'url': base_url + '/' + controller + '/getEmpDocs',
				'type': 'POST',
				'dataType': 'json',
				'data': {'globalrowid': globalrowid},
				'success': function(data)
				{
					// alert(JSON.stringify(data));
					$('#image-list').empty(); 
					for(i=0; i<data['empDocs'].length; i++)
		        	{
						var list = document.getElementById("image-list"),
						li   = document.createElement("li"),
						img  = document.createElement("img");
						var bu = '<?php echo base_url();?>';
						// var bu = base_url();
						img.src = bu + 'bootstrap/images/empdocs/' + data['empDocs'][i].docFileName;
						img.width = "150";
						img.height = "150";
						li.appendChild(img);
						list.appendChild(li);
					}
						
				}
		});

		$("#cboAbList").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});

			$("#cboAbList").focus();
		} );

</script>