<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Dsr_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          // cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].dsrRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].dsrRowId
	          cell.style.display="none";

	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].dsrDt));
	          // cell.style.display="none";

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].userRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].uid;
	          // cell.style.display="none";

	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].task;

	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].type;
	          // cell.style.display="none";

	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].abRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].name;
	          // cell.style.display="none";

	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].partyType;
	          // cell.style.display="none";

	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].remarks;

	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].further;

	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].mrRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(14);
		      cell.innerHTML = records[i].meetingRemark;

	      	  if ( records[i].nextDt != null )
		      {
		          var cell = row.insertCell(15);
		          cell.innerHTML = dateFormat(new Date(records[i].nextDt));

		          var cell = row.insertCell(16);
		          cell.innerHTML = records[i].nextTm;		      	
		      }
		      else
		      {
		      	  var cell = row.insertCell(15);
		          cell.innerHTML = "";
		      	  var cell = row.insertCell(16);
		          cell.innerHTML = "";
		      }

			  var cell = row.insertCell(17);
	          cell.innerHTML = records[i].cboLeadStage;
	          cell.style.display="none";

			  var cell = row.insertCell(18);
		      cell.innerHTML = records[i].dsr_ls_name;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}



	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						setTable(data['records']);
						alertPopup('Record deleted...', 4000);
						blankControls();
						$("#cboTask").focus();
					}
				}
			});
	}
	


	function saveData()
	{	
		var dsrDt = $("#dt").val().trim();
		// if(dsrDt !="")
		// {
			dtOk = testDate("dt");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dt").focus();
				return;
			}
		// }

		task = $("#cboTask").val();
		if(task == "-1")
		{
			alertPopup("Select task...", 8000);
			$("#cboTask").focus();
			return;
		}
		type = $("#cboType").val();
		if(type == "-1")
		{
			alertPopup("Select type...", 8000);
			$("#cboType").focus();
			return;
		}

		abRowId = $("#cboParty").val();
		if(abRowId == "-1" && task != "No-Meeting" && task != "Holiday")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}

		partyType = $("#cboPartyType").val();
		if(partyType == "-1" && task != "No-Meeting" && task != "Holiday")
		{
			alertPopup("Select party type...", 8000);
			$("#cboPartyType").focus();
			return;
		}
		// alert(partyType);
		remarks = $("#txtRemarks").val().trim();
		if(remarks.length <= 5)
		{
			alertPopup("Minimum 5 characters remarks...", 8000);
			$("#txtRemarks").focus();
			return;
		}
		further = $("#cboFurther").val();
		if(further == "-1" && task != "No-Meeting" && task != "Holiday")
		{
			alertPopup("Select further action...", 8000);
			$("#cboFurther").focus();
			return;
		}
		mrRowId = $("#cboClosing").val();
		if(mrRowId == "-1" && task != "Holiday")
		{
			alertPopup("Select remarks...", 8000);
			$("#cboClosing").focus();
			return;
		}

		cboLeadStage = $("#cboLeadStage").val();

		nextDt = $("#dtNext").val();
		dtOk = testDate("dtNext");
		if(dtOk != "")
		{
			if(dtOk == false && task != "No-Meeting")
			{
				alertPopup("Invalid date...", 5000);
				$("#dtNext").focus();
				return;
			}
		}

		nextTm = $("#tmNext").val();


		if($("#btnSave").val() == "Save")
		{

			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'dsrDt': dsrDt
								, 'task': task
								, 'type': type
								, 'abRowId': abRowId
								, 'partyType': partyType
								, 'remarks': remarks
								, 'further': further
								, 'mrRowId': mrRowId
								, 'cboLeadStage': cboLeadStage
								, 'nextDt': nextDt
								, 'nextTm': nextTm
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 4000);
								$("#cboTask").focus();
							}
							else if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#cboTask").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								// window.location.href=data;
								blankControls();
								$("#dt").val(dateFormat(new Date()));
								// $("#dtNext").val(dateFormat(new Date()));
								var uid='<?php echo $this->session->userid; ?>';
								$("#txtUser").val( uid );
								if(uid == "admin")
								{
								}
								else
								{
									$('#dt').prop('disabled', true);
								}
								$("#cboTask").focus();
							}
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert(task);
			// alert(JSON.stringify(TableData));
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'dsrDt': dsrDt
								, 'task': task
								, 'type': type
								, 'abRowId': abRowId
								, 'partyType': partyType
								, 'remarks': remarks
								, 'further': further
								, 'mrRowId': mrRowId
								, 'cboLeadStage': cboLeadStage
								, 'nextDt': nextDt
								, 'nextTm': nextTm

							},
					'success': function(data)
					{
						if(data)
						{
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 4000);
								$("#cboTask").focus();
							}
							else if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#cboTask").focus();
							}
							else
							{
								// alert(JSON.stringify(data['records']));
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#btnSave").val("Save");
								$("#dt").val(dateFormat(new Date()));
								// $("#dtNext").val(dateFormat(new Date()));
								var uid='<?php echo $this->session->userid; ?>';
								$("#txtUser").val( uid );
								if(uid == "admin")
								{
								}
								else
								{
									$('#dt').prop('disabled', true);
								}
								$("#cboTask").focus();
							}
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
	function loadLimitedRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadLimitedRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
					}
				}
			});
	}

	function storeTblHeaders()
	{
		var header = Array();
		// alert('ff');
		$("#tbl1 tr th").each(function(i, v){
		        header[i] = $(this).text();
		})

		return header;
	}

	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text().replace(/(\r\n|\n|\r)/gm,", ");
		    }); 
		})

	    return data;
	}

	///str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');
	///$(tr).find('td:eq(8)').text().replace(/(\r\n|\n|\r)/gm,", ")

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableDataHeader;
		TableDataHeader = storeTblHeaders();
		TableDataHeader = JSON.stringify(TableDataHeader);
		// alert(JSON.stringify(TableDataHeader));
		// return;

		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}


		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'TableDataHeader': TableDataHeader
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container-fluid" style="width: 97%">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h4 class="text-center" style='margin-top:-20px'>Daily Sales Report (DSR)</h4>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
	              	?>
	              	<script>
						$( "#dt" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dt").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>User ID:</label>";
						echo form_input('txtUser', $this->session->userid, "class='form-control' id='txtUser' style='' disabled='yes' ");
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- Select ---';
						$types['Meeting'] = "Meeting";
						$types['Call'] = "Call";
						$types['No-Meeting'] = "No-Meeting";
						$types['Holiday'] = "Holiday";
						$types['Action'] = "Action";
						echo "<label style='color: black; font-weight: normal;'>Task: </label>";
						echo form_dropdown('cboTask', $types, '-1', "class='form-control' id='cboTask'");
					?> 
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- Select ---';
						// $types['New'] = "New";
						// $types['Follow-up'] = "Follow-up";
						echo "<label id='lblType' style='color: black; font-weight: normal;'>Type: </label>";
						echo form_dropdown('cboType', $types, '-1', "class='form-control' id='cboType'");
					?> 
	          	</div>
			</div>


			<div class="row" style="margin-top:10px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Address Book: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>";
		              	?>
		              	<select id="cboParty" class="form-control" onChange="check(this.value);">
			              <option value="-1" addr="" mobile1="" mobile2="" townName="">--- Select ---</option>
			              <?php
			                foreach ($parties as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['abRowId']; ?>  addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
			              <?php
			                }
			              ?>
			            </select>

			            <script type="text/javascript">
				            $(document).ready(function()
				            {
				              $("#cboParty").change(function()
				              {
							        $("#txtAddress").val($('option:selected', '#cboParty').attr('addr'));
							        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('townName'));
							        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('mobile1'));
							        $("#txtAddress").val( $("#txtAddress").val() + ", " + $('option:selected', '#cboParty').attr('mobile2'));
				              });
				            });
				        </script>	
				    
				    	<?php
							echo "<label style='color: black; font-weight: normal; margin-top:10px;'>Address:</label>";
							echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtAddress'  disabled='yes' value=''");
		              	?>
				    		
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- Select ---';
						$types['Influencer'] = "Influencer";
						$types['Client'] = "Client";
						echo "<label style='color: black; font-weight: normal;'>Party Type: </label>";
						echo form_dropdown('cboPartyType', $types, '-1', "class='form-control' id='cboPartyType'");
					?> 

					<?php
						echo "<label style='color: black; font-weight: normal;margin-top:10px;'>Remarks:</label>";
						echo form_textarea('txtRemarks', '', "class='form-control' placeholder='minimum 5 characters' style='resize:none;height:100px;' id='txtRemarks' maxlength=1000 value=''");
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- Select ---';
						$types['Continue'] = "Continue";
						$types['Close'] = "Close";
						echo "<label id='lblType' style='color: black; font-weight: normal;'>Further: </label>";
						echo form_dropdown('cboFurther', $types, '-1', "class='form-control' id='cboFurther'");
					?> 
	          	</div>

				<div id="divClose" class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="display: none1;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Short Remarks:</label>";
						echo form_dropdown('cboClosing', $closingRemarks, '-1', "class='form-control' id='cboClosing'");
	              	?>
	          	</div>

				<div id="divClose" class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="display: none1;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Lead Status:</label>";
						echo form_dropdown('cboLeadStage', $cboLeadStage, '-1', "class='form-control' id='cboLeadStage'");
	              	?>
	          	</div>

	          	<div id="divContinue" class="col-lg-6 col-sm-6 col-md-6 col-xs-12" style="visibility: hidden;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;margin-top:10px;'>Next Date:</label>";
							echo form_input('dtNext', '', "class='form-control' placeholder='' id='dtNext' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtNext" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							//$("#dtNext").val(dateFormat(new Date()));
						</script>
					</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;margin-top:10px;'>Time:</label>";
			  				echo '<input type="time" id="tmNext" name="tm" class="form-control">';
		  				?>
					</div>
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" id="detail"></div>
	<div>

	<br/><br>
	<div class="row">
		<div class="col-lg-12"><h3>Edit Details</h3></div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th width="50" class="editRecord text-center">Edit</th>
					 	<th style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>dsrRowid</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>userRowId</th>
					 	<th>User</th>
					 	<th>Task</th>
					 	<th>Type</th>
					 	<th style='display:none;'>abRowId</th>
					 	<th>Party</th>
					 	<th>Party Type</th>
					 	<th>Remarks</th>
					 	<th>Further</th>
					 	<th style='display:none;'>MeetingReasonRowId</th>
					 	<th>Short Remarks</th>
					 	<th>Dt.</th>
					 	<th>Time</th>
						<th style='display:none;'>cboLeadStage</th>
						<th>Lead Status</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 	// print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['dsrRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['dsrRowId']."</td>";
						 	$vdt = strtotime($row['dsrDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['userRowId']."</td>";
						 	echo "<td>".$row['uid']."</td>";
						 	echo "<td>".$row['task']."</td>";
						 	echo "<td>".$row['type']."</td>";
						 	echo "<td style='display:none;'>".$row['abRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['partyType']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td>".$row['further']."</td>";
						 	echo "<td style='display:none;'>".$row['mrRowId']."</td>";
						 	echo "<td>".$row['meetingRemark']."</td>";

							if( $row['nextDt'] != "")
						 	{
							 	$vdt = strtotime($row['nextDt']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
						 	}
						 	else
						 	{
						 		echo "<td></td>";	
						 	}
							
							echo "<td>".$row['nextTm']."</td>";
							
							echo "<td style='display:none;'>".$row['cboLeadStage']."</td>";
							echo "<td>".$row['dsr_ls_name']."</td>";

							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">CREDO</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#dt").val($(this).closest('tr').children('td:eq(3)').text());
		$("#txtUser").val($(this).closest('tr').children('td:eq(5)').text());
		$("#cboTask").val($(this).closest('tr').children('td:eq(6)').text());
		$("#cboType").val($(this).closest('tr').children('td:eq(7)').text());
		$("#cboParty").val($(this).closest('tr').children('td:eq(8)').text());
		$('#cboParty').trigger('change');
		$("#cboPartyType").val($(this).closest('tr').children('td:eq(10)').text());
		$("#txtRemarks").val($(this).closest('tr').children('td:eq(11)').text());
		$("#cboFurther").val($(this).closest('tr').children('td:eq(12)').text());
		$("#cboClosing").val($(this).closest('tr').children('td:eq(13)').text());
		$("#dtNext").val($(this).closest('tr').children('td:eq(15)').text());
		$("#tmNext").val($(this).closest('tr').children('td:eq(16)').text());
		$("#cboLeadStage").val($(this).closest('tr').children('td:eq(17)').text());

		$('#dt').prop('disabled', true);

		// $('#dtDsr').val('dd');

		$("#cboTask").focus();
		$('#cboFurther').trigger('change');

		$("#btnSave").val("Update");
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );


  
	$("#cboTask").on('change', setTypes);
	function setTypes()
	{
		$('#cboType').empty();

		var task = $("#cboTask").val();
		if( task == "Meeting" )
		{
			$('#lblType').text("Meeting Type");
			$('#cboType').append($('<option>', { value: 'New', text : 'New'  }));
			$('#cboType').append($('<option>', { value: 'Follow-up', text : 'Follow-up'  }));
			enableControls();
		}
		else if( task == "Call" )
		{
			$('#lblType').text("Call Type");
			$('#cboType').append($('<option>', { value: 'New', text : 'New'  }));
			$('#cboType').append($('<option>', { value: 'Follow-up', text : 'Follow-up'  }));
			$('#cboType').append($('<option>', { value: 'Cold Call', text : 'Cold Call'  }));
			enableControls();
		}
		else if( task == "No-Meeting" )
		{
			disableControls();
		}
		else if( task == "Holiday" )
		{
			disableControls();
			$('#cboClosing').prop('disabled', true);
			$('#cboClosing').val("-1");
		}

		else if( task == "-1" )
		{
			$('#lblType').text("Type");
			disableControls();
			// alert("0");
		}
		if( task == "Action" )
		{
			$('#lblType').text("Meeting Type");
			$('#cboType').append($('<option>', { value: 'New', text : 'New'  }));
			$('#cboType').append($('<option>', { value: 'Follow-up', text : 'Follow-up'  }));
			enableControls();
		}
	}

	function disableControls()
	{
		$('#cboType').prop('disabled', true);
		$('#cboParty').prop('disabled', true);
		$('#cboPartyType').prop('disabled', true);
		// $('#txtRemarks').prop('disabled', true);
		$('#cboFurther').prop('disabled', true);
		// $('#cboClosing').prop('disabled', true);
		$('#dtNext').prop('disabled', true);
		$('#tmNext').prop('disabled', true);
	}


	function enableControls()
	{
		$('#cboType').prop('disabled', false);
		$('#cboParty').prop('disabled', false);
		$('#cboPartyType').prop('disabled', false);
		$('#txtRemarks').prop('disabled', false);
		$('#cboFurther').prop('disabled', false);
		$('#cboClosing').prop('disabled', false);
		$('#dtNext').prop('disabled', false);
		$('#tmNext').prop('disabled', false);
	}

	$("#cboFurther").on('change', setFurther);
	function setFurther()
	{
		// alert();
		var task = $("#cboFurther").val();
		if( task == "Close" )
		{
			$('#divClose').css("display", "block");
			$('#divContinue').css("visibility", "hidden");
		}
		else if( task == "Continue" )
		{
			// $('#divClose').css("display", "none");
			$('#divContinue').css("visibility", "visible");
		}
		else if( task == "-1" )
		{
			// $('#divClose').css("display", "none");
			$('#divContinue').css("visibility", "hidden");
		}
	}

	$( "#txtRemarks" ).keypress(function(event) 
   {
  		if ( event.which == 39 || event.which == 34) 
  		{
  			event.preventDefault();
  			$(this).val($(this).val() + '');
  		}
	});

	$(document).ready(function() {
		var uid='<?php echo $this->session->userid; ?>';
		if(uid == "admin")
		{

		}
		else
		{
			$('#dt').prop('disabled', true);
		}
	});
</script>

<script>
function check(party_nm){	
	//$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();	
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");	
	}
	  
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
		  document.getElementById('detail').innerHTML=xmlhttp.responseText;	
		} 
	}
	
	var queryString="?party_nm="+encodeURIComponent(party_nm);	 	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/Dsr_Controller/dsr_view_ajax" + queryString,true);	
	xmlhttp.send();	
}
</script>

<!-- Print Commands -->
<script>
function printDiv(divName){
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

<script type="text/javascript">
/*
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,', 
  template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', 
  base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
  format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})();*/
</script>

<script type="text/javascript">
function Export() {
	$("#example1").table2excel({
		exclude: ".excludeThisClass",
		name: "Worksheet Name",
		filename: "lead_activity_history.xls", // do include extension
		preserveColors: false // set to true if you want background colors and font colors preserved
	});
}
</script>