<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptLeaveApplication_Controller';
	var base_url='<?php echo site_url();?>';

	Date.prototype.addDays = function(days) {
       var dat = new Date(this.valueOf())
       dat.setDate(dat.getDate() + days);
       return dat;
   }

   function getDates(startDate, stopDate) {
      var dateArray = new Array();
      var currentDate = startDate;
      while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
      }
      return dateArray;
    }
    var noOfDays=0;
	function setHeadings()
	{
		$("#tbl1").empty();
		  // $("#tbl1").find("tr:gt(0)").remove();

		  ////////////// Set table headings
	      var table = document.getElementById("tbl1");
	      newRowIndex = table.rows.length;
          row = table.insertRow(newRowIndex);
          var cell = row.insertCell(0);
          cell.innerHTML = "S.N.";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";
          cell.style.width = "60px";
          var cell = row.insertCell(1);
          cell.innerHTML = "EmpRowId";
          // cell.style.width = "60px";
          cell.style.display="none";
          var cell = row.insertCell(2);
          cell.innerHTML = "Emp. Name";
          cell.style.width = "200px";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";
          var dtFrom = new Date( $("#dtFrom").val().trim() );
          var dtTo = new Date( $("#dtTo").val().trim() );

          var dateArray = getDates( new Date($("#dtFrom").val().trim()), (new Date( $("#dtTo").val().trim() )) );
	      	noOfDays=0;

			for (i = 0; i < dateArray.length; i ++ ) 
			{
			    // alert ( dateArray[i].getDate() + "-" + dateArray[i].getMonth()+1 + "-" + dateArray[i].getFullYear() );
			    var cell = row.insertCell(i+3);
			    var m = dateArray[i].getMonth()+1;
	            cell.innerHTML = dateArray[i].getDate() + "-" + m + "-" + dateArray[i].getFullYear();
	            cell.style.width = "100px";
	            cell.style.backgroundColor="#F0F0F0";
	            cell.style.fontWeight="bold";
	            noOfDays++;
			}
	}

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  
		 setHeadings();

	      var table = document.getElementById("tbl1");
	      // alert(noOfDays);
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].userRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].user;
	          cell.style.backgroundColor="#F0F0F0";
	        

	          var index = [];
				// build the index
				for (var x in records[i]) {
				   index.push(x);
				}
			
	          for (j = 1; j <= noOfDays; j++ ) 
			  {
			  	var cell = row.insertCell(j+2);
			  	cell.innerHTML = records[i][index[j+2]] ;

			  }

	
	  	  }

		$("#tbl1 tr").on("click", highlightRowAlag);
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		var userRowId = $("#cboUser").val();
		// alert(userRowId);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'userRowId': userRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

</script>
<div class="acontainer">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12"style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Leave Applications</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    
							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
							$("#dtFrom").val(dateFormat(firstDay));
							// var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
						</script>					
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    
							var date = new Date();
							var lastDay = new Date(date.getFullYear(), date.getMonth() +1, 0);
							$("#dtTo").val(dateFormat(lastDay));
						</script>					
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>User:</label>";
								echo form_dropdown('cboUser',$users, '-1',"class='form-control' id='cboUser'");
			              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <!-- <tr style="background-color: #F0F0F0;">
							 	<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none1;'>empRowId</th>
							 	<th width="150" >Emp Name</th>
							 	<th width="80" >Paying Now</th>
							 </tr> -->
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="display: none;">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
	</div>
</div>





<script type="text/javascript">


</script>