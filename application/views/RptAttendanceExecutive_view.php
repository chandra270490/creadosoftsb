<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptAttendanceExecutive_Controller';
	var base_url='<?php echo site_url();?>';

	Date.prototype.addDays = function(days) {
       var dat = new Date(this.valueOf())
       dat.setDate(dat.getDate() + days);
       return dat;
   }

   function getDates(startDate, stopDate) {
      var dateArray = new Array();
      var currentDate = startDate;
      while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
      }
      return dateArray;
    }
    var noOfDays=0;
	function setHeadings()
	{
		$("#tbl1").empty();
		  // $("#tbl1").find("tr:gt(0)").remove();

		  ////////////// Set table headings
	      var table = document.getElementById("tbl1");
	      newRowIndex = table.rows.length;
          row = table.insertRow(newRowIndex);
          var cell = row.insertCell(0);
          cell.innerHTML = "S.N.";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";
          cell.style.width = "60px";
          var cell = row.insertCell(1);
          cell.innerHTML = "userRowId";
          // cell.style.width = "60px";
          cell.style.display="none";
          var cell = row.insertCell(2);
          cell.innerHTML = "User Name";
          cell.style.width = "200px";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";
          var dtFrom = new Date( $("#dtFrom").val().trim() );
          var dtTo = new Date( $("#dtTo").val().trim() );

          var dateArray = getDates( new Date($("#dtFrom").val().trim()), (new Date( $("#dtTo").val().trim() )) );
	      	noOfDays=0;

			for (i = 0; i < dateArray.length; i ++ ) 
			{
			    // alert ( dateArray[i].getDate() + "-" + dateArray[i].getMonth()+1 + "-" + dateArray[i].getFullYear() );
			    var cell = row.insertCell(i+3);
			    var m = dateArray[i].getMonth()+1;
	            cell.innerHTML = dateArray[i].getDate() + "-" + m + "-" + dateArray[i].getFullYear();
	            cell.style.width = "100px";
	            cell.style.backgroundColor="#F0F0F0";
	            cell.style.fontWeight="bold";
	            noOfDays++;
			}

		   var cell = row.insertCell(i+3);
	       cell.innerHTML = "Present";
	       cell.style.width = "100px";
	       cell.style.backgroundColor="#F0F0F0";

		   var cell = row.insertCell(i+4);
	       cell.innerHTML = "Absent";
	       cell.style.width = "100px";
	       cell.style.backgroundColor="#F0F0F0";

		   var cell = row.insertCell(i+5);
	       cell.innerHTML = "Leave";
	       cell.style.width = "100px";
	       cell.style.backgroundColor="#F0F0F0";

		   var cell = row.insertCell(i+6);
	       cell.innerHTML = "Holiday";
	       cell.style.width = "100px";
	       cell.style.backgroundColor="#F0F0F0";

		   var cell = row.insertCell(i+7);
	       cell.innerHTML = "Tour";
	       cell.style.width = "100px";
	       cell.style.backgroundColor="#F0F0F0";
			// alert(i);
		  ////////////// END - Set table headings
	}

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  
		 setHeadings();

	      var table = document.getElementById("tbl1");
	      // alert(noOfDays);
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].userRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].userName;
	          cell.style.backgroundColor="#F0F0F0";
	        
	        	var presentCount = 0;
	        	var absentCount = 0;
	        	var leaveCount = 0;
	        	var nightShiftCount = 0;
	        	var midNightCount = 0;
	        	var tourCount = 0;
	        	var holidayCount = 0;

	          var index = [];
				// build the index
				for (var x in records[i]) {
				   index.push(x);
				}
			
	          for (j = 1; j <= noOfDays; j++ ) 
			  {
			  	var cell = row.insertCell(j+2);
			  	cell.innerHTML = records[i][index[j+2]] ;
			  	if( records[i][index[j+2]].substr(0,7) == "PR" )
			  	{
			  		presentCount++;
			  	}
			  	if( records[i][index[j+2]].substr(0,7) == "HO" )
			  	{
			  		cell.style.color="green";
			  		holidayCount++;
			  	}
			  	else if( records[i][index[j+2]].substr(0,6) == "AB" )
			  	{
			  		cell.style.color="red";
			  		absentCount++;
			  	}
			  	else if( records[i][index[j+2]].substr(0,5) == "LE" )
			  	{
			  		cell.style.color="red";
			  		leaveCount++;
			  	}
			  	else if( records[i][index[j+2]].substr(0,5) == "TO" )
			  	{
			  		cell.style.color="blue";
			  		tourCount++;
			  	}
			  	else if( records[i][index[j+2]].substr(0,5) == "NA" )
			  	{
			  		cell.style.color="lightgrey";
			  		// 
			  	}
			  	
			  }

			  var cell = row.insertCell(noOfDays + 3);
	          cell.innerHTML = presentCount;
	          cell.style.backgroundColor="#F0F0F0";

			  var cell = row.insertCell(noOfDays + 4);
	          cell.innerHTML = absentCount;
	          cell.style.backgroundColor="#F0F0F0";

			  var cell = row.insertCell(noOfDays + 5);
	          cell.innerHTML = leaveCount;
	          cell.style.backgroundColor="#F0F0F0";
			  
			  var cell = row.insertCell(noOfDays + 6);
	          cell.innerHTML = holidayCount;
	          cell.style.backgroundColor="#F0F0F0";

			  var cell = row.insertCell(noOfDays + 7);
	          cell.innerHTML = tourCount;
	          cell.style.backgroundColor="#F0F0F0";
	  	  }

		$("#tbl1 tr").on("click", highlightRowAlag);
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		// alert(dtTo);

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text();
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}


		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'noOfDays': noOfDays
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="acontainer">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h3 class="text-center" style='margin-top:-20px;font-size:3vw'>Attendance Register (Executives)</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the 1st of previous month
							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
							$("#dtFrom").val(dateFormat(firstDay));
							// var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
						</script>					
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the last of previous month
							var date = new Date();
							var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
							$("#dtTo").val(dateFormat(lastDay));
						</script>					
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <!-- <tr style="background-color: #F0F0F0;">
							 	<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none1;'>empRowId</th>
							 	<th width="150" >Emp Name</th>
							 	<th width="80" >Paying Now</th>
							 </tr> -->
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
	</div>
</div>





<script type="text/javascript">


</script>