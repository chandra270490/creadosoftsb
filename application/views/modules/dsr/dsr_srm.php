<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3 style="text-align:center"><i class="fa fa-laptop"></i>DSR Short Remarks Master</h3>
        </div> 
    </div>
    <?php
        $mrRowId = $_REQUEST["id"];

        if($mrRowId != ""){
            $sql_prev_det = "select * from meetingremarks where mrRowId = '".$mrRowId."'";
            $qry_prev_det = $this->db->query($sql_prev_det)->row();
            $meetingRemark = $qry_prev_det->meetingRemark;
        } else {
            $meetingRemark = "";
        }
    ?>
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/Dsr_Controller/dsr_srm_entry">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="color:black">Short Remarks</label>
                            <div class="col-sm-10">
                                <input type="hidden" id="dsr_sr_id" name="dsr_sr_id" value="<?=$mrRowId;?>">
                                <input type="text" class="form-control" id="dsr_short_rmks" name="dsr_short_rmks" value="<?=$meetingRemark;?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <table class="table table-bordered">
                <thead>
                    <th>Short Remarks Id</th>
                    <th>Short Remarks</th>
                    <th>Created Date</th>
                </thead>
                <tbody>
                    <?php
                        $sql_det = "select * from meetingremarks order by mrRowId desc";
                        $qry_det = $this->db->query($sql_det);
                        foreach($qry_det->result() as $row){
                    ?>
                    <tr>
                        <td><?=$row->mrRowId;?></td>
                        <td><?=$row->meetingRemark;?></td>
                        <td><?=$row->createdStamp;?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4"></div>
    </div>
  </section>
</section>