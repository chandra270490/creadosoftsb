<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3 style="text-align:center"><i class="fa fa-laptop"></i>DSR Lead Stage Master</h3>
        </div> 
    </div>
    <?php
        $dsr_ls_id = $_REQUEST["id"];

        if($dsr_ls_id != ""){
            $sql_prev_det = "select * from dsr_lead_stage_mst where dsr_ls_id = '".$dsr_ls_id."'";
            $qry_prev_det = $this->db->query($sql_prev_det)->row();
            $dsr_ls_name = $qry_prev_det->dsr_ls_name;
        } else {
            $dsr_ls_name = "";
        }
    ?>
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/Dsr_Controller/dsr_ls_entry">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="color:black">Lead Stage</label>
                            <div class="col-sm-10">
                                <input type="hidden" id="dsr_ls_id" name="dsr_ls_id" value="<?=$dsr_ls_id;?>">
                                <input type="text" class="form-control" id="dsr_ls_name" name="dsr_ls_name" value="<?=$dsr_ls_name;?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <table class="table table-bordered">
                <thead>
                    <th>Lead Stage Id</th>
                    <th>Lead Stage Name</th>
                    <th>Created Date</th>
                </thead>
                <tbody>
                    <?php
                        $sql_det = "select * from dsr_lead_stage_mst order by dsr_ls_created_date desc";
                        $qry_det = $this->db->query($sql_det);
                        foreach($qry_det->result() as $row){
                    ?>
                    <tr>
                        <td><?=$row->dsr_ls_id;?></td>
                        <td><?=$row->dsr_ls_name;?></td>
                        <td><?=$row->dsr_ls_created_date;?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4"></div>
    </div>
  </section>
</section>