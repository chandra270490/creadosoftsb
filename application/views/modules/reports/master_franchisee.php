<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Master Frachisee List</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <!-- Add Row Button -->    
    <!-- View Records -->
    <div class="row">
        <div class="col-lg-12">
        	<table class="table table-bordered">
                <!-- ListHead Starts -->
                <thead>
                    <tr>
                        <th>SNo</th>
                        <th>Franchise No.</th>
                        <th>Franchise Name</th>
                        <th>Franchise Email</th>
                        <th>Franchise Type</th>
                    </tr>
                </thead>
                <!-- ListHead Ends -->
                <!-- ListBody Starts -->
                <tbody>
                    <?php
                        $sql_tbl_val = "select * from franchisee_mst where franchisee_type = 'master'";
                        $qry_tbl_val = $this->db->query($sql_tbl_val);

                        $sno=0;
                        foreach($qry_tbl_val->result() as $row){
                            $sno++;
                    ?>
                    <tr>
                        <td><?php echo $sno; ?></td>
                        <td><?php echo $row->franchisee_no; ?></td>
                        <td><?php echo $row->franchisee_name; ?></td>
                        <td><?php echo $row->franchisee_email; ?></td>
                        <td><?php echo $row->franchisee_type; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <!-- ListBody Ends -->
            </table>
        </div>
    </div>
  </section>
</section>