<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>GST</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <?php
        $gst_id = $_REQUEST['id'];

        if($gst_id != ""){
            $sql = "select * from gst_mst where gst_id = '".$gst_id."'";
            $qry = $this->db->query($sql)->row();

            $gst_name    = $qry->gst_name;
            $gst_perc = $qry->gst_perc;
            $gst_active    = $qry->gst_active;
        } else {
            $gst_name    = "";
            $gst_perc    = "";
            $gst_active  = "";
        }
    ?>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">GST</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/saic/gst_entry">
                        <?php
                            if($gst_id != ""){
                                echo "<h3 style='text-align:center'>GST Id - ".$gst_id."</h3><br>";
                                echo "<input type='hidden' name='gst_id' id='gst_id' value='".$gst_id."'>";
                            } else {
                                echo "<input type='hidden' name='gst_id' id='gst_id' value=''>";
                            }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">GST Name</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="gst_name" name="gst_name" value="<?=$gst_name; ?>" required>
                            </div>

                            <label class="col-sm-2 control-label">GST Percentage</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="gst_perc" name="gst_perc" value="<?=$gst_perc; ?>"
                                onkeypress="return isNumberKey(event);"  required>
                            </div>

                            <label class="col-sm-2 control-label">GST Active</label>
                            <div class="col-sm-2">
                                <select class="form-control" id="gst_active" name="gst_active" required>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>