<?php $this->load->helper("sai"); ?>
<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Raw Material Products</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <?php
        $product_id = $_REQUEST['id'];

        if($product_id != ""){
            $sql = "select * from raw_mat_mst where product_id = '".$product_id."'";
            $qry = $this->db->query($sql)->row();
            
            $category_id     = $qyr->category_id;
            $product_name    = $qry->product_name;
            $product_active  = $qry->product_active;
        } else {
            $product_name    = "";
            $product_active  = "";
        }
    ?>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Raw Material Products</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/saic/raw_mat_entry">
                        <?php
                            if($product_id != ""){
                                echo "<h3 style='text-align:center'>Product Id - ".$product_id."</h3><br>";
                                echo "<input type='hidden' name='product_id' id='product_id' value='".$product_id."'>";
                            } else {
                                echo "<input type='hidden' name='product_id' id='product_id' value=''>";
                            }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category Name</label>
                            <div class="col-sm-4">
                                <select id="category_id" name="category_id" class="form-control">
                                    <?php echo category_list(); ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="product_name" name="product_name" value="<?=$product_name; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Active</label>
                            <div class="col-sm-2">
                                <select class="form-control" id="product_active" name="product_active" required>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>