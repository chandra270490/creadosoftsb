<?php $this->load->helper("sai"); ?>
<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Raw Material Puchase Bill</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <?php
        $bill_mst_id = $_REQUEST['id'];

        if($bill_mst_id != ""){

            $sql = "select * from bill_mst where bill_mst_id = '".$bill_mst_id."'";
            $qry = $this->db->query($sql)->row();

            $vendor_id    = $qry->vendor_id;
            $vendor_name  = $qry->vendor_name;
            $invoice_no   = $qry->invoice_no;
            $invoice_date = $qry->invoice_date;

        } else {

            $vendor_id    = "";
            $vendor_name  = "";
            $invoice_no   = "";
            $invoice_date = "";
        }
    ?>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Raw Material Puchase Bill</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/saic/bill_entry">
                        <?php
                            if($bill_mst_id != ""){
                                echo "<h3 style='text-align:center'>Bill Mst Id - ".$bill_mst_id."</h3><br>";
                                echo "<input type='hidden' name='bill_mst_id' id='bill_mst_id' value='".$bill_mst_id."'>";
                            } else {
                                echo "<input type='hidden' name='bill_mst_id' id='bill_mst_id' value=''>";
                            }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Vendor</label>
                            <div class="col-sm-6">
                                <select id="vendor_id" name="vendor_id" class="form-control" required>
                                    <option value="">--select--</option>
                                    <?php
                                        $sql_ven = "select * from vendor_mst where vendor_active = 'Yes'";
                                        $qry_ven = $this->db->query($sql_ven);
                                        foreach($qry_ven->result() as $row){
                                            $vendor_id = $row->vendor_id;
                                            $vendor_name = $row->vendor_name;
                                    ?>
                                        <option value="<?=$vendor_id;?>"><?=$vendor_name;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Invoice No</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="invoice_no" name="invoice_no" 
                                value="<?=$invoice_no; ?>" required>
                            </div>

                            <label class="col-sm-2 control-label">Invoice Date</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="invoice_date" name="invoice_date" 
                                value="<?=$invoice_date; ?>" required>
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="table-wrapper">
                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-12"><h3 style="text-align:center">Add Products</b></h3></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered" id="item_tbl">
                                            <thead>
                                                <tr style="font-weight:bold">
                                                    <th>Category</th>
                                                    <th>Product Name</th>
                                                    <th>Quantity</th>
                                                    <th>Rate</th>
                                                    <th>Total Amount</th>
                                                    <th><span class="glyphicon glyphicon-plus" style="font-size:15px;color:green;" onclick="addrow();"></span></th>
                                                </tr>
                                            </thead>
                                            <tbody style="text-align:left">
                                                <tr>
                                                    <td>
                                                        <select class="form-control category_id" id="category_id1" name="category_id[]" onchange="cat_prod(this.value,1);" required>
                                                            <?php echo category_list(); ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div id="ajax_div1">
                                                            <select class="form-control product_id" id="product_id1" name="product_id[]" required>
                                                                
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control quantity" id="quantity1" name="quantity[]" onkeyup="line_tot_amt(1);" value="0" onkeypress="return isNumberKey(event);">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" id="rate1" name="rate[]" onkeyup="line_tot_amt(1);"  value="0" onkeypress="return isNumberKey(event);">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control line_total_amt" id="line_total_amt1" name="line_total_amt[]" onkeyup="line_tot_amt(1);"  value="0" onkeypress="return isNumberKey(event);" readonly>
                                                    </td>
                                                    <td><span class="glyphicon glyphicon-remove" style="font-size:15px;color:red;" onclick="deleterow()"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-1"><b>Total Qty</b></div>
                                    <div class="col-lg-2"><input type="text" class="form-control" id="tot_qty" name="tot_qty" value="0" readonly></div>
                                    <div class="col-lg-1"><b>Total Amount</b></div>
                                    <div class="col-lg-2"><input type="text" class="form-control" id="tot_amt" name="tot_amt" value="0" readonly></div>
                                </div>
                                
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>

function cat_prod(category_id, row_id){
    //alert(category_id);
    //alert(row_id);

    //Ajax
    $("#ajax_div"+row_id).empty().html('<img src="<?php echo base_url(); ?>assets/images/wait.gif" />');
    
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    } 

    xmlhttp.onreadystatechange=function(){
        if(xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById("ajax_div"+row_id).innerHTML=xmlhttp.responseText;
        }
    }
    
    var queryString="?cat_id="+encodeURIComponent(category_id)+"&row_id="+encodeURIComponent(row_id);
    
    xmlhttp.open("GET","<?php echo base_url(); ?>index.php/saic/bill_add_ajax" +queryString,true);
    xmlhttp.send();
}

</script>

<script>
//select 2 box
$( function(){
    $("#vendor_id").select2();	
});

/*
$( function(){	
    $("#product_id1").select2();
});
*/

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}

//Datepicker
$( function() {
    $( "#invoice_date" ).datepicker({
        "dateFormat" : "yy-mm-dd"
    });
} );
</script>

<script>
//Item Table
function addrow(){
	
	var table = document.getElementById('item_tbl');
	
	var a =  document.getElementById('item_tbl').rows.length;
	var rowCount = a+1;
	
	var row = table.insertRow(a);

    var newCell1 = row.insertCell(0);
	newCell1.innerHTML = '<select class="form-control category_id" id="category_id'+rowCount+'" name="category_id[]" onchange="cat_prod(this.value,'+rowCount+');" required><?php echo category_list(); ?></select>';
	
	var newCell1 = row.insertCell(1);
	newCell1.innerHTML = '<div id="ajax_div'+rowCount+'"><select class="form-control product_id" id="product_id'+rowCount+'" name="product_id[]" required></select></div>';
	
	var newCell1 = row.insertCell(2);
	newCell1.innerHTML = '<input type="text" class="form-control quantity" id="quantity'+rowCount+'" name="quantity[]" onkeyup="line_tot_amt('+rowCount+');"  value="0" onkeypress="return isNumberKey(event);">';

    var newCell1 = row.insertCell(3);
	newCell1.innerHTML = '<input type="text" class="form-control" id="rate'+rowCount+'" name="rate[]" onkeyup="line_tot_amt('+rowCount+');"  value="0" onkeypress="return isNumberKey(event);">';

    var newCell1 = row.insertCell(4);
	newCell1.innerHTML = '<input type="text" class="form-control line_total_amt" id="line_total_amt'+rowCount+'" name="line_total_amt[]" onkeyup="line_tot_amt('+rowCount+');"  value="0" onkeypress="return isNumberKey(event);" readonly>';

	var newCell1 = row.insertCell(5);
	newCell1.innerHTML = '<span class="glyphicon glyphicon-remove" style="font-size:15px;color:red;" onclick="deleterow()"></span>';

    $( function(){
        $(".product_id").select2();
    });
}

function deleterow(){
	var table = document.getElementById('item_tbl');
	var rowCount = table.rows.length;
	table.deleteRow(rowCount -1);

    tot_qty();
    tot_amt();
}

//Line Total Amt
function line_tot_amt(rowCount){
    //alert();
    var quantity = document.getElementById("quantity"+rowCount).value;
    var rate = document.getElementById("rate"+rowCount).value;

    var Tot = Number(quantity)*Number(rate);
    //alert(RunTot);
    document.getElementById("line_total_amt"+rowCount).value = Tot;

    tot_qty();
    tot_amt();

}

//Total Quantity
function tot_qty(){
    //alert();
    var total = 0;
    var list = document.getElementsByClassName("quantity");
    var values = [];
    for(var i = 0; i < list.length; ++i) {
        values.push(parseFloat(list[i].value));
    }
    total = values.reduce(function(previousValue, currentValue, index, array){
        return previousValue + currentValue;
    });
    //alert(total);
    document.getElementById("tot_qty").value = total;
}

//Total Amount
function tot_amt(){
    //alert();
    var total = 0;
    var list = document.getElementsByClassName("line_total_amt");
    var values = [];
    for(var i = 0; i < list.length; ++i) {
        values.push(parseFloat(list[i].value));
    }
    total = values.reduce(function(previousValue, currentValue, index, array){
        return previousValue + currentValue;
    });
    //alert(total);
    document.getElementById("tot_amt").value = total;
}
</script>