<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Store Add</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <!--Form-->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Add Store</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/storec/store_entry">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="store_comp_id" name="store_comp_id" required>
                                    <?php
                                        $sql_comp_nm = "select * from company_mst";
                                        $qry_comp_nm = $this->db->query($sql_comp_nm);
                                        
                                        foreach($qry_comp_nm->result() as $row){
                                            $comp_no = $row->comp_no;
                                            $comp_name = $row->comp_name;
                                    ?>
                                    <option value="<?php echo $comp_no; ?>"><?php echo $comp_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Store Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_name" name="store_name" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Franchisee</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="store_franchisee_id" name="store_franchisee_id" required>
                                    <?php
                                        $sql_franchise = "select * from franchisee_mst";
                                        $qry_franchise = $this->db->query($sql_franchise);
                                        
                                        foreach($qry_franchise->result() as $row){
                                            $franchisee_no = $row->franchisee_no;
                                            $franchisee_name = $row->franchisee_name;
                                    ?>
                                    <option value="<?php echo $franchisee_no; ?>"><?php echo $franchisee_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Region</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="store_region_id" name="store_region_id" required>
                                    <?php
                                        $sql_region = "select * from region_mst";
                                        $qry_region = $this->db->query($sql_region);
                                        
                                        foreach($qry_region->result() as $row){
                                            $region_id = $row->region_id;
                                            $region_name = $row->region_name;
                                    ?>
                                    <option value="<?php echo $region_id; ?>"><?php echo $region_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_email" name="store_email" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_phone" name="store_phone" value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address Line 1</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_addr1" name="store_addr1" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address Line2</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_addr2" name="store_addr2" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">City / District</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_city" name="store_city" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">State / Province</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="store_state_id" name="store_state_id" required>
                                    <?php
                                        $sql_state = "select * from state_mst";
                                        $qry_state = $this->db->query($sql_state);
                                        
                                        foreach($qry_state->result() as $row){
                                            $state_id = $row->id;
                                            $state_name = $row->state_name;
                                    ?>
                                    <option value="<?php echo $state_id; ?>"><?php echo $state_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Postal Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="store_postcode" name="store_postcode" value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="store_country_id" name="store_country_id" required>
                                    <?php
                                        $sql_country = "select * from country_mst";
                                        $qry_country = $this->db->query($sql_country);
                                        
                                        foreach($qry_country->result() as $row){
                                            $country_id = $row->id;
                                            $country_name = $row->country_name;
                                    ?>
                                    <option value="<?php echo $country_id; ?>"><?php echo $country_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//select 2 box
$( function(){
    $("#store_comp_id").select2();	
    $("#store_franchisee_id").select2();	
    $("#store_region_id").select2();	
    $("#store_country_id").select2();	
    $("#store_state_id").select2();	
});

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>