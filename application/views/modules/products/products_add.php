<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Products Add</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Add Products</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/productsc/products_entry">
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="prod_comp_id" name="prod_comp_id" required>
                                    <?php
                                        $sql_comp_nm = "select * from company_mst";
                                        $qry_comp_nm = $this->db->query($sql_comp_nm);
                                        
                                        foreach($qry_comp_nm->result() as $row){
                                            $comp_no = $row->comp_no;
                                            $comp_name = $row->comp_name;
                                    ?>
                                    <option value="<?php echo $comp_no; ?>"><?php echo $comp_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Product Category</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="prod_cat_id" name="prod_cat_id" required>
                                    <?php
                                        $sql_comp_nm = "select * from prodcat_mst";
                                        $qry_comp_nm = $this->db->query($sql_comp_nm);
                                        
                                        foreach($qry_comp_nm->result() as $row){
                                            $prodcat_id = $row->prodcat_id;
                                            $prodcat_name = $row->prodcat_name;
                                    ?>
                                    <option value="<?php echo $prodcat_id; ?>"><?php echo $prodcat_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="prod_name" name="prod_name" value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Product Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="prod_code" name="prod_code" value="">
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Product Description</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="prod_desc" name="prod_desc" value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Unit Price</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="prod_unit_price" name="prod_unit_price" value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Available Stock</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="prod_stock" name="prod_stock" value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//select 2 box
$( function(){
    $("#prod_comp_id").select2();	
    $("#prod_cat_id").select2();	
});

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>