<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 style="text-align:center">Budget</h3>
        </div> 
    </div>
    
    <?php
        $budget_id = $_REQUEST['id'];
        if($budget_id != ''){
            foreach($get_by_id->result() as $row){
                $budget_from_dt = $row->budget_from_dt;              
                $budget_to_dt = $row->budget_to_dt;                            
                $created_id = $row->created_id;              
                $created_date = $row->created_date;              
                $modified_id = $row->modified_id;              
                $modified_date = $row->modified_date;              
            }
        } else {
            $budget_from_dt = "";              
            $budget_to_dt = "";                     
            $created_id = "";              
            $created_date = "";              
            $modified_id = "";              
            $modified_date = "";
        }
    ?>
    <div class="row" style="text-align:center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading"></header>
            <div class="panel-body">
            <form class="form-horizontal " method="post" action="<?php echo base_url(); ?>index.php/Masterc/budget_entry">
                
                <?php if($budget_id != ''){ ?>
                <div class="form-group">
                    <div class="col-sm-6" style="text-align:left">  
                        <b style="font-size:18px;">Budget ID - <i><?=$budget_id;?></i></b>
                    </div>
                </div>
                <?php } ?>

                <!--- Hidden Feilds --->
                <input type="hidden" id="budget_id" name="budget_id" value="<?php if($budget_id != ''){ echo $budget_id; } else { } ?>">

                <div class="form-group">
                    <label class="col-sm-2 control-label" style="color:black;">Start Date</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="budget_from_dt" name="budget_from_dt" 
                        value="<?php if($budget_id != ""){ echo $budget_from_dt; } ?>">
                    </div>

                    <label class="col-sm-2 control-label" style="color:black;">End Date</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="budget_to_dt" name="budget_to_dt" 
                        value="<?php if($budget_id != ""){ echo $budget_to_dt; } ?>">
                    </div>
                </div><br>

                <div class="form-group">
                    <div class="col-lg-12">
                        <table class="table table-bordered" style="text-align:left">
                            <thead>
                                <tr style="background-color:#80d4ff">
                                    <td>Master Category Name</td>
                                    <td>Annual Budget</td>
                                    <td>Monthy Budget</td>
                                    <td>Order Value (%)</td>
                                    <td>Action Annual Budget Exceeds</td>
                                    <td>Action Monthly Budget Exceeds</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if($budget_id > 0){
                                        $sql_mc = "select ProductMCId, ProductMCName, annual_budget, 
                                        monthy_budget, so_value_perc, annual_bud_exc_act, monthly_bud_exc_act from productmcc 
                                        inner join budget_det on budget_det.category_id = productmcc.ProductMCId";
                                        $qry_mc = $this->db->query($sql_mc);
                                        $sno=0;
                                        foreach($qry_mc->result() as $row){
                                            $sno++;
                                            $ProductMCId = $row->ProductMCId;
                                            $ProductMCName = $row->ProductMCName;
                                            $annual_budget = $row->annual_budget;
                                            $monthy_budget = $row->monthy_budget;
                                            $so_value_perc = $row->so_value_perc;
                                            $annual_bud_exc_act = $row->annual_bud_exc_act;
                                            $monthly_bud_exc_act = $row->monthly_bud_exc_act;
                                ?>
                                <tr>
                                    <td>
                                        <?=$ProductMCName; ?>
                                        <input type="hidden" id="category_id" name="category_id[]" value="<?=$ProductMCId; ?>">
                                    </td>
                                    <td><input type="text" id="annual_budget<?=$sno;?>" name="annual_budget[]" value="<?=$annual_budget; ?>" class="form-control" onkeypress="return isNumberKey(event);"  onkeyup="mb_cal('<?=$sno;?>')"></td>
                                    <td><input type="text" id="monthy_budget<?=$sno;?>" name="monthy_budget[]" value="<?=$monthy_budget; ?>" class="form-control" readonly></td>
                                    <td><input type="text" id="so_value_perc" name="so_value_perc[]" value="<?=$so_value_perc; ?>" class="form-control" onkeypress="return isNumberKey(event);"></td>
                                    <td>
                                        <select id="annual_bud_exc_act" name="annual_bud_exc_act[]" class="form-control" required>
                                            <option value="<?=$annual_bud_exc_act;?>"><?=$annual_bud_exc_act;?></option>
                                            <option value="">--select--</option>
                                            <option value="Stop">Stop</option>
                                            <option value="Warn">Warn</option>
                                            <option value="Ignore">Ignore</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="monthly_bud_exc_act" name="monthly_bud_exc_act[]" class="form-control" required>
                                            <option value="<?=$monthly_bud_exc_act;?>"><?=$monthly_bud_exc_act;?></option>
                                            <option value="">--select--</option>
                                            <option value="Stop">Stop</option>
                                            <option value="Warn">Warn</option>
                                            <option value="Ignore">Ignore</option>
                                        </select>
                                    </td>
                                </tr>
                                <?php
                                        }
                                ?>
                                <?php
                                    } else {    
                                        $sql_mc = "select ProductMCId, ProductMCName from productmcc";
                                        $qry_mc = $this->db->query($sql_mc);
                                        $sno=0;
                                        foreach($qry_mc->result() as $row){
                                            $sno++;
                                            $ProductMCId = $row->ProductMCId;
                                            $ProductMCName = $row->ProductMCName;
                                ?>
                                <tr>
                                    <td>
                                        <?=$ProductMCName; ?>
                                        <input type="hidden" id="category_id" name="category_id[]" value="<?=$ProductMCId; ?>">
                                    </td>
                                    <td><input type="text" id="annual_budget<?=$sno;?>" name="annual_budget[]" value="" class="form-control" onkeypress="return isNumberKey(event);" onkeyup="mb_cal('<?=$sno;?>')"></td>
                                    <td><input type="text" id="monthy_budget<?=$sno;?>" name="monthy_budget[]" value="" class="form-control" readonly></td>
                                    <td><input type="text" id="so_value_perc" name="so_value_perc[]" value="" class="form-control" onkeypress="return isNumberKey(event);"></td>
                                    <td>
                                        <select id="annual_bud_exc_act" name="annual_bud_exc_act[]" class="form-control" required>
                                            <option value="">--select--</option>
                                            <option value="Stop">Stop</option>
                                            <option value="Warn">Warn</option>
                                            <option value="Ignore">Ignore</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="monthly_bud_exc_act" name="monthly_bud_exc_act[]" class="form-control" required>
                                            <option value="">--select--</option>
                                            <option value="Stop">Stop</option>
                                            <option value="Warn">Warn</option>
                                            <option value="Ignore">Ignore</option>
                                        </select>
                                    </td>
                                </tr>
                                <?php
                                        }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-2">
                        <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
            </form>
            </div>
            <div class="col-lg-2"></div>
        </section>
        </div>
    </div>
  </section>
</section>

<script>

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}

//Datepicker
$( function() {
    $( "#budget_from_dt" ).datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
} );

$( function() {
    $( "#budget_to_dt" ).datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
} );

function mb_cal(a){
    var yb_val = document.getElementById("annual_budget"+a).value;
    var mb_val = 0;

    mb_val = yb_val/12;

    document.getElementById("monthy_budget"+a).value = mb_val;
    return true;

}
</script>