<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 style="text-align:center">Product Master Category</h3>
        </div> 
    </div>
    
    <?php
        $ProductMCId = $_REQUEST['id'];
        if($ProductMCId != ''){
            foreach($get_by_id->result() as $row){
                $productMCName = $row->productMCName;
                $productCategory = $row->productCategory;
                $productCategoryArr = explode(",",$productCategory);               
            }
        } else {
            $productMCName = "";
            $productCategory = "";
            $productCategoryArr = explode(",",$productCategory);
        }
    ?>
    <div class="row" style="text-align:center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading"></header>
            <div class="panel-body">
            <form class="form-horizontal " method="post" action="<?php echo base_url(); ?>index.php/productmcc/product_mc_entry">
                
                <?php if($ProductMCId != ''){ ?>
                <div class="form-group">
                    <div class="col-sm-6" style="text-align:left">  
                        <b style="font-size:18px;">Product Master Category ID - <i><?=$ProductMCId;?></i></b>
                    </div>
                </div>
                <?php } ?>

                <!--- Hidden Feilds --->
                <input type="hidden" id="ProductMCId" name="ProductMCId" value="<?php if($ProductMCId != ''){ echo $ProductMCId; } else { } ?>">

                <div class="form-group">
                    <label class="col-sm-3 control-label" style="color:black; text-align:left">Product Master Category Name</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="productMCName" name="productMCName" 
                        value="<?php if($ProductMCId != ""){ echo $productMCName; } ?>">
                    </div>
                </div><br>

                <div class="form-group">
                    <?php
                        $sql_pc = "select productCategoryRowId, productCategory from productcategories where deleted = 'N'";
                        $qry_pc = $this->db->query($sql_pc);
                        foreach($qry_pc->result() as $row){
                            $productCategoryRowId = $row->productCategoryRowId;
                            $productCategory = $row->productCategory;
                    ?>
                    <div class="col-sm-3" style="text-align:left">
                        <?php if (in_array($productCategoryRowId, $productCategoryArr)){ ?>
                            <input type="checkbox" id="productCategory" name = "productCategory[]" value="<?=$productCategoryRowId;?>" checked>&nbsp;
                            <?=$productCategory;?>
                        <?php } else { ?>
                            <input type="checkbox" id="productCategory" name = "productCategory[]" value="<?=$productCategoryRowId;?>">&nbsp;
                            <?=$productCategory;?>
                        <?php } ?>
                    </div>
                    <?php
                        }
                    ?>
                    <div class="col-sm-1"></div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-2">
                        <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
            </form>
            </div>
            <div class="col-lg-2"></div>
        </section>
        </div>
    </div>
  </section>
</section>