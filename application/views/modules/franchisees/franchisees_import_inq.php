<section id="main-content">
  <section class="wrapper">
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Inquires Import</h3>
        </div>
    </div>
    
    <div class="row" style="text-align:center">
        <div class="col-lg-12">
            <section class="panel">
                    <header class="panel-heading" style="text-align:center; font-size:20px">Inquires Import</header>
                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>index.php/franchiseesc/inq_import_entry">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Upload Inquiry</label>
                                <div class="col-sm-4">
                                    <input type="file" class="form-control" id="import_file" name="import_file" required accept=".xlsx">
                                </div>
                                <div class="col-sm-2">
                                    <input type="submit" id="submit" name="submit" value="Upload" class="form-control">
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </form>
                    </div>
            </section>
        </div>
    </div>
  </section>
</section>