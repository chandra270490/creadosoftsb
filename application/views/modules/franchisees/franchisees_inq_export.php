<?php
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=Inquires.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inquires</title>
</head>

<body>
    <table border="1" style='font-size:16px'>
        <tr style="background-color:#0CF;">
            <td>Date</td>
            <td>Inquiry No.</td>
            <td>Name</td>
            <td>Phone</td>
            <td>Whatsapp</td>
            <td>Email</td>
            <td>Address</td>
            <td>City</td>
            <td>State</td>
            <td>Pin</td>
            <td>Country</td>
            <td>WFM Preffered Location</td> 
            <td>Franchisee Model</td> 
            <td>Lead Source</td> 
            <td>Status</td> 
            <td>Remarks</td>
        </tr>
        <?php
            $sql_inq = "select * from franchisee_inq_mst";
            $qry_inq = $this->db->query($sql_inq);
            $sno=0;
            foreach($qry_inq->result() as $row){
                $sno++;
        ?>
        <tr>
            <td><?=$row->fran_date;?></td>
            <td><?=$row->fran_inq_no;?></td>
            <td><?=$row->fran_inq_name;?></td>
            <td><?=$row->fran_inq_phone;?></td>
            <td><?=$row->fran_inq_whatsapp;?></td>
            <td><?=$row->fran_inq_email;?></td>
            <td><?=$row->fran_inq_addr1;?></td>
            <td><?=$row->fran_inq_city;?></td>
            <td><?=$row->fran_inq_state;?></td>
            <td><?=$row->fran_inq_pin;?></td>
            <td><?=$row->fran_inq_country;?></td>
            <td><?=$row->fran_wfm_pref_loc;?></td>
            <td><?=$row->fran_model;?></td> 
            <td><?=$row->fran_lead_source;?></td> 
            <td><?=$row->fran_status;?></td> 
            <td><?=$row->fran_remarks;?></td> 
        </tr>
        <?php } ?>
    </table>
</body>
</html>