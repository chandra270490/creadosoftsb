<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='LeaveApproval_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(records.length);
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          // cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].dsrRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.style.display="none";


	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].leaveApplicationRowId
	          cell.style.display="none";

	          var cell = row.insertCell(3);
	          cell.innerHTML =  records[i].userRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].uid;
	          // cell.style.display="none";

	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].dtFrom));
	          // cell.style.display="none";

	          var cell = row.insertCell(6);
	          cell.innerHTML = dateFormat(new Date(records[i].dtTo));

	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].totalDays;

	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].reason;

	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].approved;
	          // cell.style.display="none";
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		// $("#tbl1 tr").on("click", highlightRow);
			
	}



	// function deleteRecord(rowId)
	// {
	// 	// alert(rowId);
	// 	$.ajax({
	// 			'url': base_url + '/' + controller + '/delete',
	// 			'type': 'POST',
	// 			'dataType': 'json',
	// 			'data': {'rowId': rowId},
	// 			'success': function(data){
	// 				if(data)
	// 				{
	// 					setTable(data['records']);
	// 					alertPopup('Record deleted...', 4000);
	// 					blankControls();
	// 					$("#cboTask").focus();
	// 				}
	// 			}
	// 		});
	// }
	


	function approve()
	{	
		var dt = $("#dt").val().trim();
		// if(dsrDt !="")
		// {
			dtOk = testDate("dt");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dt").focus();
				return;
			}
		// }

		leaveApplicationRowId = $("#cboUser").val();
		if(leaveApplicationRowId == "-1")
		{
			alertPopup("Select user...", 8000);
			$("#cboUser").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/approve',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'dt': dt
							, 'leaveApplicationRowId': leaveApplicationRowId
						},
				'success': function(data)
				{
					if(data)
					{
						alert("Recored Saved...");
						location.reload();
					}
				}
		});

	}

	function reject()
	{	
		var dt = $("#dt").val().trim();
		// if(dsrDt !="")
		// {
			dtOk = testDate("dt");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dt").focus();
				return;
			}
		// }

		leaveApplicationRowId = $("#cboUser").val();
		if(leaveApplicationRowId == "-1")
		{
			alertPopup("Select user...", 8000);
			$("#cboUser").focus();
			return;
		}

		var rejectReason = $("#txtRejectReason").val().trim();
		if(rejectReason == "")
		{
			alertPopup("Enter reason...", 8000);
			$("#txtRejectReason").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/reject',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'dt': dt
							, 'leaveApplicationRowId': leaveApplicationRowId
							, 'rejectReason': rejectReason
						},
				'success': function(data)
				{
					if(data)
					{
						alert("Recored Saved...");
						location.reload();
					}
				}
		});

	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Leave Approval</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
	              	?>
	              	<script>
						$( "#dt" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dt").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>User: </label>";
						echo form_dropdown('cboUser', $users, '-1', "class='form-control' id='cboUser'");
					?> 
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10' disabled='yes'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // $("#dtFrom").val(dateFormat(new Date()));
						</script>	
				
					
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10' disabled='yes'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // $("#dtTo").val(dateFormat(new Date()));
						</script>	
					
						<?php
							echo "<label style='color: black; font-weight: normal;'>Total Days:</label>";
							echo form_input('txtTotalDays', '', "class='form-control' placeholder='' id='txtTotalDays' maxlength='10' disabled='yes'");
		              	?>
					
	          	</div>
	          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Reason:</label>";
						echo form_textarea('txtReason', '', "class='form-control' placeholder='' style='resize:none;height:150px;' id='txtReason' maxlength=500 value='' disabled='yes'");
	              	?>				
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Reason if Rejected:</label>";
						echo form_input('txtRejectReason', '', "class='form-control' placeholder='' id='txtRejectReason' maxlength='200'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='approve();' value='Approve' id='btnApprove' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='reject();' value='Reject' id='btnReject' class='btn btn-danger form-control'>";
		              	?>
		          	</div>
	          	</div>
			</div>
	


		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>LARowid</th>
					 	<th style='display:none;'>UserRowId</th>
					 	<th>User</th>
					 	<th>From</th>
					 	<th>To</th>
					 	<th>Days</th>
					 	<th>Reason</th>
					 	<th>Approved</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>

	<!-- <div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div> -->
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">CREDO</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	// var globalrowid;
	// function delrowid(rowid)
	// {
	// 	globalrowid = rowid;
	// }

	// $('.editRecord').bind('click', editThis);
	// function editThis(jhanda)
	// {
	// 	rowIndex = $(this).parent().index();
	// 	colIndex = $(this).index();
	// 	globalrowid = $(this).closest('tr').children('td:eq(2)').text();

	// 	$("#dtDsr").val($(this).closest('tr').children('td:eq(3)').text());
	// 	$("#txtUser").val($(this).closest('tr').children('td:eq(5)').text());
	// 	$("#cboTask").val($(this).closest('tr').children('td:eq(6)').text());
	// 	$("#cboType").val($(this).closest('tr').children('td:eq(7)').text());
	// 	$("#cboParty").val($(this).closest('tr').children('td:eq(8)').text());
	// 	$('#cboParty').trigger('change');
	// 	$("#cboPartyType").val($(this).closest('tr').children('td:eq(10)').text());
	// 	$("#txtRemarks").val($(this).closest('tr').children('td:eq(11)').text());
	// 	$("#cboFurther").val($(this).closest('tr').children('td:eq(12)').text());
	// 	$("#cboClosing").val($(this).closest('tr').children('td:eq(13)').text());
	// 	$("#dtNext").val($(this).closest('tr').children('td:eq(15)').text());
	// 	$("#tmNext").val($(this).closest('tr').children('td:eq(16)').text());


	// 	$("#cboTask").focus();
	// 	$('#cboFurther').trigger('change');

	// 	$("#btnSave").val("Update");
	// }


	$(document).ready(function(){
        $("#cboUser").change(function(){
          var leaveApplicationRowId = $("#cboUser").val();
          if(leaveApplicationRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getDetail',
              'type': 'POST',
              'dataType': 'json',
              'data': {'leaveApplicationRowId': leaveApplicationRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                	$("#dtFrom").val( dateFormat(new Date(data['detail'][0].dtFrom)) );
                	$("#dtTo").val( dateFormat(new Date(data['detail'][0].dtTo)) );
                	$("#txtTotalDays").val( data['detail'][0].totalDays );
                	$("#txtReason").val( data['detail'][0].reason );
                	// alert(JSON.stringify(data));
                	setTable(data['lastApproved']) ///loading records in tbl1
                }
              }
          });
        });
      });


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );


  
	
	$( "#txtRemarks" ).keypress(function(event) 
   {
  		if ( event.which == 39 || event.which == 34) 
  		{
  			event.preventDefault();
  			$(this).val($(this).val() + '');
  		}
	});
</script>