<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style='border:1px solid lightgray; border-radius:25px; padding: 10px;box-shadow: 5px 5px #d3d3d3;;background-color:#fffaf0'>
		<h1 class="text-center" style='margin-top:0px'>Reset Password<br>(By Admin)</h1>
		<?php

			// $attributes[] = array("class"=>"form-control" );

			$this->load->helper('form');		// it will load 'form' so that we will be able to use form_open() etc.
			// echo validation_errors(); 
			echo form_open('Changepwdadmin_Controller/checkLogin');		// checklogin will be called on submit button.

			echo form_label('User ID', '');
			echo form_dropdown('txtUID',$users,"0","class='form-control' id='txtUID'");
			echo form_error('txtUID');

			echo form_label('New Password (CREDO)', '');
			echo form_password('txtPassword', set_value('txtPassword'),"placeholder='', class='form-control'");
			echo form_error('txtPassword');
			echo form_label('Repeat Password (CREDO)', '');
			echo form_password('txtRepeatPassword', set_value('txtRepeatPassword'),"placeholder='', class='form-control'");
			echo form_error('txtRepeatPassword');
			echo "<hr />";
			echo form_label('New Password (KINNY)', '');
			echo form_password('txtPasswordKinny', set_value('txtPasswordKinny'),"placeholder='', class='form-control'");
			echo form_error('txtPasswordKinny');
			echo form_label('Repeat Password (KINNY)', '');
			echo form_password('txtRepeatPasswordKinny', set_value('txtRepeatPasswordKinny'),"placeholder='', class='form-control'");
			echo form_error('txtRepeatPasswordKinny');


			echo "<br>";
			echo "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
			echo form_submit('btnSubmit', 'Submit',"class='btn btn-danger btn-block'");
			echo "<div>";
			echo form_close();
		?>

	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("input[type=password]").val("");
	});
</script>