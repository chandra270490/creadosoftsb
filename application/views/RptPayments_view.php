<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptPayments_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records, salaryContract, salaryMonthly)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      /////// Direct Payments
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	           cell.style.display="none";
	          cell.innerHTML = records[i].paymentRowId;

	          var cell = row.insertCell(1);
	          cell.innerHTML = dateFormat(new Date(records[i].dt));

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].amt;

	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].remarks;
	  	  }
	  	  /////// END - Direct Payments

	      /////// Salary (Contract)
	      for(i=0; i<salaryContract.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	           cell.style.display="none";
	          cell.innerHTML = salaryContract[i].paymentRowId;

	          var cell = row.insertCell(1);
	          cell.innerHTML = dateFormat(new Date(salaryContract[i].dt));

	          var cell = row.insertCell(2);
	          cell.innerHTML = salaryContract[i].payingNow;

	          var cell = row.insertCell(3);
	          cell.innerHTML = "Contract Sal";
	  	  }
	  	  /////// END - Salary (Contract)

	     //  /////// Salary (Monthly)
	     //  for(i=0; i<salaryMonthly.length; i++)
	     //  {
	     //      newRowIndex = table.rows.length;
	     //      row = table.insertRow(newRowIndex);


	     //      var cell = row.insertCell(0);
	     //       cell.style.display="none";
	     //      cell.innerHTML = salaryMonthly[i].paymentRowId;

	     //      var cell = row.insertCell(1);
	     //      cell.innerHTML = dateFormat(new Date(salaryMonthly[i].dt));

	     //      var cell = row.insertCell(2);
	     //      cell.innerHTML = salaryMonthly[i].payingNow;

	     //      var cell = row.insertCell(3);
	     //      cell.innerHTML = "Monthly Sal";
	  	  // }
	  	  // /////// END - Salary (Contract)
	  	  // $('.editRecord').bind('click', editThis);

		// myDataTable.destroy();
		// $(document).ready( function () {
	 //    myDataTable=$('#tbl1').DataTable({
		//     paging: false,
		//     iDisplayLength: -1,
		//     aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// });
		// } );

		$("#tbl1 tr").on("click", highlightRow);
			
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'empRowId': empRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
						setTable(data['records'], data['salaryContract'], data['salaryMonthly']) 
						alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text();
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}
		empName = $("#cboEmp option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'empRowId': empRowId
							, 'empName': empName
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}


</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Payments Log</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtFrom").val(dateFormat(new Date()));
						</script>					
		          	</div>
		          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Employee:</label>";
							echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
		              	?>
		          	</div>
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 
					 <tr>
						<td style='font-weight: bold; display:none;'>refRowid</td>
					 	<td style='font-weight:bold;'>Date</td>
					 	<td style='font-weight:bold;'>Amt</td>
					 	<td style='font-weight:bold;'>Remarks</td>
					 </tr>
				 
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>





<script type="text/javascript">


		// $(document).ready( function () {
		//     myDataTable = $('#tbl1').DataTable({
		// 	    paging: false,
		// 	    iDisplayLength: -1,
		// 	    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// 	});
		// } );




	// $(document).ready(function() {
	//    // $("#cboProductCategories").append('<option value="ALL">ALL</option>');
	//    var opt = "<option value='ALL'>ALL</option>";
	//    var idx=2;
	//    $(opt).insertBefore("#cboProductCategories option:nth-child(" + idx + ")");
	//   });
</script>