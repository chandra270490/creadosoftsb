<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	function validate_form()
	{
		var org=$("#cboOrg").val();
		if(org == "-1")
		{
			alert("Select Organisation...");
			return false;
		}
	}
</script>
<div style="margin-top: 100px">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style='border:1px solid gray; border-radius:10px; padding: 10px;box-shadow: 1px 1px 15px #888888;'>
		<h1 class="text-center" style='margin-top:0px'>Login</h1>
		<?php
			// $attributes[] = array("class"=>"form-control" );

			$this->load->helper('form');		// it will load 'form' so that we will be able to use form_open() etc.
			echo validation_errors(); 
			$attributes = array('onsubmit' => 'return validate_form(this)');//
			echo form_open('Login_controller/checkLogin', $attributes);		// checklogin is a name of controller, which will be called on submit button.
			if(isset($_POST['txtUID']))
			{
				echo '<div class="input-group">';
				echo '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';
				echo form_input('txtUID', $_POST['txtUID'],"placeholder='UserName', maxlength='20' class='form-control' autofocus");
				echo "</div>";
				echo "<br>";
				echo '<div class="input-group">';
				echo '<span class="input-group-addon"><i class="glyphicon glyphicon-wrench"></i></span>';
				echo form_password('txtPassword', $_POST['txtPassword'],"placeholder='Password', maxlength='20' class='form-control'");
				echo "</div>";

				echo '<div style="margin-top:15px;">';
				echo form_dropdown('cboOrg', $org, '-1', "class='form-control' id='cboOrg'");
				echo "</div>";
			}
			else
			{
				echo '<div class="input-group">';
				echo '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';
				echo form_input('txtUID', 'admin',"placeholder='UserName', maxlength='20' class='form-control' autofocus");
				echo "</div>";
				echo "<br>";

				echo '<div class="input-group">';
				echo '<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>';
				echo form_password('txtPassword', 'modicommbill',"placeholder='Password', maxlength='20' class='form-control'");
				echo "</div>";

				echo '<div style="margin-top:15px;">';
				echo form_dropdown('cboOrg', $org, '-1', "class='form-control' id='cboOrg'");
				echo "</div>";

			}
			echo "<br>";
			// echo "<div class='row'>";
			echo "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1'></div>";
			echo "<div class='col-lg-10 col-md-10 col-sm-10 col-xs-10'>";
			echo form_submit('btnSubmit', 'Login',"class='btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12'");
			// echo "</div><div class='col-lg-0 col-md-0 col-sm-0 col-xs-0'></div>";
			// echo "<div class='col-lg-5 col-md-5 col-sm-5 col-xs-5'>";
			// echo "<input type='reset' value='Clear' class='btn btn-default col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
			echo "</div><div class='col-lg-1 col-md-1 col-sm-1 col-xs-1'></div>";
			// echo "</div>";
			echo form_close();
		?>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
</div>