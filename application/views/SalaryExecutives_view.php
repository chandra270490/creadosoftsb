<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='SalaryExecutives_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].empRowId;
	          // cell.style.display="none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].empName;
	          var cell = row.insertCell(3);
	          cell.style.display="none";
	          cell.innerHTML = records[i].userRowId;
	          var cell = row.insertCell(4);
	          // var sal = records[i].salary;
	          // sal = parseFloat(sal).toFixed(2);
	          cell.innerHTML = records[i].userName;
	          var cell = row.insertCell(5);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].presents;
	          }
	          var cell = row.insertCell(6);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].holidays;
	          }
	          var cell = row.insertCell(7);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].tours;
	      	  }
	          var cell = row.insertCell(8);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].leavesTaken;
	          }
	          var cell = row.insertCell(9);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].leBf;
	          }
	          var cell = row.insertCell(10);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].leavesEarned;
	          }
	          var cell = row.insertCell(11);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].leCf;
	          }
	          var cell = row.insertCell(12);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].absents;
	          }
	          var cell = row.insertCell(13);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].countDays;
	          }
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].salPerMonth;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].hra;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].ta;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].ca;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].ma;
	          var cell = row.insertCell(19);
	          var salForManagers=0;	
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].salCalculated;
	          }
	          // else
	          // {
	          // 	salForManagers = parseFloat(records[i].salPerMonth) + parseFloat(records[i].hra) + parseFloat(records[i].ta) + parseFloat(records[i].ca) + parseFloat(records[i].ma);
	          // 	cell.innerHTML = salForManagers;
	          // }
	          cell.style.color="red";
	          var cell = row.insertCell(20);
	          cell.innerHTML = "0"
	          cell.style.display="none";
	          // cell.setAttribute("contentEditable", true);
	          // cell.className = "classPayingNow";
	          cell.style.color="blue";
	          var cell = row.insertCell(21);
	          // if(records[i].showInAttendance == "Y")
	          {
	          	cell.innerHTML = records[i].gross;
	          }
	          // else
	          // {
	          // 	cell.innerHTML = salForManagers;
	          // }
	          // cell.setAttribute("contentEditable", true);
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].pf;
	          // cell.style.display="none";
	          var cell = row.insertCell(23);
	          cell.innerHTML = records[i].esi;
	          // cell.style.display="none";
	          var cell = row.insertCell(24);
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(25);
	          cell.innerHTML = records[i].tds;
	          var cell = row.insertCell(26);
	          cell.innerHTML = records[i].net;
	          var cell = row.insertCell(27);
	          cell.innerHTML = "0";
	          var cell = row.insertCell(28);
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(29);
	          cell.innerHTML = records[i].net;
	          var cell = row.insertCell(30);
	          cell.innerHTML = records[i].showInAttendance;
	          // cell.style.display="none";
	  	  }

		$('.classPayingNow').bind('keyup', doDueCalculation);
		$("#tbl1 tr").on("click", highlightRowAlag);
	}

	function doDueCalculation()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		var netPayble = $(this).closest('tr').children('td:eq(15)').text();
		var payingNow = $(this).closest('tr').children('td:eq(16)').text();
		var dueNow = parseFloat(netPayble) - parseFloat(payingNow);
		dueNow = dueNow.toFixed(2);
		$(this).closest('tr').children('td:eq(17)').text( dueNow );
		// alert(pendingQty);
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		// alert(dtTo);

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					// alert(JSON.stringify(data));
					if(data == "Already Saved...")
					{
						alertPopup("Already saved...", 8000);
						$("#tbl1").find("tr:gt(0)").remove();
					}
					else if(data == "Attendance not marked for whole month...")
					{
						alertPopup("Attendance not marked for whole month...", 9000);
						$("#tbl1").find("tr:gt(0)").remove();
					}
					else
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				},
				'error': function(jqXHR, exception)
				{
					document.write(jqXHR.responseText);
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "empRowId" : $(tr).find('td:eq(1)').text()
		            , "empName" : $(tr).find('td:eq(2)').text()
		            , "userRowId" : $(tr).find('td:eq(3)').text()
		            , "userName" : $(tr).find('td:eq(4)').text()
		            , "presents" :$(tr).find('td:eq(5)').text()
		            , "holidays" :$(tr).find('td:eq(6)').text()
		            , "tours" :$(tr).find('td:eq(7)').text()
		            , "leavesTaken" :$(tr).find('td:eq(8)').text()
		            , "leCf" :$(tr).find('td:eq(9)').text()
		            , "leavesEarned" :$(tr).find('td:eq(10)').text()
		            , "leBf" :$(tr).find('td:eq(11)').text()
		            , "absents" :$(tr).find('td:eq(12)').text()
		            , "countDays" :$(tr).find('td:eq(13)').text()
		            , "salPerMonth" :$(tr).find('td:eq(14)').text()
		            , "hra" :$(tr).find('td:eq(15)').text()
		            , "ta" :$(tr).find('td:eq(16)').text()
		            , "ca" :$(tr).find('td:eq(17)').text()
		            , "ma" :$(tr).find('td:eq(18)').text()
		            , "salCalculated" :$(tr).find('td:eq(19)').text()
		            , "gross" :$(tr).find('td:eq(21)').text()
		            , "pf" :$(tr).find('td:eq(22)').text()
		            , "esi" :$(tr).find('td:eq(23)').text()
		            , "tds" :$(tr).find('td:eq(25)').text()
		            , "net" :$(tr).find('td:eq(26)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No rows to save...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}
		

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'dt': dt
						},
				'success': function(data)
				{
					// alert(data);
					// if(data == "Already Saved...")
					// {
					// 	alertPopup("Already saved...", 8000);
					// }
					// else
					// {
						window.location.href=data;
					// }
				}
		});
	}


	function storeTblValues1()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text();
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		var TableData;
		TableData = storeTblValues1();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("Nothing to export...", 8000);
			// $("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
</script>
<div class="acontainer">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h1 class="text-center" style='margin-top:-20px'>Calculate Salary (Executives)</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the 1st of previous month
							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
							$("#dtFrom").val(dateFormat(firstDay));
						</script>					
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the last of previous month
							var date = new Date();
							var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
							$("#dtTo").val(dateFormat(lastDay));
						</script>					
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Current is:</label>";
							echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
		              	?>
		              	<script>
							$( "#dt" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the last of previous month
							// var date = new Date();
							// var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
							$("#dt").val(dateFormat(new Date()));
						</script>	
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
							 	<td width="40" style='display:none1;font-weight: bold;'>S.N.</td>
							 	<td width="50" style='display:none1;font-weight: bold;'>Emp Code</td>
							 	<td width="150" style='font-weight: bold;'>Emp Name</td>
							 	<td width="40" style='display:none;font-weight: bold;'>userRowId</td>
							 	<td width="150" style='font-weight: bold;'>User Name</td>
							 	<td width="80" style='font-weight: bold;'>Presents</td>
							 	<td width="80" style='font-weight: bold;'>Holidays</td>
							 	<td width="80" style='font-weight: bold;'>Tour</td>
							 	<td width="80" style='font-weight: bold;'>Leaves Taken</td>
							 	<td width="80" style='font-weight: bold;'>LE B/F</td>
							 	<td width="80" style='font-weight: bold;'>Leaves Earned</td>
							 	<td width="80" style='font-weight: bold;'>LE C/F</td>
							 	<td width="80" style='font-weight: bold;'>Absent</td>
							 	
							 	<td width="80" style='font-weight: bold;'>Count Days</td>
							 	<td width="80" style='font-weight: bold;'>Sal/Month</td>
							 	<td width="80" style='font-weight: bold;'>HRA</td>
							 	<td width="80" style='font-weight: bold;'>TA</td>
							 	<td width="80" style='font-weight: bold;'>CA</td>
							 	<td width="80" style='font-weight: bold;'>Med.A.</td>
							 	<td width="85" style='font-weight: bold;'>Sal Calculated</td>
							 	<td width="80" style='font-weight: bold; display:none;'>Inc.</td>
							 	<td width="80" style='font-weight: bold;'>Gross</td>
							 	<td width="80" style='font-weight: bold;'>PF</td>
							 	<td width="80" style='font-weight: bold;'>ESI</td>
							 	<td width="80" style='font-weight: bold;display:none;'>Deduction Under IT act</td>
							 	<td width="80" style='font-weight: bold;'>TDS</td>
							 	<td width="80" style='font-weight: bold;'>Net</td>			
							 	<td width="80" style='font-weight: bold;'>Prev. Due</td>
							 	<td width="80" style='font-weight: bold;display:none;'>Adv. Paid</td>
							 	<td width="80" style='font-weight: bold;'>Net Payble</td>
							 	<td width="80" style='font-weight: bold;display:none;'>Paying Now</td>
							 	<td width="80" style='font-weight: bold;display:none;'>Due Now</td>
							 	<td width="80" style='font-weight: bold;display:none1;'>Show In Att.</td>
							 </tr>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>



	<div class="row" style="margin-top:30px;display: none1;" >
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='exportData();' value='Export Data to Excel' id='btnExportData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Save & Print' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>





<script type="text/javascript">


</script>