<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Towns_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].townRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].townRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].townName;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].districtRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].districtName;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].stateRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].stateName;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].countryRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].countryName;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].pin;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].std;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						if( data['dependent'] == "yes" )
						{
							alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						}
						else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#txtTownName").focus();
						}
					}
				}
			});
	}
	
	function saveData()
	{	
		townName = $("#txtTownName").val().trim();
		if(townName == "")
		{
			alertPopup("Town name can not be blank...", 8000);
			$("#txtTownName").focus();
			return;
		}
		districtRowId = $("#cboDistrict").val();
		if(districtRowId == "-1")
		{
			alertPopup("Select district...", 8000);
			$("#cboDistrict").focus();
			return;
		}
		pin = $("#txtPin").val().trim();
		std = $("#txtStd").val().trim();

		if($("#btnSave").val() == "Save")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'townName': townName
								, 'districtRowId': districtRowId
								, 'pin': pin
								, 'std': std
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtTownName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1

								alertPopup('Record saved...', 4000);
								blankControls();
								$("#txtTownName").focus();
								// location.reload();
							}
						}
							
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert("update");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'townName': townName
								, 'districtRowId': districtRowId								
								, 'pin': pin
								, 'std': std
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtTownName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#btnSave").val("Save");
								$("#txtTownName").focus();
							}
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						$("#txtTownName").focus();
					}
				}
			});
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Towns</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Town Name:</label>";
						echo form_input('txtTownName', '', "class='form-control' autofocus id='txtTownName' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>District Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>";
						echo "<label id='lbl' class='blank' style='color: red; font-weight: normal; text-align:right;'>.</label>";
						// echo form_dropdown('cboDistrict',$states, '-1',"class='form-control' id='cboDistrict'");
	              	?>
	              	<select id="cboDistrict" class="form-control">
		              <option value="-1" stateName="" countryName="">--- Select ---</option>
		              <?php
		                foreach ($districts as $row) 
		                {
		              ?>
		              <option value=<?php echo $row['districtRowId']; ?>  stateName="<?php echo $row['stateName']; ?>" countryName="<?php echo $row['countryName']; ?>" ><?php echo $row['districtName']; ?></option>
		              <?php
		                }
		              ?>
		            </select>

		            <script type="text/javascript">
			            $(document).ready(function()
			            {
			              $("#cboDistrict").change(function()
			              {
			                // alert($('option:selected', '#cboDistrict').attr('countryName'));
			                $("#lbl").text($('option:selected', '#cboDistrict').attr('stateName'));
			                $("#lbl").text( $("#lbl").text() + ' (' + $('option:selected', '#cboDistrict').attr('countryName') + ')');
			              });
			            });
			        </script>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>PIN Code:</label>";
						echo form_input('txtPin', '', "class='form-control' id='txtPin' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>STD Code:</label>";
						echo form_input('txtStd', '', "class='form-control' id='txtStd' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='blankControls();' value='Reset' id='btnReset' class='btn form-control' style='background-color: lightgray;'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='display:none;'>rowid</th>
					 	<th>Town Name</th>
						<th style='display:none;'>districtRowId</th>
					 	<th>District Name</th>
						<th style='display:none;'>stateRowId</th>
					 	<th>State Name</th>
						<th style='display:none;'>countryRowId</th>
					 	<th>Country Name</th>
					 	<th>PIN</th>
					 	<th>STD</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						if($errorfound=="no") ////When Saved Or Updated Successfully
						{
							// echo "<script> blankcontrol(); </script>";
							// echo "<script> document.getElementById('btnSave').value = 'Save'; </script>";
						}
						else 
						{
							// echo $errorfound;
						}


						foreach ($records as $row) 
						{
						 	$rowId = $row['townRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['townRowId']."</td>";
						 	echo "<td>".$row['townName']."</td>";
						 	echo "<td style='display:none;'>".$row['districtRowId']."</td>";
						 	echo "<td>".$row['districtName']."</td>";
						 	echo "<td style='display:none;'>".$row['stateRowId']."</td>";
						 	echo "<td>".$row['stateName']."</td>";
						 	echo "<td style='display:none;'>".$row['countryRowId']."</td>";
						 	echo "<td>".$row['countryName']."</td>";
						 	echo "<td>".$row['pin']."</td>";
						 	echo "<td>".$row['std']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		townName = $(this).closest('tr').children('td:eq(3)').text();
		districtRowId = $(this).closest('tr').children('td:eq(4)').text();
		pin = $(this).closest('tr').children('td:eq(10)').text();
		std = $(this).closest('tr').children('td:eq(11)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#txtTownName").val(townName);
		$("#cboDistrict").val(districtRowId);
		$("#txtPin").val(pin);
		$("#txtStd").val(std);
		$("#txtTownName").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );

</script>