<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptInvoice_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 // alert(records.length);
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].ciRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].ciRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].ciDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.innerHTML = '';
	          cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].discountPer;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].discountAmt;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].totalAfterDiscount;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].vatPer;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].vatAmt;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].sgstPer;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].sgstAmt;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].cgstPer;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].cgstAmt;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].igstPer;
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].igstAmt;
	          var cell = row.insertCell(21);
	          if(records[i].vType == "R")
	          {
	          	cell.innerHTML = records[i].net * -1;
	          }
	          else
	          {
	          	cell.innerHTML = records[i].net;
	          }
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].totalQty;
	          var cell = row.insertCell(23);
	          cell.innerHTML = records[i].uid;
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		$("#tbl1 tr").on('click', showDetail);
			
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		// alert(partyRowId);
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'partyRowId': partyRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					}
				}
		});
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "ciRowId" : $(tr).find('td:eq(2)').text()
		            , "vType" : $(tr).find('td:eq(3)').text()
		            , "vNo" :$(tr).find('td:eq(4)').text()
		            , "vDt" :$(tr).find('td:eq(5)').text()
		            , "partyRowId" :$(tr).find('td:eq(6)').text()
		            , "partyName" :$(tr).find('td:eq(7)').text()
		            , "totalAmt" :$(tr).find('td:eq(9)').text()
		            , "discountPer" :$(tr).find('td:eq(10)').text()
		            , "discountAmt" :$(tr).find('td:eq(11)').text()
		            , "vatPer" :$(tr).find('td:eq(13)').text()
		            , "vatAmt" :$(tr).find('td:eq(14)').text()
		            , "sgstPer" :$(tr).find('td:eq(15)').text()
		            , "sgstAmt" :$(tr).find('td:eq(16)').text()
		            , "cgstPer" :$(tr).find('td:eq(17)').text()
		            , "cgstAmt" :$(tr).find('td:eq(18)').text()
		            , "igstPer" :$(tr).find('td:eq(19)').text()
		            , "igstAmt" :$(tr).find('td:eq(20)').text()
		            , "net" :$(tr).find('td:eq(21)').text()
		            , "totalQty" :$(tr).find('td:eq(22)').text()
		            , "user" :$(tr).find('td:eq(23)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();
		var status = $("#cboStatus option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
							, 'status': status
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
	function storeTblValuesDetail()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "productCategory" : $(tr).find('td:eq(0)').text()
		            , "productName" : $(tr).find('td:eq(1)').text()
		            , "rate" :$(tr).find('td:eq(2)').text()
		            , "qty" :$(tr).find('td:eq(3)').text()
		            , "amt" :$(tr).find('td:eq(4)').text()
		            , "colour" :$(tr).find('td:eq(5)').text()
		            , "remarks" :$(tr).find('td:eq(6)').text()
		            , "despatchNo" :$(tr).find('td:eq(7)').text()
		            , "despatchDt" :$(tr).find('td:eq(8)').text()
		            , "orderNo" :$(tr).find('td:eq(9)').text()
		            , "orderDt" :$(tr).find('td:eq(10)').text()
		            , "orderType" :$(tr).find('td:eq(11)').text()
		            , "commitDt" :$(tr).find('td:eq(12)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportDetailData()
	{	
		var TableData;
		TableData = storeTblValuesDetail();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No detail to print...", 8000);
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportDataDetail',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
							, 'globalVno': globalVno
							, 'globalVdt': globalVdt
							, 'globalPartyName': globalPartyName
							, 'globalOrderType': globalOrderType
							, 'globalCommitDt': globalCommitDt
							, 'globalStatus': globalStatus
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Invoice Report</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});

						    // Set the Current Date-50 as Default
						    dt=new Date();
     					    dt.setDate(dt.getDate() - 50);
   		 					$("#dtFrom").val(dateFormat(dt));
						</script>					
		          	</div>
		          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Party:</label>";
							echo form_dropdown('cboParty',$parties, '-1',"class='form-control' id='cboParty'");
		              	?>
		          	</div>
					
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th style='display:none;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>CiRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none1;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none;'>Letter No.</th>
					 	<th>Total Amt</th>
					 	<th>Dis. Per.</th>
					 	<th>Dis. Amt.</th>
					 	<th>Amt. After Dis.</th>
					 	<th>VAT %</th>
					 	<th>VAT Amt</th>
					 	<th>SGST %</th>
					 	<th>SGST Amt</th>
					 	<th>CGST %</th>
					 	<th>CGST Amt</th>
					 	<th>IGST %</th>
					 	<th>IGST Amt</th>
					 	<th>Net</th>
					 	<th>Total Qty.</th>
					 	<th>User</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tblProducts'>
				 <thead>
					 <tr>
					 	<th>Product Category</th>
					 	<th>Product</th>
					 	<th>Rate</th>
					 	<th>Qty</th>
					 	<th>Amt</th>
					 	<th>Colour</th>
					 	<th>Remarks</th>
					 	<th>Desp.#</th>
					 	<th>Desp.Dt.</th>
					 	<th>Order.#</th>
					 	<th>Order Dt.</th>
					 	<th>Order Type</th>
					 	<th>Commit. Dt.</th>					 
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportDetailData();' value='Export Detail' id='btnLoadDetail' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>
</div>





<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );


	// $(document).ready(function()
	// {
	//     $("#tbl1 tr").on('click', showDetail);
	// });
	var globalVno, globalVdt, globalPartyName, globalOrderType,globalCommitDt, globalStatus 
	function showDetail()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		ciRowId = $(this).closest('tr').children('td:eq(2)').text();
		globalVno = $(this).closest('tr').children('td:eq(4)').text();
		globalVdt = $(this).closest('tr').children('td:eq(5)').text();
		globalPartyName = $(this).closest('tr').children('td:eq(7)').text();
		// alert(ciRowId);
		$.ajax({
			'url': base_url + '/RptInvoice_Controller/getProducts',
			'type': 'POST', 
			'data':{'rowid':ciRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(data['products'].length);
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = data['products'][i].productCategory;
		          // cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].productName + " (" + data['products'][i].productLength + " x " + data['products'][i].productWidth + " x " + data['products'][i].productHeight + " " + data['products'][i].uom + ")";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].rate;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].qty;
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].amt;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].colourName;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].remarks;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].despatchRowId;
		          var cell = row.insertCell(8);
		          cell.innerHTML = dateFormat(new Date(data['products'][i].despatchDt));
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].vType + "-" + data['products'][i].vNo;
		          var cell = row.insertCell(10);
		          cell.innerHTML = dateFormat(new Date(data['products'][i].vDt));
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['products'][i].orderType;
		          var cell = row.insertCell(12);
		          if( data['products'][i].commitmentDate == null)
		          {
		          	cell.innerHTML = "";
		          }
		          else
		          {
		            cell.innerHTML = dateFormat(new Date(data['products'][i].commitmentDate));
		          }
		        }
		        $("#tblProducts tr").on("click", highlightRow);	
			}
		});

	}
</script>