<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Stages_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].stageRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].stageRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].stageName;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].weightage;
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].remarks;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].odr;
	          cell.style.display="none";
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						if( data['dependent'] == "yes" )
						{
							alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						}
						else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#txtStageName").focus();
						}
					}
				}
			});
	}
	
	function saveData()
	{	
		stageName = $("#txtStageName").val().trim();
		if(stageName == "")
		{
			alertPopup("Stage name can not be blank...", 8000);
			$("#txtStageName").focus();
			return;
		}
		weightage = $("#txtWeightage").val().trim();
		// alert(weightage);

		remarks = $("#txtRemarks").val().trim();

		if($("#btnSave").val() == "Save")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'stageName': stageName
								, 'weightage': weightage
								, 'remarks': remarks
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtStageName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1

								alertPopup('Record saved...', 4000);
								blankControls();
								$("#txtStageName").focus();
							}
						}
							
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert("update");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'stageName': stageName
								, 'weightage': weightage
								, 'remarks': remarks
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtStageName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#btnSave").val("Save");
								$("#txtStageName").focus();
							}
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						$("#txtstageName").focus();
					}
				}
			});
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Stages</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Stage Name:</label>";
						echo form_input('txtStageName', '', "class='form-control' autofocus id='txtStageName' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Weightage:</label>";
						echo "<input type='number' id='txtWeightage' class='form-control' >"
						// echo form_dropdown('cboDistrict',$states, '-1',"class='form-control' id='cboDistrict'");
	              	?>

	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo form_input('txtRemarks', '', "class='form-control' id='txtRemarks' style='' maxlength=100 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>

		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3;width:100%">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th style='display:none;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>stageRowId</th>
					 	<th>Stage Name</th>
					 	<th>Weightage</th>
					 	<th>Remarks</th>
					 	<th style='display:none;'>Order</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 

						foreach ($records as $row) 
						{
						 	$rowId = $row['stageRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="display:none; color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['stageRowId']."</td>";
						 	echo "<td>".$row['stageName']."</td>";
						 	echo "<td>".$row['weightage']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td style='display:none;'>".$row['odr']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		stageName = $(this).closest('tr').children('td:eq(3)').text();
		weightage = $(this).closest('tr').children('td:eq(4)').text();
		remarks = $(this).closest('tr').children('td:eq(5)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#txtStageName").val(stageName);
		$("#txtWeightage").val(weightage);
		$("#txtRemarks").val(remarks);
		$("#txtStageName").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );

</script>