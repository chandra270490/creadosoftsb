<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptColourDespatch_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;

	          var cell = row.insertCell(1);
	          cell.innerHTML = dateFormat(new Date(records[i].despatchDt));

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productCategory;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].colourName;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].despatchQty;
	          
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		// $("#tbl1 tr").on('click', showDetail);
			
	}

	function loadData()
	{	
		var colorRowId="";
		$('input:checked.chkColours').each(function() 
		{
			colorRowId = $(this).val() + "," + colorRowId;
		});
		if(colorRowId == "")
		{
			alertPopup("Select colour...", 8000);
			return;
		}
		colorRowId = colorRowId.substr(0, colorRowId.length-1);

		var productRowId="";
		$('input:checked.chkProducts').each(function() 
		{
			productRowId = $(this).val() + "," + productRowId;
		});
		if(productRowId == "")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		productRowId = productRowId.substr(0, productRowId.length-1);
		// alert(colorRowId);
		// return;
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'partyRowId': partyRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'colorRowId': colorRowId
							, 'productRowId': productRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// console.log(data['records']);
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					}
				},
		          'error': function(jqXHR, exception)
		          {
		            $("#paraAjaxErrorMsg").html( jqXHR.responseText );
		            $("#modalAjaxErrorMsg").modal('toggle');
		          }
		});
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "orderNo" : $(tr).find('td:eq(0)').text()
		            , "orderDate" : $(tr).find('td:eq(1)').text()
		            , "orderType" :$(tr).find('td:eq(2)').text()
		            , "partyName" :$(tr).find('td:eq(3)').text()
		            , "category" :$(tr).find('td:eq(4)').text()
		            , "product" :$(tr).find('td:eq(5)').text()
		            , "colour" :$(tr).find('td:eq(6)').text()
		            , "qty" :$(tr).find('td:eq(7)').text()
		            , "rate" :$(tr).find('td:eq(8)').text()
		            , "amt" :$(tr).find('td:eq(9)').text()
		            , "pendingQty" :$(tr).find('td:eq(10)').text()
		            , "commitmentDate" :$(tr).find('td:eq(11)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var colorNames="";
		if( $("#chkAllColours").prop("checked") == true )
		{
			colorNames = "ALL";
		}
		else
		{
			$('input:checked.chkColours').each(function() 
			{
				colorNames = $(this).attr("txt") + "," + colorNames;
			});
			colorNames = colorNames.substr(0, colorNames.length-1);
		}
		

		var productNames="";
		if( $("#chkAllProducts").prop("checked") == true )
		{
			productNames = "ALL";
		}
		else
		{
			$('input:checked.chkProducts').each(function() 
			{
				productNames = $(this).attr("txt") + "," + productNames;
			});
			productNames = productNames.substr(0, productNames.length-1);
		}
		
		// alert(productNames);

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
							, 'colorNames': colorNames
							, 'productNames': productNames
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
	
</script>
<style type="text/css">
	#style-1::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		border-radius: 1px;
		/*margin-left: 50px;*/
		background-color: #F5F5F5;
	}

	#style-1::-webkit-scrollbar
	{
		width: 12px;
		/*margin: -20px;*/
		background-color: #F5F5F5;
	}

	#style-1::-webkit-scrollbar-thumb
	{
		border-radius: 1px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
		background-color: lightgrey;
	}
</style>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Colour-Wise Report (Despatch)</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllColours">
							<label for="chkAllColours" style="font-weight:bold;color:black;">All Colours
						</span>
						
						<?php
							foreach ($colours as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkColours" txt='<?php echo $value;?>' id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllProducts">
							<label for="chkAllProducts" style="font-weight:bold;color:black;">All Products
						</span>
						
						<?php
							foreach ($products as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkProducts" txt='<?php echo $value;?>' id='<?php echo "P".$key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo  "P".$key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div align="left" class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style='padding-left:30px;'>
						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>From:</label>";
									echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
				              	?>
				              	<script>
									$( "#dtFrom" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});

								    // Set the Current Date-50 as Default
								    dt=new Date();
		     					    dt.setDate(dt.getDate() - 50);
		   		 					$("#dtFrom").val(dateFormat(dt));
								</script>	
							</div>
						</div>
						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>To:</label>";
									echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
				              	?>
				              	<script>
									$( "#dtTo" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});
								    // Set the Current Date as Default
									$("#dtTo").val(dateFormat(new Date()));
								</script>
							</div>
						</div>
						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Party:</label>";
									echo form_dropdown('cboParty',$parties, '-1',"class='form-control' id='cboParty'");
				              	?>
							</div>
						</div>
						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
									echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
				              	?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none1;'>Odr.No.</th>
					 	<th>Odr. Dt</th>
					 	<th>Party</th>
					 	<th>Category</th>
					 	<th>Product</th>
					 	<th>Colour</th>
					 	<th>Desp.Qty.</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>

</div>





<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );


	// add multiple select / deselect functionality
	$("#chkAllColours").click(function () {
		// alert();
		  $('.chkColours').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkColours').bind('click', chkSelectAll);
	function chkSelectAll()
	{
		var x = parseInt($(".chkColours").length);
		var y = parseInt($(".chkColours:checked").length);
		if( x == y ) 
		{
			$("#chkAllColours").prop("checked", true);
		} 
		else 
		{
			$("#chkAllColours").prop("checked", false);
		}
	}

	// add multiple select / deselect functionality
	$("#chkAllProducts").click(function () {
		// alert();
		  $('.chkProducts').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkProducts').bind('click', chkSelectAllProducts);
	function chkSelectAllProducts()
	{
		var x = parseInt($(".chkProducts").length);
		var y = parseInt($(".chkProducts:checked").length);
		if( x == y ) 
		{
			$("#chkAllProducts").prop("checked", true);
		} 
		else 
		{
			$("#chkAllProducts").prop("checked", false);
		}
	}	
	
</script>