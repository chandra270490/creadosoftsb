<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='ProductStatusAtDesp_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		// alert(totalStages);
		var totalWaleColKaNo = -1;
		var burl = '<?php echo base_url();?>';
          $("#tbl1").find("tr:gt(0)").remove();
         //  if(records != null) 
         //  {
          	var table = document.getElementById("tbl1");
	        

         //  	var tot = 0;
            for(var i=0; i<records.length; i++)
            {
            	newRowIndex = table.rows.length;
	        	row = table.insertRow(newRowIndex);
            	var cell = row.insertCell(0);
	        	cell.innerHTML = i+1;
	        	// cell.style.display="none";

		        var cell = row.insertCell(1);
		        cell.innerHTML = records[i].productRowId;
		        cell.style.display="none";

		        var cell = row.insertCell(2);
		        cell.innerHTML = records[i].productName;// + " - " +  records[i].colourName; 
		        // cell.style.display="none";
		        
		        var tot = 0;
		        var cell = row.insertCell(3);
		        cell.innerHTML = records[i].despStageQty; 
		        tot += parseInt(eval(records[i].despStageQty));

		        
		        // alert(totalGodowns);
		        for(var j=1; j<=totalGodowns; j++)
		        {
		        	var cell = row.insertCell(j+3);
		        	
		        	var key = Object.keys(records[i])[j+6];
		        	cell.innerHTML = (records[i][key]);
		        	// alert(godown);
		        	// if(j>1)  ///excluding no stage
			        {
			        	tot += parseInt(eval(records[i][key]));
			        }
		        }
		        // alert(JSON.stringify(records));
		        // alert(j);
		        var cell = row.insertCell(j+3);
		        cell.innerHTML = tot; 
		        cell.style.color="red";
		        totalWaleColKaNo = j+3;
		        
		        var cell = row.insertCell(j+4);
		        cell.innerHTML = records[i].pendingQty; 
		        cell.style.color="blue";


		        var cell = row.insertCell(j+5);
		         cell.innerHTML = tot - records[i].pendingQty; 
		        cell.style.color="green";
	        
            }

		//////Delete rows With 0 total 
		var i=0;
		// alert(  totalWaleColKaNo );
		$('#tbl1 tr').each(function(row, tr)
    	{
    		if( i > 0 )  ///ignore headings
    		{
    			if( $(tr).find('td:eq(' + totalWaleColKaNo + ')').text() <= 0) 
    			{
    				$(this).remove();
    			}
    		}
    		i++; 
    	});
    	if($('#tbl1').find('tr:eq(1)').length <= 0)
    	{
    		alert("Zero total qty in selected products...");
    	}
    	////// END - Delete rows With 0 total 
	}

	function loadData()
	{	
		
		var productRowId="";
		$('input:checked.chkProducts').each(function() 
		{
			// alert($(this).val());
			productRowId = $(this).val() + "," + productRowId;
		});
		if(productRowId == "")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		productRowId = productRowId.substr(0, productRowId.length-1);
		// alert(productRowId);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/getData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productRowId': productRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']);
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	// function storeTblValues()
	// {
	//     var TableData = new Array();
	//     var i=0;
	//     $('#tbl1 tr').each(function(row, tr)
	//     {
	//     	if( i == 0 )  ///store headings
	//     	{
	//         	TableData[i]=
	//         	{
	// 	            "sn" : $(tr).find('th:eq(0)').text()
	// 	            , "productName" : $(tr).find('th:eq(2)').text()
	// 	            , "qtyAtDesp" : $(tr).find('th:eq(3)').text()
	// 	            , "orders" : $(tr).find('th:eq(4)').text()
	// 	            , "diff" : $(tr).find('th:eq(5)').text()
	//         	}   
	//         	i++; 
	//         }
	//      //    else if( i == 1 )  ///dont store stageRowIDs
	//     	// {
	//     	// 	i++;
	//     	// }
	//         else /// Storing table detail
	//     	{
	    		
	// 	        	TableData[i]=
	// 	        	{
	// 		            "sn" : $(tr).find('td:eq(0)').text()
	// 		            , "productName" : $(tr).find('td:eq(2)').text()
	// 		            , "qtyAtDesp" : $(tr).find('td:eq(3)').text()
	// 		            , "orders" : $(tr).find('td:eq(4)').text()
	// 		            , "diff" : $(tr).find('td:eq(5)').text()
	// 	        	}   
	// 	        	i++;
	        	
	//         }
	//     }); 
	//     // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	//     tblRowsCount = i-1;
	//     return TableData;
	// }

	// var tblRowsCount;
	// function storeTblValues()
	// {
	// 	var data = Array();
    
	// 	$("#tbl1 tr").each(function(i, v){
	// 	    data[i] = Array();
	// 	    $(this).children('td').each(function(ii, vv){
	// 	        data[i][ii] = $(this).text();
	// 	    }); 
	// 	})

	//     return data;
	// }

	function storeTblHeaders()
	{
		var header = Array();
		// alert('ff');
		$("#tbl1 tr th").each(function(i, v){
		        header[i] = $(this).text();
		})

		return header;
	}

	var tblRowsCount=0;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text().replace(/(\r\n|\n|\r)/gm,", ");
		    }); 
		})
	    return data;
	}

	function exportDataExcel()
	{	
		
		var TableDataHeader;
		TableDataHeader = storeTblHeaders();
		TableDataHeader = JSON.stringify(TableDataHeader);
		// alert(JSON.stringify(TableDataHeader));
		// return;

		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		// var TableData;
		// TableData = storeTblValues();
		// TableData = JSON.stringify(TableData);
		// alert(TableData);
		// return;
		$.ajax({
				'url': base_url + '/' + controller + '/exportDataExcel',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'TableDataHeader': TableDataHeader
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
							window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px'>Product Status (Desp. Stage)</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1',"class='form-control' id='cboProductCategories'");
		              	?>
		          	</div>					
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllProducts" />
							<label for="chkAllProducts" style="font-weight:bold;color:black;">All Products</label>
						</span>

					</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Load Detail' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:350px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
					 <tr>
						<th style='display:none1;'>S.N.</th>
						<th style='display:none;'>ProductRowId</th>
						<th style='display:none1;'>Product</th>
						<th style='display:none1;'>Qty at Desp</th>
						<?php
							$totalGodowns = 0;
							foreach ($godowns4table as $row) 
							{
								echo "<th>".$row['name']."</th>";
								$totalGodowns++;
							}
						?>
						<th style='display:none1;'>Total</th>
						<th style='display:none1;'>Orders</th>
						<th style='display:none1;'>Diff</th>
						
					 </tr>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 20px;" >
		
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10%">
			<?php
				echo "<input type='button' onclick='exportDataExcel();' value='Export Data - Excel' id='btnSaveChanges' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
		</div>

	</div>
	
</div>





<script type="text/javascript">
	var totalGodowns ='<?php echo $totalGodowns; ?>';

		// $(document).ready( function () {
		//     myDataTable = $('#tbl1').DataTable({
		// 	    paging: false,
		// 	    iDisplayLength: -1,
		// 	    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// 	});
		// } );

	// add multiple select / deselect functionality
	$("#chkAllProducts").click(function () {
		// alert();
		  $('.chkProducts').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkProducts').bind('click', chkSelectAllProducts);
	function chkSelectAllProducts()
	{
		var x = parseInt($(".chkProducts").length);
		var y = parseInt($(".chkProducts:checked").length);
		if( x == y ) 
		{
			$("#chkAllProducts").prop("checked", true);
		} 
		else 
		{
			$("#chkAllProducts").prop("checked", false);
		}
	}	



	$(document).ready(function(){
        $("#cboProductCategories").change(function(){
          var productCategoryRowId = $("#cboProductCategories").val();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                	$("#style-1 input:gt(0)").remove();
                	$("#style-1 label:gt(0)").remove();
                	$("#style-1 br").remove();
                	for(var i=0; i<data['products'].length; i++)
                    {
                    	$("#style-1").append("<br />");
                    	$("#style-1").append("<input type='checkbox' class='chkProducts' txt='"+ data['products'][i].productName +"' id="+data['products'][i].productRowId+" value="+data['products'][i].productRowId+">" );
                    	$("#style-1").append("<label  style='color: black;margin-left:10px;' for="+data['products'][i].productRowId+">"+ data['products'][i].productName +"</label>" );
                  	}

                }
              }
          });
        });
      });
</script>