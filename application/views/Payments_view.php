<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Payments_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].paymentRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].paymentRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].dt));
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].empRowId;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].name;
	          // 
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].amt;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].remarks;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}
	function deleteRecord(paymentRowId)
	{
		// alert(paymentRowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'paymentRowId': paymentRowId},
				'success': function(data){
					if(data)
					{
						setTable(data['records'])
						alertPopup('Record deleted...', 4000);
						blankControls();
						$("#cboEmp").focus();
					}
				}
			});
	}
	
	function saveData()
	{	
		var dt = $("#dt").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dt");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dt").focus();
				return;
			}
		// }

		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}

		amt = parseFloat($("#txtAmt").val().trim());
		if(amt == 0)
		{
			alertPopup("Enter valid amt...", 8000);
			$("#txtAmt").focus();
			return;
		}

		remarks = $("#txtRemarks").val().trim();
		if(remarks == "")
		{
			alertPopup("Enter Remarks...", 8000);
			$("#txtRemarks").focus();
			return;
		}

		if($("#btnSave").val() == "Save")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'dt': dt
								, 'empRowId': empRowId
								, 'amt': amt
								, 'remarks': remarks
							},
					'success': function(data)
					{
						if(data)
						{
							setTable(data['records']) ///loading records in tbl1

							alertPopup('Record saved...', 4000);
							blankControls();
							$("#dt").val(dateFormat(new Date()));
							$("#cboEmp").focus();
							// location.reload();
						}
							
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert(globalrowid);
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'dt': dt
								, 'empRowId': empRowId
								, 'amt': amt
								, 'remarks': remarks
							},
					'success': function(data)
					{
						if(data)
						{
							setTable(data['records']) ///loading records in tbl1
							alertPopup('Record updated...', 4000);
							blankControls();
							$("#dt").val(dateFormat(new Date()));
							$("#btnSave").val("Save");
							$("#cboEmp").focus();
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		// alert(paymentRowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						$("#txtTownName").focus();
					}
				}
			});
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Payment</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
	              	?>
	              	<script>
						$( "#dt" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dt").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
							echo "<label style='color: black; font-weight: normal;'>Employee Name:</label>";
							echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
		              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Amount:</label>";
						echo '<input type="number" step="0.01" name="txtAmt" value="" class="form-control" maxlength="10" id="txtAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo form_input('txtRemarks', '', "class='form-control' id='txtRemarks' style='' maxlength=250 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='display:none;'>paymentRowId</th>
					 	<th>Date</th>
					 	<th style='display:none1;'>Emp Code</th>
						<th style='display:none1;'>Employee</th>
					 	<th>Amt</th>
						<th style='display:none1;'>Remarks</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						if($errorfound=="no") ////When Saved Or Updated Successfully
						{
							// echo "<script> blankcontrol(); </script>";
							// echo "<script> document.getElementById('btnSave').value = 'Save'; </script>";
						}
						else 
						{
							// echo $errorfound;
						}


						foreach ($records as $row) 
						{
						 	$paymentRowId = $row['paymentRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$paymentRowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['paymentRowId']."</td>";
						 	$vdt = strtotime($row['dt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none1;'>".$row['empRowId']."</td>";
						 	echo "<td style='display:none1;'>".$row['name']."</td>";
						 	echo "<td>".$row['amt']."</td>";
						 	echo "<td style='display:none1;'>".$row['remarks']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(paymentRowId)
	{
		globalrowid = paymentRowId;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		dt = $(this).closest('tr').children('td:eq(3)').text();
		empRowId = $(this).closest('tr').children('td:eq(4)').text();
		amt = $(this).closest('tr').children('td:eq(6)').text();
		remarks = $(this).closest('tr').children('td:eq(7)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#dt").val(dt);
		$("#cboEmp").val(empRowId);
		$("#txtAmt").val(amt);
		$("#txtRemarks").val(remarks);
		$("#cboEmp").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );

</script>