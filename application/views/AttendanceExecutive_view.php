<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='AttendanceExecutive_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 
			 var flag = 0;
			  for(i=0; i<records.length; i++)
		      {
		      	if( records[i].attendance != null )
	      		{
	      			flag = 1;
	      		}
		      }
		  
	      if( flag == 0 ) //// new marking
	      {
		      var table = document.getElementById("tbl1");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].userRowId;	///user row id
		          cell.style.backgroundColor="#F0F0F0";
		          cell.style.display="none1";
		      		// alert("new");

		          var cell = row.insertCell(2);
		          // cell.style.display="none";
		          cell.innerHTML = records[i].name;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(3);
		          cell.innerHTML = "P";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(4);
		          cell.innerHTML = "9:00 AM";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(5);
		          cell.innerHTML = "5:30 PM";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";
		          

		          var cell = row.insertCell(6);
		          cell.innerHTML = "00:00";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";
		          
		          // cell.style.display="none";

		          var cell = row.insertCell(7);
		          // cell.innerHTML = "---Select---";
		          cell.innerHTML = $("#cboDefaultAttendance option:selected").text();
		          cell.setAttribute("contentEditable", true);

		          var cell = row.insertCell(8);
		          cell.innerHTML = "";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";
		  	  }
	  	  }
	  	  else ///// if already marked attendance
	  	  {
	  	  	
		      var table = document.getElementById("tbl1");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].userRowId;
		          cell.style.backgroundColor="#F0F0F0";
		          cell.style.display="none1";
		          // alert('already');
		      

		          var cell = row.insertCell(2);
		          // cell.style.display="none";
		          cell.innerHTML = records[i].name;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(3);
		          cell.innerHTML = "P";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(4);
		          // cell.innerHTML = records[i].tmIn;
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(5);
		          // cell.innerHTML = records[i].tmOut;
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";
		          

		          var cell = row.insertCell(6);
		          // cell.innerHTML = records[i].extraHours;
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(7);
		          if( records[i].attendance != null )
	      		  {
		          	cell.innerHTML = records[i].attendance;
		          }
		          else
		          {
		          	cell.innerHTML = $("#cboDefaultAttendance option:selected").text();
		          	// cell.innerHTML = "---Select---";
		          }
		          cell.setAttribute("contentEditable", true);

		          var cell = row.insertCell(8);
		          // cell.innerHTML = records[i].remarks;
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";
		  	  }
	  	  }
	  	  $('td').on("click focus", setDropDown);
	}

	function loadData()
	{	
		// alert();
		// return;
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}
		defaultAttendance = $("#cboDefaultAttendance").val();
		if(defaultAttendance == "-1")
		{
			$("#cboDefaultAttendance").focus();
			alertPopup("Select Default Attendance...", 6000);
			return;
		}
		// alert(dt);
		// return;
		$.ajax({
			'url': base_url + '/' + controller + '/showData',
			'type': 'POST',
			'dataType': 'json',
			'data': {
						'dt': dt
					},
			'success': function(data)
			{
				if(data)
				{
					// console.log(JSON.stringify(data));
					////check - sare emp ko alag user Id hai k nahi
					var ekSeZyada=0;
					for(i=0; i<data['records'].length; i++)
		      		{
						vFound=0;
		      			var one = data['records'][i].userRowId;
		      			for(j=0; j<data['records'].length; j++)
		      			{
		      				if(one == data['records'][j].userRowId)
		      				{
		      					vFound++;
		      				}
		      			}
		      			if(vFound > 1)
		      			{
		      				ekSeZyada=1;
		      				break;
		      			}
		      			// alert(vFound);
		      		}
		      		////check END - sare emp ko alag user Id hai k nahi

		      		if(ekSeZyada == 1)
		      		{
						alertPopup('Multiple employees with single ID found...', 5000);
		      		}
		      		else
		      		{
						$("#tbl1").find("tr:gt(0)").remove();
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					}
				}
			},
			'error': function(jqXHR, exception)
			{
				document.write(jqXHR.responseText);
			}
		});
		
	}


	var tblRowsCount;
	var dutySelected;
	var falseExtra = 0;
	var falseExtraRow = 0;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    dutySelected = 1;
	    var timeFormat = /^([0-9]{2})\:([0-9]{2})$/;
	    
	    $('#tbl1 tr').each(function(row, tr)
	    {
        	TableData[i]=
        	{
	            "userRowId" : $(tr).find('td:eq(1)').text()
	            , "attendance" :$(tr).find('td:eq(7)').text()
        	}   
        	i++; 
        	if( $(tr).find('td:eq(7)').text() =="---Select---" )
        	{
        		dutySelected = 0;
        		return false;
        	}
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		
		if( dutySelected == 0 )
		{
			alertPopup("Select all duties...", 8000);
			// $("#cboProducts").focus();
			return;
		}
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("Nothing to save...", 8000);
			// $("#cboProducts").focus();
			return;
		}
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dt': dt
						},
				'success': function(data)
				{
					alertPopup('Changes saved... Wait... Page reloading', 4000);
					location.reload();
				},
				'error': function(jqXHR, exception)
				{
					document.write(jqXHR.responseText);
				}
		});
		
	}

</script>
<div class="container">
	
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
				<h3 class="text-center" style='margin-top:-20px'>Mark Attendance (Executives)</h3>
				<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Date:</label>";
								echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
			              	?>
			              	<script>
								$( "#dt" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the Current Date as Default
								$("#dt").val(dateFormat(new Date()));
							</script>					
			          	</div>
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				          	<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
			              	?>
				          	<select class='form-control' id='cboDefaultAttendance'>
							  	<option value="-1">---Select---</option>
							  	<option value="PR">PR</option>
							  	<option value="AB">AB</option>
							  	<option value="LE">LE</option>
							  	<option value="HO">HO</option>
							</select>
						</div>
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
								echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
			              	?>
			          	</div>
			          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<br />";
								echo "<label style='color: blue; font-weight: normal;'  id='lblDay'>-</label>";
			              	?>
			          	</div>
				</form>
			</div>

			<div class="row" style="margin-top: 20px;">
				<style>
			      table, th, td{border:1px solid gray; padding: 7px;}
			    </style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;border:1px solid lightgray;padding: 0;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
								<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="50" style='display:none1;'>userRowId</th>
							 	<th width="150" style='display:none1;'>Name</th>
							 	<th width="50" style='display:none;'>P / A</th>
							 	<th width="100" style='display:none;'>In</th>
							 	<th width="100" style='display:none;'>Out</th>
							 	<th width="100" style='display:none;'>Extra Hrs.(<span style="color:red;">hh:mm</span>)</th>
							 	<th width="100">Duty</th>
							 	<th width="100" style='display:none;'>Remarks</th>
							 </tr>
						 <tbody>

						 </tbody>
						</table>
					</div>
			</div>

			<div class="row" style="margin-top:20px;margin-bottom:20px;" >
				<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
				</div>

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnLoadAll' class='btn btn-primary form-control'>";
			      	?>
				</div>
			</div>

		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>

<div id="divTmIn" style="display: none;">
  <select class='form-control' id='cboTmIn'> </select>
</div>

<div id="divTmOut" style="display: none;">
  <select class='form-control' id='cboTmOut'> </select>
</div>

<div id="divDuty" style="display: none;">
  <select class='form-control' id='cboDuty'>
  	<option value="---Select---">---Select---</option>
  	<option value="PR">PR</option>
  	<option value="AB">AB</option>
  	<option value="LE">LE</option>
  	<option value="HO">HO</option>
  	<option value="NA">NA</option>
  </select>
</div>

<script type="text/javascript">
	row4Bold = -1;
	var col1 = -1;
	var row1 = -1;
	var colO1 = -1;
	var rowO1 = -1;
	var colDuty1 = -1;
	var rowDuty1 = -1;
	$('td').on("click", setDropDown);
	function setDropDown()
	{
		var col = $(this).parent().children().index($(this));
		var row = $(this).parent().parent().children().index($(this).parent());
	    var xPos = Math.floor($(this).offset().left + 2);
	    var yPos = Math.floor($(this).offset().top + 1);
	    var cellWidth=$(this).innerWidth();
	    var cellHeight=$(this).innerHeight() - 20;
	    var existingCellValue=$('#tbl1').find('tr:eq('+ (row) +')').find('td:eq(' + (col) + ')').text();
	    // alert(existingCellValue);
	    ///Doing Formatting of Name normal
	    var nm = $('#tbl1').find('tr:eq('+ (row4Bold) +')').find('td:eq(' + (2) + ')').text();
	    $('#tbl1').find('tr:eq('+ (row4Bold) +')').find('td:eq(' + (2) + ')').html(nm);

	    ///Doing Formatting of Name Bold
	    var nm = $('#tbl1').find('tr:eq('+ (row) +')').find('td:eq(' + (2) + ')').text();
	    $('#tbl1').find('tr:eq('+ (row) +')').find('td:eq(' + (2) + ')').html('<font color=red>' + nm + '</font>');
	    row4Bold = row;



		////for Duty
		if( col == 7 )
		{
			if(row != rowDuty1)
			{
				hideCboDuty();
			}
		    colDuty1=col;
		    rowDuty1=row;
		    $('#divDuty').css({'top':yPos,'left':xPos, 'position':'absolute', 'border':'0px solid black', 'padding':'0px'});
        	$('#divDuty').css({'width': cellWidth-2, 'height': cellHeight, 'margin-left': '0px' });
        	$('#divDuty').show();
        	$('#cboDuty').val(existingCellValue);	
        	$('#cboDuty').focus();
		}
		else if(col != 7 && row != rowDuty1)
		{
			hideCboDuty();
		}
		else if(col != col1)
		{
			hideCboDuty();
		}


	}


	function hideCboDuty()
	{
		if( rowDuty1 != -1)
		{
			$('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (colDuty1) + ')').html($("#cboDuty option:selected").text());
		}
		$('#divDuty').hide();	
		rowDuty1 = -1;
		colDuty1 = -1;
	}


	$('#cboDuty').keydown( function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 9) 
        {
          e.preventDefault();
          if(e.shiftKey) 
          {
            {
	            if(rowDuty1==1)   //in case of 1st row
	            {
	            	$('#tbl1').find('tr:eq('+ (1) +')').find('td:eq(' + (7) + ')').focus();
	            }
	            else
	            {
	            	$('#tbl1').find('tr:eq('+ (rowDuty1-1) +')').find('td:eq(' + (7) + ')').focus();
	            }
            }
          }
          else
          {
          		var tc = $('#tbl1 tr').length;
          		if(rowDuty1 == tc )   //in case of 1st row
	            {
	            	$('#tbl1').find('tr:eq('+ (tc) +')').find('td:eq(' + (7) + ')').focus();
	            }
	            else
	            {
	            	$('#tbl1').find('tr:eq('+ (rowDuty1+1) +')').find('td:eq(' + (7) + ')').focus();
	            }
                // $('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (8) + ')').focus();
          }
            // hideCboIn();
        }
    });

    $('#cboDuty').change( function(e) 
    {    
    	$('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (colDuty1) + ')').html($("#cboDuty option:selected").text());
    });

    $('#cboDuty').blur( function(e) 
    {    
    	$('#divDuty').hide();
    });

	$(document).ready(function() {
		setDefaultAttendance();
	});

	$('#dt').change( function(e) 
	{
		$("#tbl1").find("tr:gt(0)").remove();
		setDefaultAttendance();
	});

	function setDefaultAttendance()
	{
		var dayName = "";
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			$("#dt").focus();
			alertPopup("Invalid date...", 4000);
			return;
		}
		$.ajax({
				'url': base_url + '/' + controller + '/getIsHoliday',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dt': dt
						},
				'success': function(data)
				{
					// alert(data['records'][0].holidayName);
					if(data['records'].length>0) ///agar holiday marked h
					{
						dayName = data['records'][0].holidayName;
						$("#lblDay").text(dayName);
						$("#cboDefaultAttendance").val("HO");
					}
					else
					{
						// $("#lblDay").text("Normal Day");
					}
					// location.reload();
				}
		});
		
		dt = $("#dt").val();
		//alert((new Date(dt)).getDay());
		var d = ["Sun","Mon","Tues","Wednes","Thurs","Fri","Satur"][(new Date(dt)).getDay()]+"day";
		if( d == "Sunday" )
		{
			$("#cboDefaultAttendance").val("HO");
			$("#lblDay").text(d);
		}
		else
		{
			$("#cboDefaultAttendance").val("-1");
			$("#lblDay").text(d);
		}
	}

</script>