<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='OpeningBalLeaves_Controller';
	var base_url='<?php echo site_url();?>';

	
	function setHeadings()
	{
		$("#tbl1").empty();
		  // $("#tbl1").find("tr:gt(0)").remove();

		  ////////////// Set table headings
	      var table = document.getElementById("tbl1");
	      newRowIndex = table.rows.length;
          row = table.insertRow(newRowIndex);
          var cell = row.insertCell(0);
          cell.innerHTML = "S.N.";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";
          cell.style.width = "60px";

          var cell = row.insertCell(1);
          cell.innerHTML = "Emp Code";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";
          cell.style.width = "50px";


          var cell = row.insertCell(2);
          cell.innerHTML = "Emp Name";
          cell.style.width = "200px";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";

          var cell = row.insertCell(3);
          cell.innerHTML = "userRowId";
          // cell.style.width = "60px";
          cell.style.display="none";
          var cell = row.insertCell(4);
          cell.innerHTML = "User Name";
          cell.style.width = "200px";
          cell.style.backgroundColor="#F0F0F0";
          cell.style.fontWeight="bold";


		   var cell = row.insertCell(5);
	       cell.innerHTML = "Op. Bal";
	       cell.style.width = "100px";
	       cell.style.backgroundColor="#F0F0F0";
	       cell.style.fontWeight="bold";

		  ////////////// END - Set table headings
	}

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  
		 setHeadings();

	      var table = document.getElementById("tbl1");
	      // alert(noOfDays);
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].empRowId;
	          cell.style.backgroundColor="#F0F0F0";
	          // cell.style.display="none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].name;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].rowid;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].uid;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].leavesOpBal;
	          cell.setAttribute("contentEditable", true);
	        
	  	  }

		// $("#tbl1 tr").on("click", highlightRowAlag);
	}

	function loadData()
	{	
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': 'dtFrom'
							, 'dtTo': 'dtTo'
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

	
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
        	TableData[i]=
        	{
	            "empRowId" : $(tr).find('td:eq(1)').text()
	            , "opBal" :$(tr).find('td:eq(5)').text()
        	}   
        	i++; 
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);

		// alert(JSON.stringify(TableData));
		// return;


		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					alert('Changes saved...');
					location.reload();
				}
		});
		
	}
	

</script>
<div class="acontainer" >

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h3 class="text-center" style='margin-top:-20px';font-size:3vw>Opening Balance (Leaves)</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
									
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
										
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn form-control' style='background-color: lightgray;'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <!-- <tr style="background-color: #F0F0F0;">
							 	<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none1;'>empRowId</th>
							 	<th width="150" >Emp Name</th>
							 	<th width="80" >Paying Now</th>
							 </tr> -->
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnSave' class='btn btn-primary form-control'>";
	      	?>
		</div>
	</div>
</div>





<script type="text/javascript">
setHeadings();

</script>