<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">


<script type="text/javascript">
	var controller='Colours_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].colourRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].colourRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].colourName;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].remarks;
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].hike;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].imgExists;
	          var cell = row.insertCell(7);
	          cell.innerHTML = "<button class='clsBtnImage btn btn-primary form-control'>Image</button>";
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		// $('#tbl1 tr').each(function(){
		// 	$(this).bind('click', highlightRow);
		// });
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						if( data['dependent'] == "yes" )
						{
							alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						}
						else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#txtColourName").focus();
						}
					}
				}
			});
	}
	
	function saveData()
	{	
		colourName = $("#txtColourName").val().trim();
		remarks = $("#txtRemarks").val().trim();
		if(colourName == "")
		{
			alertPopup("Colour name can not be blank...", 8000);
			$("#txtColourName").focus();
			return;
		}
		hike = $("#txtHike").val().trim();
		active = $("#active").val().trim();

		if($("#btnSave").val() == "Save")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'colourName': colourName, 
								'remarks': remarks, 
								'hike': hike,
								'active': active
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtColourName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1

								alertPopup('Record saved...', 4000);
								blankControls();
								$("#txtColourName").focus();
								// location.reload();
							}
						}
							
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert("update");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {
						'globalrowid': globalrowid, 
						'colourName': colourName, 
						'remarks': remarks, 
						'hike': hike,
						'active': active
					},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtColourName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#btnSave").val("Save");
								$("#txtColourName").focus();
							}
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						$("#txtColourName").focus();
					}
				}
			});
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Colours</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Colour Name:</label>";
						echo form_input('txtColourName', '', "class='form-control' autofocus id='txtColourName' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo form_input('txtRemarks', '', "class='form-control'  id='txtRemarks' style='' maxlength=255 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Hike (%):</label>";
						echo form_input('txtHike', '', "class='form-control'  id='txtHike' style='' maxlength=7 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Color Active:</label>";
						echo '<select id="active" name="active" class="form-control"><option value="1">Yes</option><option value="0">No</option></select>';
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='width:0px;display:none;'>rowid</th>
					 	<th>Colour</th>
					 	<th>Remarks</th>
					 	<th>Hike</th>
					 	<th>Image</th>
					 	<th>Image</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						if($errorfound=="no") ////When Saved Or Updated Successfully
						{
							// echo "<script> blankcontrol(); </script>";
							// echo "<script> document.getElementById('btnSave').value = 'Save'; </script>";
						}
						else 
						{
							// echo $errorfound;
						}


						foreach ($records as $row) 
						{
						 	$rowId = $row['colourRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='width:0px;display:none;'>".$row['colourRowId']."</td>";
						 	echo "<td>".$row['colourName']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td>".$row['hike']."</td>";
						 	echo "<td>".$row['imgExists']."</td>";
						 	// if( $row['imgExists'] != "N")
						 	// {
								// $pic = base_url();
			 				// 	$pic = $pic."bootstrap/images/colours/".$rowId."_thumb.jpg";
							 // 	echo "<td class='text-center'><img id='".$rowId."' src='".$pic."' width='60px' /></td>";
						 	// }
						 	// else
						 	// {
						 	// 	echo "<td class='text-center'><img src='' width='60px' /></td>";
						 	// }
						 	echo "<td><button class='clsBtnImage btn btn-primary form-control'>Image</button></td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				// echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>



	<!-- Model Image-->
		  <div class="modal" id="myModalImage" role="dialog" data-backdrop="static">
		    <div class="modal-dialog modal-lg">
		      <div class="modal-content">
				<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <label id="modalLblName">Image</label>
		          <label id="modalLblRowId" style="color: lightgrey;">ImageId</label>
		          <label id="modalTableRowNo" style="color: lightgrey;">ImageId</label>
		          <input type="hidden" name="txtHiddenRowId" id="txtHiddenRowId">
		        </div>
		        <div class="modal-body" style="overflow: auto; height: 300px;">
		          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
						<input type="file" id="imageFile" name="imageFile" class='form-control' style="" />
				  </div>
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-top: 30px;">
						<img src="" id="img"  alt="." />
						
					</div>
		        </div>
		        <div class="modal-footer">
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        		<button id="deleteImage" class='btn btn-danger btn-block' onclick="delImage();">Delete Image</button>
		        	</div>
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        		
		        	</div>
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        	</div>
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        		<!-- <button type="submit" class="btn btn-primary">Update</button> -->
		        		<input type="submit" value="Save" id="btnSave"  class='btn btn-primary btn-block' />
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>	



<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		colourName = $(this).closest('tr').children('td:eq(3)').text();
		remarks = $(this).closest('tr').children('td:eq(4)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#txtColourName").val(colourName);
		$("#txtRemarks").val(remarks);
		$("#txtHike").val($(this).closest('tr').children('td:eq(5)').text());
		$("#txtColourName").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});

			$(".clsBtnImage").on('click', uploadImage);

		} );
</script>



<!-- Image job -->
<script type="text/javascript">
	function uploadImage()
	{ 
		rowIndex = $(this).parent().parent().index();
		rowId = $(this).closest('tr').children('td:eq(2)').text();
		name = $(this).closest('tr').children('td:eq(3)').text();
		$("#modalLblRowId").text(rowId);
		$("#modalLblName").text(name);
		$("#txtHiddenRowId").val(rowId);
		$("#modalTableRowNo").text(rowIndex);
		// console.log(productRowId);
		// return;
		
		$.ajax({
			'url': base_url + '/' + controller + '/getImageName',
			'type': 'POST',
			'dataType': 'json',
			'data': {
						'rowId': rowId
					},
			'success': function(data)
			{
				if( data == "NF" )
				{
					$('#img').attr("src", "");
				}
				else
				{
					$('#img').attr("src", "");
					$('#img').attr("src", data + "?" + new Date().getTime());
				}
				$('#img').attr("height", 200);
				$('#myModalImage').modal('show');
			},
			'error': function(jqXHR, exception)
	          {
	            $("#paraAjaxErrorMsg").html( jqXHR.responseText );
	            $("#modalAjaxErrorMsg").modal('toggle');
	          }
		});	
	}

	function delImage()
	{ 
		rowId = $("#modalLblRowId").text();
		
		$.ajax({
			'url': base_url + '/' + controller + '/deleteImage',
			'type': 'POST',
			// 'dataType': 'json',
			'data': {
						'rowId': rowId
					},
			'success': function(data)
			{
				$('#imageFile').val("");
				$('#img').attr("src", "");
				$("#tbl1 tbody").find("tr:eq("+ $('#modalTableRowNo').text() +")").find('td:eq(6)').text('N');
				alertPopup("Image deleted...", 2000);
			},
			'error': function(jqXHR, exception)
	          {
	            $("#paraAjaxErrorMsg").html( jqXHR.responseText );
	            $("#modalAjaxErrorMsg").modal('toggle');
	          }
		});	
		$('#myModalImage').modal('hide');
	}
</script>


<!-- upload image -->
<script type="text/javascript">
	$(document).ready(function(e)
	{
		$("#frm").on("submit", (function(e)
		{
			e.preventDefault();
			imageFile =	$('#imageFile').val();
			if(imageFile == "" )
			{
				return;
			}
			$.ajax({
					'url': base_url + '/' + controller + '/uploadImage',
					'type': 'POST',
					'data': new FormData(this),
					'contentType':false,
					'cache': false,
					'processData': false,
					// 'dataType': 'json',
					'success': function(data)
					{
						if(data == "Done...")
						{
							// $("#tbl1").closest(+ $('#modalTableRowNo').text() +).children('td:eq(4)').text();
							// console.log($('#modalTableRowNo').text());
							$("#tbl1 tbody").find("tr:eq("+ $('#modalTableRowNo').text() +")").find('td:eq(6)').text('Y');
							alertPopup('Record updated...', 2000);
						}
						else
						{
							alert(data);
						}
					},
					'error': function(jqXHR, exception)
			          {
			            $("#paraAjaxErrorMsg").html( jqXHR.responseText );
			            $("#modalAjaxErrorMsg").modal('toggle');
			          }
				});
				$('#imageFile').val('');
				$('#myModalImage').modal('hide');
		}));



		$("#imageFile").on('change', function() 
	    {
		      //Get count of selected files
			var src = document.getElementById("imageFile");
			var target = document.getElementById("img");
			// var target1 = document.getElementById("imgPreview");
			var countFiles = $(this)[0].files.length;
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		  	if (extn == "jpg" || extn == "jpeg") 
		  	{
		        if (typeof(FileReader) != "undefined") 
		        {
		          //loop for each file selected for uploaded.
		          for (var i = 0; i < countFiles; i++) 
		          {
		            var reader = new FileReader();
		            reader.onload = function(e) {
		            	target.src = e.target.result; 
		            	target.height=200;
		            	// target1.src = e.target.result; 
		            }
		            reader.readAsDataURL($(this)[0].files[i]);
		          }
		          // $("#txtPhotoChange").val("yes");
		        } 
		        else 
		        {
		          alert("This browser does not support FileReader.");
		        }
		    } 
		    else 
		    {
				$('#imageFile').val('');
		    	alertPopup("Pls select only JPG images", 3000, "red", "white");
		    }
	    });
	  
	});
</script>