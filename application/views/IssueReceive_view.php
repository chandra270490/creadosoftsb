<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='IssueReceive_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.style.display="none";
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].irRowId +",'"+ records[i].vType +"',"+ records[i].vNo +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].irRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          // cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = dateFormat(new Date(records[i].dt));
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].empRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].name;
	          // cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].productCategoryRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].qty;	
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].remarks;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].vNo;
	          cell.style.display="none";
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}



	function deleteRecord(rowId, vType, vNo)
	{
		// alert(rowId + "  " + vType + "  " + vNo);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId, 'vType': vType, 'vNo': vNo},
				'success': function(data){
					// alert(JSON.stringify(data));
					if(data)
					{
						if( data == "cannot")
						{
							alertPopup('Cannot cancel, dependent rows exists...', 7000);
						}
						else
						{
							setTable(data['records']);
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#txtLetterNo").focus();
							$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
				}
			});
	}
	

	function saveData()
	{	
		var dt = $("#dtIr").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dtIr");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtIr").focus();
				return;
			}
		// }
		vType = $("#cboIssueReceive").val();
		if(vType == "-1")
		{
			alertPopup("Select transaction mode...", 8000);
			$("#cboIssueReceive").focus();
			return;
		}

		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboParty").focus();
			return;
		}
		productCategoryRowId = $("#cboProductCategories").val();
		if(productCategoryRowId == "-1")
		{
			alertPopup("Select product category...", 8000);
			$("#cboProductCategories").focus();
			return;
		}
		productRowId = $("#cboProducts").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			$("#cboProducts").focus();
			return;
		}

		currentQty = $("#txtCurrentQty").val();
		qty = $("#txtQty").val();
		var remarks = $("#txtRemarks").val().trim();

		if(vType == "Issue" && (parseFloat(currentQty) - parseFloat(qty)) < 0)
		{
			alertPopup("Issueing more than available...", 8000);
			$("#txtQty").focus();
			return;
		}
		// alert((parseFloat(qty) - parseFloat(currentQty)));
		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'empRowId': empRowId
								, 'vDt': dt
								, 'vType': vType
								, 'productCategoryRowId': productCategoryRowId
								, 'productRowId': productRowId
								, 'qty': qty
								, 'remarks': remarks
							},
					'success': function(data)
					{
						if(data)
						{
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 8000);
							}
							else
							{

								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								// window.location.href=data;
								// blankControls();
								$("#cboProducts").val("-1");
								$("#txtQty").val("1");
								$("#txtRemarks").val("");
								$("#txtCurrentQty").val("0");
								$("#tblProducts").find("tr:gt(0)").remove(); 
								$("#dtIr").val(dateFormat(new Date()));
							}
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			var vNo = globalVno;
			// alert(JSON.stringify(TableData));
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'empRowId': empRowId
								, 'vDt': dt
								, 'vType': vType
								, 'vNo': vNo
								, 'productCategoryRowId': productCategoryRowId
								, 'productRowId': productRowId
								, 'qty': qty
								, 'remarks': remarks
							},
					'success': function(data)
					{
						// alert();
						if(data)
						{
							setTable(data['records']) ///loading records in tbl1
							alertPopup('Record updated...', 5000);
								// window.location.href=data;
							blankControls();
							$("#cboIssueReceive").prop("disabled", false);
							$("#tblProducts").find("tr:gt(0)").remove(); 
							$("#dtIr").val(dateFormat(new Date()));
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		$.ajax({
			'url': base_url + '/' + controller + '/loadAllRecords',
			'type': 'POST',
			'dataType': 'json',
			'success': function(data)
			{
				JSON.stringify(data);
				if(data)
				{
					setTable(data['records'])
					alertPopup('Records loaded...', 4000);
					blankControls();
					// $("#txtTownName").focus();
				}
			}
		});
	}



	function storeTblHeaders()
	{
		var header = Array();
		// alert('ff');
		$("#tbl1 tr th").each(function(i, v){
		        header[i] = $(this).text();
		})

		return header;
	}

	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text().replace(/(\r\n|\n|\r)/gm,", ");
		    }); 
		})

	    return data;
	}


	function exportData()
	{	
		var TableDataHeader;
		TableDataHeader = storeTblHeaders();
		TableDataHeader = JSON.stringify(TableDataHeader);
		// alert(JSON.stringify(TableDataHeader));
		// return;

		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("Nothing to export...", 8000);
			// $("#cboProducts").focus();
			return;
		}


		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'TableDataHeader': TableDataHeader
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Issue / Receive</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dtIr', '', "class='form-control' placeholder='' id='dtIr' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtIr" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dtIr").val(dateFormat(new Date()));
					</script>
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- Select ---';
						$types['Issue'] = "Issue";
						$types['Receive'] = "Receive";
						echo "<label style='color: black; font-weight: normal;'>Mode: </label>";
						echo form_dropdown('cboIssueReceive', $types, '-1', "class='form-control' id='cboIssueReceive'");
					?> 
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Employee Name:</label>";
						echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;display: none;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Address:</label>";
						echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtAddress'  maxlength='255' value=''");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="row">
							
				    </div>			
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					    </div>	
				    </div>			
	          	</div>
			</div>

			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
					<?php
						echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
						echo form_dropdown('cboProductCategories',$productCategories, '-1', "class='form-control' id='cboProductCategories'");
	              	?>

	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						$products="--- Select ---";
						echo "<label style='color: black; font-weight: normal;'>Products:</label>";
						echo form_dropdown('cboProducts',$products, '-1', "class='form-control' id='cboProducts'");
	              	?>
	              	<script type="text/javascript">
			            $(document).ready(function()
			            {
			              $("#cboProducts").change(function()
			              {
						        $("#txtRate").val($('option:selected', '#cboProducts').attr('rate'));
						        $("#txtSize").val($('option:selected', '#cboProducts').attr('productLength'));
						        $("#txtSize").val( $("#txtSize").val() + ' x ' + $('option:selected', '#cboProducts').attr('productWidth') );
						        $("#txtSize").val( $("#txtSize").val() + ' x ' + $('option:selected', '#cboProducts').attr('productHeight') );
						        $("#txtSize").val( $("#txtSize").val() + ' ' + $('option:selected', '#cboProducts').attr('uom') );
						        $("#txtRoLevel").val($('option:selected', '#cboProducts').attr('roLevel'));
			              });
			            });
			        </script>	
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Qty:</label>";
						echo '<input type="number" step="1" name="txtQty" value="1" placeholder="" class="form-control" maxlength="20" id="txtQty" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="display: none1;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Size:</label>";
						echo '<input type="text" disabled name="txtSize" placeholder="" class="form-control" maxlength="40" id="txtSize" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="display: none1;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Current Qty:</label>";
						echo '<input type="text" disabled name="txtCurrentQty"  value="0" class="form-control" maxlength="40" id="txtCurrentQty" />';
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo '<input type="text" name="txtRemarks" placeholder="" class="form-control" maxlength="255" id="txtRemarks" />';
	              	?>
	          	</div>
			</div>

			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
				</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>rowid</th>
					 	<th style='display:none1;'>V.Type</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>EmpRowId</th>
					 	<th>Employee</th>
					 	<th style='display:none;'>ProductCategoryRowId</th>
					 	<th style='display:none;'>ProductRowId</th>
					 	<th>ProductName</th>
					 	<th>Qty</th>
					 	<th>Remarks</th>
					 	<th style='display:none;'>vNo</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['irRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="display:none;color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.', \''.$row['vType'].'\', '.$row['vNo'].');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['irRowId']."</td>";
						 	echo "<td style='display:none1;'>".$row['vType']."</td>";
						 	$vdt = strtotime($row['dt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['empRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td style='display:none;'>".$row['productCategoryRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['productRowId']."</td>";
						 	echo "<td>".$row['productName']."</td>";
						 	echo "<td>".$row['qty']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 10px;" >
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>	
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid,globalVType,globalVno);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	var globalVType;
	var globalVno = 0;
	function delrowid(rowid, vType, vNo)
	{
		globalrowid = rowid;
		globalVno = vNo;
		globalVType = vType;
		// alert(rowid + ' ' + vType + ' ' + vNo);
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();
		globalVno = $(this).closest('tr').children('td:eq(12)').text();
		$("#dtIr").val($(this).closest('tr').children('td:eq(4)').text());
		$("#cboIssueReceive").val($(this).closest('tr').children('td:eq(3)').text());
		$("#cboEmp").val($(this).closest('tr').children('td:eq(5)').text());
		// 
		$("#cboProductCategories").val($(this).closest('tr').children('td:eq(7)').text());
		// $("#cboProducts").val($(this).closest('tr').children('td:eq(8)').text());
		var productRowId = $(this).closest('tr').children('td:eq(8)').text();
		// 
		$("#txtQty").val($(this).closest('tr').children('td:eq(10)').text());
		$("#txtRemarks").val($(this).closest('tr').children('td:eq(11)').text());

		var productCategoryRowId = $(this).closest('tr').children('td:eq(7)').text();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                  // alert(data);
                  $('#cboProducts').empty();
                  if(data['products'] != null) 
                  {
                    var options = "<option value='-1' rate='0' productLength='0' productWidth='0' productHeight='0' uom='-1' roLevel='0'>" + "--- Select ---" + "</option>";
                    for(var i=0; i<data['products'].length; i++)
                    {
                      options += "<option value=" + data['products'][i].productRowId + " rate=" + data['products'][i].productRate + " productLength=" + data['products'][i].productLength + " productWidth=" + data['products'][i].productWidth + " productHeight=" + data['products'][i].productHeight + " uom=" + data['products'][i].uom +  " roLevel=" + data['products'][i].roLevel + ">" + data['products'][i].productName + "</option>";
                    }
                    $('#cboProducts').append(options);

                    $("#cboProducts").val(productRowId);
                    $('#cboProducts').trigger('change');

                  }
                  else
                  {
                  }

                }
              }
          });

          $("#cboIssueReceive").prop("disabled", true);
		$("#btnSave").val("Update");
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );



      $(document).ready(function(){
        $("#cboProductCategories").change(function(){
          var productCategoryRowId = $("#cboProductCategories").val();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                  // alert(data);
                  $('#cboProducts').empty();
                  if(data['products'] != null) 
                  {
                    var options = "<option value='-1' rate='0' productLength='0' productWidth='0' productHeight='0' uom='-1' roLevel='0'>" + "--- Select ---" + "</option>";
                    for(var i=0; i<data['products'].length; i++)
                    {
                      options += "<option value=" + data['products'][i].productRowId + " rate=" + data['products'][i].productRate + " productLength=" + data['products'][i].productLength + " productWidth=" + data['products'][i].productWidth + " productHeight=" + data['products'][i].productHeight + " uom=" + data['products'][i].uom +  " roLevel=" + data['products'][i].roLevel + ">" + data['products'][i].productName + "</option>";
                    }
                    $('#cboProducts').append(options);

                  }
                  else
                  {
                  }

                }
              }
          });
        });
      });
              


	  $(document).ready(function(){
        $("#cboProducts").change(function(){
          var productRowId = $("#cboProducts").val();
          if(cboProducts == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getCurrentQty',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productRowId': productRowId},
              'success': function(data)
              {
              	// 
                if(data)
                {
                  // alert( JSON.stringify(data));
                  if(data['currentQty'] != null) 
                  {
                  	if(data['currentQty'][0]['currentQty'] !=null )
                  	{
                  		$("#txtCurrentQty").val( data['currentQty'][0]['currentQty'] );
                  	}
                  	else
                  	{
                  		$("#txtCurrentQty").val( '0' );
                  	}
                  	// var requiredQty = $("#txtRoLevel").val() - $("#txtCurrentQty").val() ; 
                  	// if( requiredQty > 0 )
                  	// {
                  	// 	// $("#txtQty").val(requiredQty);
                  	// }
                  	// else
                  	// {
                  	// 	// $("#txtQty").val('0');
                  	// }
                  }
                  // else
                  // {
                  // 	$("#txtCurrentQty").val( '0' );
                  // }
                }
              }
          });
        });
      });

</script>