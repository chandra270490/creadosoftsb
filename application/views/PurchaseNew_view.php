<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='PurchaseNew_Controller';
	var base_url='<?php echo site_url();?>';

	var globalPoRowIdForReprint=0;
	var globalPiRowIdForReprint=0;
	var globalvDt="";
	var globalBillNoForReprint="";
	var globalBillDtForReprint="";
	function showDetails()
      {
      	// alert();
	    $("#tblDetail").find("tr:gt(0)").remove();
	    poRowId = $(this).parent().find("td:eq(2)").text();
	    globalPoRowIdForReprint = $(this).parent().find("td:eq(2)").text();
	    globalPiRowIdForReprint = $(this).parent().find("td:eq(4)").text();
	    globalBillNoForReprint = $(this).parent().find("td:eq(12)").text();
	    globalBillDtForReprint = $(this).parent().find("td:eq(13)").text();
	    globalvDt = $(this).parent().find("td:eq(5)").text();
	    // alert(globalvDt);
	    $.ajax({
				'url': base_url + '/' + controller + '/getPurchaseDetial',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'poRowId': poRowId
						},
				'success': function(data)
				{
					// alert( JSON.stringify(data) );
					// $("#tblDetail").empty();
					$("#tblDetail").find("tr:gt(0)").remove();
	      			var table = document.getElementById("tblDetail");
					for(i=0; i<data['purchaseDetail'].length; i++)
				      {
				          newRowIndex = table.rows.length;
				          row = table.insertRow(newRowIndex);


				          var cell = row.insertCell(0);
				          cell.innerHTML = data['purchaseDetail'][i].poDetailRowId;
				          cell.style.display="none";
				          var cell = row.insertCell(1);
				          cell.innerHTML = data['purchaseDetail'][i].productRowId;
				          cell.style.display="none";
				          var cell = row.insertCell(2);
				          cell.innerHTML = data['purchaseDetail'][i].productName;
				          var cell = row.insertCell(3);
				          cell.innerHTML = data['purchaseDetail'][i].receivedQty;
				          var cell = row.insertCell(4);
				          cell.innerHTML = data['purchaseDetail'][i].rate;
				          var cell = row.insertCell(5);
				          cell.innerHTML = data['purchaseDetail'][i].receivedQty * data['purchaseDetail'][i].rate;
				          var cell = row.insertCell(6);
				          cell.innerHTML = data['purchaseDetail'][i].billNo;
				          cell.style.display="none";
				          var cell = row.insertCell(7);
				          cell.innerHTML = data['purchaseDetail'][i].billDt;
				          cell.style.display="none";
				          var cell = row.insertCell(8);
				          cell.innerHTML = data['purchaseDetail'][i].discount;
				          var cell = row.insertCell(9);
				          cell.innerHTML = data['purchaseDetail'][i].amtAfterDiscount;
				          var cell = row.insertCell(10);
				          cell.innerHTML = data['purchaseDetail'][i].gstRate;
				          var cell = row.insertCell(11);
				          cell.innerHTML = data['purchaseDetail'][i].gstAmt;
				          var cell = row.insertCell(12);
				          cell.innerHTML = data['purchaseDetail'][i].netAmt;
				          
				  	  }						
				}
				
		});
      };


	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid("+ records[i].poRowId +","+ records[i].vNo +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].poRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vNo;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.style.display="none";
	          // cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(9);
	          // cell.innerHTML = records[i].totalQty;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;	
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].purchaseOrExpense;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].billNo;
	          var cell = row.insertCell(13);
	          cell.innerHTML = dateFormat(new Date(records[i].billDt));
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
        $("#tbl1 tr").find("td:gt(1)").on('click', showDetails);

		// $("#tbl1 tr").on("click", showDetails);
			
	}

	function setTableProducts(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tblProducts").find("tr:gt(0)").remove();
	      var table = document.getElementById("tblProducts");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);
	          var cell = row.insertCell(0);
	          cell.innerHTML = records[i].podetailRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].vType;
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].vNo;
	          // cell.style.display="none";
	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].orderQty;
	          // cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].pendingQty;
	          var cell = row.insertCell(8);
	          cell.innerHTML = '0';
	          cell.style.color="red";
	          cell.setAttribute("contentEditable", true);
	          cell.className = "classQtyReceived";

	          var cell = row.insertCell(9);
	          cell.innerHTML = "";
	          cell.style.color="blue";
	          cell.setAttribute("contentEditable", true);
	          cell.style.display="none";

	          var cell = row.insertCell(10);
	          cell.innerHTML = "";
	          cell.style.color="blue";
	          cell.setAttribute("contentEditable", true);
	          cell.style.display="none";

	          var cell = row.insertCell(11);
	          cell.innerHTML =  records[i].rate;
	          // cell.style.color="red";
	          // cell.setAttribute("contentEditable", true);

	          var cell = row.insertCell(12);
	          cell.innerHTML = '0';
	          cell.style.color="green";
	          // cell.setAttribute("contentEditable", true);

	          var cell = row.insertCell(13);
	          cell.innerHTML =  records[i].discount;
	          // cell.style.color="red";
	          // cell.setAttribute("contentEditable", true);

	          var cell = row.insertCell(14);
	          cell.innerHTML =  "0";
	          // cell.style.color="red";
	          // cell.setAttribute("contentEditable", true);

	          var cell = row.insertCell(15);
	          cell.innerHTML =  "0";

	          var cell = row.insertCell(16);	//GST %
	          cell.innerHTML =  "0";
	          cell.style.color="red";
	          cell.setAttribute("contentEditable", true);
	          cell.className = "classGstPer";

	          var cell = row.insertCell(17);
	          cell.innerHTML =  "0";
	          // cell.style.color="red";
	          // cell.setAttribute("contentEditable", true);

	          var cell = row.insertCell(18);
	          cell.innerHTML =  '0';
	          // cell.style.color="red";
	          // cell.setAttribute("contentEditable", true);
	          var cell = row.insertCell(19);
			  cell.innerHTML = records[i].purchaseOrExpense;

			  var cell = row.insertCell(20);
			  cell.innerHTML = "<button id='po_sc' name='po_sc' onclick='ShortClosePO("+records[i].vNo+");' class='btn btn-primary form-control'>Short Close PO</button>";
	  	  }
	  	  $('.classQtyReceived, .classGstPer').bind('keyup', calculateAmt);
			
	}
	function calculateAmt()
	{
		// alert();
		var q = parseFloat( $(this).parent().find("td:eq(8)").text() );
		var r = parseFloat( $(this).parent().find("td:eq(11)").text() );
		var a = q * r;
		a = a.toFixed(2);
		$(this).parent().find("td:eq(12)").text( a );

		var disPer = parseFloat( $(this).parent().find("td:eq(13)").text() );
		var disAmt = a * disPer/100;
		disAmt = disAmt.toFixed(2);
		$(this).parent().find("td:eq(14)").text( disAmt );


		var amtAfterDis = a - disAmt;
		amtAfterDis = amtAfterDis.toFixed(2);
		$(this).parent().find("td:eq(15)").text( amtAfterDis );

		var gstPer = parseFloat( $(this).parent().find("td:eq(16)").text() );
		var gstAmt = amtAfterDis * gstPer/100;
		gstAmt = gstAmt.toFixed(2);
		$(this).parent().find("td:eq(17)").text( gstAmt );

		var netAmt = parseFloat(amtAfterDis) + parseFloat(gstAmt);
		netAmt = netAmt.toFixed(2);
		$(this).parent().find("td:eq(18)").text( netAmt );

		///////////////
				var gst5=0;
				var gst12=0;
				var gst18=0;
				var gst28=0;
				var taxAmt5 = 0;
				var taxAmt12 = 0;
				var taxAmt18 = 0;
				var taxAmt28 = 0;
				$("#tblProducts").find("tr:gt(0)").each(function(i){
					if( $(this).find('td:eq(12)').text() > 0 )//agar amt 0 se zyada ho to
	    			{
						if( (parseFloat( $(this).find("td:eq(16)").text() )) == 5 )
						{
							gst5 += parseFloat( $(this).find("td:eq(15)").text() );				//sum of amtAfterDis
							// alert(gst5);
						}
						else if( (parseFloat( $(this).find("td:eq(16)").text() )) == 12 )
						{
							gst12 += parseFloat( $(this).find("td:eq(15)").text() );			//sum of amtAfterDis
						}
						else if( (parseFloat( $(this).find("td:eq(16)").text() )) == 18 )
						{
							gst18 += parseFloat( $(this).find("td:eq(15)").text() );			//sum of amtAfterDis
						}
						else if( (parseFloat( $(this).find("td:eq(16)").text() )) == 28 )
						{
							gst28 += parseFloat( $(this).find("td:eq(15)").text() );			//sum of amtAfterDis
						}

						// amtTotal += parseFloat( $(this).find("td:eq(12)").text() );
					}
				});
				$("#tblTax").find("tr:eq(1)").find("td:eq(1)").text(gst5);
				$("#tblTax").find("tr:eq(1)").find("td:eq(2)").text(gst12);
				$("#tblTax").find("tr:eq(1)").find("td:eq(3)").text(gst18);
				$("#tblTax").find("tr:eq(1)").find("td:eq(4)").text(gst28);
				taxAmt5 = gst5 * 5 / 100;
				taxAmt5 = taxAmt5.toFixed(2);
				$("#tblTax").find("tr:eq(2)").find("td:eq(1)").text(taxAmt5);

				taxAmt12 = gst12 * 12 / 100;
				taxAmt12 = taxAmt12.toFixed(2);
				$("#tblTax").find("tr:eq(2)").find("td:eq(2)").text(taxAmt12);

				taxAmt18 = gst18 * 18 / 100;
				taxAmt18 = taxAmt18.toFixed(2);
				$("#tblTax").find("tr:eq(2)").find("td:eq(3)").text(taxAmt18);

				taxAmt28 = gst28 * 28 / 100;
				taxAmt28 = taxAmt28.toFixed(2);
				$("#tblTax").find("tr:eq(2)").find("td:eq(4)").text(taxAmt28);
		/////////////



		totalAmt = 0;
		totalQty = 0;
		totalDisAmt = 0;
		totalAmtAfterDis = 0;
		totalNetAmt = 0;

		$('#tblProducts tr').each(function(row, tr)
	    {
	    	if( $(tr).find('td:eq(12)').text() > 0 )
	    	{
	    		totalAmt += parseFloat( $(tr).find('td:eq(12)').text() );
	    		totalQty += parseFloat( $(tr).find('td:eq(8)').text() );
	    		totalDisAmt += parseFloat( $(tr).find('td:eq(14)').text() );
	    		totalAmtAfterDis += parseFloat( $(tr).find('td:eq(15)').text() );
	    		totalNetAmt += parseFloat( $(tr).find('td:eq(18)').text() );
	    	}
	    });
	    totalAmt = totalAmt.toFixed(2);
	    $("#txtTotalAmt").val(totalAmt);
	    totalQty = totalQty.toFixed(2);
	    $("#txtTotalQty").val(totalQty);
	    totalDisAmt = totalDisAmt.toFixed(2);
	    $("#txtTotalDisAmt").val(totalDisAmt);
	    totalAmtAfterDis = totalAmtAfterDis.toFixed(2);
	    $("#txtTotalAmtAfterDis").val(totalAmtAfterDis);
	    totalNetAmt = totalNetAmt.toFixed(2);
	    $("#txtTotalNetAmt").val(totalNetAmt);

	    // /// totalAmt ka x% = totalDisAmt
	    
	    var disPer = totalDisAmt * 100 / totalAmt
	    disPer = disPer.toFixed(5);
	    $("#txtAvgDisPer").val(disPer);


	}

	function deleteRecord(rowId)
	{
		// alert(rowId);
		// return;
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId, 'piRowId': globalPiRowId},
				'success': function(data){
					//alert(JSON.stringify(data));
					if(data)
					{
						if( data == "cannot")
						{
							alertPopup('Cannot cancel, dependent rows exists...', 7000);
						}
						else
						{
							// setTable(data['records']);
							alertPopup('Record deleted...', 1000);
							location.reload();
							// $("#btnShow").trigger("click");
							// blankControls();
							// $("#txtLetterNo").focus();
							// $("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
				}
			});
	}
	

	var tblRowsCount;
	var receivedMoreThanPending = 0;
	var billNoOrBillDtMissing = 0;
	var billNoOrBilDtMismatch = 0;
	var vBillNo = "";
	var vBillDt = "";
	var vSarePurchaseYaSareExp = "";
	var vPahlaItemPurchaseYaExp = "";
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	if( $(tr).find('td:eq(8)').text() > 0 )
	    	{
	    		// if(i==0) ///Pahla BillNo or Dt, P/E
	    		// {
	    		// 	vBillNo = $(tr).find('td:eq(9)').text().trim();
	    		// 	vBillDt = $(tr).find('td:eq(10)').text().trim();
	    		// 	vPahlaItemPurchaseYaExp = $(tr).find('td:eq(19)').text().trim();
	    		// }
	    		// else  ///baki k bill no or dt same agar nahi h to...
	    		// {
	    		// 	if(vBillNo != $(tr).find('td:eq(9)').text().trim() || vBillDt != $(tr).find('td:eq(10)').text().trim())
	    		// 	{
	    		// 		billNoOrBilDtMismatch = 1;
	    		// 	}
	    		// 	if(vPahlaItemPurchaseYaExp != $(tr).find('td:eq(19)').text().trim() )
	    		// 	{
	    		// 		vSarePurchaseYaSareExp = 1;
	    		// 	}
	    		// }
	    		if( parseFloat($(tr).find('td:eq(8)').text()) > parseFloat($(tr).find('td:eq(7)').text()) )
	    		{
	    			receivedMoreThanPending = 1;
	    		}
	    		// if( $(tr).find('td:eq(9)').text().trim() == "" || $(tr).find('td:eq(10)').text().trim() == "" || $(tr).find('td:eq(9)').text().trim().length > 39  || $(tr).find('td:eq(10)').text().trim().length > 19 )
	    		// {
	    		// 	// alert();
	    		// 	billNoOrBillDtMissing = 1;
	    		// }
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(0)').text()
		            , "poVno" : $(tr).find('td:eq(2)').text()
		            , "productRowId" : $(tr).find('td:eq(4)').text()
		            , "receivedQty" : $(tr).find('td:eq(8)').text()
		            , "billNo" : $(tr).find('td:eq(9)').text()
		            , "billDt" : $(tr).find('td:eq(10)').text()
		            , "rate" : $(tr).find('td:eq(11)').text()
		            , "amt" : $(tr).find('td:eq(12)').text()
		            , "disPer" : $(tr).find('td:eq(13)').text()
		            , "amtAfterDiscount" : $(tr).find('td:eq(15)').text()
		            , "gstRate" : $(tr).find('td:eq(16)').text()
		            , "gstAmt" : $(tr).find('td:eq(17)').text()
		            , "netAmt" : $(tr).find('td:eq(18)').text()
		            , "productName" : $(tr).find('td:eq(5)').text()
		            , "purchaseOrExpense" : $(tr).find('td:eq(19)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i;
	    return TableData;
	}


	function saveData()
	{	

		// var task = $("#cboTask").val();
		// if(task == "-1")
		// {
		// 	alertPopup("Select task...", 8000);
		// 	$("#cboTask").focus();
		// 	return;
		// }
		

		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		if(tblRowsCount <= 0)
		{
			alert("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		if( receivedMoreThanPending == 1)
		{
			alert("Despatching more than pending...", 8000);
			receivedMoreThanPending = 0;
			return;
		}
		// if( billNoOrBillDtMissing == 1)
		// {
		// 	alert("Bill No Or Bill Dt Missing...\nOR length of bill no or bill dt is too long");
		// 	billNoOrBillDtMissing = 0;
		// 	return;
		// }
		// if( billNoOrBilDtMismatch == 1)
		// {
		// 	alert("Bill No Or Bill Dt Mismatch...");
		// 	billNoOrBilDtMismatch = 0;
		// 	return;
		// }
		if( vSarePurchaseYaSareExp == 1)
		{
			alert("All items must be PURCHASE or EXP....");
			vSarePurchaseYaSareExp = 0;
			return;
		}

		var refBillNo = $("#txtRefBillNo").val().trim();
		if(refBillNo == "")
		{
			alert("Enter Ref Bill No...", 6000);
			$("#txtRefBillNo").focus();
			return;
		}
		var refBillDt = $("#dtRefBill").val().trim();
		dtOk = testDate("dtRefBill");
		if(dtOk == false)
		{
			alert("Invalid date...", 5000);
			// $("#dtRefBill").focus();
			return;
		}

		partyRowId = globalPartyRowId;
		if(partyRowId == "-1" || partyRowId == "")
		{
			alert("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}

		totalAmt = $("#txtTotalAmt").val();
		totalQty = $("#txtTotalQty").val();
		avgDiscountPer = $("#txtAvgDisPer").val();
		totalDisAmt = $("#txtTotalDisAmt").val();
		totalAmtAfterDis = $("#txtTotalAmtAfterDis").val();
		totalNetAmt = $("#txtTotalNetAmt").val();

		gst5TotalAmt = $("#tblTax").find("tr:eq(1)").find("td:eq(1)").text();
		gst5TaxAmt   = $("#tblTax").find("tr:eq(2)").find("td:eq(1)").text();
		gst12TotalAmt = $("#tblTax").find("tr:eq(1)").find("td:eq(2)").text();
		gst12TaxAmt   = $("#tblTax").find("tr:eq(2)").find("td:eq(2)").text();
		gst18TotalAmt = $("#tblTax").find("tr:eq(1)").find("td:eq(3)").text();
		gst18TaxAmt   = $("#tblTax").find("tr:eq(2)").find("td:eq(3)").text();
		gst28TotalAmt = $("#tblTax").find("tr:eq(1)").find("td:eq(4)").text();
		gst28TaxAmt   = $("#tblTax").find("tr:eq(2)").find("td:eq(4)").text();

		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'refBillNo': refBillNo
								, 'refBillDt': refBillDt
								, 'totalAmt': totalAmt
								, 'totalQty': totalQty
								, 'TableData': TableData
								, 'avgDiscountPer': avgDiscountPer
								, 'totalDisAmt': totalDisAmt
								, 'totalAmtAfterDis': totalAmtAfterDis
								, 'totalNetAmt': totalNetAmt
								, 'gst5TotalAmt': gst5TotalAmt
								, 'gst5TaxAmt': gst5TaxAmt
								, 'gst12TotalAmt': gst12TotalAmt
								, 'gst12TaxAmt': gst12TaxAmt
								, 'gst18TotalAmt': gst18TotalAmt
								, 'gst18TaxAmt': gst18TaxAmt
								, 'gst28TotalAmt': gst28TotalAmt
								, 'gst28TaxAmt': gst28TaxAmt
								
							},
					'success': function(data)
					{
						if(data)
						{
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 8000);
							}
							else
							{

								// setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								window.location.href=data;
								blankControls();
								$("#cboParty").focus();
								$("#tblProducts").find("tr:gt(0)").remove(); 
							}
						}
					}
			});
		}

	}

	//ShortClose PO
	function ShortClosePO(vNo)
	{
		if (confirm("Want To Short Close PO-"+vNo)) {
			$.ajax({
					'url': base_url + '/' + controller + '/ShortClose?vNo='+vNo,
					'type': 'POST',
					// 'dataType': 'json',
					'data': {
								'vNo': vNo
							},
					'success': function(data)
					{
						if(data)
						{
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 8000);
							}
							else
							{

								// setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								blankControls();
								$("#cboParty").focus();
								$("#tblProducts").find("tr:gt(0)").remove(); 
							}
						}
					}
			});

		} else {

			return false;

		}

	}

	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					//JSON.stringify(data);
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
						// $("#tbl1 tr").on('click', showDetails);
					}
				}
			});
	}

	var globalPartyRowId="";
	var globalPoVno="-1";
	function showData()
	{
		// partyRowId = $(this).closest('tr').children('td:eq(0)').text();
		// globalPartyRowId=partyRowId;
		// var task = $("#cboTask").val();
		// if(task == "-1")
		// {
		// 	alertPopup("Select task...", 8000);
		// 	$("#cboTask").focus();
		// 	return;
		// }	

		partyRowId = $("#cboParty").val();
		globalPartyRowId=partyRowId;
		if(partyRowId == "-1")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}	

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'partyRowId': partyRowId
						},
				'success': function(data)
				{
					// alert(JSON.stringify(data));
					if(data)
					{
						
						setTableProducts(data['pendingProducts']) ///loading records in tbl1
						alertPopup('Record loaded...', 4000);
					}
				}
		});			
	}

</script>
<div class="container-fluid" style="width: 97%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Purchase/Expense</h1>
			<div class="row" style="margin-top:25px;">
				
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Ref. Bill No.:</label>";
						echo form_input('txtRefBillNo', '', "class='form-control' placeholder='' id='txtRefBillNo' maxlength='19' autofocus");
	              	?>
				</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Ref. Date:</label>";
						echo form_input('dtRefBill', '', "class='form-control' placeholder='' id='dtRefBill' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtRefBill" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dtRefBill").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						// $parties=9;
						echo "<label style='color: black; font-weight: normal;'>Party Name:</label>";
						// echo form_dropdown('cboParty',$parties, '-1', "class='form-control' id='cboParty'");
	              	?>
			        <select id="cboParty" class="form-control">
			              <option value="-1">--- Select ---</option>
			              <?php
			                foreach ($parties as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['partyRowId']; ?> ><?php echo $row['name']; ?></option>
			              <?php
			                }
			              ?>
				    </select>
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='showData();' value='Show Orders' id='btnShow' class='btn form-control' style='background-color: #FF5733; color:white;'>";
	              	?>

	          	</div>
			</div>


			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom:1px dashed lightgray; height:250px; overflow:auto;">
					<table class='table table-bordered' id='tblProducts' style="">
						<tr>
						 	<th  style='display:none;' width="50" class="text-center">PoDetailRow Id</th>
							<th style='display:none1;'>vType</th>
						 	<th style='display:none1;'>vNo</th>
						 	<th>Dt</th>
						 	<th style='display:none;'>Product RowId</th>
						 	<th>Product</th>
						 	<th>Odr Qty</th>
						 	<th>Pend Qty</th>
						 	<th>Recd Qty</th>
						 	<th style='display:none;'>Bill#</th>
						 	<th style='display:none;'>Bill Dt.</th>
						 	<th>Rate</th>
						 	<th>Amt</th>
						 	<th>Dis%</th>
						 	<th>DisAmt</th>
						 	<th>Amt After Dis.</th>
						 	<th>GST%</th>
						 	<th>GST Amt</th>
						 	<th>Net</th>
						 	<th>P/E</th>
						 	<th>Short Close PO</th>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row" style="margin-top:1px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Amt.:</label>";
						echo '<input type="number" disabled name="txtTotalAmt" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAmt" />';
	              	?>
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Qty.:</label>";
						echo '<input type="number" disabled name="txtTotalQty" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalQty" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Avg Dis %:</label>";
						echo '<input type="number" disabled name="txtAvgDisPer" value="" placeholder="" class="form-control" maxlength="20" id="txtAvgDisPer" />';
	              	?>
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Dis. Amt:</label>";
						echo '<input type="number" disabled name="txtTotalDisAmt" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalDisAmt" />';
	              	?>
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Amt After Dis:</label>";
						echo '<input type="number" disabled name="txtTotalAmtAfterDis" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAmtAfterDis" />';
	              	?>
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Net Amt:</label>";
						echo '<input type="number" disabled name="txtTotalNetAmt" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalNetAmt" />';
	              	?>
	          	</div>
	          	<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
				</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

	<!-- TAX TABLE -->
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 sticky-table sticky-headers sticky-ltr-cells" style="margin: 0 5px; display: none;">
			<table class='table table-hover' id='tblTax'>
				 <tr class="sticky-row">
				 	<th class="sticky-cell"></th>
					<th style="text-align: center;">GST 5%</th>
					<th style="text-align: center;">GST 12%</th>
					<th style="text-align: center;">GST 18%</th>
					<th style="text-align: center;">GST 28%</th>
				 </tr>
				 <tr>
				 	<td class="sticky-cell">Total Amt.</td>
					<td style="text-align: center;">0</td>
					<td style="text-align: center;">0</td>
					<td style="text-align: center;">0</td>
					<td style="text-align: center;">0</td>
				 </tr>
				 <tr>
				 	<td class="sticky-cell">Tax Amt.</td>
					<td style="text-align: center;">0</td>
					<td style="text-align: center;">0</td>
					<td style="text-align: center;">0</td>
					<td style="text-align: center;">0</td>
				 </tr>
			</table>
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>poRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none;'>Total Amt</th>
					 	<th style='display:none;'>Total Qty</th>
					 	<th>V.No.</th>
					 	<th>Task</th>
					 	<th>RefBillNo</th>
					 	<th>RefBillDt</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	$PiRowId = $row['vNo'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="display:none;color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.', '.$PiRowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td style='display:none;'>".$row['totalAmt']."</td>";
						 	echo "<td style='display:none;'>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['purchaseOrExpense']."</td>";
						 	echo "<td>".$row['billNo']."</td>";
						 	$vdt = strtotime($row['billDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div id="divTableDetail" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tblDetail'>
				 <thead>
					 <tr>
						<th style='display:none;'>poDetailRowid</th>
					 	<th style='display:none;'>ProductRowId</th>
					 	<th>Product</th>
					 	<th>R.Qty</th>
					 	<th>Rate</th>
					 	<th>Amt</th>
					 	<th style='display:none;'>Bill#</th>
					 	<th style='display:none;'>B.Dt</th>
					 	<th>Dis</th>
					 	<th>Amt After Dis.</th>
					 	<th>GST%</th>
					 	<th>GST Amt</th>
					 	<th>Net</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 20px;" >
		<!-- <div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div> -->

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='rePrint();' value='Reprint' id='btnReprint' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">SB</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
		
	function storeTblValuesToReprint()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblDetail tr').each(function(row, tr)
	    {
	    	
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(0)').text()
		            , "productRowId" : $(tr).find('td:eq(1)').text()
		            , "productName" : $(tr).find('td:eq(2)').text()
		            , "rQty" : $(tr).find('td:eq(3)').text()
		            , "rate" : $(tr).find('td:eq(4)').text()
		            , "amt" : $(tr).find('td:eq(5)').text()
		            , "disPer" : $(tr).find('td:eq(8)').text()
		            , "amtAfterDiscount" : $(tr).find('td:eq(9)').text()
		            , "gstRate" : $(tr).find('td:eq(10)').text()
		            , "gstAmt" : $(tr).find('td:eq(11)').text()
		            , "netAmt" : $(tr).find('td:eq(12)').text()
		            , "billNo" : $(tr).find('td:eq(6)').text()
		            , "billDt" : $(tr).find('td:eq(7)').text()
	        	}   
	        	i++; 
	        
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    // tblRowsCount = i;
	    return TableData;
	}
	function rePrint()
	{
		// alert(globalPiRowIdForReprint);
		var TableDataProducts;
		TableDataProducts = storeTblValuesToReprint();
		TableDataProducts = JSON.stringify(TableDataProducts);
		// alert(JSON.stringify(TableData));
		// return;

		// alert(globalPoRowIdForReprint);
		if(globalPoRowIdForReprint == 0 )
		{
			msgBoxError("error",'Select the old voucher from table...');
			return;
		}

		poRowId = globalPoRowIdForReprint;
		vDt = globalvDt;

		// alert(globalBillDtForReprint);
		$.ajax({
				'url': base_url + '/' + controller + '/reprint',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'poRowId': poRowId
							, 'vDt': vDt
							, 'globalBillNoForReprint': globalBillNoForReprint
							, 'globalBillDtForReprint': globalBillDtForReprint
							// , 'TableData': TableData
							, 'TableDataProducts': TableDataProducts
						},
				'success': function(data)
				{
					globalPoRowIdForReprint=0;
					 window.location.href=data;
				}
				
		});
	}


	var globalrowid;
	var globalPiRowId;
	function delrowid(rowid, piRowId)
	{
		globalrowid = rowid;
		globalPiRowId = piRowId;
		// alert(globalPiRowId);
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});

		$("#tblParties tr").on("click", highlightRow);
		$("#tblParties tr").on("click", showData);
	} );


</script>

 <script type="text/javascript">
    $(document).ready(function()
    {
      $("#cboParty").change(function()
      {
	        $("#tblProducts").find("tr:gt(0)").remove();
      });


      // $("#tbl1 tr").on('click', showDetails);
      $("#tbl1 tr").find("td:gt(1)").on('click', showDetails);
      

    });


</script>