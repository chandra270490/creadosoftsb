<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
	label{color:black;}
</style>
<script type="text/javascript">
	var controller='Right_Controller';
	var base_url='<?php echo site_url();?>';

	function loaddata()
	{	
		var str="";
		$('#tree input:checked').each(function() {
			str = $(this).val() + "," + str;
		});

		//////User for Address Book
		var users4ab="";
		$('#divUsers4ab input:checked').each(function() {
			users4ab = $(this).val() + "," + users4ab;
		});
		users4ab = users4ab.substring(0, users4ab.length-1);
		// alert(users4ab);
		// return;
		//////END - User for Address Book


		var OcboUsers = document.getElementById("cboUsers");
		var uid = OcboUsers.options[OcboUsers.selectedIndex].value;
		if(uid==-1)
		{
			myAlert('Please Select User');
			return;	
		}
		if(str=="")
		{
			myAlert('Please Select Atleast one right');
			return;
		}

		arr = str.split(",");
		arr = arr.slice(0,arr.length-1);
		str = arr.reverse().join(",");
		$.ajax
		(
			{
				'url': base_url + '/' + controller + '/insertRights',
				'type': 'POST', 
				'data': {'uid' : uid, 'rights' : str, 'users4ab' : users4ab},
				'success': function(data){
					myAlert("Rights Saved...!!!");
				}
			}
		);

	 	blankcontrol();
	}

	function blankcontrol()
	{
		document.getElementById("cboUsers").selectedIndex = "0";
		$('#tree input:checked').each(function() {
			$(this).removeAttr('checked');
		});		 
	}
	$(document).ready(function(){
		$("#cboUsers").change(function(){
			$.ajax({
	        'url': base_url + '/Menu_Controller/getRights',
	        'data': {'uid':$("#cboUsers").val()},
	        'type': 'POST', 
	        'success': function(data)
	        {
				arr = data.split(",");
				arr = arr.slice(0,arr.length-1);
				arr = arr.reverse();
	            $('input[type="checkbox"]').each(function(){
					for(j=0;j<arr.length;j++)
					{
						if($(this).val() === arr[j])
						{
							$(this).prop("checked",true);
							break;
						}
						else
						{
							$(this).removeAttr("checked");
						}
					}
	            });
	        }
	      });
		});
	});
</script>


<div class="row" >
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style='border:1px solid lightgray; border-radius:10px; padding: 10px; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px'>
		<h1 class="text-center" style='margin-top:0px'>User Rights</h1>
		<?php
			$attributes[] = array("class"=>"form-control" );
			$this->load->helper('form');
			echo validation_errors(); 
			echo form_open('Right_Controller/insertRights', "onsubmit='return(false);'");
			$arr = array();
			foreach ($users as $row)
			{
        		$arr[$row['rowid']]= $row['uid'];
			}
			$temp["-1"] = "--- SELECT USER ---";
			$users = $temp+$arr;
			echo form_dropdown('uid',$users,"-1","class='form-control' id='cboUsers'");
		?>
		<br/>
	</div>
</div>
<br>
<br>
	<ul id="tree">
		<div class="row" style="position:relative;border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">

			<div class="col-lg-4 col-sm-4 col-md-4" style="border-right:1px solid gray;height:300px;overflow:scroll">
				<li>
					<label>
						<input type="checkbox" class="rights_menu" value="Masters"/>
						<b>Masters</b>
					</label>
					<ul>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Salary Masters" />
									Salary Masters
							</label>
							<ul>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Department Types" />
											Department Types
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Designation Types" />
											Designation Types
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Holidays" />
											Holidays
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Employees" />
											Employees
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Employee Contract Detail" />
											Employee Contract Detail
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Base Limits" />
											Base Limits
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Allowances" />
											Allowances
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Meeting Remarks" />
											Meeting Remarks
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Income Tax Slabs" />
											Income Tax Slabs
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Opening Balance (Leaves)" />
											Opening Balance (Leaves)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Employee Discontinue" />
											Employee Discontinue
									</label>								
								</li>
							</ul>
						</li>

						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Contact Types" />
									Prefix Types
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Contact Types" />
									Contact Types
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Countries" />
									Countries
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="States" />
									States
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Districts" />
									Districts
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Towns" />
									Towns
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Address Book" />
									Address Book
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Parties" />
									Parties
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Transporters" />
									Transporters
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Colours" />
									Colours
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Placements" />
									Placements
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Order Types" />
									Order Types
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Product Categories" />
									Product Categories
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Products" />
									Products
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Opening Balance (Purchase)" />
									Opening Balance (Purchase)
							</label>								
						</li>

					</ul>
				</li>
			</div>


			<div class="col-lg-4 col-sm-4 col-md-4" style="border-right:1px solid gray;height:300px;overflow:scroll">
				<li>
					<label>
						<input type="checkbox" class="rights_menu" value="Transactions"/>
						<b>Transactions</b>
					</label>
					<ul>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Salary Transactions" />
									Salary Transactions
							</label>
							<ul>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Payments" />
											Payments
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Calculate Salary (Contract)" />
											Calculate Salary (Contract)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Mark Attendance" />
											Mark Attendance
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Mark Attendance (Executives)" />
											Mark Attendance (Executives)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Leave Approval" />
											Leave Approval
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Calculate Salary (Executives)" />
											Calculate Salary (Executives)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Opening Balance (Leaves)" />
											Opening Balance (Leaves)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Salary Incentive (Others)" />
											Salary Incentive (Others)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Calculate Salary (Monthly)" />
											Calculate Salary (Monthly)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Leave Application" />
											Leave Application
									</label>								
								</li>

							</ul>
						</li>

						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Purchase Transactions" />
									Purchase Transactions
							</label>
							<ul>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Purchase Order" />
											Purchase Order
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Purchase Order Approve" />
											Purchase Order Approve
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Purchase Order Save and Print" />
											Purchase Order Save and Print
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Purchase New" />
											Purchase New
									</label>								
								</li>
							</ul>
						</li>

						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Quotations" />
									Quotations
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Proforma Invoice" />
									Proforma Invoice
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Orders" />
									Orders
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Despatch" />
									Despatch
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Challan" />
									Challan
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Invoice" />
									Invoice
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Sales Return" />
									Sales Return
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Daily Sales Report (DSR)" />
									Daily Sales Report (DSR)
							</label>								
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Scheduled Appointments" />
									Scheduled Appointments
							</label>								
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Issue / Receive" />
									Issue / Receive
							</label>								
						</li>

					</ul>
				</li>
			</div>


			<div class="col-lg-4 col-sm-4 col-md-4" style="border-right:1px solid gray;height:300px;overflow:scroll">
				<li>
					<label>
						<input type="checkbox" class="rights_menu" value="Production"/>
						<b>Production</b>
					</label>
					<ul>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Stages Master" />
									Stages Master
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Opening Balance" />
									Opening Balance
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Move In Stage" />
									Move In Stage
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Move In Godown" />
									Move In Godown
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Move In Godown Approve" />
									Move In Godown Approve
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Product Status (Desp. Stage)" />
									Product Status (Desp. Stage)
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Product Status" />
									Product Status
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Product Status (Desp. Stage)" />
									Product Status (Desp. Stage)
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Stage Movement Log" />
									Stage Movement Log
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Points To Employee" />
									Points To Employee
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Employee Performance" />
									Employee Performance
							</label>
						</li>
					</ul>
				</li>
			</div>

			<div class="col-lg-4 col-sm-4 col-md-4" style="border-right:1px solid gray;height:300px;overflow:scroll">
				<li>
					<label>
						<input type="checkbox" class="rights_menu" value="Reports"/>
						<b>Reports</b>
					</label>
					<ul>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Salary Reports" />
									Salary Reports
							</label>
							<ul>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Attendance Register" />
											Attendance Register
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Attendance Register (Executives)" />
											Attendance Register (Executives)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Salary Register (Monthly)" />
											Salary Register (Monthly)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Salary Register (Executives)" />
											Salary Register (Executives)
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="ESI Report" />
											ESI Report
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Leave Applications" />
											Leave Applications
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Payments Log" />
											Payments Log
									</label>								
								</li>
								<li>
									<label>
										<input type="checkbox" class="rights_menu" value="Employee Ledger (Monthly)" />
											Employee Ledger (Monthly)
									</label>								
								</li>
							</ul>
						</li>


						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Products List" />
									Products List
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Quotations Log" />
									Quotations Log
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Proforma Invoice Log" />
									Proforma Invoice Log
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Orders Status" />
									Orders Status
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Purchase Report" />
									Purchase Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Despatch Report" />
									Despatch Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Challan Report" />
									Challan Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Invoice Report" />
									Invoice Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Colour-Wise Report" />
									Colour-Wise Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Transporter-Wise Report" />
									Transporter-Wise Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Address Book Report" />
									Address Book Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Stock Report" />
									Stock Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="RO Level Report" />
									RO Level Report
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Product In/Out Log" />
									Product In/Out Log
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="DSR Report" />
									DSR Report
							</label>
						</li>
					</ul>
				</li>
			</div>
			<div class="col-lg-4 col-sm-4 col-md-4" style="border-right:1px solid gray;height:300px;overflow:scroll">
				<li>
					<label>
						<input type="checkbox" class="rights_menu" value="Tools" />
						<b>Tools</b>
					</label>
					<ul>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Create Users" />
								Create Users
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="User Rights" />
								User Rights
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Reset Password" />
								Reset Password
							</label>
						</li>
						<li>
							<label>
								<input type="checkbox" class="rights_menu" value="Backup Data" />
								Backup Data
							</label>
						</li>
					</ul>
				</li>
			</div>

			<div class="col-lg-4 col-sm-4 col-md-4" style="border-right:1px solid gray;height:300px;overflow:auto">
				<li>
					<label style1="color:black;">
						<!-- <input type="checkbox" class="rights_menu" value="Tools" /> -->
						<b>Access to Users</b>
					</label>
					<?php
						foreach ($users4ab as $key => $value) 
						{
					?>
					<div id="divUsers4ab" class="checkbox" style="margin-left:40px;">
						<input type="checkbox" id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
						<label  style="color: black; margin-left:-10px;" for='<?php echo $key;?>'><?php echo $value;?></label>
					</div>
					<?php
						}
					?>

				</li>
			</div>

		</div>
	</ul>
	</div>
	<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/checktree/js/jquery-checktree.js"></script>
	<script>
		$('#tree').checktree();
	</script>
	<br/>
	<div class="container">
	<div class="row" style="margin-bottom:15px;">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
		<?php
			echo "<input type='button' onclick='loaddata();' value='Save' id='btnSave' class='btn btn-danger btn-block'>";
			echo form_close();
		?>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
		</div>
	</div>
	</div>