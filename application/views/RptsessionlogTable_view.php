<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row" id="container">
		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style='border:1px solid lightgray; padding: 10px; overflow:auto; height:400px;'>
			<table class='table table-hover' id='tbl1'>
			 <!-- <caption></caption> -->
			 <thead>

			 <tr>
				 <th>S.No.</th>
				 <th>User</th>
				 <th>Login Date</th>
				 <th>Login Time</th>
				 <th>Logout Date</th>
				 <th>Logout Time</th>
			 </tr>
			 </thead>
			 <tbody>
			 <?php 

			 $i = 1;
			 foreach ($records as $row) 
			 {

			 	echo "<tr>";						//editThis('.$rw.', \"'.$acsgname.'\");
			 	echo "<td>".$i."</td>";
			 	echo "<td>".$row['uid']."</td>";
			 	$dt = strtotime($row['loginstamp']);
			 	$tm = '';
			 	if($dt != '')
			 	{
			 		$dt = date('d-M-Y', $dt);
			 		$tm = date("h:i:s a",strtotime($row['loginstamp']));
			 	}
			 	echo "<td>".$dt."</td>";
			 	echo "<td>".$tm."</td>";

			 	$dt = strtotime($row['logoutstamp']);
			 	$tm = '';
			 	if($dt != '')
			 	{
			 		$dt = date('d-M-Y', $dt);
			 		$tm = date("h:i:s a",strtotime($row['logoutstamp']));
			 	}
			 	echo "<td>".$dt."</td>";
			 	echo "<td>".$tm."</td>";

				echo "</tr>";
				$i++;
			 }			 	

			 


			// onclick="delrow('.$rw.');"

			 ?>

			 </tbody>
			</table>
		</div>
		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-0">
		</div>
	</div>


	 <!-- JQuery -->
	<script>
		$(document).ready( function () {
		    $('#tbl1').DataTable({
			    paging: true,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
			});
		} );

	</script>



	