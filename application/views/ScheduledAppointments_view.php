<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='ScheduledAppointments_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].dsrRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(2); ///
	          cell.innerHTML = dateFormat(new Date(records[i].nextDt));
	          // cell.style.display="none";
	          // var cell = row.insertCell(3);
	          // cell.innerHTML = records[i].nextDt;
	          var cell = row.insertCell(3); ///
	          cell.innerHTML = records[i].nextTm;
	          // cell.style.display="none";
	          var cell = row.insertCell(4);  /// 
	          cell.innerHTML = records[i].userRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5); ///
	          cell.innerHTML = records[i].uid;
	          // 
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].abRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].addr;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].mobile1;
	  	  }
		$("#tbl1 tr").on("click", highlightRowAlag);
	}

	

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		// alert(dtTo);

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
						// alert(JSON.stringify(data));
							setTable(data['records'], data['employerContri']) 
							alertPopup('Records loaded...', 6000);
				}
		});
		
	}



	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        // data[i][ii] = $(this).text();
		        data[i][ii] = $(this).text().replace(/(\r\n|\n|\r)/gm,", ");
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}


</script>
<div class="acontainer">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h1 class="text-center" style='margin-top:-20px'>Scheduled Appointments</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
							$("#dtFrom").val(dateFormat(new Date()));
						</script>					
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:500px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
							 	<td style='display:none1; font-weight: bold;'>S.N.</td>
							 	<td style='display:none; font-weight: bold;'>dsrRowId</td>
							 	<td style='display:none1; font-weight: bold;'>Date</td>
							 	<td style='font-weight: bold;'>Time</td>
							 	<td style='display:none;font-weight: bold;'>UserRowId</td>
							 	<td style='display:none1;font-weight: bold;'>User</td>
							 	<td style='display:none;font-weight: bold;'>AbRowId</td>
							 	<td style='font-weight: bold;'>Party</td>
							 	<td style='font-weight: bold;'>Address</td>
							 	<td style='font-weight: bold;'>Mobile</td>
							 </tr>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>



	<div class="row" style="margin-top:30px;" >
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>

	</div>
</div>


