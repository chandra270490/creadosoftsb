<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js">
	</script> -->

<script type="text/javascript">
	var controller='RptAddressbook_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display='none';

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].abRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.style.display='none';

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].abRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].prefixTypeRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].prefixType;
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].abType;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].labelName;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].addr;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].townRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].townName;

	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].districtName;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].stateName;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].pin;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].landLine1;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].landLine2;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].mobile1;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].mobile2;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].email1;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].email2;
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].pan;
	          var cell = row.insertCell(21);
	          cell.innerHTML = records[i].din;
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].passport;
	          var cell = row.insertCell(23);
	          if( records[i].passportValidUpto == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].passportValidUpto));
	          }
	          var cell = row.insertCell(24);
	          cell.innerHTML = records[i].drivingLic;
	          var cell = row.insertCell(25);
	          if( records[i].drivingLicValidUpto == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].drivingLicValidUpto));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].drivingLicValidUpto));
	          var cell = row.insertCell(26);
	          cell.innerHTML = records[i].familyCard;
	          var cell = row.insertCell(27);
	          if( records[i].familyCardValidUpto == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].familyCardValidUpto));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].familyCardValidUpto));
	          var cell = row.insertCell(28);
	          if( records[i].dob == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].dob));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].dob));
	          var cell = row.insertCell(29);
	          if( records[i].dom == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].dom));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].dom));
	          var cell = row.insertCell(30);
	          cell.innerHTML = records[i].tinState;
	          var cell = row.insertCell(31);
	          cell.innerHTML = records[i].tinCentre;
	          var cell = row.insertCell(32);
	          cell.innerHTML = records[i].contactPerson;
	          var cell = row.insertCell(33);
	          cell.innerHTML = records[i].contactMobile;
	          var cell = row.insertCell(34);
	          cell.innerHTML = records[i].contactDepartment;
	          var cell = row.insertCell(35);
	          cell.innerHTML = records[i].contactDesignation;
	          var cell = row.insertCell(36);
	          cell.innerHTML = records[i].contactPerson2;
	          var cell = row.insertCell(37);
	          cell.innerHTML = records[i].contactMobile2;
	          var cell = row.insertCell(38);
	          cell.innerHTML = records[i].contactDepartment2;
	          var cell = row.insertCell(39);
	          cell.innerHTML = records[i].contactDesignation2;
	          var cell = row.insertCell(40);
	          cell.innerHTML = records[i].contactPerson3;
	          var cell = row.insertCell(41);
	          cell.innerHTML = records[i].contactMobile3;
	          var cell = row.insertCell(42);
	          cell.innerHTML = records[i].contactDepartment3;
	          var cell = row.insertCell(43);
	          cell.innerHTML = records[i].contactDesignation3;
	          var cell = row.insertCell(44);
	          cell.innerHTML = records[i].contactPriority;
	          var cell = row.insertCell(45);
	          cell.innerHTML = records[i].contactType;
	          var cell = row.insertCell(46);
	          cell.innerHTML = records[i].refName;

	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: true,
		    iDisplayLength: 5,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    initComplete: function () {
		            this.api().columns().every( function () {
		                var column = this;

		                var select = $('<select><option value=""></option></select>')
		                    .appendTo( $(column.footer()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		 
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		 
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value="'+d+'">'+d+'</option>' )
		                } );

		                
		            } );
		        }

		});
		} );

		// $("#tbl1 tr").on("click", highlightRow);
		// $('#tbl1 tr').each(function(){
		// 	$(this).bind('click', highlightRow);
		// });
			
	}

	function loadData()
	{	
		abType = $("#cboType").val();
		$.ajax({
			'url': base_url + '/' + controller + '/showData',
			'type': 'POST',
			'dataType': 'json',
			'data': {
						'abType': abType
					},
			'success': function(data)
			{
				if(data)
				{
					// alert(JSON.stringify(data));
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
				}
			}
		});
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	    		// tmpAddress = $(tr).find('td:eq(8)').text().replace(/(\r\n|\n|\r)/gm,"");
	        	TableData[i]=
	        	{
		            "prefix" : $(tr).find('td:eq(4)').text()
		            , "name" : $(tr).find('td:eq(5)').text()
		            , "abType" :$(tr).find('td:eq(6)').text()
		            , "labelName" :$(tr).find('td:eq(7)').text()
		            , "addr" : $(tr).find('td:eq(8)').text().replace(/(\r\n|\n|\r)/gm,", ")
		            , "townName" :$(tr).find('td:eq(10)').text()
		            , "districtName" :$(tr).find('td:eq(11)').text()
		            , "stateName" :$(tr).find('td:eq(12)').text()
		            , "pin" :$(tr).find('td:eq(13)').text()
		            , "landLine1" :$(tr).find('td:eq(14)').text()
		            , "landLine2" :$(tr).find('td:eq(15)').text()
		            , "mobile1" :$(tr).find('td:eq(16)').text()
		            , "mobile2" :$(tr).find('td:eq(17)').text()
		            , "email1" :$(tr).find('td:eq(18)').text()
		            , "email2" :$(tr).find('td:eq(19)').text()
		            , "pan" :$(tr).find('td:eq(20)').text()
		            , "din" :$(tr).find('td:eq(21)').text()
		            , "passport" :$(tr).find('td:eq(22)').text()
		            , "passportValidUpto" :$(tr).find('td:eq(23)').text()
		            , "drivingLic" :$(tr).find('td:eq(24)').text()
		            , "drivingLicValidUpto" :$(tr).find('td:eq(25)').text()
		            , "familyCard" :$(tr).find('td:eq(26)').text()
		            , "familyCardValidUpto" :$(tr).find('td:eq(27)').text()
		            , "dob" :$(tr).find('td:eq(28)').text()
		            , "dom" :$(tr).find('td:eq(29)').text()
		            , "tinState" :$(tr).find('td:eq(30)').text()
		            , "tinCentre" :$(tr).find('td:eq(31)').text()
		            , "contactPerson" :$(tr).find('td:eq(32)').text()
		            , "contactMobile" :$(tr).find('td:eq(33)').text()
		            , "contactDepartment" :$(tr).find('td:eq(34)').text()
		            , "contactDesignation" :$(tr).find('td:eq(35)').text()
		            , "contactPerson2" :$(tr).find('td:eq(36)').text()
		            , "contactMobile2" :$(tr).find('td:eq(37)').text()
		            , "contactDepartment2" :$(tr).find('td:eq(38)').text()
		            , "contactDesignation2" :$(tr).find('td:eq(39)').text()
		            , "contactPerson3" :$(tr).find('td:eq(40)').text()
		            , "contactMobile3" :$(tr).find('td:eq(41)').text()
		            , "contactDepartment3" :$(tr).find('td:eq(42)').text()
		            , "contactDesignation3" :$(tr).find('td:eq(43)').text()
		            , "contactPriority" :$(tr).find('td:eq(44)').text()
		            , "contactType" :$(tr).find('td:eq(45)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// alert(tblRowsCount);
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}


		var abType = $("#cboType option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'abType': abType
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container1">
<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
	<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
	</div>
	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		<h3 class="text-center" style='margin-top:-20px'>Address Book Report</h3>
		<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- ALL ---';
						$types['I'] = "Individual";
						$types['N'] = "Non Individual";
						echo "<label style='color: black; font-weight: normal;'>Type: <span style='color: red;'>*</span></label>";
						echo form_dropdown('cboType', $types, '-1', "class='form-control' id='cboType'");
					?>
				</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
					?>
				</div>
			</div>
		</form>
	</div>
	<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
	</div>
</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:430px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' class="editRecord text-center">Edit</th>
					 	<th style='display:none;' class="text-center">Delete</th>
						<th style='display:none;'>rowid</th>
					 	<th style='display:none;'>PrefixRowId</th>
					 	<th>Prefix</th>
					 	<th>Name</th>
					 	<th>Type</th>
					 	<th>Label Name</th>
					 	<th>Address</th>
					 	<th style='display:none;'>TownRowId</th>
					 	<th>Town</th>
					 	<th>District</th>
					 	<th>State</th>
					 	<th>Pin</th>
					 	<th>Land Line1</th>
					 	<th>Land Line2</th>
					 	<th>Mobile1</th>
					 	<th>Mobile2</th>
					 	<th>Email1</th>
					 	<th>Email2</th>
					 	<th>PAN</th>
					 	<th>DIN</th>
					 	<th>Passport#</th>
					 	<th>Passport Dt.</th>
					 	<th>Dl#</th>
					 	<th>Dl Dt.</th>
					 	<th>Family Card#</th>
					 	<th>Famil Card Dt.</th>
					 	<th>DOB</th>
					 	<th>DOM</th>
					 	<th>TIN State</th>
					 	<th>TIN Centre</th>
					 	<th>Contact Person</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Person 2</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Person 3</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Priority</th>
					 	<th>Contact Type</th>
					 	<th>Ref.</th>
					 </tr>
				 </thead>
				 <tfoot>
					 <tr>
					 	<th style='display:none;' class="editRecord text-center">Edit</th>
					 	<th style='display:none;' class="text-center">Delete</th>
						<th style='display:none;'>rowid</th>
					 	<th style='display:none;'>PrefixRowId</th>
					 	<th>Prefix</th>
					 	<th>Name</th>
					 	<th>Type</th>
					 	<th>Label Name</th>
					 	<th>Address</th>
					 	<th style='display:none;'>TownRowId</th>
					 	<th>Town</th>
					 	<th>District</th>
					 	<th>State</th>
					 	<th>Pin</th>
					 	<th>Land Line1</th>
					 	<th>Land Line2</th>
					 	<th>Mobile1</th>
					 	<th>Mobile2</th>
					 	<th>Email1</th>
					 	<th>Email2</th>
					 	<th>PAN</th>
					 	<th>DIN</th>
					 	<th>Passport#</th>
					 	<th>Passport Dt.</th>
					 	<th>Dl#</th>
					 	<th>Dl Dt.</th>
					 	<th>Family Card#</th>
					 	<th>Famil Card Dt.</th>
					 	<th>DOB</th>
					 	<th>DOM</th>
					 	<th>TIN State</th>
					 	<th>TIN Centre</th>
					 	<th>Contact Person</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Person 2</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Person 3</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Priority</th>
					 	<th>Contact Type</th>
					 	<th>Ref.</th>
					 </tr>
				 </tfoot>				 
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Limited Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportCompleteAddressBook();' value='Export Complete Address Book' id='btnExportAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
	
</div>




<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: true,
		    iDisplayLength: 5,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    initComplete: function () {
		            this.api().columns().every( function () {
		                var column = this;
		                
		                var select = $('<select><option value=""></option></select>')
		                    .appendTo( $(column.footer()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		 
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		 
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value="'+d+'">'+d+'</option>' )
		                } );

		                
		            } );
		        }

		});
		} );

</script>

<script type="text/javascript">
	function exportCompleteAddressBook()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/exportCompleteAddressBook',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': 'TableData'
							, 'abType': 'abType'
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
	}
</script>