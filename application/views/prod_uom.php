<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3 style="text-align:center"><i class="fa fa-laptop"></i>UOM Master</h3>
        </div> 
    </div>
    <?php
        $prod_uom_id = $_REQUEST["id"];

        if($prod_uom_id != ""){
            $sql_prev_det = "select * from prod_uom_mst where prod_uom_id = '".$prod_uom_id."'";
            $qry_prev_det = $this->db->query($sql_prev_det)->row();
            $prod_uom_name = $qry_prev_det->prod_uom_name;
        } else {
            $prod_uom_name = "";
        }
    ?>
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/Products_Controller/prod_uom_entry">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="color:black">UOM</label>
                            <div class="col-sm-10">
                                <input type="hidden" id="prod_uom_id" name="prod_uom_id" value="<?=$prod_uom_id;?>">
                                <input type="text" class="form-control" id="prod_uom_name" name="prod_uom_name" value="<?=$prod_uom_name;?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <table class="table table-bordered">
                <thead>
                    <th>UOM Id</th>
                    <th>UOM Name</th>
                    <th>Created Date</th>
                </thead>
                <tbody>
                    <?php
                        $sql_det = "select * from prod_uom_mst order by created_date desc";
                        $qry_det = $this->db->query($sql_det);
                        foreach($qry_det->result() as $row){
                    ?>
                    <tr>
                        <td><?=$row->prod_uom_id;?></td>
                        <td><?=$row->prod_uom_name;?></td>
                        <td><?=$row->created_date;?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4"></div>
    </div>
  </section>
</section>