<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='IncomeTaxSlabs_Controller';
	var base_url='<?php echo site_url();?>';

	// function setTable(records)
	// {
	// 	 // alert(JSON.stringify(records));
	// 	 var burl = '<?php echo base_url();?>';
	// 	  $("#tbl1").empty();
	//       var table = document.getElementById("tbl1");
	//       for(i=0; i<records.length; i++)
	//       {
	//           newRowIndex = table.rows.length;
	//           row = table.insertRow(newRowIndex);


	//           var cell = row.insertCell(0);
	//           cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	//           cell.style.display="none";
	//           cell.style.textAlign = "center";
	//           cell.style.color='lightgray';
	//           cell.setAttribute("onmouseover", "this.style.color='green'");
	//           cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	//           cell.className = "editRecord";

	//           var cell = row.insertCell(1);
	// 		  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	// 		  // cell.style.display="none";
	//           cell.style.textAlign = "center";
	//           cell.style.color='lightgray';
	//           cell.setAttribute("onmouseover", "this.style.color='red'");
	//           cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	//           cell.setAttribute("onclick", "delrowid(" + records[i].productRowId +")");
	//           // data-toggle="modal" data-target="#myModal"
	//           cell.setAttribute("data-toggle", "modal");
	//           cell.setAttribute("data-target", "#myModal");

	//           var cell = row.insertCell(2);
	//           cell.style.display="none";
	//           cell.innerHTML = records[i].productRowId;
	//           var cell = row.insertCell(3);
	//           cell.innerHTML = records[i].productName;
	//           var cell = row.insertCell(4);
	//           cell.innerHTML = records[i].productCategoryRowId;
	//           cell.style.display="none";
	//           var cell = row.insertCell(5);
	//           cell.innerHTML = records[i].productCategory;
	//           var cell = row.insertCell(6);
	//           cell.innerHTML = records[i].productLength;
	//           var cell = row.insertCell(7);
	//           cell.innerHTML = records[i].productHeight;
	//           var cell = row.insertCell(8);
	//           cell.innerHTML = records[i].productWidth;
	//           var cell = row.insertCell(9);
	//           cell.innerHTML = records[i].uom;
	//           var cell = row.insertCell(10);
	//           cell.innerHTML = records[i].productRate;
	//           var cell = row.insertCell(11);
	//           cell.innerHTML = records[i].imagePath;
	//           cell.style.display="none";
	//           var cell = row.insertCell(12);
	//           if( records[i].imagePathThumb != "N" )
	//           {
	// 			  var pic = burl + "bootstrap/images/products/" + records[i].imagePathThumb;
	// 	          cell.innerHTML = "<img id=\'" + records[i].productRowId + "\' src=\'" + pic + "\' width='60px' alt='' />";
	//       	  }
	//       	  else
	//       	  {
	//       	  	  cell.innerHTML = "<img src='' width='60px' alt='' />";
	//       	  }
	//           var cell = row.insertCell(13);
	//           cell.innerHTML = records[i].imagePathThumb;
	//           cell.style.display="none";
	//   	  }
	// }




	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
        	TableData[i]=
        	{
	            "lowerRange" : $(tr).find('td:eq(3)').text()
	            , "upperRange" : $(tr).find('td:eq(4)').text()
	            , "rate" :$(tr).find('td:eq(5)').text()
	            , "calculatedFigure" :$(tr).find('td:eq(6)').text()
        	}   
        	i++; 
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveChanges()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("Nothing to save...", 5000);
			$("#txtLower").focus();
			return;
		}

		surcharge = $("#txtSurcharge").val();
		cess = $("#txtCess").val();

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'surcharge': surcharge
							, 'cess': cess
						},
				'success': function(data)
				{
					alert(data);
					if(data)
					{
						location.reload();
					}
				}
		});
		
	}

	function confirm()
	{
		  // $("#tbl1").empty();
		  // $("#tbl1").find("tr:gt(0)").remove();

		  var lowerRange = $("#txtLower").val();
		  if(lowerRange < 0)
	      {
	      	alertPopup("Invalid lower range...", 5000);
	      	$("#txtLower").focus();
	      	return;
	      }

		  var upperRange = $("#txtUpper").val();
		  if(upperRange <= 0)
	      {
	      	alertPopup("Invalid upper range...", 5000);
	      	$("#txtUpper").focus();
	      	return;
	      }

	      var rate = $("#txtRate").val();
		  if(rate < 0)
	      {
	      	alertPopup("Invalid rate...", 5000);
	      	$("#txtRate").focus();
	      	return;
	      }


		  var flag=0;
		  $('#tbl1 tr').each(function(row, tr)
		  {
		    	if ( parseInt($(tr).find('td:eq(3)').text()) == parseInt(lowerRange) && parseInt($(tr).find('td:eq(4)').text()) == parseInt(upperRange) )
		    	{
		    		flag=1;
			    }
		   }); 
		  if( flag == 1)
		  {
		  	alertPopup("This range is already added...", 6000);
			$("#txtLower").focus();
			return;
		  }


	      var table = document.getElementById("tbl1");
          newRowIndex = table.rows.length;
          row = table.insertRow(newRowIndex);


          var cell = row.insertCell(0);
          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
          cell.style.display="none";
          cell.style.textAlign = "center";
          cell.style.color='lightgray';
          cell.setAttribute("onmouseover", "this.style.color='green'");
          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
          cell.className = "editRecord";

          var cell = row.insertCell(1);
		  cell.innerHTML = "<span onClick='delTableRow(this, 1);' style='color: lightgray;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'red\';\"  onmouseout=\"this.style.color=\'lightgray\';\" class='glyphicon glyphicon-remove'></span>";
		  cell.style.textAlign = "center";

           var cell = row.insertCell(2);
		  cell.innerHTML = "";
		  cell.style.display="none";

           var cell = row.insertCell(3);
		  cell.innerHTML = $("#txtLower").val();

           var cell = row.insertCell(4);
		  cell.innerHTML = $("#txtUpper").val();

           var cell = row.insertCell(5);
		  cell.innerHTML = $("#txtRate").val();

           var cell = row.insertCell(6);
		  cell.innerHTML = "";

		  $("#txtLower").val('0');
	      $("#txtUpper").val('0');
	      $("#txtRate").val('0');
	      calculateFigure();
	}

	function delTableRow(x, y)
	{
	    var totalRows = $('#tbl1 tr').length;
	    if(y=="1")
	    {
	    	rowToDelete = x.parentNode.parentNode.rowIndex;
	    }
	    else
	    {
	    	rowToDelete = x.parentNode.rowIndex;
	    }

	    if( rowToDelete != (totalRows-1))
	    {
	    	alertPopup("You can delete last row only...",8000);
	    	return;
	    }
	    // alert(totalRows + "  " + rowToDelete);
	    // return;
	    var table = document.getElementById("tbl1");
	    table.deleteRow(rowToDelete);
	    calculateFigure();
	}

	function calculateFigure()
	{
		var totCalc = 0;
		var totalRows = $('#tbl1 tr').length;
		for( i=1; i<totalRows; i++)
		{
			if (i>1)
			{
				totCalc += parseFloat( $('#tbl1').find("tr:eq(" + i + ")").find("td:eq(6)").text() );
			}
			var lowerRange = $('#tbl1').find("tr:eq(" + i + ")").find("td:eq(3)").text();
			var upperRange = $('#tbl1').find("tr:eq(" + i + ")").find("td:eq(4)").text();
			var rate = $('#tbl1').find("tr:eq(" + i + ")").find("td:eq(5)").text();

			var calc = totCalc + ( (upperRange - lowerRange) * rate / 100 );

			$('#tbl1').find("tr:eq(" + (i+1) + ")").find("td:eq(6)").text( calc.toFixed(0) );
			// alert(calc);
		}
		// alert(totalRows);
		// return
	  // $('#tbl1 tr').each(function(row, tr)
	  // {
	  //   	if ( parseInt($(tr).find('td:eq(1)').text()) == parseInt(productRowId) && parseInt($(tr).find('td:eq(7)').text()) == parseInt(colourRowId) )
	  //   	{
	  //   		flag=1;
		 //    }
	  //  }); 
	}
</script>
<div class="container">
	<?php 
		$surcharge = 0;
		$cess = 0;
		if( count($records) > 0 )
		{
			$surcharge = $records[0]['surcharge'];
			$cess = $records[0]['cess'];
		}
	?>
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Income Tax Slabs</h1>
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Lower Range:</label>";
							echo '<input type="number" name="txtLower" value="0" placeholder="" class="form-control" maxlength="10" id="txtLower" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Upper Range:</label>";
							echo '<input type="number" name="txtUpper" value="0" placeholder="" class="form-control" maxlength="10" id="txtUpper" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Rate(%):</label>";
							echo '<input type="number" name="txtRate" value="0" placeholder="" class="form-control" maxlength="10" id="txtRate" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='confirm();' value='Confirm' id='btnConfirm' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>itSlabRowId</th>
					 	<th>Lower Range</th>
						<th style='display:none1;'>Upper Range</th>
					 	<th>Rate</th>
					 	<th>Calc. Figure</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						 if( count($records) > 0 )
						 {
							foreach ($records as $row) 
							{
							 	$rowId = $row['itSlabRowId'];
							 	echo "<tr>";						//onClick="editThis(this);
								echo '<td style="display:none; color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
									   <td style="color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delTableRow(this);" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
							 	echo "<td style='width:0px;display:none;'>".$row['itSlabRowId']."</td>";
							 	echo "<td>".$row['lowerRange']."</td>";
							 	echo "<td>".$row['upperRange']."</td>";
							 	echo "<td>".$row['rate']."</td>";
							 	echo "<td>".$row['calculatedFigure']."</td>";
								echo "</tr>";
							}

							$surcharge = $records[0]['surcharge'];
							$cess = $records[0]['cess'];
						 }
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>

	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;width:83%;margin-left:8%;box-shadow: 5px 5px #d3d3d3;border-radius:25px;margin-top:2%">
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Surcharge:</label>";
							echo '<input type="number" name="txtSurcharge" value='.$surcharge .' placeholder="" class="form-control" maxlength="10" id="txtSurcharge" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>CESS:</label>";
							echo '<input type="number" name="txtCess" value='.$cess.' placeholder="" class="form-control" maxlength="10" id="txtCess" />';
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='saveChanges();' value='Save Changes' id='btnConfirm' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>
</div>





<script type="text/javascript">


		// $(document).ready( function () {
		//     myDataTable = $('#tbl1').DataTable({
		// 	    paging: false,
		// 	    iDisplayLength: -1,
		// 	    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// 	});
		// } );




	
</script>