<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Purchase_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].poRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].poRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vNo;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.style.display="none";
	          // cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(9);
	          // cell.innerHTML = records[i].totalQty;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;	
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}

	function setTableProducts(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tblProducts").find("tr:gt(0)").remove();
	      var table = document.getElementById("tblProducts");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);
	          var cell = row.insertCell(0);
	          cell.innerHTML = records[i].podetailRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].vType;
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].vNo;
	          // cell.style.display="none";
	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].orderQty;
	          // cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].pendingQty;
	          var cell = row.insertCell(8);
	          cell.innerHTML = '0';
	          cell.style.color="red";
	          cell.setAttribute("contentEditable", true);

	          var cell = row.insertCell(9);
	          cell.innerHTML = "";
	          cell.style.color="blue";
	          cell.setAttribute("contentEditable", true);
	          var cell = row.insertCell(10);
	          cell.innerHTML = "";
	          cell.style.color="blue";
	          cell.setAttribute("contentEditable", true);
	  	  }
			
	}

	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					// alert(JSON.stringify(data));
					if(data)
					{
						if( data == "cannot")
						{
							alertPopup('Cannot cancel, dependent rows exists...', 7000);
						}
						else
						{
							setTable(data['records']);
							alertPopup('Record deleted...', 4000);
							location.reload();
							// $("#btnShow").trigger("click");
							// blankControls();
							// $("#txtLetterNo").focus();
							// $("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
				}
			});
	}
	

	var tblRowsCount;
	var receivedMoreThanPending = 0;
	var billNoOrBillDtMissing = 0;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	if( $(tr).find('td:eq(8)').text() > 0 )
	    	{
	    		if( parseFloat($(tr).find('td:eq(8)').text()) > parseFloat($(tr).find('td:eq(7)').text()) )
	    		{
	    			receivedMoreThanPending = 1;
	    		}
	    		if( $(tr).find('td:eq(9)').text().trim() == "" || $(tr).find('td:eq(10)').text().trim() == "" || $(tr).find('td:eq(9)').text().trim().length > 39  || $(tr).find('td:eq(10)').text().trim().length > 19 )
	    		{
	    			// alert();
	    			billNoOrBillDtMissing = 1;
	    		}
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(0)').text()
		            , "poVno" : $(tr).find('td:eq(2)').text()
		            , "productRowId" : $(tr).find('td:eq(4)').text()
		            , "receivedQty" : $(tr).find('td:eq(8)').text()
		            , "billNo" : $(tr).find('td:eq(9)').text()
		            , "billDt" : $(tr).find('td:eq(10)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i;
	    return TableData;
	}


	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		if(tblRowsCount <= 0)
		{
			alert("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		if( receivedMoreThanPending == 1)
		{
			alert("Despatching more than pending...", 8000);
			receivedMoreThanPending = 0;
			return;
		}
		if( billNoOrBillDtMissing == 1)
		{
			alert("Bill No Or Bill Dt Missing...\nOR length of bill no or bill dt is too long");
			billNoOrBillDtMissing = 0;
			return;
		}

		var vDt = $("#dtPi").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dtPi");
			if(dtOk == false)
			{
				alert("Invalid date...", 5000);
				// $("#dtPi").focus();
				return;
			}
		// }

		partyRowId = globalPartyRowId;
		if(partyRowId == "")
		{
			alert("Select party...", 8000);
			// $("#cboParty").focus();
			return;
		}

		// totalAmt = $("#txtTotalAmt").val();
		// totalQty = $("#txtTotalQty").val();
		
		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'vDt': vDt
								// , 'partyName': partyName
								// , 'totalAmt': totalAmt
								, 'TableData': TableData
							},
					'success': function(data)
					{
						if(data)
						{
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 8000);
							}
							else
							{

								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								location.reload();
								// $("#btnShow").trigger("click");
								// blankControls();
								// $("#tblProducts").find("tr:gt(0)").remove(); 
								// $("#dtPi").val(dateFormat(new Date()));
							}
						}
					}
			});
		}

	}

	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					JSON.stringify(data);
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}

	var globalPartyRowId="";
	var globalPoVno="-1";
	function showData()
	{
		partyRowId = $(this).closest('tr').children('td:eq(0)').text();
		globalPartyRowId=partyRowId;
		globalPoVno = $(this).closest('tr').children('td:eq(2)').text();
		// alert(globalPartyRowId);
		// return;
		// partyRowId = $("#cboParty").val();
		if(partyRowId == "")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}	

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'partyRowId': partyRowId
							, 'poVno': globalPoVno
						},
				'success': function(data)
				{
					// alert(JSON.stringify(data));
					if(data)
					{
						
						setTableProducts(data['pendingProducts']) ///loading records in tbl1
						alertPopup('Record loaded...', 4000);
					}
				}
		});			
	}

</script>
<div class="container">
	<div class="row">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Purchase</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dtPi', '', "class='form-control' placeholder='' id='dtPi' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtPi" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dtPi").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

	          	</div>
					
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				</div> 
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">
					<div id="divTableParties" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:200px; overflow:auto;">
						<table class='table table-hover' id='tblParties'>
						 <thead>
							 <tr>
								<th style='display:none;'>partyRowId</th>
							 	<th>Party</th>
							 	<th>PO vNo</th>
							 </tr>
						 </thead>
						 <tbody>
							 <?php 
							 // print_r($parties);
								foreach ($parties as $row) 
								{
								 	$rowId = $row['partyRowId'];
								 	echo "<tr>";						//onClick="editThis(this);

								 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
								 	echo "<td style='display:none1;'>".$row['name']."</td>";
								 	echo "<td style='display:none1;'>".$row['poVno']."</td>";
									echo "</tr>";
								}
							 ?>
						 </tbody>
						</table>
					</div>				
				</div> 
			</div>

			<div class="row" style="margin-top:15px;display: none;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Address:</label>";
						echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtAddress'  maxlength='255' value=''");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="row">
							
				    </div>			
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					    </div>	
				    </div>			
	          	</div>
			</div>

			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:0 dashed lightgray; padding: 10px;height:200px; overflow:auto;">
					<table class='table table-bordered' id='tblProducts'>
						<tr>
						 	<th  style='display:none;' width="50" class="text-center">PoDetailRow Id</th>
							<th style='display:none1;'>vType</th>
						 	<th style='display:none1;'>vNo</th>
						 	<th>Dt</th>
						 	<th style='display:none;'>Product RowId</th>
						 	<th>Product</th>
						 	<th>Odr Qty</th>
						 	<th>Pend Qty</th>
						 	<th>Recd Qty</th>
						 	<th>Bill#</th>
						 	<th>Bill Dt.</th>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row" style="margin-top:1px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
	          	</div>

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">

	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>poRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none;'>Total Amt</th>
					 	<th style='display:none;'>Total Qty</th>
					 	<th>V.No.</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="display:none;color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td style='display:none;'>".$row['totalAmt']."</td>";
						 	echo "<td style='display:none;'>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});

		$("#tblParties tr").on("click", highlightRow);
		$("#tblParties tr").on("click", showData);
	} );


</script>