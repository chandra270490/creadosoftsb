<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptStock_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 var burl = '<?php echo base_url();?>';
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.style.display="none";
	          cell.innerHTML = records[i].productRowId;

	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].productName;

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].currentQty;

	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productCategory;

	  	  }


		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);	
	}


	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	

		// productCategoryRowId = $("#cboProductCategories").val();
		// if(productCategoryRowId == "-1")
		// {
		// 	alertPopup("Select category...", 8000);
		// 	$("#cboProductCategories").focus();
		// 	return;
		// }

		var productCategoryRowId="";
		$('input:checked.chkProductCategories').each(function() 
		{
			productCategoryRowId = $(this).val() + "," + productCategoryRowId;
		});
		if(productCategoryRowId == "")
		{
			alertPopup("Select product category...", 8000);
			return;
		}
		productCategoryRowId = productCategoryRowId.substr(0, productCategoryRowId.length-1);

		// alert(productCategoryRowId);
		// return;

		$("#tbl1").empty();
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productCategoryRowId': productCategoryRowId
						},
				'success': function(data)
				{
					// alert(JSON.stringify(data));
					if(data)
					{
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}



	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "productRowId" : $(tr).find('td:eq(0)').text()
		            , "productName" : $(tr).find('td:eq(1)').text()
		            , "inHandQty" :$(tr).find('td:eq(2)').text()
		            , "category" :$(tr).find('td:eq(3)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}

		productCategory = $("#cboProductCategories option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'productCategory': productCategory
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Stock Report</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<!-- <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1',"class='form-control' id='cboProductCategories'");
		              	?>
		          	</div> -->
		          	<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllProductCategories">
							<label for="chkAllProductCategories" style="font-weight:bold;color:black;">All Products Categories
						</span>
						
						<?php
							foreach ($productCategories as $key => $value) 
							{
						?>
								<div id='<?php echo "dP".$key;?>' class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkProductCategories" txt='<?php echo $value;?>' id='<?php echo "P".$key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo  "P".$key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>productRowId</th>
					 	<th>Product Name</th>
					 	<th>In Hand Qty</th>
					 	<th>Category</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

	</div>
</div>





<script type="text/javascript">


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );




	$(document).ready(function() {
	   // $("#cboProductCategories").append('<option value="ALL">ALL</option>');
	   // var opt = "<option value='ALL'>ALL</option>";
	   // var idx=2;
	   // $(opt).insertBefore("#cboProductCategories option:nth-child(" + idx + ")");

	   $("#dP-1").remove();

	   // add multiple select / deselect functionality
		$("#chkAllProductCategories").click(function () {
			// alert();
			  $('.chkProductCategories').prop('checked', this.checked);
		});	


		// if all checkbox are selected, check the selectall checkbox
		// and viceversa
		$('.chkProductCategories').bind('click', chkSelectAllProductCategories);
		function chkSelectAllProductCategories()
		{
			var x = parseInt($(".chkProductCategories").length);
			var y = parseInt($(".chkProductCategories:checked").length);
			if( x == y ) 
			{
				$("#chkAllProductCategories").prop("checked", true);
			} 
			else 
			{
				$("#chkAllProductCategories").prop("checked", false);
			}
		}	


	  });
</script>