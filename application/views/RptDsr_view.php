<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptDsr_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          cell.innerHTML = records[i].dsrRowId
	          cell.style.display="none";

	          var cell = row.insertCell(1);
	          cell.innerHTML = dateFormat(new Date(records[i].dsrDt));
	          // cell.style.display="none";

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].userRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].uid;
	          // cell.style.display="none";

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].task;

	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].type;
	          // cell.style.display="none";

	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].abRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          // cell.style.display="none";

	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].partyType;
	          // cell.style.display="none";

	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].remarks;

	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].further;

	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].mrRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(12);
		      cell.innerHTML = records[i].meetingRemark;

	      	  if ( records[i].nextDt != null )
		      {
		          var cell = row.insertCell(13);
		          cell.innerHTML = dateFormat(new Date(records[i].nextDt));

		          var cell = row.insertCell(14);
		          cell.innerHTML = records[i].nextTm;		      	
		      }
		      else
		      {
		      	  var cell = row.insertCell(13);
		          cell.innerHTML = "";
		      	  var cell = row.insertCell(14);
		          cell.innerHTML = "";
		      }

			  var cell = row.insertCell(15);
		      cell.innerHTML = records[i].dsr_ls_name;
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		// myDataTable.destroy();
		// $(document).ready( function () {
	 //    myDataTable=$('#tbl1').DataTable({
		//     paging: false,
		//     iDisplayLength: -1,
		//     aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// });
		// } );

		$("#tbl1 tr").on("click", highlightRow);
		// $("#tbl1 tr").on('click', showDetail);
			
	}

	function loadData()
	{	
		var abRowId="";
		$('input:checked.chkParties').each(function() 
		{
			abRowId = $(this).val() + "," + abRowId;
		});
		if(abRowId == "")
		{
			alertPopup("Select colour...", 8000);
			return;
		}
		abRowId = abRowId.substr(0, abRowId.length-1);

		var userRowId="";
		$('input:checked.chkUsers').each(function() 
		{
			userRowId = $(this).val() + "," + userRowId;
		});
		if(userRowId == "")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		userRowId = userRowId.substr(0, userRowId.length-1);
		// alert(abRowId);
		// return;
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var chkleadstatus="";
		$('input:checked.chkleadstatus').each(function() 
		{
			chkleadstatus = $(this).val() + "," + chkleadstatus;
		});
		if(chkleadstatus == "")
		{
			alertPopup("Select Lead Status...", 8000);
			return;
		}
		chkleadstatus = chkleadstatus.substr(0, chkleadstatus.length-1);



		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'abRowId': abRowId
							, 'userRowId': userRowId
							, 'chkleadstatus': chkleadstatus
						},
				'success': function(data)
				{
					if(data)
					{
						// console.log(data);

						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					}
				}
		});
	}


	// var tblRowsCount;
	// function storeTblValues()
	// {
	//     var TableData = new Array();
	//     var i=0;
	//     $('#tbl1 tr').each(function(row, tr)
	//     {
	//     	// if( $(tr).find('td:eq(3)').text() > 0 )
	//     	// {
	//         	TableData[i]=
	//         	{
	// 	            "orderNo" : $(tr).find('td:eq(0)').text()
	// 	            , "orderDate" : $(tr).find('td:eq(1)').text()
	// 	            , "orderType" :$(tr).find('td:eq(2)').text()
	// 	            , "partyName" :$(tr).find('td:eq(3)').text()
	// 	            , "category" :$(tr).find('td:eq(4)').text()
	// 	            , "product" :$(tr).find('td:eq(5)').text()
	// 	            , "colour" :$(tr).find('td:eq(6)').text()
	// 	            , "qty" :$(tr).find('td:eq(7)').text()
	// 	            , "rate" :$(tr).find('td:eq(8)').text()
	// 	            , "amt" :$(tr).find('td:eq(9)').text()
	// 	            , "pendingQty" :$(tr).find('td:eq(10)').text()
	// 	            , "commitmentDate" :$(tr).find('td:eq(11)').text()
	//         	}   
	//         	i++; 
	//         // }
	//     }); 
	//     // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	//     tblRowsCount = i-1;
	//     return TableData;
	// }

	function exportData()
	{	
		// alert();
		// return;
		// var TableData;
		// TableData = storeTblValues();
		// TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		// if(tblRowsCount == 0)
		// {
		// 	alertPopup("No product selected...", 8000);
		// 	$("#cboProducts").focus();
		// 	return;
		// }
		var abRowId="";
		$('input:checked.chkParties').each(function() 
		{
			abRowId = $(this).val() + "," + abRowId;
		});
		if(abRowId == "")
		{
			alertPopup("Select colour...", 8000);
			return;
		}
		abRowId = abRowId.substr(0, abRowId.length-1);

		var userRowId="";
		$('input:checked.chkUsers').each(function() 
		{
			userRowId = $(this).val() + "," + userRowId;
		});
		if(userRowId == "")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		userRowId = userRowId.substr(0, userRowId.length-1);
		// alert(abRowId);
		// return;
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'abRowId': abRowId
							, 'userRowId': userRowId
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
	
</script>
<style type="text/css">
	#style-1::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		border-radius: 1px;
		/*margin-left: 50px;*/
		background-color: #F5F5F5;
	}

	#style-1::-webkit-scrollbar
	{
		width: 12px;
		/*margin: -20px;*/
		background-color: #F5F5F5;
	}

	#style-1::-webkit-scrollbar-thumb
	{
		border-radius: 1px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
		background-color: lightgrey;
	}
</style>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>DSR Report</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">

					<div id="style-1" class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllParties">
							<label for="chkAllParties" style="font-weight:bold;color:black;">All Parties
						</span>
						
						<?php
							foreach ($parties as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkParties" txt='<?php echo $value;?>' id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>

					<div id="style-1" class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllUsers">
							<label for="chkAllUsers" style="font-weight:bold;color:black;">All Users
						</span>
						
						<?php
							foreach ($users as $key => $value) 
							{
						?>
								<div class="checkbox2" style='margin-left:-30px;'>
									<input type="checkbox" class="chkUsers" txt='<?php echo $value;?>' id='<?php echo "u".$key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo "u".$key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
						
					<div id="style-1" class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="CheckAllleadstatus">
							<label for="CheckAllleadstatus" style="font-weight:bold;color:black;">Lead Status
						</span>
						
						<?php
							foreach ($leadstatus as $key => $value) 
							{
						?>
								<div class="checkbox2" style='margin-left:-30px;'>
									<input type="checkbox" class="chkleadstatus" txt='<?php echo $value;?>' id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>

					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style='padding-left:30px;'>
						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>From:</label>";
									echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
				              	?>
				              	<script>
									$( "#dtFrom" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});

								    // Set the Current Date-50 as Default
								    dt=new Date();
		     					    dt.setDate(dt.getDate() - 50);
		   		 					$("#dtFrom").val(dateFormat(dt));
								</script>	
							</div>
						</div>

						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>To:</label>";
									echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
				              	?>
				              	<script>
									$( "#dtTo" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});
								    // Set the Current Date as Default
									$("#dtTo").val(dateFormat(new Date()));
								</script>
							</div>
						</div>
						
						<div class="row" style='margin-top:5px;'>
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
									echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
				              	?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	
	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>dsrRowid</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>userRowId</th>
					 	<th>User</th>
					 	<th>Task</th>
					 	<th>Type</th>
					 	<th style='display:none;'>abRowId</th>
					 	<th>Party</th>
					 	<th>Party Type</th>
					 	<th>Remarks</th>
					 	<th>Further</th>
					 	<th style='display:none;'>MeetingReasonRowId</th>
					 	<th>Reason</th>
					 	<th>Dt.</th>
					 	<th>Time</th>
					 	<th>Lead Status</th>
					 </tr>
				 </thead>
				 <tbody>
					 
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>

</div>





<script type="text/javascript">
		// $(document).ready( function () {
		//     myDataTable = $('#tbl1').DataTable({
		// 	    paging: false,
		// 	    iDisplayLength: -1,
		// 	    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// 	});
		// } );


	// add multiple select / deselect functionality
	$("#chkAllParties").click(function () {
		// alert();
		  $('.chkParties').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkParties').bind('click', chkSelectAll);
	function chkSelectAll()
	{
		var x = parseInt($(".chkParties").length);
		var y = parseInt($(".chkParties:checked").length);
		if( x == y ) 
		{
			$("#chkAllParties").prop("checked", true);
		} 
		else 
		{
			$("#chkAllParties").prop("checked", false);
		}
	}

	// add multiple select / deselect functionality
	$("#chkAllUsers").click(function () {
		// alert();
		  $('.chkUsers').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkUsers').bind('click', chkSelectAllUsers);
	function chkSelectAllUsers()
	{
		var x = parseInt($(".chkUsers").length);
		var y = parseInt($(".chkUsers:checked").length);
		if( x == y ) 
		{
			$("#chkAllUsers").prop("checked", true);
		} 
		else 
		{
			$("#chkAllUsers").prop("checked", false);
		}
	}	

	// add multiple select / deselect functionality
	$("#CheckAllleadstatus").click(function () {
		// alert();
		  $('.chkleadstatus').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkleadstatus').bind('click', chkSelectAllleadstatus);
	function chkSelectAllleadstatus()
	{
		var x = parseInt($(".chkleadstatus").length);
		var y = parseInt($(".chkleadstatus:checked").length);
		if( x == y ) 
		{
			$("#CheckAllleadstatus").prop("checked", true);
		} 
		else 
		{
			$("#CheckAllleadstatus").prop("checked", false);
		}
	}
	
</script>