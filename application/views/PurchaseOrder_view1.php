<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='PurchaseOrder_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].poRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].poRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vNo;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].totalQty;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;	
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].orderValidity;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].paymentTerms;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].freight;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].inspection;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].delayClause;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].modeOfDespatch;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].guarantee;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].insurance;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].specialNote;
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].purchaseOrExpense;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}



	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					// alert(JSON.stringify(data));
					if(data)
					{
						if( data == "cannot")
						{
							alertPopup('Cannot cancel, dependent rows exists...', 7000);
						}
						else
						{
							setTable(data['records']);
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#txtLetterNo").focus();
							$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
				}
			});
	}
	

	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "productRowId" : $(tr).find('td:eq(1)').text()
		            , "productName" : $(tr).find('td:eq(3)').text()
		            , "productRate" :$(tr).find('td:eq(4)').text()
		            , "productQty" :$(tr).find('td:eq(5)').text()
		            , "productAmt" :$(tr).find('td:eq(6)').text()
		            , "productDiscount" :$(tr).find('td:eq(7)').text()
		            , "amtAfterDiscount" :$(tr).find('td:eq(8)').text()
		            , "colour" :$(tr).find('td:eq(9)').text()
		            , "deliveryDt" :$(tr).find('td:eq(10)').text()
		            , "remarks" :$(tr).find('td:eq(11)').text()
		            , "lastPurchaseInfo" :$(tr).find('td:eq(12)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}


	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		var task = $("#cboTask").val();
		if(task == "-1")
		{
			alertPopup("Select task...", 8000);
			$("#cboTask").focus();
			return;
		}

		var vDt = $("#dtPo").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dtPo");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtPo").focus();
				return;
			}
		// }
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		if(partyRowId == "-1")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}

		totalAmt = $("#txtTotalAmt").val();
		totalQty = $("#txtTotalQty").val();

		var orderValidity = $("#txtOrderValidity").val().trim();
		var paymentTerms = $("#txtPaymentTerms").val().trim();
		var freight = $("#txtFreight").val().trim();
		var inspection = $("#txtInspection").val().trim();
		var delayClause = $("#txtDelayClause").val().trim();
		var modeOfDespatch = $("#txtModeOfDespatch").val().trim();
		var guarantee = $("#txtGuarantee").val().trim();
		var insurance = $("#txtInsurance").val().trim();
		var specialNote = $("#txtSpecialNote").val().trim();
		partyName = $("#cboParty option:selected").text();
		// alert(partyName);
		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'vDt': vDt
								, 'totalQty': totalQty
								, 'totalAmt': totalAmt
								, 'orderValidity': orderValidity
								, 'paymentTerms': paymentTerms
								, 'freight': freight
								, 'inspection': inspection
								, 'delayClause': delayClause
								, 'modeOfDespatch': modeOfDespatch
								, 'guarantee': guarantee
								, 'insurance': insurance
								, 'specialNote': specialNote
								, 'partyName': partyName
								, 'TableData': TableData
								, 'task': task
							},
					'success': function(data)
					{
						if(data)
						{
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 8000);
							}
							else
							{

								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								// window.location.href=data;
								blankControls();
								$("#tblProducts").find("tr:gt(0)").remove(); 
								$("#cboTask").attr("disabled", false);
								$("#dtPo").val(dateFormat(new Date()));
							}
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert(JSON.stringify(TableData));
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'partyRowId': partyRowId
								, 'vDt': vDt
								, 'totalQty': totalQty
								, 'totalAmt': totalAmt
								, 'orderValidity': orderValidity
								, 'paymentTerms': paymentTerms
								, 'freight': freight
								, 'inspection': inspection
								, 'delayClause': delayClause
								, 'modeOfDespatch': modeOfDespatch
								, 'guarantee': guarantee
								, 'insurance': insurance
								, 'specialNote': specialNote
								, 'TableData': TableData

							},
					'success': function(data)
					{
						// alert();
						if(data)
						{
							setTable(data['records']) ///loading records in tbl1
							alertPopup('Record updated...', 5000);
								// window.location.href=data;
							blankControls();
							$("#tblProducts").find("tr:gt(0)").remove(); 
							$("#dtPo").val(dateFormat(new Date()));
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					JSON.stringify(data);
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
	function loadLimitedRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadLimitedRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						// alertPopup('Records loaded...', 4000);
						// blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
</script>
<div class="container-fluid" style="width:95%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Purchase Order</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dtPo', '', "class='form-control' placeholder='' id='dtPo' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtPo" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dtPo").val(dateFormat(new Date()));
					</script>
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						$types = array();
						$types['-1'] = '--- Select ---';
						$types['P'] = "Purchase";
						$types['E'] = "Expense";
						
						echo "<label style='color: black; font-weight: normal;'>Task: </label>";
						echo form_dropdown('cboTask', $types, '-1', "class='form-control' id='cboTask'");
					?> 
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Party Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>";
						// echo "<label id='lbl' class='blank' style='color: red; font-weight: normal; text-align:right;'>.</label>";
	              	?>
	              	<select id="cboParty" class="form-control">
		              <option value="-1" addr="" mobile1="" mobile2="" townName="">--- Select ---</option>
		              <?php
		                foreach ($parties as $row) 
		                {
		              ?>
		              <option value=<?php echo $row['partyRowId']; ?>  addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
		              <?php
		                }
		              ?>
		            </select>

		            <script type="text/javascript">
			            $(document).ready(function()
			            {
			              $("#cboParty").change(function()
			              {
						        $("#txtAddress").val($('option:selected', '#cboParty').attr('addr'));
						        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('townName'));
						        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('mobile1'));
						        $("#txtAddress").val( $("#txtAddress").val() + ", " + $('option:selected', '#cboParty').attr('mobile2'));
			              });
			            });
			        </script>	
					    
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;display: none;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Address:</label>";
						echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtAddress'  maxlength='255' value=''");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="row">
							
				    </div>			
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					    </div>	
				    </div>			
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >

	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

	          	</div>
			</div>

			<hr />
			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
					<?php
						echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
						echo form_dropdown('cboProductCategories',$productCategories, '-1', "class='form-control' id='cboProductCategories'");
	              	?>

	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						$products="--- Select ---";
						echo "<label style='color: black; font-weight: normal;'>Products:</label>";
						echo form_dropdown('cboProducts',$products, '-1', "class='form-control' id='cboProducts'");
	              	?>
	              	<script type="text/javascript">
			            $(document).ready(function()
			            {
			              $("#cboProducts").change(function()
			              {
						        $("#txtRate").val($('option:selected', '#cboProducts').attr('rate'));
						        $("#txtSize").val($('option:selected', '#cboProducts').attr('productLength'));
						        $("#txtSize").val( $("#txtSize").val() + ' x ' + $('option:selected', '#cboProducts').attr('productWidth') );
						        $("#txtSize").val( $("#txtSize").val() + ' x ' + $('option:selected', '#cboProducts').attr('productHeight') );
						        $("#txtSize").val( $("#txtSize").val() + ' ' + $('option:selected', '#cboProducts').attr('uom') );
						        $("#txtRoLevel").val($('option:selected', '#cboProducts').attr('roLevel'));
			              });
			            });
			        </script>	
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='showHistory();' value='History' id='btnConfirm' class='btn btn-block btn-success'>";
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Rate:</label>";
						echo '<input type="number" step="0" name="txtRate" value="0" placeholder="" class="form-control" maxlength="20" id="txtRate" disabled />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Qty:</label>";
						echo '<input type="number" step="1" name="txtQty" value="1" placeholder="" class="form-control" maxlength="20" id="txtQty" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Amt.:</label>";
						echo '<input type="number" step="1" name="txtAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtAmt" disabled/>';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Discount(%):</label>";
						echo '<input type="number" step="1" name="txtDiscount" value="0" placeholder="" class="form-control" maxlength="20" id="txtDiscount" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Net:</label>";
						echo '<input type="number" step="1" name="txtAmtAfterDiscount" value="0" placeholder="" class="form-control" maxlength="20" id="txtAmtAfterDiscount" disabled />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="display: none1;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Size:</label>";
						echo '<input type="text" disabled name="txtSize" placeholder="" class="form-control" maxlength="40" id="txtSize" />';
	              	?>
	          	</div>
			</div>


			<div class="row" style="margin-top:15px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Color:</label>";
						echo '<input type="text" name="txtColor" placeholder="" class="form-control" maxlength="20" id="txtColor" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Delivery Schedule:</label>";
						echo '<input type="text" name="dtDelivery" placeholder="" class="form-control" maxlength="20" id="dtDelivery" />';
	              	?>
	              	<script>
						$( "#dtDelivery" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						// $("#dtDelivery").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>RO Level:</label>";
						echo '<input type="number" step="1" name="txtRoLevel" value="0"  class="form-control" maxlength="20" id="txtRoLevel" disabled />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="display: none1;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Current Qty:</label>";
						echo '<input type="text" disabled name="txtCurrentQty"  value="0" class="form-control" maxlength="40" id="txtCurrentQty" />';
						echo "<label id='lblLastPurchaseQtyAndDate' style='color: red; font-size: 10pt;'>Prev. Purchase Qty(Dt):</label><br />";

	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo '<input type="text" name="txtRemarks" placeholder="" class="form-control" maxlength="255" id="txtRemarks" />';
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='confirm();' value='Confirm Product' id='btnConfirm' class='btn btn-block btn-danger'>";
	              	?>
	          	</div>
			</div>


			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:0 dashed lightgray; padding: 10px;height:200px; overflow:auto;>
					<table class='table table-bordered' id='tblProducts'>
						<tr>
						 	<th  width="50" class="text-center">Delete</th>
							<th style='display:none1;'>productRowId</th>
						 	<th style='display:none1;'>Product Category Riw Id</th>
						 	<th>Product</th>
						 	<th>Rate</th>
						 	<th>Qty</th>
						 	<th>Amt</th>
						 	<th>Dis(%)</th>
						 	<th>After Dis Amt</th>
						 	<th>Colour</th>
						 	<th>Del Dt.</th>
						 	<th>Remarks</th>
						 	<th>lastPurchaseInfo</th>
						</tr>
					</table>
				</div>
			</div>

			<hr />
			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Amt.:</label>";
						echo '<input type="number" disabled name="txtTotalAmt" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Qty.:</label>";
						echo '<input type="number" disabled name="txtTotalQty" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalQty" />';
	              	?>
	          	</div>

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Order Validity:</label>";
						echo '<input type="text" name="txtOrderValidity" class="form-control" maxlength="50" id="txtOrderValidity" />';
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Payment Terms:</label>";
						echo '<input type="text" name="txtPaymentTerms" class="form-control" maxlength="50" id="txtPaymentTerms" />';
	              	?>
	          	</div>				
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Freight:</label>";
						echo '<input type="text" name="txtFreight" class="form-control" maxlength="50" id="txtFreight" />';
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Inspection:</label>";
						echo '<input type="text" name="txtInspection" class="form-control" maxlength="50" id="txtInspection" />';
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Delay Clause:</label>";
						echo '<input type="text" name="txtDelayClause" class="form-control" maxlength="50" id="txtDelayClause" />';
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Mode of Despatch:</label>";
						echo '<input type="text" name="txtModeOfDespatch" class="form-control" maxlength="50" id="txtModeOfDespatch" />';
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Guarantee/Warranty:</label>";
						echo '<input type="text" name="txtGuarantee" class="form-control" maxlength="50" id="txtGuarantee" />';
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Insurance:</label>";
						echo '<input type="text" name="txtInsurance" class="form-control" maxlength="50" id="txtInsurance" />';
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Special Note:</label>";
						echo '<input type="text" name="txtSpecialNote" class="form-control" maxlength="100" id="txtSpecialNote" />';
	              	?>
	          	</div>

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>poRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Total Amt</th>
					 	<th>Total Qty</th>
					 	<th>V.No.</th>
					 	<th>Order Validity</th>
					 	<th>Payment Terms</th>
					 	<th>Freight</th>
					 	<th>Inspection</th>
					 	<th>Delay Clause</th>
					 	<th>Mode Of Despatch</th>
					 	<th>Guarantee</th>
					 	<th>Insurance</th>
					 	<th>Special Note</th>
					 	<th>Purchase/Exp</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['totalAmt']."</td>";
						 	echo "<td>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['orderValidity']."</td>";
						 	echo "<td>".$row['paymentTerms']."</td>";
						 	echo "<td>".$row['freight']."</td>";
						 	echo "<td>".$row['inspection']."</td>";
						 	echo "<td>".$row['delayClause']."</td>";
						 	echo "<td>".$row['modeOfDespatch']."</td>";
						 	echo "<td>".$row['guarantee']."</td>";
						 	echo "<td>".$row['insurance']."</td>";
						 	echo "<td>".$row['specialNote']."</td>";
						 	echo "<td>".$row['purchaseOrExpense']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10%">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>

		  <!-- Model Product History-->
		  <div class="modal" id="myModalHistory" role="dialog" data-backdrop="static">
		    <div class="modal-dialog modal-lg">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <!-- <h4 class="modal-title" id="modalAgainstHeading">Against</h4> -->
		          <label id="modalHistoryHeading">History of </label>
		          <label id="lblHistoryProduct" style="color: red; font-weight: normal;"></label>
		        </div>
		        <div class="modal-body" style="overflow: auto; height: 300px;">
		          <table id='tblHistory' class="table table-bordered">
					 	<th>vType</th>
					 	<th>vNo</th>
					 	<th>Dt.</th>
					 	<th>Party</th>
					 	<th>Qty</th>
					 	<th>Rate</th>
					 	<th>Amt</th>
		          </table>
		        </div>
		        <div class="modal-footer">
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        		
		        	</div>
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        		
		        	</div>
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        	</div>
		        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		        		<button type="button" class="btn btn-block btn-default" data-dismiss="modal">Close</button>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>	


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#dtQ").val($(this).closest('tr').children('td:eq(5)').text());
		$("#cboParty").val($(this).closest('tr').children('td:eq(6)').text());
		$('#cboParty').trigger('change');
		$("#txtTotalAmt").val($(this).closest('tr').children('td:eq(8)').text());
		$("#txtTotalQty").val($(this).closest('tr').children('td:eq(9)').text());
		$("#txtOrderValidity").val($(this).closest('tr').children('td:eq(11)').text());
		$("#txtPaymentTerms").val($(this).closest('tr').children('td:eq(12)').text());
		$("#txtFreight").val($(this).closest('tr').children('td:eq(13)').text());
		$("#txtInspection").val($(this).closest('tr').children('td:eq(14)').text());
		$("#txtDelayClause").val($(this).closest('tr').children('td:eq(15)').text());
		$("#txtModeOfDespatch").val($(this).closest('tr').children('td:eq(16)').text());
		$("#txtGuarantee").val($(this).closest('tr').children('td:eq(17)').text());
		$("#txtInsurance").val($(this).closest('tr').children('td:eq(18)').text());
		$("#txtSpecialNote").val($(this).closest('tr').children('td:eq(19)').text());
		$("#cboTask").val($(this).closest('tr').children('td:eq(20)').text());

		$("#cboTask").attr("disabled", true);

		$.ajax({
			'url': base_url + '/PurchaseOrder_Controller/getProducts',
			'type': 'POST', 
			'data':{'rowid':globalrowid},
			'dataType': 'json',
			'success':function(data)
			{
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          // cell.innerHTML =""onClick='editProduct(this);'
		          cell.innerHTML =  "<span  style='color: lightgray;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'green\';\"  onmouseout=\"this.style.color=\'lightgray\';\" class='editThisProduct glyphicon glyphicon-pencil'></span>";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].productRowId;
		          // cell.style.display = "none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].productCategoryRowId;
		          // cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].productName + " (" + data['products'][i].productLength + " x " + data['products'][i].productWidth + " x " + data['products'][i].productHeight + " " + data['products'][i].uom + ")";
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].rate;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].orderQty;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].amt;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].discount;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].amtAfterDiscount;
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].colour;
		          // cell.style.display = "none";
		          var cell = row.insertCell(10);
		          if( data['products'][i].deliveryDt == null )
					{
						cell.innerHTML = '';
					}
					else
					{
		          		cell.innerHTML = dateFormat(new Date(data['products'][i].deliveryDt));
		          	}
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['products'][i].remarks;
		          var cell = row.insertCell(12);
		          cell.innerHTML = data['products'][i].lastPurchaseInfo;
		        }	
		        i=0;
		        $('#tblProducts tr').each(function (i) 
                {
                  if(i>0) //Excluding header row 
                  {
                    $("td", this).eq(0).bind("click", editProduct);
                  }
                }); 
			}
		});
		/////END - Setting Product Detail

		$("#btnSave").val("Update");
	}

	// $('.editThisProduct').bind('click', editProduct);
	var editingProductFlag = 0;
	var editingProductRowId = 0;
	function editProduct()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		editingProductRowId = $(this).closest('tr').children('td:eq(1)').text();
		productCategoryRowId = $(this).closest('tr').children('td:eq(2)').text();
		// alert( $(this).closest('tr').children('td:eq(1)').text()  );
		$("#cboProductCategories").val($(this).closest('tr').children('td:eq(2)').text());
		$("#cboProducts").val($(this).closest('tr').children('td:eq(1)').text());

		// $("#cboProducts").val( $(this).closest('tr').children('td:eq(1)').text() );
		$("#txtRate").val( $(this).closest('tr').children('td:eq(4)').text() );
		$("#txtQty").val( $(this).closest('tr').children('td:eq(5)').text() );
		$("#txtAmt").val( $(this).closest('tr').children('td:eq(6)').text() );
		$("#txtDiscount").val( $(this).closest('tr').children('td:eq(7)').text() );
		$("#txtAmtAfterDiscount").val( $(this).closest('tr').children('td:eq(8)').text() );
		$("#txtColor").val( $(this).closest('tr').children('td:eq(9)').text() );
		$("#dtDelivery").val( $(this).closest('tr').children('td:eq(10)').text() );
		$("#txtRemarks").val( $(this).closest('tr').children('td:eq(11)').text() );
		// alert();
		$("#cboProductCategories").attr("disabled", true);
		$("#cboProducts").attr("disabled", true);
		
		editingProductFlag = 1;
		// $("#cboProducts").val( productRowId );
		// alert(productCategoryRowId);

		$("#cboTask").trigger('change');
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );



      $(document).ready(function(){
        $("#cboProductCategories").change(function(){
          var productCategoryRowId = $("#cboProductCategories").val();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                  // alert(data);
                  $('#cboProducts').empty();
                  if(data['products'] != null) 
                  {
                    var options = "<option value='-1' rate='0' productLength='0' productWidth='0' productHeight='0' uom='-1' roLevel='0'>" + "--- Select ---" + "</option>";
                    for(var i=0; i<data['products'].length; i++)
                    {
                      options += "<option value=" + data['products'][i].productRowId + " rate=" + data['products'][i].productRate + " productLength=" + data['products'][i].productLength + " productWidth=" + data['products'][i].productWidth + " productHeight=" + data['products'][i].productHeight + " uom=" + data['products'][i].uom +  " roLevel=" + data['products'][i].roLevel + ">" + data['products'][i].productName + "</option>";
                    }
                    $('#cboProducts').append(options);

                  }
                  else
                  {
                  }

                }
              }
          });
        });
      });
              

	$(document).ready(function()
    {
      $("#txtRate").on('keyup change', calcAmt);
      $("#txtQty").on('keyup change', calcAmt);
      $("#txtDiscount").on('keyup change', calcDiscount);

      $("#cboProducts").on('change', calcAmt);
      // $("#txtDiscountPer").on('keyup change', doDiscount);
      // $("#txtDiscountAmt").on('keyup change', doDiscountOnAmtChange);
      // $("#txtVatPer").on('keyup change', doVat);
      // $("#txtAdvance").on('keyup change', doAdvance);

    });
    function calcAmt()
    {
      	var rate = $("#txtRate").val();
      	var qty = $("#txtQty").val();
      	var amt = (rate * qty);
	    $("#txtAmt").val(amt.toFixed(2));
	    calcDiscount()
    }

    function calcDiscount()
    {
    	var amt = $("#txtAmt").val();
      	var dis = $("#txtDiscount").val();
      	var discountAmt = (amt * dis)/100;
      	var amtAfterDiscount = amt - discountAmt
	    $("#txtAmtAfterDiscount").val(amtAfterDiscount.toFixed(2));
    }


	  function confirm() 
	  {
	  	if( editingProductFlag == 1)
	  	{
	  	  editingProductFlag = 0;
	      var rate = $("#txtRate").val();
	      var qty = $("#txtQty").val();
	      var amt = $("#txtAmt").val();
	      var dis = $("#txtDiscount").val();
	      var amtAfterDiscount = $("#txtAmtAfterDiscount").val();
	      var colour = $("#txtColor").val().trim();
	      var deliveryDt = $("#dtDelivery").val().trim();
	      var remarks = $("#txtRemarks").val().trim();


	      $('#tblProducts tr').each(function(row, tr)
		  {
		    	if ( parseInt($(tr).find('td:eq(1)').text()) == parseInt(editingProductRowId) )
		    	{
		    		$(tr).find('td:eq(4)').text(rate);
		    		$(tr).find('td:eq(5)').text(qty);
		    		$(tr).find('td:eq(6)').text(amt);
		    		$(tr).find('td:eq(7)').text(dis);
		    		$(tr).find('td:eq(8)').text(amtAfterDiscount);
		    		$(tr).find('td:eq(9)').text(colour);
		    		$(tr).find('td:eq(10)').text(deliveryDt);
		    		$(tr).find('td:eq(11)').text(remarks);
			    }
		   }); 
	      // $("#cboProductCategories").val('-1');
	      $("#cboProducts").val('-1');
	      $("#txtRate").val('0');
	      $("#txtQty").val('0');
	      $("#txtAmt").val('0');
	      $("#txtDiscount").val('0');
	      $("#txtAmtAfterDiscount").val('0');
	      $("#txtColor").val('');
	      $("#dtDelivery").val('');
	      $("#txtRemarks").val('');
	  	}
	  	else /////////While NOT editing product that is, 1st time saving
	  	{
	  		var task = $("#cboTask").val();
			if(task == "-1")
			{
				alertPopup("Select task...", 8000);
				$("#cboTask").focus();
				return;
			}	

	      var productCategoryRowId = $("#cboProductCategories").val();
	      if(productCategoryRowId == "-1")
	      {
	      	alertPopup("Select product category...", 6000);
	      	$("#cboProductCategories").focus();
	      	return;
	      }

	      var productRowId = $("#cboProducts").val();
	      if(productRowId == "-1")
	      {
	      	alertPopup("Select product...", 6000);
	      	$("#cboProducts").focus();
	      	return;
	      }


	      var flag=0;
		  $('#tblProducts tr').each(function(row, tr)
		  {
		    	if ( parseInt($(tr).find('td:eq(1)').text()) == parseInt(productRowId) )
		    	{
		    		flag=1;
			    }
		   }); 
		  if( flag == 1)
		  {
		  	alertPopup("This product already added...", 6000);
			$("#cboProducts").focus();
			return;
		  }


	      var productCategory = $("#cboProductCategories option:selected").text();
	      var product = $("#cboProducts option:selected").text();
	      var productSize = ' (' + $("#txtSize").val() + ')'; 
	      var rate = $("#txtRate").val();
	      var qty = $("#txtQty").val();
	      var amt = $("#txtAmt").val();
	      var dis = $("#txtDiscount").val();
	      var amtAfterDiscount = $("#txtAmtAfterDiscount").val();
	      var colour = $("#txtColor").val().trim();
	      var deliveryDt = $("#dtDelivery").val().trim();
	      var remarks = $("#txtRemarks").val().trim();
	      // alert(remarks);
	      var lastPurchaseInfo = $("#lblLastPurchaseQtyAndDate").text();

	      var table = document.getElementById("tblProducts");
	      var newRowIndex = table.rows.length;
	      var row = table.insertRow(newRowIndex);
	      var cell0 = row.insertCell(0);
	      var cell1 = row.insertCell(1);
	      // cell1.style.display="none";
	      var cell2 = row.insertCell(2);
	      // cell2.style.display="none";
	      var cell3 = row.insertCell(3);
	      var cell4 = row.insertCell(4);
	      var cell5 = row.insertCell(5);
	      var cell6 = row.insertCell(6);
	      var cell7 = row.insertCell(7);
	      var cell8 = row.insertCell(8);
	      var cell9 = row.insertCell(9);
	      var cell10 = row.insertCell(10);
	      var cell11 = row.insertCell(11);
	      var cell12 = row.insertCell(12);

	      cell0.innerHTML = "<span onClick='delTableRowProducts(this);' style='color: lightgray;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'red\';\"  onmouseout=\"this.style.color=\'lightgray\';\" class='glyphicon glyphicon-remove'></span>";
	      cell1.innerHTML = productRowId;
	      cell2.innerHTML = productCategoryRowId;
	      cell3.innerHTML = product + productSize;
	      cell4.innerHTML = rate;
	      cell5.innerHTML = qty;
	      cell6.innerHTML = amt;
	      cell7.innerHTML = dis;
	      cell8.innerHTML = amtAfterDiscount;
	      cell9.innerHTML = colour;
	      cell10.innerHTML = deliveryDt;
	      cell11.innerHTML = remarks;
	      cell12.innerHTML = lastPurchaseInfo;

	      // $("#cboProductCategories").val('-1');
	      $("#cboProducts").val('-1');
	      $("#txtRate").val('0');
	      $("#txtQty").val('0');
	      $("#txtAmt").val('0');
	      $("#txtDiscount").val('0');
	      $("#txtAmtAfterDiscount").val('0');
	      $("#txtColor").val('');
	      $("#dtDelivery").val('');
	      $("#txtRemarks").val('');

	      $("#cboProductCategories").focus();

	      $("#cboTask").attr("disabled", true);
	    }

	    doTotals();
	  }

	  function showHistory()
	  {
	  	  var productCategoryRowId = $("#cboProductCategories").val();
	      if(productCategoryRowId == "-1")
	      {
	      	alertPopup("Select product category...", 6000);
	      	$("#cboProductCategories").focus();
	      	return;
	      }

	      var productRowId = $("#cboProducts").val();
	      if(productRowId == "-1")
	      {
	      	alertPopup("Select product...", 6000);
	      	$("#cboProducts").focus();
	      	return;
	      }
	      $("#lblHistoryProduct").text( $("#cboProducts option:selected").text() );

	      $.ajax({
				'url': base_url + '/' + controller + '/showHistory',
				'type': 'POST',
				'dataType': 'json',
				'data': {
						'dt': 'dt'
						, 'productRowId': productRowId
					},
				'success': function(data)
				{
					// console.log(data);
					setHistoryTable(data['historyData']);
				},
	          'error': function(jqXHR, exception)
	          {
	            // $("#paraAjaxErrorMsg").html( jqXHR.responseText );
	            // $("#modalAjaxErrorMsg").modal('toggle');
	          }
			});

		  $('#myModalHistory').modal('toggle');

	  }

	  function setHistoryTable(records)
		{
			// alert(JSON.stringify(records));
			  $("#tblHistory").find("tr:gt(0)").remove();
		      var table = document.getElementById("tblHistory");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = records[i].vType;
		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].vNo;
		          var cell = row.insertCell(2);
		          cell.innerHTML = dateFormat(new Date(records[i].vDt));
		          var cell = row.insertCell(3);
		          cell.innerHTML = records[i].name;
		          var cell = row.insertCell(4);
		          cell.innerHTML = records[i].receivedQty;
		          var cell = row.insertCell(5);
		          cell.innerHTML = records[i].rate;
		          var cell = row.insertCell(6);
		          cell.innerHTML = records[i].amt;
		          
		  	  }
		}

	  function delTableRowProducts(x)
	  {
	      var rowToDelete = x.parentNode.parentNode.rowIndex
	      var table = document.getElementById("tblProducts");
	      table.deleteRow(rowToDelete);
	      doTotals();
	  }

	  function doTotals()
	  {
	  	var totalAmt=0;
	  	var totalQty=0;
	  	$('#tblProducts tr').each(function(row, tr)
	    {
	    	if ( isNaN(parseFloat($(tr).find('td:eq(8)').text())) == false )
	    	{
		    	totalAmt += parseFloat($(tr).find('td:eq(8)').text());
		    	totalQty += parseFloat($(tr).find('td:eq(5)').text());
		    }
	    }); 
	    // alert(totalAmt);
	    $("#txtTotalAmt").val(totalAmt.toFixed(2));
	    $("#txtTotalQty").val(totalQty.toFixed(2));
	    // doDiscount()
	  }


	  $(document).ready(function(){
        $("#cboProducts").change(function(){
          var productRowId = $("#cboProducts").val();
          if(cboProducts == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getCurrentQty',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productRowId': productRowId},
              'success': function(data)
              {
              	// 
                if(data)
                {
                  // alert( JSON.stringify(data['lastPurchaseInfo']));
                  if(data['currentQty'] != null) 
                  {
                  	if(data['currentQty'][0]['currentQty'] !=null )
                  	{
                  		$("#txtCurrentQty").val( data['currentQty'][0]['currentQty'] );
                  	}
                  	else
                  	{
                  		$("#txtCurrentQty").val( '0' );
                  	}
                  	var requiredQty = $("#txtRoLevel").val() - $("#txtCurrentQty").val() ; 
                  	if( requiredQty > 0 )
                  	{
                  		$("#txtQty").val(requiredQty);
                  	}
                  	else
                  	{
                  		$("#txtQty").val('0');
                  	}
                  }
                  // alert( JSON.stringify(data['lastPurchaseInfo'].length));
	              	if(data['lastPurchaseInfo'].length > 0 )
	              	{
	              		// $("#lblLastPurchaseQtyAndDate").text('Last Pur. Qty.[Dt]: ' + data['lastPurchaseInfo'][0]['receive'] + " [" + dateFormat(new Date(data['lastPurchaseInfo'][0]['dt'])) + "]"  );
	              		$("#lblLastPurchaseQtyAndDate").text('Last Pur. Qty.[Dt]: ' + data['lastPurchaseInfo'][0]['receive'] + " [" + data['lastPurchaseInfo'][0]['dt'] + "]"  );
	              		
	              	}
	              	else
	              	{
	              		$("#lblLastPurchaseQtyAndDate").text('Last Pur. Qty.[Dt]: ' +  '--' );
	              		// $("#lblLastPurchaseDate").text('Date: ' );
	              	}
                }
              }
          });
        });
      });


		$(document).ready(function()
        {
          $("#cboTask").change(function()
          {
          	// alert($("#cboTask").val());
          	if( $("#cboTask").val() == "-1" )
          	{
          		$("#txtRate").attr("disabled",true);
          	}
          	else if( $("#cboTask").val() == "P" )
          	{
          		$("#txtRate").attr("disabled",true);
          	}
          	else if( $("#cboTask").val() == "E" )
          	{
          		$("#txtRate").attr("disabled",false);
          	}
          });
		});
</script>