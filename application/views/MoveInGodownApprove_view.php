<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='MoveInGodownApprove_Controller';
	var base_url='<?php echo site_url();?>';

	

	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(10)').text()
		            , "orderQty" :$(tr).find('td:eq(3)').text()
		            , "amt" :$(tr).find('td:eq(4)').text()
		            , "amtAfterDiscount" :$(tr).find('td:eq(6)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}


	function saveData()
	{	
		// var TableData;
		// TableData = storeTblValues();
		// TableData = JSON.stringify(TableData);
		// // alert(JSON.stringify(TableData));
		// // return;


		if(globalRowId <= 0)
		{
			// alertPopup("No product selected...", 8000);
			msgBoxError("Error", "No product selected...");
			return;
		}

		rowId = globalRowId;

		// alert(fromStage + ", " +toStage );
		// return;
		$.ajax({
				'url': base_url + '/' + controller + '/insert',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'rowId': rowId
							, 'productRowId': productRowId
							, 'colourRowId': colourRowId
							, 'fromStage': fromStage
							, 'toStage': toStage
							, 'qty': qty
						},
				'success': function(data)
				{
					 alertPopup("Approved...", 8000);
					 location.reload();
				}
				
		});
	}

	function reject()
	{	
		if(globalRowId <= 0)
		{
			// alertPopup("No product selected...", 8000);
			msgBoxError("Error", "No product selected...");
			return;
		}
		rowId = globalRowId;
		rejectReason = $("#txtRejectReason").val().trim();
		if(rejectReason == "")
		{
			// alertPopup("Write Reason", 8000);
			msgBoxError("Error", "Write Reason...");
			return;
		}
		$.ajax({
				'url': base_url + '/' + controller + '/reject',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'rowId': rowId
							, 'rejectReason': rejectReason
						},
				'success': function(data)
				{
					 alertPopup("Rejected...", 8000);
					 location.reload();
				}
				
		});
	}

</script>
<div class="container">



	<div class="row" style="margin-top:-30px;border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 align="center">Move in Godown Approve</h3>
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; ;height:400px; overflow:auto;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>rowId</th>
					 	<th style='display:none1;'>Date</th>
						<th style='display:none;'>ProductRowId</th>
						<th style='display:none1;'>Product</th>
						<th style='display:none;'>ColourRowId</th>
						<th style='display:none1;'>Colour</th>
						<th style='display:none;'>From</th>
						<th style='display:none1;'>From</th>
						<th style='display:none;'>To</th>
						<th style='display:none1;'>To</th>
						<th style='display:none1;'>Qty</th>
						<th style='display:none1;'>Rem</th>
						<th style='display:none1;'>Auth.</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['rowId'];
						 	echo "<tr>";						
							
						 	echo "<td style='display:none;'>".$row['rowId']."</td>";
						 	$vdt = strtotime($row['dt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['productRowId']."</td>";
						 	echo "<td>".$row['productName']."</td>";
						 	echo "<td style='display:none;'>".$row['colourRowId']."</td>";
						 	echo "<td>".$row['colourName']."</td>";
						 	echo "<td style='display:none;'>".$row['fromStage']."</td>";
						 	echo "<td>".$row['fromStageName']."</td>";
						 	echo "<td style='display:none;'>".$row['toStage']."</td>";
						 	echo "<td>".$row['toStageName']."</td>";
						 	echo "<td>".$row['qty']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td>".$row['authorised']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>


		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>Reject Reason:</label>";
				echo form_textarea('txtRejectReason', '', "class='form-control' style='resize:none;height:100px;' id='txtRejectReason'  maxlength='255' value=''");
          	?>
      	</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='reject();' value='Reject' id='btnReject' class='btn btn-danger form-control'>";
          	?>
      	</div>
      	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
      	</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Approve' id='btnSave' class='btn btn-primary form-control'>";
          	?>
      	</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

</div>



<script type="text/javascript">
	$(document).ready(function(){
		$("#tbl1 tr").on('click', getSelectedRowData);
	});

	var globalRowId = 0;
	var globalRowIndex = -1;
	var productRowId = -1;
	var colourRowId = -1;
	var fromStage = -1;
	var toStage = -1;
	var qty = -1;

	function getSelectedRowData()
	{
		rowIndex = $(this).closest('tr').index();
		globalRowIndex = rowIndex;
		// alert(globalRowIndexOfPo);
		colIndex = $(this).index();
		rowId = $(this).closest('tr').children('td:eq(0)').text();
		globalRowId=rowId;
		productRowId = $(this).closest('tr').children('td:eq(2)').text();
		colourRowId = $(this).closest('tr').children('td:eq(4)').text();
		fromStage = $(this).closest('tr').children('td:eq(6)').text();
		toStage = $(this).closest('tr').children('td:eq(8)').text();
		qty = $(this).closest('tr').children('td:eq(10)').text();
		// alert(roswId);
		// globalUserRowId = $(this).closest('tr').children('td:eq(19)').text();
	}

</script>