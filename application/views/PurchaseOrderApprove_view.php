<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='PurchaseOrderApprove_Controller';
	var base_url='<?php echo site_url();?>';

	function showDetails()
      {
      	// if( $(this) )
      	// alert();
	    $("#tblDetail").find("tr:gt(0)").remove();
	    poRowId = $(this).parent().find("td:eq(2)").text();
	    // globalPoRowIdForReprint = $(this).find("td:eq(2)").text();
	    // globalPiRowIdForReprint = $(this).find("td:eq(4)").text();
	    // globalvDt = $(this).find("td:eq(5)").text();
	    $.ajax({
				'url': base_url + '/' + controller + '/getProducts',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'poRowId': poRowId
						},
				'success': function(data)
				{
					// alert( JSON.stringify(data) );
					// $("#tblDetail").empty();
					$("#tblDetail").find("tr:gt(0)").remove();
	      			var table = document.getElementById("tblDetail");
					for(i=0; i<data['products'].length; i++)
			        {
			          var newRowIndex = table.rows.length;
			          var row = table.insertRow(newRowIndex);
			          var cell = row.insertCell(0);
			          cell.innerHTML = data['products'][i].productRowId;
			          cell.style.display = "none";
			          var cell = row.insertCell(1);
			          cell.innerHTML = data['products'][i].productName;
			          var cell = row.insertCell(2);
			          cell.innerHTML = data['products'][i].rate;
			          var cell = row.insertCell(3);
			          cell.innerHTML = data['products'][i].orderQty;
			          cell.className = "qty";
			          // cell.style.color="red";
		          	  // cell.setAttribute("contentEditable", true);
			          var cell = row.insertCell(4);
			          cell.innerHTML = data['products'][i].amt;
			          var cell = row.insertCell(5);
			          cell.innerHTML = data['products'][i].discount;
			          var cell = row.insertCell(6);
			          cell.innerHTML = data['products'][i].amtAfterDiscount;
			          var cell = row.insertCell(7);
			          cell.innerHTML = data['products'][i].colour;
			          var cell = row.insertCell(8);
			          cell.innerHTML = data['products'][i].deliveryDt;
			          var cell = row.insertCell(9);
			          cell.innerHTML = data['products'][i].remarks;
			          var cell = row.insertCell(10);
			          cell.innerHTML = data['products'][i].poDetailRowId;
			          cell.style.display="none";
			          var cell = row.insertCell(11);
			          cell.innerHTML = data['products'][i].lastPurchaseInfo.substr(20);
			        }				
				}
				
		});
      }
	
	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tblOld").empty();
	      var table = document.getElementById("tblOld");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].poRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].poRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vNo;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.style.display="none";
	          // cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(9);
	          // cell.innerHTML = records[i].totalQty;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;	
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		// myDataTable.destroy();
		// $(document).ready( function () {
	 //    myDataTable=$('#tbl1').DataTable({
		//     paging: false,
		//     iDisplayLength: -1,
		//     aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// });
		// } );

		$("#tblOld tr").on("click", highlightRow);
		$("#tblOld tr").find("td:gt(1)").on('click', showDetails);
	}


	var globalrowid;
	var globalPoRowId;
	function delrowid(rowid, poRowId)
	{
		globalrowid = rowid;
		globalPoRowId = poRowId;
		// alert(globalrowid);
	}

	function deleteRecord(rowId)
	{
		// alert(rowId);
		// return;
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId, 'poRowId': globalPoRowId},
				'success': function(data){
					// alert(JSON.stringify(data));
					if(data)
					{
						if( data == "cannot")
						{
							alertPopup('Cannot cancel, Approval already saved...', 7000);
						}
						else
						{
							setTable(data['records']);
							alertPopup('Record deleted...', 4000);
							location.reload();
							// $("#btnShow").trigger("click");
							// blankControls();
							// $("#txtLetterNo").focus();
							// $("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
				}
			});
	}
	

	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(10)').text()
		            , "orderQty" :$(tr).find('td:eq(3)').text()
		            , "amt" :$(tr).find('td:eq(4)').text()
		            , "amtAfterDiscount" :$(tr).find('td:eq(6)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}


	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;


		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}

		poRowId = globalPoRowId;
		totalQty = globalTotalQty;
		totalAmt = globalTotalAmt;
		// alert(totalAmt + "  " + totalQty);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/insert',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'poRowId': poRowId
							, 'totalQty': totalQty
							, 'totalAmt': totalAmt
							, 'globalUserRowId': globalUserRowId
							, 'globalPartyName': globalPartyName
							, 'TableData': TableData
						},
				'success': function(data)
				{
					 alertPopup("PO Approved...", 8000);
					 location.reload();
				}
				
		});
	}

	function reject()
	{	
		poRowId = globalPoRowId;
		rejectReason = $("#txtRejectReason").val().trim();
		if(rejectReason == "")
		{
			alertPopup("Write Reason", 8000);
			$("#txtRejectReason").focus();
			return;
		}
		$.ajax({
				'url': base_url + '/' + controller + '/reject',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'poRowId': poRowId
							, 'rejectReason': rejectReason
							, 'globalUserRowId': globalUserRowId
							, 'globalPartyName': globalPartyName
						},
				'success': function(data)
				{
					 alertPopup("PO Rejected...", 8000);
					 location.reload();
				}
				
		});
	}


	function loadAllRecords()
	{
		// alert();
		$.ajax({
			'url': base_url + '/' + controller + '/loadAllRecords',
			'type': 'POST',
			'dataType': 'json',
			'success': function(data)
			{
				// alert(JSON.stringify(data));
				if(data)
				{
					setTable(data['records'])
					alertPopup('Records loaded...', 4000);
					// blankControls();
				}
			}
		});
	}

</script>
<div class="container-fluid" style="width:95%;">



	<div class="row" style="margin-top:-30px;border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 align="center">Purchase Order Approve</h3>
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; ;height:200px; overflow:auto;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>poRowId</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Total Amt</th>
					 	<th>Total Qty</th>
					 	<th>V.No.</th>
					 	<th>Punched By</th>
					 	<th>Order Validity</th>
					 	<th>Payment Terms</th>
					 	<th>Freight</th>
					 	<th>Inspection</th>
					 	<th>Delay Clause</th>
					 	<th>Mode Of Despatch</th>
					 	<th>Guarantee</th>
					 	<th>Insurance</th>
					 	<th>Special Note</th>
					 	<th style='display:none;'>userRowId</th>
					 	<th>Purchase/Exp</th>
					 	<th>Payment Type</th>

					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['totalAmt']."</td>";
						 	echo "<td>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['uid']."</td>";
						 	echo "<td>".$row['orderValidity']."</td>";
						 	echo "<td>".$row['paymentTerms']."</td>";
						 	echo "<td>".$row['freight']."</td>";
						 	echo "<td>".$row['inspection']."</td>";
						 	echo "<td>".$row['delayClause']."</td>";
						 	echo "<td>".$row['modeOfDespatch']."</td>";
						 	echo "<td>".$row['guarantee']."</td>";
						 	echo "<td>".$row['insurance']."</td>";
						 	echo "<td>".$row['specialNote']."</td>";
						 	echo "<td style='display:none;'>".$row['createdBy']."</td>";
						 	echo "<td>".$row['purchaseOrExpense']."</td>";
						 	echo "<td>".$row['PayType']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px; height:200px; overflow:auto;">
			<table class='table table-bordered' id='tblProducts'>
				<tr>
					<th style='display:none;'>productRowId</th>
				 	<th>Product</th>
				 	<th>Rate</th>
				 	<th>Qty</th>
				 	<th>Amt</th>
				 	<th>Dis(%)</th>
				 	<th>After Dis Amt</th>
				 	<th>Colour</th>
				 	<th>Del Dt.</th>
				 	<th>Remarks</th>
				 	<th style='display:none;'>PoDetailRowId</th>
				 	<th>Last Pur. Qty. [Dt.]</th>
					<th>RO Level</th>
					<th>Stock</th>
				</tr>
			</table>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>Reject Reason:</label>";
				echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtRejectReason'  maxlength='255' value=''");
          	?>
      	</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='reject();' value='Reject' id='btnReject' class='btn btn-danger form-control'>";
          	?>
      	</div>
      	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
      	</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Approve' id='btnSave' class='btn btn-primary form-control'>";
          	?>
      	</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tblOld'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>poRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none;'>Total Amt</th>
					 	<th style='display:none;'>Total Qty</th>
					 	<th>V.No.</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($approvedRecords as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	$PoRowId = $row['vNo'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="display:none;color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.', '.$PoRowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td style='display:none;'>".$row['totalAmt']."</td>";
						 	echo "<td style='display:none;'>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div id="divTableDetail" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px; box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tblDetail'>
				 <thead>
					 <tr>
						<th style='display:none;'>productRowId</th>
					 	<th>Product</th>
					 	<th>Rate</th>
					 	<th>Qty</th>
					 	<th>Amt</th>
					 	<th>Dis(%)</th>
					 	<th>After Dis Amt</th>
					 	<th>Colour</th>
					 	<th>Del Dt.</th>
					 	<th>Remarks</th>
					 	<th style='display:none;'>PoDetailRowId</th>
					 	<th>Last Pur. Qty. [Dt.]</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 20px;" >
		<!-- <div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div> -->

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margib-bottom:10%">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			
		</div>
	</div>
</div>


		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">SB</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	$(document).ready(function(){
		$("#tbl1 tr").on('click', showDetail);
		$("#tblOld tr").on("click", highlightRow);

		$("#tblOld tr").find("td:gt(1)").on('click', showDetails);
	});

	var globalPoRowId = 0;
	globalRowIndexOfPo = -1;
	globalUserRowId = -1;
	globalPartyName = "";
	var globalTotalAmt=0;
	var globalTotalQty=0;

	function showDetail()
	{
		// alert();
		rowIndex = $(this).closest('tr').index();
		globalRowIndexOfPo = rowIndex;
		// alert(globalRowIndexOfPo);
		colIndex = $(this).index();
		poRowId = $(this).closest('tr').children('td:eq(0)').text();
		globalUserRowId = $(this).closest('tr').children('td:eq(19)').text();
		globalPartyName = $(this).closest('tr').children('td:eq(5)').text();
		globalTotalAmt = $(this).closest('tr').children('td:eq(6)').text();
		globalTotalQty = $(this).closest('tr').children('td:eq(7)').text();
		globalPoRowId = poRowId;
		$.ajax({
			'url': base_url + '/PurchaseOrderApprove_Controller/getProducts',
			'type': 'POST', 
			'data':{'poRowId':poRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data));
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].productName;
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].rate;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].orderQty;
		          cell.className = "qty";
		          cell.style.color="red";
	          	  cell.setAttribute("contentEditable", true);
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].amt;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].discount;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].amtAfterDiscount;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].colour;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].deliveryDt;
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].remarks;
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].poDetailRowId;
		          cell.style.display="none";
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['products'][i].lastPurchaseInfo.substr(20);
		          var cell = row.insertCell(12);
		          cell.innerHTML = data['products'][i].roLevel;
		          var cell = row.insertCell(13);
		          cell.innerHTML = data['products'][i].stock;
		        }
		        // $("#tblProducts tr").on("click", highlightRow);	
		        $('.qty').bind('keyup', doQtyCalculation);
			}
		});
	}

	function doQtyCalculation()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		var rate = $(this).closest('tr').children('td:eq(2)').text();
		var qty = $(this).closest('tr').children('td:eq(3)').text();
		var amt = parseFloat(rate) * parseFloat(qty);
		amt = amt.toFixed(2);
		$(this).closest('tr').children('td:eq(4)').text( amt );

		var dis = $(this).closest('tr').children('td:eq(5)').text();
		var disAmt = amt * dis /100;
		var amtAfterDis = amt - disAmt;
		amtAfterDis = amtAfterDis.toFixed(2);
		$(this).closest('tr').children('td:eq(6)').text( amtAfterDis );
		// alert(pendingQty);

		totalQty=0;
		totalAmt=0;
		$('#tblProducts tr').each(function(row, tr)
	    {
	    	if(isNaN(parseFloat($(tr).find('td:eq(3)').text())) == false)
	    	{
	    		totalQty += parseFloat($(tr).find('td:eq(3)').text());
	    		totalAmt += parseFloat($(tr).find('td:eq(6)').text());
	    	}
	    }); 
	    // alert(globalRowIndexOfPo);
	    x=parseInt(globalRowIndexOfPo) + 1;
	    $("#tbl1 tr:eq("+ x +")").find('td:eq(7)').text( totalQty );
	    $("#tbl1 tr:eq("+ x +")").find('td:eq(6)').text( totalAmt );

	    globalTotalQty = totalQty;
	    globalTotalAmt = totalAmt;
	}
</script>