<?php 
$party_nm = $_REQUEST['party_nm'];

$sql_party_info  = "SELECT addressbook.name, addressbook.addr, addressbook.pin, 
addressbook.mobile1, addressbook.email1, towns.townName 
FROM `addressbook` inner join `towns` on `towns`.townRowId = `addressbook`.townRowId 
where `addressbook`.abRowId = '".$party_nm."'";

$qry_party_info = $this->db->query($sql_party_info)->row();

$name = $qry_party_info->name;
$addr = $qry_party_info->addr;
$townName = $qry_party_info->townName;
$pin = $qry_party_info->pin;
$comp_addr = $addr.", ".$townName."-".$pin;

$mobile1 = $qry_party_info->mobile1;
$email1 = $qry_party_info->email1;

?>
<br/><br>
<div class="row">
    <div class="col-lg-9"><h3>Conversation History</h3></div>
    <div class="col-lg-1">
        <button onclick="Export()" class="form-control">
            Excel 
        </button>
    </div>
    <div class="col-lg-1">
        <button onclick="printDiv('printableArea')" class="form-control">
            PDF 
        </button>
    </div>
    <div class="col-lg-1">
        <button onclick="printDiv('printableArea')" class="form-control">
            Print
        </button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div id="printableArea">
            <table class="table table-bordered" id="example1">
                <thead>
                    <tr>
                        <th colspan="3"><b>Party Name:</b></th>
                        <th colspan="8"><?=$name; ?></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th colspan="3"><b>Address:</b></th>
                        <th colspan="8"><?=$comp_addr; ?></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th colspan="3"><b>Email:</b></th>
                        <th colspan="8"><?=$email1; ?></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th colspan="3"><b>Contact No:</b></th>
                        <th colspan="8"><?=$mobile1; ?></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th><b>Dt</b></th>
                        <th><b>User</b></th>
                        <th><b>Task</b></th>
                        <th><b>Type</b></th>
                        <th><b>Party</b></th>
                        <th><b>Party Type</b></th>
                        <th><b>Remarks</b></th>
                        <th><b>Further</b></th>
                        <th><b>Reason</b></th>
                        <th><b>Dt.</b></th>
                        <th><b>Time</b></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $sql_party_det = "select dsr.*, addressbook.name, meetingremarks.meetingRemark, users.uid
                    from dsr 
                    left join addressbook on addressbook.abRowId = dsr.abRowId 
                    left join meetingremarks on meetingremarks.mrRowId = dsr.mrRowId 
                    left join users on users.rowid = dsr.userRowId 
                    where dsr.deleted = 'N' and dsr.orgRowId = 2 and addressbook.abRowId = '".$party_nm."' 
                    order by dsr.dsrRowId desc ";

                    //echo $sql_party_det; die;

                    $qry_party_det = $this->db->query($sql_party_det);

                    foreach($qry_party_det->result() as $row){
                        $vdt = strtotime($row->dsrDt);
                        $vdt = date('d-M-Y', $vdt);
                        $userRowId = $row->userRowId;
                        $uid = $row->uid;
                        $task = $row->task;
                        $type = $row->type;
                        $name = $row->name;
                        $partyType = $row->partyType;
                        $remarks = $row->remarks;
                        $further = $row->further;
                        $meetingRemark = $row->meetingRemark;

                        if( $row->nextDt != "")
                        {
                            $vdt = strtotime($row->nextDt);
                            $vdt = date('d-M-Y', $vdt);
                        }
                        else
                        {
                            $vdt = "";	
                        }

                        $nextTm = $row->nextTm;
                ?>
                    <tr>
                        <td><?=$vdt;?></td>
                        <td><?=$uid;?></td>
                        <td><?=$task;?></td>
                        <td><?=$type;?></td>
                        <td><?=$name;?></td>
                        <td><?=$partyType;?></td>
                        <td><?=$remarks;?></td>
                        <td><?=$further;?></td>
                        <td><?=$meetingRemark;?></td>
                        <td><?=$vdt;?></td>
                        <td><?=$nextTm;?></td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>