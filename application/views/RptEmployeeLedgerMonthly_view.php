<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptEmployeeLedgerMonthly_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(payments, dues, paymentsOpBal, duesOpBal)
	{
		 // alert(JSON.stringify(payments));
		  $("#tbl1").empty();
		  bal = 0;
		  // $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      newRowIndex = table.rows.length;
          row = table.insertRow(newRowIndex);
          row.style.color = 'Black';

          var cell = row.insertCell(0);
           cell.style.display="none";
          // cell.innerHTML = payments[i].paymentRowId;

          var cell = row.insertCell(1);
          cell.innerHTML = "<span style='visibility:hidden;'>01-Jan-2000</span>";

          var cell = row.insertCell(2);
          // console.log(paymentsOpBal[0].amt);
          if( paymentsOpBal[0].amt != null )
          {
          	cell.innerHTML = paymentsOpBal[0].amt;
          }
          else
          {
          	cell.innerHTML = "0";
          }
          var cell = row.insertCell(3);
          if( duesOpBal[0].net != null )
          {
          	cell.innerHTML = duesOpBal[0].net;
          }
          else
          {
          	cell.innerHTML = "0";
          }
          var cell = row.insertCell(4);
          // cell.innerHTML = parseInt(paymentsOpBal[0].amt) - parseInt(duesOpBal[0].net);
          // bal = parseInt(paymentsOpBal[0].amt) - parseInt(duesOpBal[0].net);

          var cell = row.insertCell(5);
          cell.innerHTML = "Opening Bal.";

	      /////// Direct Payments
	      for(i=0; i<payments.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);
	          row.style.color = 'red';

	          var cell = row.insertCell(0);
	           cell.style.display="none";
	          cell.innerHTML = payments[i].paymentRowId;
	          var cell = row.insertCell(1);
	          cell.innerHTML = dateFormat(new Date(payments[i].dt));
	          var cell = row.insertCell(2);
	          cell.innerHTML = payments[i].amt;
	          var cell = row.insertCell(3);
	          cell.innerHTML = "0";
	          var cell = row.insertCell(4);
	          // bal = parseFloat(bal) + parseFloat(records[i].amt);
          	//   cell.innerHTML = bal;
	          var cell = row.insertCell(5);
	          cell.innerHTML = payments[i].remarks;
	  	  }
	  	  /////// END - Direct Payments

	  	  /////// Dues in salary
	      for(i=0; i<dues.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);
	          row.style.color = 'blue';

	          var cell = row.insertCell(0);
	           cell.style.display="none";
	          cell.innerHTML = "x";
	          var cell = row.insertCell(1);
	          cell.innerHTML = dateFormat(new Date(dues[i].dt));
	          var cell = row.insertCell(2);
	          cell.innerHTML = "0";
	          var cell = row.insertCell(3);
	          cell.innerHTML = dues[i].net;
	          var cell = row.insertCell(4);
	          var cell = row.insertCell(5);
	          cell.innerHTML = "Salary";
	  	  }
	  	  /////// END - Dues in salary

	  	  

	  // 	  ////Cloning last row
	  //   	var $tableBody = $('#tbl1');
			// $trLast = $tableBody.find("tr:last");
		 //    $trow = $tableBody.find("tr:last"),
		 //    $trNew = $trow.clone();
			// $trLast.after($trNew);

			// //// Set all null in last copied row
			// $('table#tbl1 tr:last td').text('');
			// $('table#tbl1 tr:last').css('color', 'blue');
			// $('table#tbl1 tr:last td:eq(0)').html('<span style="display:none;">z</span>');	/// taki asc sort m last m rahe
			// $('table#tbl1 tr:last td:eq(1)').html('<span style="display:none;">z</span>Total'); /// taki asc sort m last m rahe

			// //// Calling function for col total of table
			// $('#tbl1 th').each(function(i) {
	  //               calculateColumn(i);
	  //           });

		$("#tbl1 tr").on("click", highlightRow);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    order: [[ 1, "asc" ]]
		});
		} );

		////loading bal
	  	  $('#tbl1 tbody tr').each(function() {
	  	  		// if( $(this).index() >0 )
	  	  		{
		            var paid = parseInt($('td', this).eq(2).text());
		            var due = parseInt($('td', this).eq(3).text());
		            bal = bal + paid - due;
		            $('td', this).eq(4).text( bal );
	        	}
	        });
			
	}

	function calculateColumn(index) {
		if( index > 1 && index < 8 )			/// shuru k col ka sum ni karna
        {
	        var total = 0;
	        $('#tbl1 tr').each(function() {
	            var value = parseFloat($('td', this).eq(index).text());
	            if (!isNaN(value)) {
	                total += value;
	            }
	        });
	        // console.log(total);
	        // $('table tfoot td').eq(index).text('Total: ' + total);
	        $('#tbl1').find("tr:last").find("td:eq("+index+")").text('' + total);
		}

    }

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'empRowId': empRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
						setTable(data['payments'], data['dues'], data['paymentsOpBal'], data['duesOpBal']);
						alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text();
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}
		empName = $("#cboEmp option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'empRowId': empRowId
							, 'empName': empName
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}


</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px;font-size:3vw'>Employee Ledger (Monthly)</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							// $("#dtFrom").val(dateFormat(new Date()));
							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
							$("#dtFrom").val(dateFormat(firstDay));
						</script>					
		          	</div>
		          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Employee:</label>";
							echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
		              	?>
		          	</div>
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<td style='font-weight: bold; display:none;'>refRowid</td>
					 	<td style='font-weight:bold;'>Date</td>
					 	<td style='font-weight:bold;'>Paid</td>
					 	<td style='font-weight:bold;'>Recd.</td>
					 	<td style='font-weight:bold;'>Bal.</td>
					 	<td style='font-weight:bold;'>Remarks</td>
					 </tr>
				 </thead>
				 <tbody></tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; display: none;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>





<script type="text/javascript">


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );
</script>