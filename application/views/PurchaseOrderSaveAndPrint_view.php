<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='PurchaseOrderSaveAndPrint_Controller';
	var base_url='<?php echo site_url();?>';

	

	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    // alert(globalPoRowId);
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// alert($(tr).find('td:eq(0)').text() + "      " + globalPoRowId);
	    	if( parseInt($(tr).find('td:eq(0)').text()) == parseInt(globalPoRowId) )
	    	{
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(10)').text()
		            , "dt" : $(tr).find('td:eq(3)').text()
		            , "partyRowId" : $(tr).find('td:eq(4)').text()
		            , "totalAmt" : $(tr).find('td:eq(6)').text()
		            , "totalQty" : $(tr).find('td:eq(7)').text()
		            , "vNo" : $(tr).find('td:eq(8)').text().substring(3)
		            , "orderValidity" : $(tr).find('td:eq(10)').text()
		            , "paymentTerms" : $(tr).find('td:eq(11)').text()
		            , "freight" : $(tr).find('td:eq(12)').text()
		            , "inspection" : $(tr).find('td:eq(13)').text()
		            , "delayClause" : $(tr).find('td:eq(14)').text()
		            , "modeOfDespatch" : $(tr).find('td:eq(15)').text()
		            , "guarantee" : $(tr).find('td:eq(16)').text()
		            , "insurance" : $(tr).find('td:eq(17)').text()
		            , "specialNote" : $(tr).find('td:eq(18)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}

	function storeTblValuesProducts()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( parseInt($(tr).find('td:eq(0)').text()) == parseInt(globalPoRowId) )
	    	// {
	        	TableData[i]=
	        	{
		            "productRowId" : $(tr).find('td:eq(0)').text()
		            , "productName" : $(tr).find('td:eq(1)').text()
		            , "rate" : $(tr).find('td:eq(2)').text()
		            , "qty" : $(tr).find('td:eq(3)').text()
		            , "amt" : $(tr).find('td:eq(4)').text()
		            , "discount" : $(tr).find('td:eq(5)').text()
		            , "amtAfterDiscount" : $(tr).find('td:eq(6)').text()
		            , "colour" : $(tr).find('td:eq(7)').text()
		            , "deliveryDt" : $(tr).find('td:eq(8)').text()
		            , "remarks" : $(tr).find('td:eq(9)').text()
		            , "poDetailRowId" : $(tr).find('td:eq(10)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    // tblRowsCount = i-1;
	    return TableData;
	}
	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		var TableDataProducts;
		TableDataProducts = storeTblValuesProducts();
		TableDataProducts = JSON.stringify(TableDataProducts);
		// alert(JSON.stringify(TableDataProducts));
		// return;

		// if(tblRowsCount == 0)
		// {
		// 	alertPopup("No product selected...", 8000);
		// 	$("#cboProducts").focus();
		// 	return;
		// }

		poRowId = globalPoRowId;


		$.ajax({
				'url': base_url + '/' + controller + '/insert',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'poRowId': poRowId
							, 'TableData': TableData
							, 'TableDataProducts': TableDataProducts
						},
				'success': function(data)
				{
					 alertPopup("PO Approved...", 8000);
					 window.location.href=data;
					 // location.reload();
				}
				
		});
	}

	function storeTblValuesReprint()
	{
	    var TableData = new Array();
	    var i=0;
	    // alert(globalPoRowId);
	    $('#tblReprint tr').each(function(row, tr)
	    {
	    	if( parseInt($(tr).find('td:eq(0)').text()) == parseInt(globalPoRowIdReprint) )
	    	{
	        	TableData[i]=
	        	{
		            "poDetailRowId" : $(tr).find('td:eq(10)').text()
		            , "dt" : $(tr).find('td:eq(3)').text()
		            , "partyRowId" : $(tr).find('td:eq(4)').text()
		            , "totalAmt" : $(tr).find('td:eq(6)').text()
		            , "totalQty" : $(tr).find('td:eq(7)').text()
		            , "vNo" : $(tr).find('td:eq(8)').text().substring(3)
		            , "orderValidity" : $(tr).find('td:eq(10)').text()
		            , "paymentTerms" : $(tr).find('td:eq(11)').text()
		            , "freight" : $(tr).find('td:eq(12)').text()
		            , "inspection" : $(tr).find('td:eq(13)').text()
		            , "delayClause" : $(tr).find('td:eq(14)').text()
		            , "modeOfDespatch" : $(tr).find('td:eq(15)').text()
		            , "guarantee" : $(tr).find('td:eq(16)').text()
		            , "insurance" : $(tr).find('td:eq(17)').text()
		            , "specialNote" : $(tr).find('td:eq(18)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}

	function storeTblValuesProductsReprint()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProductsReprint tr').each(function(row, tr)
	    {
	    	// if( parseInt($(tr).find('td:eq(0)').text()) == parseInt(globalPoRowId) )
	    	// {
	        	TableData[i]=
	        	{
		            "productRowId" : $(tr).find('td:eq(0)').text()
		            , "productName" : $(tr).find('td:eq(1)').text()
		            , "rate" : $(tr).find('td:eq(2)').text()
		            , "qty" : $(tr).find('td:eq(3)').text()
		            , "amt" : $(tr).find('td:eq(4)').text()
		            , "discount" : $(tr).find('td:eq(5)').text()
		            , "amtAfterDiscount" : $(tr).find('td:eq(6)').text()
		            , "colour" : $(tr).find('td:eq(7)').text()
		            , "deliveryDt" : $(tr).find('td:eq(8)').text()
		            , "remarks" : $(tr).find('td:eq(9)').text()
		            , "poDetailRowId" : $(tr).find('td:eq(10)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    // tblRowsCount = i-1;
	    return TableData;
	}

	function reprint()
	{	
		var TableData;
		TableData = storeTblValuesReprint();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		var TableDataProducts;
		TableDataProducts = storeTblValuesProductsReprint();
		TableDataProducts = JSON.stringify(TableDataProducts);
		// alert(JSON.stringify(TableDataProducts));
		// return;

		poRowId = globalPoRowIdReprint;


		$.ajax({
				'url': base_url + '/' + controller + '/reprint',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'poRowId': poRowId
							, 'TableData': TableData
							, 'TableDataProducts': TableDataProducts
						},
				'success': function(data)
				{
					 window.location.href=data;
				}
				
		});
	}


	function deleteRecord(rowId)
	{
		// alert(globalPoVnoForDeletion);
		// return;
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'poRowId': globalPoRowIdForDeletion, 'poVno': globalPoVnoForDeletion},
				'success': function(data){
					// alert();
					if( data == "cannot")
						{
							alertPopup('Cannot cancel, dependent rows exists...', 10000);
						}
						else
						{
							alertPopup('Record deleted...', 1000);
							location.reload();
						}
					
				}
			});
	}

</script>
<div class="container-fluid" style="width: 95%;">



	<div class="row" style="margin-top:-30px;border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 align="center">Purchase Order Save and Print</h3>
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; ;height:250px; overflow:auto;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>poRowId</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Total Amt</th>
					 	<th>Total Qty</th>
					 	<th>V.No.</th>
					 	<th>Punched By</th>
					 	<th>Order Validity</th>
					 	<th>Payment Terms</th>
					 	<th>Freight</th>
					 	<th>Inspection</th>
					 	<th>Delay Clause</th>
					 	<th>Mode Of Despatch</th>
					 	<th>Guarantee</th>
					 	<th>Insurance</th>
					 	<th>Special Note</th>
					 	<th>Purchase/Exp.</th>
					 	<th>PayType</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['totalAmt']."</td>";
						 	echo "<td>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['uid']."</td>";
						 	echo "<td>".$row['orderValidity']."</td>";
						 	echo "<td>".$row['paymentTerms']."</td>";
						 	echo "<td>".$row['freight']."</td>";
						 	echo "<td>".$row['inspection']."</td>";
						 	echo "<td>".$row['delayClause']."</td>";
						 	echo "<td>".$row['modeOfDespatch']."</td>";
						 	echo "<td>".$row['guarantee']."</td>";
						 	echo "<td>".$row['insurance']."</td>";
						 	echo "<td>".$row['specialNote']."</td>";
							echo "<td>".$row['purchaseOrExpense']."</td>";
							echo "<td>".$row['PayType']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px; height:200px; overflow:auto;">
			<table class='table table-bordered' id='tblProducts'>
				<tr>
					<th style='display:none;'>productRowId</th>
				 	<th>Product</th>
				 	<th>Rate</th>
				 	<th>Qty</th>
				 	<th>Amt</th>
				 	<th>Dis(%)</th>
				 	<th>After Dis Amt</th>
				 	<th>Colour</th>
				 	<th>Del Dt.</th>
				 	<th>Remarks</th>
				 	<th>PoDetailRowId</th>
				</tr>
			</table>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
      	</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Save And Print' id='btnSave' class='btn btn-primary form-control'>";
          	?>
      	</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<hr style="border-color: hotpink;" />
<!-- Reprint Design -->

	<div class="row" style="margin-top:30px; background-color: #f6f6f6;padding: 5px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="background-color: white;">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; ;height:250px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tblReprint'>
				 <thead>
					 <tr>
						<th style='display:none;'>poRowId</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Total Amt</th>
					 	<th>Total Qty</th>
					 	<th>V.No.</th>
					 	<th>Punched By</th>
					 	<th>Order Validity</th>
					 	<th>Payment Terms</th>
					 	<th>Freight</th>
					 	<th>Inspection</th>
					 	<th>Delay Clause</th>
					 	<th>Mode Of Despatch</th>
					 	<th>Guarantee</th>
					 	<th>Insurance</th>
					 	<th>Special Note</th>
					 	<th>Purchase/Exp.</th>
					 	<th>Payment Type</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($recordsSaved as $row) 
						{
						 	$rowId = $row['poRowId'];
						 	$PoVno = $row['vNo'];
						 	echo "<tr>";						//onClick="editThis(this);
						 	echo "<td style='display:none;'>".$row['poRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['vType']."</td>";
						 	echo "<td style='display:none;'>".$row['vNo']."</td>";
						 	$vdt = strtotime($row['vDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['totalAmt']."</td>";
						 	echo "<td>".$row['totalQty']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['uid']."</td>";
						 	echo "<td>".$row['orderValidity']."</td>";
						 	echo "<td>".$row['paymentTerms']."</td>";
						 	echo "<td>".$row['freight']."</td>";
						 	echo "<td>".$row['inspection']."</td>";
						 	echo "<td>".$row['delayClause']."</td>";
						 	echo "<td>".$row['modeOfDespatch']."</td>";
						 	echo "<td>".$row['guarantee']."</td>";
						 	echo "<td>".$row['insurance']."</td>";
						 	echo "<td>".$row['specialNote']."</td>";
						 	echo "<td>".$row['purchaseOrExpense']."</td>";
						 	echo "<td>".$row['PayType']."</td>";
						 	echo '<td  style="display:none1; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.', '.$PoVno.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px; height:200px; overflow:auto;background-color: white;border-radius:25px;box-shadow:5px 5px #d3d3d3">
			<table class='table table-bordered' id='tblProductsReprint'>
				<tr>
					<th style='display:none;'>productRowId</th>
				 	<th>Product</th>
				 	<th>Rate</th>
				 	<th>Qty</th>
				 	<th>Amt</th>
				 	<th>Dis(%)</th>
				 	<th>After Dis Amt</th>
				 	<th>Colour</th>
				 	<th>Del Dt.</th>
				 	<th>Remarks</th>
				 	<th>PoDetailRowId</th>
				</tr>
			</table>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
      	</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="margin-bottom:10%">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='reprint();' value='Re-Print' id='btnReprint' class='btn btn-danger btn-block'>";
          	?>
      	</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">SB</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalPoRowIdForDeletion);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>




<script type="text/javascript">
	$(document).ready(function(){
		$("#tbl1 tr").on('click', showDetail);
		$("#tblReprint tr").find("td:lt(20)").on('click', showDetailReprint);
		// $("#tblReprint tr").on('click', showDetailReprint);
		$("#tblReprint tr").on("click", highlightRow);
	});

	var globalPoRowIdForDeletion=0;
	var globalPoVnoForDeletion=0;
	function delrowid(poRowId, poVno)
	{
		globalPoRowIdForDeletion = poRowId;
		globalPoVnoForDeletion = poVno;
	}

	var globalPoRowId = 0;
	function showDetail()
	{
		// alert();
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		poRowId = $(this).closest('tr').children('td:eq(0)').text();
		globalPoRowId = poRowId;
		$.ajax({
			'url': base_url + '/PurchaseOrderApprove_Controller/getProducts',
			'type': 'POST', 
			'data':{'poRowId':poRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data));
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].productName;
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].rate;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].orderQty;
		          cell.style.color="red";
	          	  // cell.setAttribute("contentEditable", true);
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].amt;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].discount;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].amtAfterDiscount;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].colour;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].deliveryDt;
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].remarks;
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].poDetailRowId;
		        }
		        // $("#tblProducts tr").on("click", highlightRow);	
			}
		});
	}

	var globalPoRowIdReprint = 0;
	function showDetailReprint()
	{
		// alert();
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		poRowId = $(this).closest('tr').children('td:eq(0)').text();
		globalPoRowIdReprint = poRowId;
		$.ajax({
			'url': base_url + '/PurchaseOrderApprove_Controller/getProducts',
			'type': 'POST', 
			'data':{'poRowId':poRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data));
				$("#tblProductsReprint").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProductsReprint");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].productName;
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].rate;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].orderQty;
		          cell.style.color="red";
	          	  // cell.setAttribute("contentEditable", true);
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].amt;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].discount;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].amtAfterDiscount;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].colour;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].deliveryDt;
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].remarks;
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].poDetailRowId;
		        }
		        // $("#tblProducts tr").on("click", highlightRow);	
			}
		});
	}

</script>