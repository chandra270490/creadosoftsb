<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='OurProduct_Controller';
	var base_url='<?php echo site_url();?>';


	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  
		 // setHeadings();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      // alert(noOfDays);
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.backgroundColor="#F0F0F0";
	          // cell.style.display="none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].productName;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].ourProduct;
	          cell.setAttribute("contentEditable", true);
	        
	  	  }

		// $("#tbl1 tr").on("click", highlightRowAlag);
	}

	function loadData()
	{	
		productCategoryRowId = $("#cboProductCategories").val();
		if( productCategoryRowId == -1)
		{
			alertPopup("Select Category...", 8000);
			cboProductCategories.focus();
			return;
		}
		// alert(productCategoryRowId);
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productCategoryRowId': productCategoryRowId
							, 'dtTo': 'dtTo'
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

	var invalidChar="";
	function storeTblValues()
	{
	    var TableData = new Array();
	    invalidChar = "N";
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if(row>0)
	    	{
		    	if( ($(tr).find('td:eq(3)').text() == "Y" || $(tr).find('td:eq(3)').text() == "y" || $(tr).find('td:eq(3)').text() == "N" || $(tr).find('td:eq(3)').text() == "n" ) )
		    	{
		        	TableData[i]=
		        	{
			            "productRowId" : $(tr).find('td:eq(1)').text()
			            , "ourProduct" :$(tr).find('td:eq(3)').text().toUpperCase()
		        	}   
		        	i++; 
		        }
		        else
		        {
		        	invalidChar = "Y";
		        }
	    	}
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    // tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(invalidChar == "Y")
		{
			msgBoxError("Error","Only Y or N allowed...");
			return;
		}

		// alert("Sahi");
		// return;
		productCategoryRowId = $("#cboProductCategories").val();
		if( productCategoryRowId == -1)
		{
			alertPopup("Select Category...", 8000);
			cboProductCategories.focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'productCategoryRowId': productCategoryRowId
						},
				'success': function(data)
				{
					alert('Changes saved...');
					location.reload();
				}
		});
		
	}
	

</script>
<div class="acontainer">
    <div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px'>Our Products</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1', "class='form-control' id='cboProductCategories'");
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn form-control' style='background-color: lightgray;'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
							 	<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none1;'>productRowId</th>
							 	<th width="150" >Product Name</th>
							 	<th width="80" >Our Product</th>
							 </tr>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='setAllYes();' value='Set All Yes' id='btnSave' class='btn btn-success form-control'>";
	      	?>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='setAllNo();' value='Set All No' id='btnSave' class='btn btn-danger form-control'>";
	      	?>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnSave' class='btn btn-primary form-control'>";
	      	?>
		</div>
	</div>
</div>





<script type="text/javascript">
	function setAllYes()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if(row>0)
	    	{
	    		$(tr).find('td:eq(3)').text( "Y" );
	    	}
	    }); 
	}

	function setAllNo()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if(row>0)
	    	{
	    		$(tr).find('td:eq(3)').text( "N" );
	    	}
	    }); 
	}
</script>