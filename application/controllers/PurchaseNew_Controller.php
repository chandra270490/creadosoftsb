<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseNew_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Purchasenew_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Purchase New') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Purchasenew_model->getDataLimit();
			$data['parties'] = $this->Purchasenew_model->loadParties();
			// $data['parties'] = $this->Util_model->getPartyList();
			$this->load->model('Productcategories_model');
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('PurchaseNew_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['pendingProducts'] = $this->Purchasenew_model->showData();
		echo json_encode($data);
	}

	public function getPurchaseDetial()
	{
		$data['purchaseDetail'] = $this->Purchasenew_model->getPurchaseDetail();
		echo json_encode($data);
	}
	

	
	public function insert()
	{
		if($this->Util_model->isSessionExpired() == 1)
        {
        	$data = "Session out...";
        	echo json_encode($data);
        	// echo $data;
        }
        else
        {
			$this->Purchasenew_model->insert();
			// $data['records'] = $this->Purchasenew_model->getDataLimit();
			// echo json_encode($data);
			$this->printToPdf('Save');
		}
	}

	public function ShortClose()
	{
		$vNo = $_REQUEST['vNo'];
		
		if($this->Util_model->isSessionExpired() == 1)
        {
        	$data = "Session out...";
        	echo json_encode($data);
        	// echo $data;
        }
        else
        {
			$this->Purchasenew_model->ShortClose($vNo);
			// $data['records'] = $this->Purchasenew_model->getDataLimit();
			echo json_encode($data);
		}
	}


	public function delete()
	{
		$this->Purchasenew_model->delete();
		$data['records'] = $this->Purchasenew_model->getDataLimit();
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Purchasenew_model->getDataAll();
		echo json_encode($data);
	}

	public function printToPdf($arg)
	{
		$rowId = -10;
		if($arg == "Update")
		{
			$rowId = $this->input->post('globalrowid');
		}
		else if($arg == "Save")
		{
			$rowId = $this->Purchasenew_model->getPurchaseNo();
		}
		

		$data['purchase'] = $this->Purchasenew_model->getPurchase($rowId);

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Purchase');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg();
		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, base_url());
		// $pdf->Write(15, $_SERVER['DOCUMENT_ROOT']);
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['tin'] . ', GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Line(0, 73, 210, 73);

		$addr = $data['purchase'][0]['addr'];
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$addrNew = "";
		for($c=0; $c<$cnt; $c++)
		{
			$addrNew .= $textlines[$c] . "<br />";
		}


		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;


		$poNo = $data['purchase'][0]['vNo'];
		$vDt = date('d-m-Y');
		// $yr  =  substr($this->input->post('vDt'),7); 
		$yr  =  date('Y');

		$html='<table border="0" cellpadding="5">
					<tr>
						<td colspan="3" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Purchase</td>
					</tr>
					<tr>
						<td colspan="3" align="center" style="height:3mm;"></td>
					</tr>
					<tr>
						<td style="width:95mm;height:7mm;">P.No. [Date]: '.$data['org'][0]['shortName'] .'/'. $yr . '/'.$poNo.' ['.$vDt.']</td>
						<td colspan="2" align="left" style="width:95mm;"></td>
					</tr>
					<tr>
						<td style="height:6mm;font-size: 12pt; font-weight:bold;">'.$data['purchase'][0]['prefixType']. ' ' .$data['purchase'][0]['name'].'</td>
						<td colspan="2" align="left" style="height:6mm; font-weight:bold;"></td>
						
					</tr>
					<tr>
						<td style="font-size: 10pt;">'.$addrNew.'</td>
						<td colspan="2" align="left" style="border-left:1px solid lightgrey;"></td>
					</tr>
					<tr>
						<td style="height:10mm;font-size: 10pt;">GSTIN: '.$data['purchase'][0]['gstIn'].'</td>
						<td style="font-size: 10pt;"></td>
					</tr>
					
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$productRows = "";
		$despDetRowIDs = "";
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
				$productRows .= "<td style=\"font-size: 9pt;\">". $sn ."</td>";
				$productRows .= "<td style=\"font-size: 9pt;\">". $myTableData[$k]['productName'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableData[$k]['receivedQty'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableData[$k]['rate'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableData[$k]['amt'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableData[$k]['disPer'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableData[$k]['gstRate'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableData[$k]['netAmt'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $this->input->post('refBillNo') ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $this->input->post('refBillDt') ."</td>";
			$productRows .= "</tr>";
		}
		
		$html='<table border="1" cellpadding="5">
					<tr>
						<th style="width:10mm; color:white; background-color:black;">#</th>
						<th style="width:54mm; color:white; background-color:black;">Product</th>
						<th style="width:20mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Amt.</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Dis%</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">GST%</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Net</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Bill#</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Bill Dt.</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$html='<table border="0" cellpadding="5">
				<tr>
					<td>Total Amt.: '. $this->input->post('totalNetAmt') .'</td>
				</tr></table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$html='<table border="0">
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">Partner/Auth. Signatory</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$partyName = $data['purchase'][0]['name'];
		// $invNo=22;
		$pdf->Output(FCPATH . '/downloads/Purchase['.$poNo.']_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."/downloads/Purchase[".$poNo.']_'. $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}


	public function reprint()
	{
		$this->printToPdfRePrint(' ');
	}


	public function printToPdfRePrint($arg)
	{


		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Purchase');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg();
		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, base_url());
		// $pdf->Write(15, $_SERVER['DOCUMENT_ROOT']);
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['tin'] . ', GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Line(0, 73, 210, 73);

		$data['purchase'] = $this->Purchasenew_model->getPurchase($this->input->post('poRowId'));
		$addr = $data['purchase'][0]['addr'];
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$addrNew = "";
		for($c=0; $c<$cnt; $c++)
		{
			$addrNew .= $textlines[$c] . "<br />";
		}

		$poNo = $data['purchase'][0]['vNo'];
		$yr  =  substr($this->input->post('vDt'),7); 

		$html='<table border="0" cellpadding="5">
					<tr>
						<td colspan="3" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Purchase</td>
					</tr>
					<tr>
						<td colspan="3" align="center" style="height:3mm;"></td>
					</tr>
					<tr>
						<td style="width:95mm;height:7mm;">P.No. [Date]: '.$data['org'][0]['shortName'] .'/'. $yr . '/'.$poNo.' ['.$this->input->post('vDt').']</td>
						<td colspan="2" align="left" style="width:95mm;"></td>
					</tr>
					<tr>
						<td style="height:6mm;font-size: 12pt; font-weight:bold;">'.$data['purchase'][0]['prefixType']. ' ' .$data['purchase'][0]['name'].'</td>
						<td colspan="2" align="left" style="height:6mm; font-weight:bold;"></td>
						
					</tr>
					<tr>
						<td style="font-size: 10pt;">'.$addrNew.'</td>
						<td colspan="2" align="left" style="border-left:1px solid lightgrey;"></td>
					</tr>
					<tr>
						<td style="height:10mm;font-size: 10pt;">GSTIN: '.$data['purchase'][0]['gstIn'].'</td>
						<td style="font-size: 10pt;"></td>
					</tr>
					
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');




		$myTableDataProducts = $this->input->post('TableDataProducts');
        $myTableDataProducts = stripcslashes($myTableDataProducts);
        $myTableDataProducts = json_decode($myTableDataProducts,TRUE);
        $myTableRowsProducts = count($myTableDataProducts);
		$r = $myTableRowsProducts;
		$productRows = "";
		$totalAmt=0;
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
				$productRows .= "<td style=\"font-size: 9pt;\">". $sn ."</td>";
				$productRows .= "<td style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['productName'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['rQty'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['rate'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['amt'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['disPer'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['gstRate'] ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $myTableDataProducts[$k]['netAmt'] ."</td>";
				$totalAmt += $myTableDataProducts[$k]['netAmt'];
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $this->input->post('globalBillNoForReprint') ."</td>";
				$productRows .= "<td align=\"right\" style=\"font-size: 9pt;\">". $this->input->post('globalBillDtForReprint') ."</td>";
			$productRows .= "</tr>";
		}
		$html='<table  border="1" cellpadding="4">
					<tr>
						<th style="width:10mm; color:white; background-color:black;">#</th>
						<th style="width:54mm; color:white; background-color:black;">Product</th>
						<th style="width:20mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Amt.</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Dis%</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">GST%</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Net</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Bill#</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Bill Dt.</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$html='<table border="0" cellpadding="5">
				<tr>
					<td>Total Amt.: '. $totalAmt .'</td>
				</tr></table>';
		$pdf->writeHTML($html, true, false, true, false, '');



		$html='<table border="0">
					<tr>
						<td align="right" style="width:180mm;height:15mm;font-size:11pt;"></td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:15mm;font-size:11pt;">For '.$data['org'][0]['orgName'].'</td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">Partner/Auth. Signatory</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		// $dt = date("Y_m_d");
		// $tm = date("H_i_s");
		// // $pdf->Output(FCPATH . '/downloads/PO_'. $dt . ' (' . $tm . ').pdf', 'F');
		// $fileName = "".$PoVno."_".$dt."[".$tm."]_".$partyName;
		// $pdf->Output(FCPATH . '/downloads/'. $fileName.'.pdf', 'F');

		// echo base_url()."/downloads/". $fileName.".pdf";

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$partyName = 'xx';//$data['purchase'][0]['name'];
		$poNo=22;
		$pdf->Output(FCPATH . '/downloads/PurchaseRP['.$poNo.']_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."/downloads/PurchaseRP[".$poNo.']_'. $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}

}
