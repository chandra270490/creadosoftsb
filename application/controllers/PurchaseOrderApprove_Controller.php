<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseOrderApprove_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Purchaseorderapprove_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Purchase Order Approve') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Purchaseorderapprove_model->getPurchaseOrders();
			$data['approvedRecords'] = $this->Purchaseorderapprove_model->getDataLimit();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('PurchaseOrderApprove_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getProducts()
	{
		$data['products'] = $this->Purchaseorderapprove_model->getProducts();
		echo json_encode($data);
	}

	public function insert()
	{
		$this->Purchaseorderapprove_model->insert();
		echo json_encode('$data');
	}

	public function reject()
	{
		$this->Purchaseorderapprove_model->reject();
		echo json_encode('$data');
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Purchaseorderapprove_model->getDataAll();
		echo json_encode($data);
	}


	public function delete()
	{
		$c = $this->Purchaseorderapprove_model->checkDependency();
		// echo json_encode($c);
		// return;
		if( $c == 1)
		{
			echo json_encode('cannot');
		}
		else
		{
			$this->Purchaseorderapprove_model->delete();
			$data['records'] = $this->Purchaseorderapprove_model->getDataLimit();
			echo json_encode($data);
		}
	}	
}
