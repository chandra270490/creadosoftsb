<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptPerformance_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptperformance_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Employee Performance') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['employees'] = $this->Rptperformance_model->getEmpList();
			$this->load->view('RptPerformance_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptperformance_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/Q.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/Q.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Employee Performance Report');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));


		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:L6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Emp. Name');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Points');

		$objPHPExcel->getActiveSheet()->getStyle('F7:L7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "L" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['dt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['empName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['points']);

		 	$i++;
		}
		$r=$i-1;
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '=SUM(D8:D'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(0);	


	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// $objWriter->save("excelfiles/$acname$branch.xls");
		$objWriter->save("excelfiles/Q.xls");
		echo base_url()."excelfiles/Q.xls";

	}


}
