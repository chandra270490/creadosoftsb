<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Products_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Products')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Products_model->getDataLimit();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Products_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	} 
	
	//Added By <CNS-202012241506>
	public function del_prod()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Products')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Products_model->getDataLimitDel();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Products_view_del', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}
	//END CNS

	public function insert()
	{
		if($this->Products_model->checkDuplicate() == 1)
        {
        	// $data = "Duplicate record...";
        	// echo json_encode($data);
        	echo "Duplicate...";
        }
        else
        {
          $rowId = $this->Products_model->getRowId(); 
          $rowId = sprintf("%04d", $rowId);
          $tmp = base_url()."bootstrap/images/products/";
          // echo $tmp;
          $config = array(
            'upload_path' => "bootstrap/images/products/",
            'file_name' => $rowId . '.jpg',
            'allowed_types' => "jpg|jpeg",
            'overwrite' => TRUE,
            'max_size' => "1024000", // Can be set to particular file size , here it is 2 MB(2048 Kb = 2048000 B)
            'max_height' => "600", 
            'max_width' => "800"  
          );
          $this->load->library('upload', $config);

          if($_FILES["imageFile"]["name"] != "")
		  {
		      if( $this->upload->do_upload('imageFile') )
		      {
	          
	            $config['image_library'] = 'gd2';
	            $config['source_image'] = 'bootstrap/images/products/' . $rowId . '.jpg';
	            $config['create_thumb'] = TRUE;
	            $config['maintain_ratio'] = TRUE;
	            $config['width']         = 60;
	            $config['height']       = 60;
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();    

	            $this->Products_model->insert();   
	            echo "Done...";
	          }
	          else
	          {
	          	$error = array('error' => $this->upload->display_errors());
	          	echo json_encode($error);	
	          }
      	  }
      	  else ///in case no file selected but detail store
      	  {
      	  	$this->Products_model->insert();   
	        echo "Done...";
      	  }
        }
	}

	public function update()
	{
		if($this->Products_model->checkDuplicateOnUpdate() == 1)
        {
        	echo "Duplicate...";
        }
        else
        {
			if($_REQUEST['txtPhotoChange'] == "yes")
	        {
		          $rowId = $_REQUEST['txtHiddenRowId']; 
		          $rowId = sprintf("%04d", $rowId);
		          $tmp = base_url()."bootstrap/images/products/";
		          // echo $tmp;
		          $config = array(
		            'upload_path' => "bootstrap/images/products/",
		            'file_name' => $rowId . '.jpg',
		            'allowed_types' => "jpg|jpeg",
		            'overwrite' => TRUE,
		            'max_size' => "1024000", // Can be set to particular file size , here it is 2 MB(2048 Kb = 2048000 B)
		            'max_height' => "600", 
		            'max_width' => "800"  
		          );
		          $this->load->library('upload', $config);

		          if($_FILES["imageFile"]["name"] != "")
				  {
				      if( $this->upload->do_upload('imageFile') )
				      {
			          
			            $config['image_library'] = 'gd2';
			            $config['source_image'] = 'bootstrap/images/products/' . $rowId . '.jpg';
			            $config['create_thumb'] = TRUE;
			            $config['maintain_ratio'] = TRUE;
			            $config['width']         = 60;
			            $config['height']       = 60;
			            $this->load->library('image_lib', $config);
			            $this->image_lib->resize();    

			            $this->Products_model->update();   
			            echo "Done...";
			          }
			          else
			          {
			          	$error = array('error' => $this->upload->display_errors());
			          	echo json_encode($error);	
			          }
		      	  }
		      	  else ///in case no file selected but detail store
		      	  {
		      	  	$this->Products_model->update();   
			        echo "Done...";
		      	  }
		    }
		    else //if image not changed
		    {
		    	$this->Products_model->update();   
			    echo "Done...";
		    }
		}
	}

	public function delete()
	{
		$this->Products_model->delete();
		$data['records'] = $this->Products_model->getDataLimit();
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Products_model->getDataAll();
		echo json_encode($data);
	}

	public function loadAllRecordsDel()
	{
		$data['records'] = $this->Products_model->getDataAllDel();
		echo json_encode($data);
	}

	public function loadRecords()
	{
		$data['records'] = $this->Products_model->getDataLimit();
		echo json_encode($data);
	}

	//Product UOM View
	public function prod_uom(){
		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('prod_uom'); 
		$this->load->view('includes/footer');
	}

	//Product UOM Entry
	public function prod_uom_entry(){ 
		$data = array();
		$data['prod_uom_entry'] = $this->Products_model->prod_uom_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'Products_Controller/prod_uom?id=';
		$this->load->view('QueryPage',$data); 
	}

}
