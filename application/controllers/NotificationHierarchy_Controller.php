<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationHierarchy_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Notificationhierarchy_model');
            $this->load->model('User_model');
            // $this->load->model('Menu_model');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '')
		{
			if($this->Util_model->getRight($this->session->userRowId,'Notification Hierarchy')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['users'] = $this->User_model->getData('rowid','uid');
			$data['users4notification'] = $this->User_model->getUsersForCheckBox();
			$data['records'] = $this->Notificationhierarchy_model->getDataLimit();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $data['vehicles'] = $this->Right_model->getDataForCheckBox();
			$this->load->view('NotificationHierarchy_view',$data);
			$this->load->view('includes/footer');
		}
		else
		{
            $this->load->view('includes/header');
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}    

	public function insert()
	{
		$this->Notificationhierarchy_model->insertAjax();
		$data['users'] = $this->User_model->getData('rowid','uid');
		$this->load->view('NotificationHierarchy_view',$data);
		$this->load->view('includes/footer');
	}

	 public function getRights()
    {
    	$arr = $this->Notificationhierarchy_model->getRights($this->input->post('uid'));
    	$msg="";
    	foreach ($arr as $key => $value) {
    		$msg = $value['userRowIdMgr'].",".$msg;
    	}
    	// echo json_encode($temp);
    	echo $msg;
    }


}
