<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductStatus_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Productstatus_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Product Status')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['products'] = $this->Productstatus_model->getProducts4CheckBox();
			$data['stages4table'] = $this->Productstatus_model->getStages4table();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['ff'] = 'gg';
			$this->load->view('ProductStatus_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getProductList()
	{
		$data['products'] = $this->Productstatus_model->getProductList();
		echo json_encode($data);
	}
	
	public function getData()
	{
		// $data['records'] = $this->Productstatus_model->getData();	///Using Database Table
		$data['records'] = $this->Productstatus_model->getDataFromPhpArray(); ///Using PHP Array
		echo json_encode($data);
	}


	public function exportData()
	{
		$this->printToPdf('Save');
	}
	public function exportDataExcel()
	{
		$this->printToExcel();
	}
	public function exportDataExternal()
	{
		$this->printToPdfExternal('Save');
	}


	public function printToPdf($arg)
	{
		$this->load->library('Pdf');
		$pdf = new Pdf('Landscape', 'mm', 'A4 Landscape', true, 'UTF-8', false);
		$pdf->SetTitle('Products List');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage('L', 'A4');

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
		$orgImagePath = $data['org'][0]['imagePath'];
		$html='<table border="0">
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['tin'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// Line ($x1, $y1, $x2, $y2, $style=array())
		$pdf->Line(0, 40, 310, 40);

		$pdf->writeHTML("<br />", true, false, true, false, '');
		$pdf->writeHTML("<h3>Products Status</h3>", true, false, true, false, '');
		$pdf->writeHTML("<br />", true, false, true, false, '');

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;


		// // $pdf->writeHTML(100,"dd");


		$productRows = "";
		$sn =0;
		for($k=0; $k < $r; $k++)
		{
			if($k == 1)
			{

			}
			else
			{
				$productRows .= "<tr nobr=\"true\">";
					if($sn==0)
						$productRows .= "<td>". $myTableData[$k]['sn'] ."</td>";
					else
						$productRows .= "<td>". $sn ."</td>";
					$productRows .= "<td>". $myTableData[$k]['productName'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S01'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S02'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S03'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S04'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S05'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S06'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S07'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S08'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S09'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S10'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S11'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S12'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S13'] . "</td>";
					$productRows .= "<td>". $myTableData[$k]['S14'] . "</td>";
		          	$fileName = $myTableData[$k]['imagePathThumb'];
		          	if( $fileName != "N" && $k>1)
		          	{
			          	$imgSrc = FCPATH."/bootstrap/images/products/".$fileName;
						$productRows .= "<td align=\"center\"><img src=\"". $imgSrc ."\" alt=\"nn\"></td>";
		          	}
					else
					{
						$productRows .= "<td>". "" ."</td>";
					}
				$productRows .= "</tr>";
				$sn++;
			}

		}
		$html='<table border="1" cellpadding="5">
					'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$pdf->Output(FCPATH . '/downloads/PL_'. $dt . ' (' . $tm . ').pdf', 'F');
		echo base_url()."/downloads/PL_". $dt . " (" . $tm . ").pdf";
	}


	public function printToPdfExternal($arg)
	{
		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Products List');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage('P', 'A4');

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
		$orgImagePath = $data['org'][0]['imagePath'];
		$html='<table border="0">
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['tin'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// Line ($x1, $y1, $x2, $y2, $style=array())
		$pdf->Line(0, 40, 310, 40);

		$pdf->writeHTML("<br />", true, false, true, false, '');
		$pdf->writeHTML("<h3>Products Status</h3>", true, false, true, false, '');
		$pdf->writeHTML("<br />", true, false, true, false, '');

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;


		// // $pdf->writeHTML(100,"dd");


		$productRows = "";
		$sn =1;
		for($k=0; $k < $r; $k++)
		{
			if($k == 1) /// rowIds
			{

			}
			else if($k == 0)  ///Headings
			{
				$productRows .= "<tr nobr=\"true\">";
					$productRows .= "<td width=\"15mm\">". $myTableData[$k]['sn'] ."</td>";
					$productRows .= "<td width=\"110mm\">". $myTableData[$k]['productName'] . "</td>";
					$productRows .= "<td width=\"20mm\">Qty.(Avbl-Odrs)</td>";
		          	$fileName = $myTableData[$k]['imagePathThumb'];
		          	if( $fileName != "N" && $k>1 && 1==2)
		          	{
			          	$imgSrc = FCPATH."/bootstrap/images/products/".$fileName;
						$productRows .= "<td align=\"center\"><img src=\"". $imgSrc ."\" alt=\"nn\"></td>";
		          	}
					else
					{
						$productRows .= "<td>". "" ."</td>";
					}
				$productRows .= "</tr>";
			}
			else
			{
				$productRows .= "<tr nobr=\"true\">";
					$productRows .= "<td width=\"15mm\">". $sn ."</td>";
					$productRows .= "<td width=\"110mm\">". $myTableData[$k]['productName'] . "</td>";
					$qtyAvailable = $myTableData[$k]['total'] - $myTableData[$k]['orders'];
					$productRows .= "<td width=\"20mm\">". $qtyAvailable . "</td>";
		          	$fileName = $myTableData[$k]['imagePathThumb'];
		          	if( $fileName != "N" && $k>1 )
		          	{
			          	$imgSrc = FCPATH."/bootstrap/images/products/".$fileName;
			         //  	$r = file_exists($imgSrc);
				        // if($r == true)
				        {
							$productRows .= "<td align=\"center\"><img src=\"". $imgSrc ."\" alt=\"nn\"></td>";
						}
						// else
						// {
						// 	$productRows .= "<td>". "" ."</td>";
						// }
		          	}
					else
					{
						$productRows .= "<td>". "" ."</td>";
					}
				$productRows .= "</tr>";
				$sn++;
			}
		}
		$html='<table border="1" cellpadding="5">
					'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$pdf->Output(FCPATH . '/downloads/PL_'. $dt . ' (' . $tm . ').pdf', 'F');
		echo base_url()."/downloads/PL_". $dt . " (" . $tm . ").pdf";
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/Q.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/Q.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:Q1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:Q2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:Q3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Product Status');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:Q4');
		// $objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));


		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:Q6');

		// $objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		// $objPHPExcel->getActiveSheet()->setCellValue('B7', 'Date');
		// $objPHPExcel->getActiveSheet()->setCellValue('C7', 'Emp. Name');
		// $objPHPExcel->getActiveSheet()->setCellValue('D7', 'Points');

		$objPHPExcel->getActiveSheet()->getStyle('F7:S7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (8) . ":" . "S" . (8);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		$sn=0;
		for($k=0; $k < $r; $k++)
		{
			if($k == 1)
			{

			}
			else
			{
			    $myCol = 0;
			    // $sn = $k + 1;
			    if($sn==0)
			    	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['sn']);
				else
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
			 	
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['productName']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S01']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S02']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S03']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S04']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S05']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S06']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S07']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S08']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S09']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S10']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S11']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S12']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S13']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['S14']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['total']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['orders']);
			    $myCol = $myCol + 1;
			 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['diff']);
			 	$sn++;
			 	$i++;
			 }
		}
		$r=$i-1;
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '=SUM(D8:D'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '=SUM(E8:E'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '=SUM(G8:G'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H8:H'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I8:I'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J8:J'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '=SUM(M8:M'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=SUM(N8:N'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '=SUM(O8:O'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '=SUM(P8:P'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=SUM(Q8:Q'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "S" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "S" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);	


	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// $objWriter->save("excelfiles/$acname$branch.xls");
		$objWriter->save("excelfiles/Q.xls");
		echo base_url()."excelfiles/Q.xls";
	}
}
