<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Towns_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Towns_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Towns')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Towns_model->getDataLimit();
			$this->load->model('Districts_model');
			$data['districts'] = $this->Districts_model->getDistrictList();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Towns_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function insert()
	{
		if($this->Towns_model->checkDuplicate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Towns_model->insert();
			$data['records'] = $this->Towns_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function update()
	{
		if($this->Towns_model->checkDuplicateOnUpdate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Towns_model->update();
			$data['records'] = $this->Towns_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function delete()
	{
		if($this->Util_model->isDependent('addressbook', 'townRowId', $this->input->post('rowId')) == 1)
        {
        	$data['dependent'] = "yes";
        	echo json_encode($data);
        }
        else
        {
			$this->Towns_model->delete();
			$data['records'] = $this->Towns_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Towns_model->getDataAll();
		echo json_encode($data);
	}

}
