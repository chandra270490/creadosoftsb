<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Productmcc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('productmcm');
	}

	//Ticket List
	public function index(){
		$tbl_nm = "productmcc";
		$data = array();
		$data['list_title'] = "Product Master Categories List";
		$data['list_url'] = "Productmcc/index";
		$data['tbl_nm'] = "productmcc";
		$data['primary_col'] = "ProductMCId";
		$data['edit_url'] = "Productmcc/productmcc_form";
		$data['edit_enable'] = "yes";
		$data['where_str'] = " ";

		$data['ViewHead'] = $this->productmcm->ListHead($tbl_nm);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('ListView', $data); 
		$this->load->view('includes/footer');
	}

	public function productmcc_form(){
		$id = $_REQUEST['id'];

		if($id != ""){
			$data['get_by_id'] = $this->productmcm->get_by_id('productmcc','ProductMCId',$id);
		} else {
			$data['get_by_id'] = "";
		}

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/masters/product_mc_form', $data); 
		$this->load->view('includes/footer');	
	}

	public function product_mc_entry(){
		$data = array();
		$data['product_mc_entry'] = $this->productmcm->product_mc_entry($data);
		$data['url'] = 'productmcc';
		$this->load->view('QueryPage', $data);	
	}
}
