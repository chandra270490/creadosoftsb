<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Invoice_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Invoice')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Util_model->getDataLimitCi('I');
			$data['parties'] = $this->Util_model->getPartyList();
			// $data['parties'] = $this->Invoice_model->getPartyList();
			// $data['transporters'] = $this->Invoice_model->getTransporterList();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Invoice_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  


	public function insert()
	{
		$res = $this->Invoice_model->insert();
		if($res == "error")
		{
			return;
		}
		
		if( $this->input->post('exportIn') == "W")
		{
			$this->printData('Save');
		}
		else
		{
			$this->printToPdf('Save');
		}

	}

	public function update()
	{
		$this->Invoice_model->update();
		if( $this->input->post('exportIn') == "W")
		{
			$this->printData('Update');
		}
		else
		{
			$this->printToPdf('Update');
		}
	}

	public function delete()
	{
		$this->Invoice_model->delete();
		$data['records'] = $this->Util_model->getDataLimitCi('I');
		echo json_encode($data);
	}

	public function printToPdf($arg)
	{
		$rowId = -10;
		if($arg == "Update")
		{
			$rowId = $this->input->post('globalrowid');
		}
		else if($arg == "Save")
		{
			$rowId = $this->Invoice_model->getInvNo();
		}
		

		$data['ci'] = $this->Invoice_model->getCi($rowId);

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Invoice');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg();
		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, base_url());
		// $pdf->Write(15, $_SERVER['DOCUMENT_ROOT']);
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['tin'] . ', GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Line(0, 73, 210, 73);

		$addr = ($this->input->post('addr'));
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$addrNew = "";
		for($c=0; $c<$cnt; $c++)
		{
			$addrNew .= $textlines[$c] . "<br />";
		}

		if( $this->input->post('cpe') == "Y" )
		{
			$cpe = "Children's Playground Equipments";
		}
		else
		{
			$cpe = "";
		}

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;

		$deliveryAddress = $this->input->post('deliveryAddress');
		$textlines = explode("\n", $deliveryAddress);
		$cnt = count($textlines);
		$addrNewDelivery = "";
		for($c=0; $c<$cnt; $c++)
		{
			$addrNewDelivery .= $textlines[$c] . "<br />";
		}

		$data['despInfo'] = $this->Invoice_model->despInfo($myTableData[0]['despatchDetailRowId']);

		
		if ($data['despInfo'][0]['orderType'] != 'Normal' ) 
		{
			$SaleType = "NOT FOR SALE";
		}
		else
		{
			$SaleType = "";	
		}
		$invNo = $data['ci'][0]['vNo'];
		$html='<table border="0" cellpadding="5">
					<tr>
						<td colspan="3" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Invoice</td>
					</tr>
					<tr>
						<td colspan="3" align="center" style="height:3mm;"></td>
					</tr>
					<tr>
						<td style="width:95mm;height:7mm;">Inv. No./Date: '.$data['ci'][0]['vNo'].'/'.$this->input->post('ciDt').'</td>
						<td colspan="2" align="left" style="width:95mm;">Odr.#/Date/Type: '.$myTableData[0]['odrNo'].'/'. $myTableData[0]['odrDt'] .'/'.$data['despInfo'][0]['orderType'].'</td>
					</tr>
					<tr>
						<td style="height:6mm;font-size: 12pt; font-weight:bold;">'.$data['ci'][0]['prefixType']. ' ' .$data['ci'][0]['name'].'</td>
						<td colspan="2" align="left" style="height:6mm; font-weight:bold;">Delivery Address:</td>
						
					</tr>
					<tr>
						<td style="font-size: 10pt;">'.$addrNew.'</td>
						<td colspan="2" align="left" style="border-left:1px solid lightgrey;">'.$addrNewDelivery.'</td>
					</tr>
					<tr>
						<td style="height:10mm;font-size: 10pt;">GSTIN: '.$data['ci'][0]['gstIn'].'</td>
						<td style="font-size: 10pt;">'.$SaleType.'</td>
					</tr>
					<tr>
						<td colspan="2" style="color:blue;font-size: 10pt;">'.$cpe.'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$productRows = "";
		$despDetRowIDs = "";
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
			$despDetRowIDs .= $myTableData[$k]['despatchDetailRowId'] . ",";
				$productRows .= "<td>". $sn ."</td>";
				$productRows .= "<td>". $myTableData[$k]['productName']. " <span style=\"color:grey;\">[PO# " . $myTableData[$k]['letterNo'] ."]</span></td>";
				$productRows .= "<td>". $myTableData[$k]['hsn'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['qty'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['rate'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['amt'] ."</td>";
			$productRows .= "</tr>";
		}
		$challanInfo = $this->Invoice_model->challanInfo($despDetRowIDs);
		$html='<table border="1" cellpadding="5">
					<tr>
						<th style="width:10mm; color:white; background-color:black;">#</th>
						<th style="width:70mm; color:white; background-color:black;">Product</th>
						<th style="width:25mm; text-align:center; color:white; background-color:black;">HSN</th>
						<th style="width:25mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="width:25mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="width:34mm; text-align:center; color:white; background-color:black;">Amt.</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$col1="";
		$col2="";
		$col3="";
		$col4="";
		$col5="";

		$col3 .= 'Total Amt.:' . '<br>';	
		// $col4 .=  '<br>';	
		$col5 .= $this->input->post('totalAmt') . '<br>';	
		if($this->input->post('discountAmt') == 0) 
		{
			$printDiscountTitle="";
			$printDiscountPer="";
			$printDiscountAmt="";
		}
		else
		{
			$printDiscountTitle = "Discount Amt.:";
			$printDiscountPer = $this->input->post('discountAmt').' ('. $this->input->post('discountPer')  .'%)' ;
			$printDiscountAmt = $this->input->post('totalAfterDiscount');
			$col3 .= $printDiscountTitle . ' [' . $printDiscountPer . ']<br>';
			// $col4 .= $printDiscountPer . '<br>';
			$col5 .= $printDiscountAmt . '<br>';
		}

		if($this->input->post('vatPer') == 0) 
		{
			$printVatTitle="";
			$printVatPer="";
			$printVatAmt="";
		}
		else
		{
			$printVatTitle = "VAT:";
			$printVatPer = $this->input->post('vatPer').'%';
			$printVatAmt = $this->input->post('vatAmt');
			$col3 .= $printVatTitle . ' (' . $printVatPer . ')<br>';
			// $col4 .= $printVatPer . '<br>';
			$col5 .= $printVatAmt . '<br>';			
		}

		if($this->input->post('sgstPer') == 0) 
		{
			$printSgstTitle="";
			$printSgstPer="";
			$printSgstAmt="";
		}
		else
		{
			$printSgstTitle = "SGST:";
			$printSgstPer = $this->input->post('sgstPer').'%';
			$printSgstAmt = $this->input->post('sgstAmt');
			$col3 .= $printSgstTitle . ' (' . $printSgstPer . ')<br>';
			$col5 .= $printSgstAmt . '<br>';			
		}
		if($this->input->post('cgstPer') == 0) 
		{
			$printCgstTitle="";
			$printCgstPer="";
			$printCgstAmt="";
		}
		else
		{
			$printCgstTitle = "CGST:";
			$printCgstPer = $this->input->post('cgstPer').'%';
			$printCgstAmt = $this->input->post('cgstAmt');
			$col3 .= $printCgstTitle . ' (' . $printCgstPer . ')<br>';
			$col5 .= $printCgstAmt . '<br>';			
		}
		if($this->input->post('igstPer') == 0) 
		{
			$printIgstTitle="";
			$printIgstPer="";
			$printIgstAmt="";
		}
		else
		{
			$printIgstTitle = "IGST:";
			$printIgstPer = $this->input->post('igstPer').'%';
			$printIgstAmt = $this->input->post('igstAmt');
			$col3 .= $printIgstTitle . ' (' . $printIgstPer . ')<br>';
			$col5 .= $printIgstAmt . '<br>';			
		}

		if($this->input->post('otherExpAmt') == 0) 
		{
			$printOtherTitle="";
			$printOtherName="";
			$printOtherAmt="";
		}
		else
		{
			$printOtherTitle = "Other Exp.:";
			$printOtherName = $this->input->post('otherExpName');
			$printOtherAmt = $this->input->post('otherExpAmt');
			$col3 .= $printOtherTitle . ' (' . $printOtherName . ')<br>';
			// $col4 .= $printOtherName . '<br>';
			$col5 .= $printOtherAmt . '<br>';			
		}
		$col3 .= 'Round:' . '<br>';
		// $col4 .= '<br>';
		$col5 .= $this->input->post('roundAmt') . '<br>';			

		$col3 .= 'Net Amt.:';
		// $col4 .= $printOtherName . '<br>';
		$col5 .= $this->input->post('net');			

		$col1 .= 'Challan No.: '.$challanInfo . '<br>';
		$col1 .= 'PR/GR No.: '.$data['despInfo'][0]['prGrNo'] . '<br>';
		$col1 .= 'Desp.Dt.: '.$data['despInfo'][0]['despatchDt'] . '<br>';
		$col1 .= 'Desp. To: '. '<br>';
		$col1 .= 'Through: '.$data['despInfo'][0]['name'];

		$col2 .= 'Total Qty.: '. $this->input->post('totalQty') . '<br>';

		if( strlen($challanInfo) == 0 )
		{
			$html='<table border="0" cellpadding="5">
						<tr>
							<td  align="left" style="width:50mm;height:7mm;font-size: 10pt;">' . $col1 . '</td>
							<td  align="left" style="width:30mm;height:7mm;font-size: 10pt;">' . $col2 .'</td>
							<td align="right" style="width:75mm;height:7mm;font-size: 10pt;">' . $col3 . '</td>
							<td align="right" style="width:33mm;height:7mm;font-size: 10pt;">' . $col5 .'</td>
						</tr>

						<tr>
							<td align="right" colspan="4" style="height:7mm;font-size: 8pt;">'. $this->input->post('inWords') .'</td>
						</tr>
				</table>';
		}
		else
		{
			$html='<table border="0" cellpadding="5">
						<tr>
							<td  align="left" style="width:50mm;height:7mm;font-size: 10pt;">' . $col1 . '</td>
							<td  align="left" style="width:30mm;height:7mm;font-size: 10pt;">' . $col2 .'</td>
							<td align="right" style="width:75mm;height:7mm;font-size: 10pt;">' . $col3 . '</td>
							<td align="right" style="width:33mm;height:7mm;font-size: 10pt;">' . $col5 .'</td>
						</tr>

						<tr>
							<td align="right" colspan="4" style="height:5mm;font-size: 8pt;">'. $this->input->post('inWords') .'</td>
						</tr>
				</table>';
		}
		$pdf->writeHTML($html, true, false, true, false, '');


		$html='<table border="0">
					<tr>
						<td style="width:180mm;height:3mm;font-size:7pt;">1. The discrepancy if any in the bill should be brought to our notice within a week from the date here of.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:3mm;font-size:7pt;">2. All disputes will have to be settled at Ajmer.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:3mm;font-size:7pt;">3. Goods supplied to order will not be accepted back.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:3mm;font-size:7pt;">4. Interest will be charged @18% if the bill remains unpaid for 30 days from the date of bill.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:3mm;font-size:8pt;">5. 50% advance while booking the order and balance 50% before despatch. </td>
					</tr>

					<tr>
						<td style="width:180mm;height:3mm;font-size:7pt;">6. E. & O.E.</td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:15mm;font-size:11pt;">For '.$data['org'][0]['orgName'].'</td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">Partner/Auth. Signatory</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$html='<table border="1" cellpadding="1">
					<tr>
						<td align="center" style="width:180mm;font-size:9pt;">Bank Details: ' . $data['org'][0]['bankName'] . ', Branch: ' . $data['org'][0]['branch'] . ', IFSC: ' . $data['org'][0]['ifsc'] . ', Ac.No.: ' . $data['org'][0]['acNo'] . '</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$partyName = $data['ci'][0]['name'];
		$pdf->Output(FCPATH . '/downloads/Inv['.$invNo.']_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."/downloads/Inv[".$invNo.']_'. $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}

	public function printData($arg)
	{

		$rowId = -10;
		if($arg == "Update")
		{
			$rowId = $this->input->post('globalrowid');
		}
		else if($arg == "Save")
		{
			$rowId = $this->Invoice_model->getInvNo();
		}
		$data['ci'] = $this->Invoice_model->getCi($rowId);

		require_once 'PHPWord-master/src/PhpWord/Autoloader.php';
		\PhpOffice\PhpWord\Autoloader::register();
		// Creating the new document...
		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		$section = $phpWord->addSection(array('orientation'=>'portrait'
																, 'marginTop' => 850
																, 'marginBottom' => 1000
																, 'marginLeft' => 850
																, 'marginRight' => 850
																, 'pageNumberingStart' => 1
																, 'breakType' => 'nextPage'
																));
		$footer = $section->addFooter();
		$footer->addPreserveText('Page {PAGE} of {SECTIONPAGES}.');


		$data['org'] = $this->Util_model->getOrg($rowId);
		$orgImagePath = $data['org'][0]['imagePath'];
	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => 'FFFFFF',
						    'borderBottomSize' => 18, 
						    'borderBottomColor' => '000000',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable', $tableStyle);
		$table = $section->addTable('myTable');
		
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false);
		$table->addRow(); // argument is height
		$table->addCell(2000, array('valign'=>'bottom', 'borderRightSize'=>10, 'borderRightColor'=>'FFFFFF'))->addImage(FCPATH.'/bootstrap/images/'.$orgImagePath , array('width'=>140,'align'=>'center'));
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 2));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>0));
		// $cell = $table->addCell(2000, array('valign'=>'top', 'gridSpan' => 2));
		// $cell->addText(" ", $fontStyle, array('spaceAfter'=>0));
		$table->addCell(2000, array('valign'=>'bottom', 'halign'=>'right','borderRightSize'=>1, 'borderRightColor'=>'FFFFFF', 'borderLeftColor'=>'FFFFFF'));


		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['orgName'], array('name' => 'Arial', 'size' => 14, 'bold'=>true), array('spaceAfter'=>0));
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['add1'].$data['org'][0]['add2'].$data['org'][0]['add3'].$data['org'][0]['add4'], $fontStyle, array('spaceAfter'=>0));
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['mobile1'].$data['org'][0]['email'], $fontStyle, array('spaceAfter'=>0));
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['tin'] . ', GSTIN: ' . $data['org'][0]['gstIn'], $fontStyle, array('spaceAfter'=>0));


		$section->addText(' ', $fontStyle, array('align' => 'center','spaceBefore'=>10, 'spaceAfter'=>4));
		$fontStyle = array('name' => 'Arial', 'size' => 14, 'bold'=>true);
		$section->addText('Invoice', $fontStyle, array('align' => 'center','spaceBefore'=>10, 'spaceAfter'=>4));

	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => 'FFFFFF',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable1', $tableStyle);
		$table = $section->addTable('myTable1');
		
		$fontStyle = array('name' => 'Arial', 'size' => 11, 'bold'=>false);

		if( $this->input->post('cpe') == "Y" )
		{
			$cpe = "Children's Playground Equipments";
		}
		else
		{
			$cpe = "";
		}

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;

		$data['despInfo'] = $this->Invoice_model->despInfo($myTableData[0]['despatchDetailRowId']);
		// $challanInfo = $this->Invoice_model->challanInfo($myTableData[0]['despatchDetailRowId']);
		if ($data['despInfo'][0]['orderType'] != 'Normal' ) 
		{
			$SaleType = "NOT FOR SALE";
		}
		else
		{
			$SaleType = "";	
		}

		$invNoForFileName = $data['ci'][0]['vNo'];
		// $data['qpo'] = $this->Quotations_model->getQpo($rowId);
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false,  'color'=>'red');
		$table->addRow(); // argument is height
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText("Inv.#/Dt.: ".$data['ci'][0]['vNo'] . "/" . $this->input->post('ciDt') , $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText("Odr.#/Dt./Type: ".$myTableData[0]['odrNo'] ."/".$myTableData[0]['odrDt']."/".$data['despInfo'][0]['orderType'], $fontStyle, array('spaceAfter'=>3));


		$table->addRow(); // argument is height
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$fontStyle = array('name' => 'Arial', 'size' => 12, 'bold'=>true);
		$cell->addText($data['ci'][0]['prefixType']. ' ' .htmlspecialchars($data['ci'][0]['name']), $fontStyle, array('spaceAfter'=>12));
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText("Delivery Address: ", $fontStyle, array('spaceAfter'=>12));

		$partyNameForFileName = $data['ci'][0]['name'];

		$fontStyle = array('name' => 'Arial', 'size' => 11, 'bold'=>false);
		
		$addr = ($this->input->post('addr'));
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$newAddr = "";
		for($c=0; $c<$cnt; $c++)
		{
			$newAddr .= htmlspecialchars($textlines[$c]) . '<w:br/>';
		}	

		$addr = ($this->input->post('deliveryAddress'));
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$newDeliveryAddr = "";
		for($c=0; $c<$cnt; $c++)
		{
			$newDeliveryAddr .= htmlspecialchars($textlines[$c]) . '<w:br/>';
		}

		$table->addRow(); // argument is height
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText($newAddr, $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText($newDeliveryAddr, $fontStyle, array('spaceAfter'=>3));

		
		$table->addRow(); // argument is height
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText("GSTIN: ".$data['ci'][0]['gstIn'], $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(5250, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));

		$table->addRow(); // argument is height
		$cell = $table->addCell(5000, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText($cpe, $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(3000, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(2500, array('valign'=>'top', 'gridSpan' => 1));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));

		$section->addText(' ');
	    /////////// Define table style arrays
		$tableStyle1 = array(
						    'borderColor' => '000000',
						    'borderSize' => 6,
						    'cellMargin' => 80
						);

		$phpWord->addTableStyle('myTable2', $tableStyle1);
		$table = $section->addTable('myTable2');
		$cellStyle = array('valign'=>'center', 'borderSize'=>6, 'borderColor'=>'000000', 'bgColor'=>'000000', 'color'=>'white', 'bold'=>true);
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false);
		$table->addRow(); // argument is height
		$cell = $table->addCell(200, $cellStyle);
		$cell->addText("S.N.", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(4000, $cellStyle);
		$cell->addText("Product", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(1500, $cellStyle);
		$cell->addText("HSN", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(1200, $cellStyle);
		$cell->addText("Qty", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(1300, $cellStyle);
		$cell->addText("Rate", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(1500, $cellStyle);
		$cell->addText("Amt.", $fontStyle, array('spaceAfter'=>0));


		$cellStyle = array('valign'=>'center', 'borderSize'=>6, 'borderColor'=>'000000');
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold' => false);
		$paraStyleLeft = array('spaceAfter'=>1, 'align' => 'left');
		$paraStyleRight = array('spaceAfter'=>1, 'align' => 'right');

		$despDetRowIDs = "";
		for($k=0; $k < $r; $k++)
		{
			$despDetRowIDs .= $myTableData[$k]['despatchDetailRowId'] . ",";
			$table->addRow(); // argument is height
			$table->addCell(200, $cellStyle)->addText($k+1, $fontStyle, $paraStyleLeft);
			$table->addCell(4000, $cellStyle)->addText(htmlspecialchars($myTableData[$k]['productName']) . " [PO# " . htmlspecialchars($myTableData[$k]['letterNo']) . "]", $fontStyle, $paraStyleLeft);
			$table->addCell(1500, $cellStyle)->addText($myTableData[$k]['hsn'], $fontStyle, $paraStyleLeft);
			$table->addCell(1200, $cellStyle)->addText($myTableData[$k]['qty'], $fontStyle, $paraStyleRight);
			$table->addCell(1300, $cellStyle)->addText($myTableData[$k]['rate'], $fontStyle, $paraStyleRight);
			$table->addCell(1500, $cellStyle)->addText($myTableData[$k]['amt'], $fontStyle, $paraStyleRight);
		}
		$challanInfo = $this->Invoice_model->challanInfo($despDetRowIDs);
		$section->addText(' ', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));

	    /////////// 
		$tableStyle = array(
						    'borderColor' => 'FFFFFF',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable3', $tableStyle);
		$table = $section->addTable('myTable3');
		
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false);

		$col1="";
		$col2="";
		$col3="";
		$col4="";
		$col5="";

		$col3 .= 'Total Amt.:' . '<w:br/>';	
		// $col4 .=  '<w:br/>';	
		$col5 .= $this->input->post('totalAmt') . '<w:br/>';			

		if($this->input->post('discountAmt') == 0) 
		{
			$printDiscountTitle="";
			$printDiscountPer="";
			$printDiscountAmt="";
		}
		else
		{
			$printDiscountTitle = "Discount Amt.:";
			$printDiscountPer = $this->input->post('discountAmt').' ('. $this->input->post('discountPer')  .'%)' ;
			$printDiscountAmt = $this->input->post('totalAfterDiscount');
			$col3 .= $printDiscountTitle . ' [' . $printDiscountPer . ']<w:br/>';
			// $col4 .= $printDiscountPer . '<w:br/>';
			$col5 .= $printDiscountAmt . '<w:br/>';
		}

		if($this->input->post('vatPer') == 0) 
		{
			$printVatTitle="";
			$printVatPer="";
			$printVatAmt="";
		}
		else
		{
			$printVatTitle = "VAT:";
			$printVatPer = $this->input->post('vatPer').'%';
			$printVatAmt = $this->input->post('vatAmt');
			$col3 .= $printVatTitle . ' (' . $printVatPer . ')<w:br/>';
			// $col4 .= $printVatPer . '<w:br/>';
			$col5 .= $printVatAmt . '<w:br/>';			
		}

		if($this->input->post('sgstPer') == 0) 
		{
			$printSgstTitle="";
			$printSgstPer="";
			$printSgstAmt="";
		}
		else
		{
			$printSgstTitle = "SGST:";
			$printSgstPer = $this->input->post('sgstPer').'%';
			$printSgstAmt = $this->input->post('sgstAmt');
			$col3 .= $printSgstTitle . ' (' . $printSgstPer . ')<w:br/>';
			$col5 .= $printSgstAmt . '<w:br/>';			
		}
		if($this->input->post('cgstPer') == 0) 
		{
			$printCgstTitle="";
			$printCgstPer="";
			$printCgstAmt="";
		}
		else
		{
			$printCgstTitle = "CGST:";
			$printCgstPer = $this->input->post('cgstPer').'%';
			$printCgstAmt = $this->input->post('cgstAmt');
			$col3 .= $printCgstTitle . ' (' . $printCgstPer . ')<w:br/>';
			$col5 .= $printCgstAmt . '<w:br/>';			
		}
		if($this->input->post('igstPer') == 0) 
		{
			$printIgstTitle="";
			$printIgstPer="";
			$printIgstAmt="";
		}
		else
		{
			$printIgstTitle = "IGST:";
			$printIgstPer = $this->input->post('igstPer').'%';
			$printIgstAmt = $this->input->post('igstAmt');
			$col3 .= $printIgstTitle . ' (' . $printIgstPer . ')<w:br/>';
			$col5 .= $printIgstAmt . '<w:br/>';			
		}

		if($this->input->post('otherExpAmt') == 0) 
		{
			$printOtherTitle="";
			$printOtherName="";
			$printOtherAmt="";
		}
		else
		{
			$printOtherTitle = "Other Exp.:";
			$printOtherName = $this->input->post('otherExpName');
			$printOtherAmt = $this->input->post('otherExpAmt');
			$col3 .= $printOtherTitle . ' (' . $printOtherName . ')<w:br/>';
			// $col4 .= $printOtherName . '<w:br/>';
			$col5 .= $printOtherAmt . '<w:br/>';			
		}
		$col3 .= 'Round:' . '<w:br/>';
		// $col4 .= '<w:br/>';
		$col5 .= $this->input->post('roundAmt') . '<w:br/>';			

		$col3 .= 'Net Amt.:';
		// $col4 .= $printOtherName . '<w:br/>';
		$col5 .= $this->input->post('net');			

		$col1 .= 'Challan No.: '.$challanInfo . '<w:br/>';
		$col1 .= 'PR/GR No.: '.$data['despInfo'][0]['prGrNo'] . '<w:br/>';
		$col1 .= 'Desp.Dt.: '.$data['despInfo'][0]['despatchDt'] . '<w:br/>';
		$col1 .= 'Desp. To: '. '<w:br/>';
		$col1 .= 'Through: '.$data['despInfo'][0]['name'];

		$col2 .= 'Total Qty.: '. $this->input->post('totalQty') . '<w:br/>';

		if( strlen($challanInfo) == 0 ) ///agar challan NAHI banaya h
		{
			$table->addRow(); // argument is height
			$cell = $table->addCell(2000, array('valign'=>'top', 'gridSpan' => 1));
			$cell->addText($col1 , $fontStyle, array('spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top', 'halign'=>'left'));
			$cell->addText($col2, $fontStyle, array('align' => 'left','spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top'));
			$cell->addText('', $fontStyle, array('spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top'));
			$cell->addText($col3, $fontStyle, array('spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top', 'halign'=>'right'));
			$cell->addText($col5, $fontStyle, array('align' => 'right','spaceAfter'=>3));

			$table->addRow(); // argument is height
			$cell = $table->addCell(2000, array('valign'=>'top', 'gridSpan' => 5));
			$cell->addText($this->input->post('inWords'), $fontStyle, array('spaceAfter'=>3));
		}
		else
		{
			$table->addRow(); // argument is height
			$cell = $table->addCell(2000, array('valign'=>'top', 'gridSpan' => 1));
			$cell->addText($col1 , $fontStyle, array('spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top', 'halign'=>'left'));
			$cell->addText($col2, $fontStyle, array('align' => 'left','spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top'));
			$cell->addText('', $fontStyle, array('spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top'));
			$cell->addText($col3, $fontStyle, array('spaceAfter'=>3));
			$cell = $table->addCell(2000, array('valign'=>'top', 'halign'=>'right'));
			$cell->addText($col5, $fontStyle, array('align' => 'right','spaceAfter'=>3));

			$table->addRow(); // argument is height
			$cell = $table->addCell(2000, array('valign'=>'top', 'gridSpan' => 5));
			$cell->addText($this->input->post('inWords'), $fontStyle, array('spaceAfter'=>3));
		}




		$section->addText(' ', $fontStyle, array('align' => 'center','spaceBefore'=>10, 'spaceAfter'=>4));
		$fontStyle = array('name' => 'Arial', 'size' => 7, 'bold'=>false);
		$section->addText('1. The discrepancy if any in the bill should be brought to our notice within a week from the date here of.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('2. All disputes will have to be settled at Ajmer.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('3. Goods supplied to order will not be accepted back.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('4. Interest will be charged @18% if the bill remains unpaid for 30 days from the date of bill.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('5. E. and O.E.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		// $section->addText('5. E. & O.E.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));

		$fontStyle = array('name' => 'Arial', 'size' => 11, 'bold'=>false);
		$section->addText("For ".$data['org'][0]['orgName'], $fontStyle, array('align' => 'right','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText("", $fontStyle, array('align' => 'right','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText("Partner/Auth. Signatory", $fontStyle, array('align' => 'right','spaceBefore'=>10, 'spaceAfter'=>4));

		$section->addText(' ', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));

	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => '000000',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable3', $tableStyle);
		$table = $section->addTable('myTable3');
		
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false);
		$table->addRow(); // argument is height
		$cell = $table->addCell(10000, $cellStyle);
		$cell->addText("Bank Details: " . $data['org'][0]['bankName'] . ', Branch: ' . $data['org'][0]['branch'] . ', IFSC: ' . $data['org'][0]['ifsc'] . ', Ac.No.: ' . $data['org'][0]['acNo'], $fontStyle, array('align'=>'center', 'spaceAfter'=>3));

		$dt = date("Y_m_d");
		// date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		// $partyName = "";

		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		// $fileName = "downloads/I_" . $dt . ' (' . $tm . ') ' . $partyName . ".docx";
		$fileName = "downloads/INV[". $invNoForFileName ."]_" . $dt . ' (' . $tm . ') ' . $partyNameForFileName . ".docx";
		$objWriter->save($fileName);
		echo base_url().$fileName;
	

	}   

	public function loadAllRecords()
	{
		$data['records'] = $this->Util_model->getDataAllCi('I');
		echo json_encode($data);
	}
	public function loadLimitedRecords()
	{
		$data['records'] = $this->Util_model->getDataLimitCi('I');
		echo json_encode($data);
	}


	public function getProducts()
	{
		$data['products'] = $this->Invoice_model->getProducts();
		$data['gstIn'] = $this->Invoice_model->getGstIn();
		echo json_encode($data);
	}
	
	public function getProductsFromCi()
	{
		$data['products'] = $this->Invoice_model->getProductsFromCi();
		echo json_encode($data);
	}
	// public function getProductsFromDespatch()
	// {
	// 	$data['products'] = $this->Invoice_model->getProductsFromDespatch();
	// 	echo json_encode($data);
	// }
}
