<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptQuotations_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptquotations_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Quotations Log')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['parties'] = $this->Rptquotations_model->getPartyList();
			$this->load->view('RptQuotations_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptquotations_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/Q.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/Q.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Quotations Log');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:L5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Party: ' . $this->input->post('party'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:L6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'V.No');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Party');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Letter#');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Total Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Dis.Per');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Dis.Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'VAT Per');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'VAT Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'Net Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Tot. Qty.');

		$objPHPExcel->getActiveSheet()->getStyle('F7:L7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "L" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['partyName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['letterNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discountPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discountAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vatPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vatAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['net']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalQty']);
		 	$i++;
		}
		$r=$i-1;
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H8:H'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J8:J'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// // $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// // $objWriter->save("excelfiles/$acname$branch.xls");
		// $objWriter->save("excelfiles/Q.xls");
		// echo base_url()."excelfiles/Q.xls";
		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('excelfiles/QuotationLog_'. $dt . ' (' . $tm . ') ' .'.xls');
		echo base_url().'excelfiles/QuotationLog_'. $dt . ' (' . $tm . ') ' .'.xls';

	}


}
