<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddressBook_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Addressbook_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Address Book')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Addressbook_model->getDataLimit();
			$this->load->model('Prefixtypes_model');
			$data['prefixes'] = $this->Prefixtypes_model->getPrefixes();
			$this->load->model('Contacttypes_model');
			$data['contactTypes'] = $this->Contacttypes_model->getContactTypes();
			$this->load->model('Towns_model');
			$data['towns'] = $this->Towns_model->getTownList();
			$data['abList'] = $this->Util_model->getAbList();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('AddressBook_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getTownsRefresh()
	{
		$this->load->model('Towns_model');
		$data['towns'] = $this->Towns_model->getTownListRefresh();
		echo json_encode($data);
	}

	public function insert()
	{
		if($this->Addressbook_model->checkDuplicate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Addressbook_model->insert();
			$data['records'] = $this->Addressbook_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function update()
	{
		if($this->Addressbook_model->checkDuplicateOnUpdate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Addressbook_model->update();
			$data['records'] = $this->Addressbook_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function delete()
	{
		if($this->Util_model->isDependent('parties', 'abRowId', $this->input->post('rowId')) == 1)
        {
        	$data['dependent'] = "yes";
        	echo json_encode($data);
        }
        else if($this->Util_model->isDependent('transporters', 'abRowId', $this->input->post('rowId')) == 1)
        {
        	$data['dependent'] = "yes";
        	echo json_encode($data);
        }
        else
        {
			$this->Addressbook_model->delete();
			$data['records'] = $this->Addressbook_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Addressbook_model->getDataAll();
		echo json_encode($data);
	}

	public function getDataForCheckox()
	{
		$data['contacts'] = $this->Addressbook_model->getAllContacts();
		$data['bankDetails'] = $this->Addressbook_model->getbankDetails();
		// $data['accountBalance'] = $this->Util_model->getAccountBalance();
		echo json_encode($data);
	}
	public function printData()
	{

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Contact Detail');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true, 20); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 12, '', true); 
		$data['abData'] = $this->Addressbook_model->getAbData();
		$contactTypes = $this->Addressbook_model->getContactTypes();
		$data['pt'] = $this->Addressbook_model->getPrefixType();
		$data['ref'] = $this->Addressbook_model->getReferencerName();
		$data['bankDetail'] = $this->Addressbook_model->getbankDetails();
		if( $data['abData'][0]['abType'] == "I" )
		{
			$abType = "Indevidual";
		}
		else
		{
			$abType = "Non Indevidual";
		}
		if( $data['abData'][0]['contactPriority'] == "N" )
		{
			$contactPriority = "Normal";
		}
		else if( $data['abData'][0]['contactPriority'] == "H" )
		{
			$contactPriority = "High";
		}
		else 
		{
			$contactPriority = "Low";
		}
		$bankDetail = "";
		if( count($data['bankDetail']) > 0 )
		{
			$bankDetail = "Bank Name: " . $data['bankDetail'][0]['bankName'] . ", Branch: " . $data['bankDetail'][0]['branchName'] . ", IFSC: " . $data['bankDetail'][0]['ifsc'] . ", Ac. No.: " . $data['bankDetail'][0]['acNo'];
		}
		$html='<table border="0" cellpedding="5" cellspacing="5">
				<tr>
					<td width="30mm" style="font-size:14pt; font-weight:bold;">Name</td>
					<td width="10mm" style="font-size:14pt; font-weight:bold;">:</td>
					<td width="130mm" style="font-size:14pt; font-weight:bold;">' . $data['pt'][0]['prefixType'] . ' ' . $data['abData'][0]['name'] . '</td>
				</tr>
				<tr>
					<td>Type</td>
					<td>:</td>
					<td>' . $abType . '</td>
				</tr>
				<tr>
					<td>Priority</td>
					<td>:</td>
					<td>' . $contactPriority . '</td>
				</tr>
				<tr>
					<td>Address</td>
					<td>:</td>
					<td>' . $data['abData'][0]['addr'] . '</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>' . $data['abData'][0]['townName'] . ', ' . $data['abData'][0]['districtName'] . ', ' . $data['abData'][0]['stateName'] . ', ' . $data['abData'][0]['countryName'] . ', ' . $data['abData'][0]['pin'] .'</td>
				</tr>
				<tr>
					<td>Referencer</td>
					<td>:</td>
					<td>' . $data['ref'][0]['refName'] . '</td>
				</tr>
				<tr>
					<td>Land Line</td>
					<td>:</td>
					<td>' . $data['abData'][0]['landLine1']. ', ' . $data['abData'][0]['landLine2'] . '</td>
				</tr>
				<tr>
					<td>Mobile</td>
					<td>:</td>
					<td>' . $data['abData'][0]['mobile1']. ', ' . $data['abData'][0]['mobile2'] . '</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td>' . $data['abData'][0]['email1'] . ', ' . $data['abData'][0]['email2'] . '</td>
				</tr>
				<tr>
					<td>PAN</td>
					<td>:</td>
					<td>' . $data['abData'][0]['pan'] . '</td>
				</tr>
				<tr>
					<td>DIN</td>
					<td>:</td>
					<td>' . $data['abData'][0]['din'] . '</td>
				</tr>
				<tr>
					<td>GSTIN</td>
					<td>:</td>
					<td>' . $data['abData'][0]['gstIn'] . '</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size:14pt; font-weight:bold;">Contact Person:</td>
				</tr>
				<tr>
					<td>Person 1</td>
					<td>:</td>
					<td>' . $data['abData'][0]['contactPerson']. ', ' . $data['abData'][0]['contactMobile']. ', ' . $data['abData'][0]['contactDepartment']. ', ' . $data['abData'][0]['contactDesignation'] . '</td>
				</tr>
				<tr>
					<td>Person 2</td>
					<td>:</td>
					<td>' . $data['abData'][0]['contactPerson2']. ', ' . $data['abData'][0]['contactMobile2']. ', ' . $data['abData'][0]['contactDepartment2']. ', ' . $data['abData'][0]['contactDesignation2'] . '</td>
				</tr>
				<tr>
					<td>Person 3</td>
					<td>:</td>
					<td>' . $data['abData'][0]['contactPerson3']. ', ' . $data['abData'][0]['contactMobile3']. ', ' . $data['abData'][0]['contactDepartment3']. ', ' . $data['abData'][0]['contactDesignation3'] . '</td>
				</tr>
				<tr>
					<td>Contact Type</td>
					<td>:</td>
					<td>' . $contactTypes . '</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size:14pt; font-weight:bold;">Bank Details:</td>
				</tr>
				<tr>
					<td colspan="3">' . $bankDetail . '</td>
				</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// $dt = date("Y_m_d");
		// date_default_timezone_set("Asia/Kolkata");
		// $tm = date("H_i_s");
		// $pdf->Output(FCPATH . '/downloads/abd_'. $dt . ' (' . $tm . ').pdf', 'F');
		// echo base_url()."/downloads/abd_". $dt . " (" . $tm . ").pdf";
			$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$partyName = "SS";
		// $pdf->Output($_SERVER['DOCUMENT_ROOT'] . 'sberp/downloads/Q_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		$pdf->Output(FCPATH . '/downloads/Q_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."/downloads/Q_". $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}
}
