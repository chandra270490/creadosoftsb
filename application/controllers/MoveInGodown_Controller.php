<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MoveInGodown_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Moveingodown_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Move In Godown') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Moveingodown_model->getDataLimit();
			$data['products'] = $this->Moveingodown_model->getProductList();
			// $data['employees'] = $this->Moveingodown_model->getEmpList();
			// $data['stagesTo'] = $this->Moveingodown_model->getStagesTo();
			$data['stagesFrom'] = $this->Moveingodown_model->getStagesFrom();
			// $data['stages4table'] = $this->Moveingodown_model->getStages4table();
			$this->load->model('Colours_model');
			$data['colours'] = $this->Colours_model->getColourList();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('MoveInGodown_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getCurrentQty()
	{
		$data['currentQty'] = $this->Moveingodown_model->getCurrentQty();
		echo json_encode($data);
	}

	public function getProductStatus()
	{
		$data['ob'] = $this->Moveingodown_model->getProductStatus();
		$data['despStageQty'] = $this->Moveingodown_model->getDespStageQty();
		$data['pendingOrders'] = $this->Moveingodown_model->getPendingOrders();
		echo json_encode($data);
	}


	public function moveData()
	{
		$this->Moveingodown_model->moveData();
		$data['records'] = $this->Moveingodown_model->getDataLimit();
		// $data['ob'] = $this->Moveingodown_model->getProductStatus();
		// $data['despStageQty'] = $this->Moveingodown_model->getDespStageQty();
		// $data['pendingOrders'] = $this->Moveingodown_model->getPendingOrders();
		$data['ob'] = 'done...';
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Moveingodown_model->getDataAll();
		echo json_encode($data);
	}

}
