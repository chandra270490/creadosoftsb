<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseLimits_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Baselimits_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Base Limits')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Baselimits_model->getBaseLimits();
			// $this->load->model('Districts_model');
			// $data['districts'] = $this->Districts_model->getDistrictList();
			// $data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('BaseLimits_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  


	public function update()
	{
		$this->Baselimits_model->update();
		// $data['records'] = $this->Baselimits_model->getDataLimit();
		// echo json_encode($data);
	}

}
