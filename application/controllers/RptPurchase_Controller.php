<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptPurchase_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptpurchase_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Purchase Report')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['parties'] = $this->Rptpurchase_model->getPartyList();
			$this->load->view('RptPurchase_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptpurchase_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:Q1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:Q2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:Q3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Purchase Report');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:Q4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "Ref. Bill Dt. From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:Q5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Party: ' . $this->input->post('party'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:Q6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'V.No');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Ref. Bill Dt');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Party');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Dis%');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Dis Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Amt After Dis');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'GST 5');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'GST 12');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'GST 18');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'GST 28');
		$objPHPExcel->getActiveSheet()->setCellValue('M7', 'Net');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', 'Total Qty');
		$objPHPExcel->getActiveSheet()->setCellValue('O7', 'V.No.[Dt]');
		$objPHPExcel->getActiveSheet()->setCellValue('P7', 'Pur./Exp.');
		$objPHPExcel->getActiveSheet()->setCellValue('Q7', 'Ref. Bill#');

		$objPHPExcel->getActiveSheet()->getStyle('F7:Q7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "Q" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['partyName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalAmt']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['avgDiscountPer']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalDiscountAmt']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalAmtAfterDiscount']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['gst5TaxAmt']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['gst12TaxAmt']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['gst18TaxAmt']);
		    $myCol = $myCol + 1;
		    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['gst28TaxAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['net']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalQty']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vNo2']);
		 	$myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['purchaseOrExpense']);
		 	$myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['refBillNoDt']);
		 	$i++;
		}
		$r=$i-1;
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H8:H'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "Q" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "Q" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/PRepo_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/PRepo_" . $dt . ' (' . $tm . ') ' . ".xls";

	}

	public function getProducts()
	{
		$data['purchaseDetail'] = $this->Rptpurchase_model->getProducts();
		echo json_encode($data);
	}

	public function exportDataDetail()
	{
		$this->printToExcelDetail();
	}

	public function printToExcelDetail()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Invoice Detail');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "Ref. Bill Dt. From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E4:L4');
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'P.Inv#: ' . $this->input->post('globalVno'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:D5');
		// $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Date: ' . $this->input->post('globalVdt'));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:L5');
		$objPHPExcel->getActiveSheet()->setCellValue('E5', '');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:D6');
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Party: ' . $this->input->post('globalPartyName'));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:L6');
		$objPHPExcel->getActiveSheet()->setCellValue('E6', ' ' );

		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:L6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Product');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Qty');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Rate');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Bill No.');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Bill Dt.');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Discount');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'Amt After Dis');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'GST Per');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'GST Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Net');

		$objPHPExcel->getActiveSheet()->getStyle('F7:L7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "L" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['productName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['rQty']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['rate']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['amt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['billNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['billDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discount']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['amtAfterDiscount']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['gstPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['gstAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['net']);
		 	$i++;
		}
		$r=$i-1;
		// $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '=SUM(D8:D'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/PDetailRepo_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/PDetailRepo_" . $dt . ' (' . $tm . ') ' . ".xls";

	}

}
