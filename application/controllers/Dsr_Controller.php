<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dsr_Controller extends CI_Controller
{
	public function __construct()
    {
		parent::__construct();
		$this->load->model('Dsr_model');
		$this->load->helper('form');
		$this->load->helper('url');
	}
	
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Daily Sales Report (DSR)')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['parties'] = $this->Util_model->getAbList();
			$data['records'] = $this->Dsr_model->getDataLimit();
			$data['closingRemarks'] = $this->Dsr_model->getClosingRemarks();
			$data['cboLeadStage'] = $this->Dsr_model->getCboLeadStage();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Dsr_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
            $this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}
	
	public function dsr_view_ajax(){
		$this->load->view("Dsr_view_ajax");
	}


	public function insert()
	{
		if($this->Util_model->isSessionExpired() == 1)
        {
        	$data = "Session out...";
        	echo json_encode($data);
        }
        else
        {
        	if($this->Dsr_model->checkDuplicate() == 1)
	        {
	        	$data = "Duplicate record...";
	        	echo json_encode($data);
	        }
	        else
	        {
				$this->Dsr_model->insert();
				$data['records'] = $this->Dsr_model->getDataLimit();
				echo json_encode($data);
			}
		}
	}

	public function update()
	{
		if($this->Util_model->isSessionExpired() == 1)
        {
        	$data = "Session out...";
        	echo json_encode($data);
        }
        else
        {
        	if($this->Dsr_model->checkDuplicateOnUpdate() == 1)
	        {
	        	$data = "Duplicate record...";
	        	echo json_encode($data);
	        }
	        else
	        {
				$this->Dsr_model->update();
				$data['records'] = $this->Dsr_model->getDataLimit();
				echo json_encode($data);
			}
		}
	}


	public function delete()
	{
		$this->Dsr_model->delete();
		$data['records'] = $this->Dsr_model->getDataLimit();
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Dsr_model->getDataAll();
		echo json_encode($data);
	}
	// public function loadLimitedRecords()
	// {
	// 	$data['records'] = $this->Util_model->getDataLimitQpo('Q');
	// 	echo json_encode($data);
	// }




	public function exportData()
	{
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:Q1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:Q2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:Q3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'D.S.R.');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:Q4');
		// $objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));



		$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (6) . ":" . "Q" . (6);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

	 	////////// table heading
		$myTableData = $this->input->post('TableDataHeader');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);

        $myTableRows = count($myTableData);
		$r = $myTableRows;
		// $noOfDays = $this->input->post('noOfDays');
		$i = 6;
		for($k=0; $k < $r; $k++)
		{
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($k, $i, $myTableData[$k]);
		}

		//////////END - Table Heading

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$noOfDays = $this->input->post('noOfDays');
		$i = 7;
		for($k=0; $k < $r; $k++)
		{
			for($c=0; $c<count($myTableData[$k]); $c++)
			{
				$newValue = htmlspecialchars($myTableData[$k][$c]);
				$newValue = str_replace("&amp;", "&", $newValue);
				// str_replace("world","Peter","Hello world!");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $i, $newValue ); //htmlspecialchars
			}
			// $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
		 	$i++;
		}
		$r=$i-1;

		$cellRange1 = "A" . ($i) . ":" . "Q" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	// $objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (6) . ":" . "Q" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(11);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);	

		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '=SUM(G7:G'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=SUM(H7:Q'.$r.')');
		
		// $i++;
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Employer Contri.(%)');
		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $this->input->post('employerContriPer'));

		// $i++;
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Employer Contri.(Rs)');
		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $this->input->post('employerContriAmt'));

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// $objWriter->save("excelfiles/$acname$branch.xls");
		$objWriter->save("excelfiles/tmp.xls");
		echo base_url()."excelfiles/tmp.xls";

	}

	//DSR Short Remarks Master
	public function dsr_srm()
	{
		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/dsr/dsr_srm'); 
		$this->load->view('includes/footer');
	}

	//DSR Short Remarks Query
	public function dsr_srm_entry(){ 
		$data = array();
		$data['dsr_srm_entry'] = $this->Dsr_model->dsr_srm_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'Dsr_Controller/dsr_srm?id=';
		$this->load->view('QueryPage',$data); 
	}

	//DSR Short Remarks Master
	public function dsr_ls()
	{
		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/dsr/dsr_lead_stage'); 
		$this->load->view('includes/footer');
	}

	//DSR Short Remarks Query
	public function dsr_ls_entry(){ 
		$data = array();
		$data['dsr_lead_stage_entry'] = $this->Dsr_model->dsr_lead_stage_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'Dsr_Controller/dsr_ls?id=';
		$this->load->view('QueryPage',$data); 
	}



}
