<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->helper('url');
        // $this->load->library('session');
    }
	public function index()
	{
        set_time_limit(0);
		$this->load->helper('url');
		$this->load->view('includes/header');
        $data['org'] = $this->Login_model->getOrgList();
		$this->load->view('login_view', $data);
		$this->load->view('includes/footer');
	}

	public function checkLogin()
	{
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txtUID', 'User name', 'trim|required');
        $this->form_validation->set_rules('txtPassword', 'Password', 'trim|required');
 
        if($this->form_validation->run() == FALSE)
        {
            $this->load->view('includes/header');           // with Jumbotron
            $this->load->view('login_view');
            $this->load->view('includes/footer');
            return;
        }

        if(isset($_POST['txtUID'])==False)
        {
            $this->load->view('includes/header');           // with Jumbotron
            $this->load->view('login_view');
            $this->load->view('includes/footer');
            return;
        }

        $userRec = $this->Login_model->checkuser($_POST['txtUID'], $_POST['txtPassword'],$_POST['cboOrg']);
        $cnt=count($userRec);
        // echo $cnt;
        if($cnt>0)
        {
            $this->session->set_userdata('userid', $_POST['txtUID']);
            $this->session->set_userdata('userRowId', $userRec['rowid']);
            $this->session->set_userdata('abAccess', $userRec['abAccess']);
            $this->session->set_userdata('abAccessIn', $userRec['abAccessIn']);
            $this->session->set_userdata('orgRowId', $_POST['cboOrg']);
            $data['org'] = $this->Login_model->getOrgDetail($_POST['cboOrg']);
            $this->session->set_userdata('orgChar', '[.'. substr($data['org'][0]['orgName'], 0, 1) .'.]'); 
            $this->session->set_userdata('session_id', rand());
            $this->session->set_userdata('isLogin', True);
            // $this->Login_model->insert_session($_POST['txtUID']);
            $this->load->view('includes/header4all');       
            $MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);     
            $data['fileName'] = $this->Login_model->getImageFile();   
          	$this->load->view('success_view', $data);
        }
        else
        {
            $this->load->view('includes/header');           // with Jumbotron
            $data['errMsg'] = "Invalid Username or Password...";
            $this->load->view('error_view', $data);
            $data['org'] = $this->Login_model->getOrgList();
            $this->load->view('login_view', $data);
        }
        $this->load->view('includes/footer');
	}
    public function logout()
    {

        // $this->Login_model->logout_session($this->session->session_id);
        // $this->session->userid='';          // this method.
        $this->session->unset_userdata('userid');
        // $this->session->isLogin=false;
        $this->session->unset_userdata('isLogin');
        // $this->session->session_id='';
        // echo "<br>" . $this->session->userdata('session_id');
        $this->session->unset_userdata('session_id');
        // $this->session->userRowId='';
        $this->session->unset_userdata('userRowId');
        $this->session->unset_userdata('abAccess');
        $this->session->unset_userdata('abAccessIn');
        $this->session->unset_userdata('orgRowId');
        $this->session->sess_destroy();
        // echo "<br>" . $this->session->userdata('session_id'). "bye...";
        $this->load->view('includes/header');           // with Jumbotron
        $data['org'] = $this->Login_model->getOrgList();
        $this->load->view('login_view', $data);
        $this->load->view('includes/footer');
    }
    public function logout1()
    {

        // $this->Login_model->logout_session($this->session->session_id);
        // $this->session->userid='';          // this method.
        $this->session->unset_userdata('userid');
        // $this->session->isLogin=false;
        $this->session->unset_userdata('isLogin');
        // $this->session->session_id='';
        // echo "<br>" . $this->session->userdata('session_id');
        $this->session->unset_userdata('session_id');
        // $this->session->userRowId='';
        $this->session->unset_userdata('userRowId');
        $this->session->unset_userdata('orgRowId');
        // $this->session->sess_destroy();
        // echo "<br>" . $this->session->userdata('session_id'). "bye...";
        // $this->load->view('includes/header');           // with Jumbotron
        // $this->load->view('login_view');
        // $this->load->view('includes/footer');
    }
    // public function doMail()
    // {
    //   $to =  "ajmerblooddonors@gmail.com"; //"surendralekhyani@gmail.com";
    //   $subject = "Login at www.AjmerBloodDonors.com";
    //   $message = $this->session->userRowId." [".$this->session->userid."] - Login at www.AjmerBloodDonors.com...";
    //   $message .= "\nUser: ". $this->session->userRowId;
    //   // $message .= "\nBlood Group: ". $this->input->post('cboBloodGroup');

    //   // headers
    //   $from = "ajmerblooddonors@gmail.com";   
    //   $headers = "From:" . $from . "\r\n";
    //   $headers .= "Cc:surendralekhyani@gmail.com\r\n";

    //   // Setting content-type is required when sending HTML email
    //   $headers .= "MIME-Version: 1.0" . "\r\n";
    //   $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

    //   $result = mail($to,$subject,$message);
    //   // $result = mail($to,$subject,$message,$headers);
    //   if($result)
    //   {
    //       // echo "Mail sent successfully.";
    //   }
    //   else
    //   {
    //       // echo "Mail Fail";
    //   }        
    // }





    public function loadNotifications()
    {
        $data['notificationCount'] = $this->Login_model->getNotifications();
        echo json_encode($data);
    }
    public function loadNotifications4table()
    {
        $data['notificationCount'] = $this->Login_model->getNotifications();
        // $this->Login_model->markPadhLiya();
        echo json_encode($data);
    }
    public function notificationPadhLiya()
    {
        $this->Login_model->markPadhLiya();
        $data['notificationCount'] = $this->Login_model->getNotifications();
        echo json_encode($data);
    }
}
