<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IncomeTaxSlabs_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Incometaxslabs_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Income Tax Slabs') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Incometaxslabs_model->getDataAll();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('IncomeTaxSlabs_view', $data);
			// $this->load->view('IncomeTaxSlabs_view');
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function saveData()
	{
		$this->Incometaxslabs_model->insert();
		echo "Done...";
	}

	// public function exportData()
	// {
	// 	$this->printToPdf('Save');
	// }

	// public function printToPdf($arg)
	// {
	// 	$this->load->library('Pdf');
	// 	$pdf = new Pdf('Landscape', 'mm', 'A4', true, 'UTF-8', false);
	// 	$pdf->SetTitle('Products List');
	// 	// $pdf->SetHeaderMargin(1);
	// 	$pdf->SetPrintHeader(false);
	// 	$pdf->SetTopMargin(15);
	// 	$pdf->setFooterMargin(12);
	// 	$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
	// 	$pdf->SetAuthor('Suri');
	// 	$pdf->SetDisplayMode('real', 'default');
	// 	$pdf->AddPage();

	// 	$pdf->SetFont('', '', 10, '', true); 
	// 	$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
	// 	$orgImagePath = $data['org'][0]['imagePath'];
	// 	$html='<table border="0">
	// 				<tr>
	// 					<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
	// 					<td style="width:90mm;"></td>
	// 					<td style="width:50mm;"></td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2">' . $data['org'][0]['tin'] . '</td>
	// 				</tr>
	// 		</table>';

	// 	$pdf->writeHTML($html, true, false, true, false, '');
	// 	// Line ($x1, $y1, $x2, $y2, $style=array())
	// 	$pdf->Line(0, 73, 210, 73);


	// 	$html='<table border="0">
	// 				<tr>
	// 					<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Products List</td>
	// 				</tr>
	// 		</table>';
	// 	$pdf->writeHTML($html, true, false, true, false, '');

	// 	$myTableData = $this->input->post('TableData');
 //        $myTableData = stripcslashes($myTableData);
 //        $myTableData = json_decode($myTableData,TRUE);
 //        $myTableRows = count($myTableData);
	// 	$r = $myTableRows;


	// 	// // $pdf->writeHTML(100,"dd");


	// 	$productRows = "";
	// 	for($k=0; $k < $r-1; $k++)
	// 	{
	// 		$sn=$k+1;
	// 		$productRows .= "<tr nobr=\"true\">";
	// 			$productRows .= "<td>". $sn ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['productName'] . "<br />(". $myTableData[$k]['length'] ."x".$myTableData[$k]['width']."x".$myTableData[$k]['height']. ")" . $myTableData[$k]['uom'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['productCategory'] ."</td>";
	// 			$productRows .= "<td align=\"right\">". $myTableData[$k]['rate'] ."</td>";
	// 			$productRowId = $myTableData[$k]['productRowId'];
	// 			$data['img'] = $this->Incometaxslabs_model->getImageName($productRowId);
	//           	$fileName = $data['img'][0]['imagePathThumb'];
	//           	if( $fileName != "N")
	//           	{
	// 	          	$imgSrc = FCPATH."/bootstrap/images/products/".$fileName;
	// 				$productRows .= "<td align=\"center\"><img src=\"". $imgSrc ."\" alt=\"nn\"></td>";
	//           	}
	// 			else
	// 			{
	// 				$productRows .= "<td>". "" ."</td>";
	// 			}
	// 		$productRows .= "</tr>";
	// 	}
	// 	$html='<table border="1" cellpadding="5">
	// 				<tr>
	// 					<th style="width:12mm; color:white; background-color:black;">S.N.</th>
	// 					<th style="width:55mm; color:white; background-color:black;">Product</th>
	// 					<th style="width:55mm; color:white; background-color:black;">Category</th>
	// 					<th style="width:25mm; text-align:center; color:white; background-color:black;">Rate</th>
	// 					<th style="width:35mm; text-align:center; color:white; background-color:black;">Preview</th>
	// 				</tr>'.$productRows.
	// 		'</table>';
	// 	$pdf->writeHTML($html, true, false, true, false, '');



	// 	$dt = date("Y_m_d");
	// 	date_default_timezone_set("Asia/Kolkata");
	// 	$tm = date("H_i_s");
	// 	$pdf->Output(FCPATH . '/downloads/PL_'. $dt . ' (' . $tm . ').pdf', 'F');
	// 	echo base_url()."/downloads/PL_". $dt . " (" . $tm . ").pdf";
	// }
}
