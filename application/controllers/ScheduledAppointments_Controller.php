<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ScheduledAppointments_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Scheduledappointments_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Scheduled Appointments')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $data['parties'] = $this->Scheduledappointments_model->getPartyList();
			$this->load->view('ScheduledAppointments_view');
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Scheduledappointments_model->getDataForReport();
		echo json_encode($data);
	}


	public function exportData()
	{
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:J2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:J3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Scheduled Appointments');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));



		$objPHPExcel->getActiveSheet()->getStyle('A6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (6) . ":" . "J" . (6);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		// $noOfDays = $this->input->post('noOfDays');
		$i = 6;
		for($k=0; $k < $r; $k++)
		{
			for($c=0; $c<count($myTableData[$k]); $c++)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $i, $myTableData[$k][$c]);
			}
			$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
		 	$i++;
		}
		$r=$i-1;

		$cellRange1 = "A" . ($i) . ":" . "J" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	// $objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (6) . ":" . "J" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(11);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);

		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '=SUM(G7:G'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H7:H'.$r.')');
		
		
	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// $objWriter->save("excelfiles/$acname$branch.xls");
		$objWriter->save("excelfiles/tmp.xls");
		echo base_url()."excelfiles/tmp.xls";

	}
}
