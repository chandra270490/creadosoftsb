<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Franchiseesc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('franchiseesm');
		 $this->load->library('excel');
	}
	
	//Franchise Master Dashboard
	public function index(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
		);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/franchisees/franchisees_db', $data); 
		$this->load->view('includes/footer');
	}

	//Franchisees List
	public function franchisees_list(){

		$tbl_nm = "franchisee_mst";
		$data = array();
		$data['list_title'] = "Franchisees List";
		$data['list_url'] = "franchiseesc/franchisees_list";
		$data['tbl_nm'] = "franchisee_mst";
		$data['primary_col'] = "franchisee_no";
		$data['edit_url'] = "franchiseesc/franchisees_add";
		$data['edit_enable'] = "yes";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
			'Franchisee List' => 'franchiseesc/franchisees_list',
		);

		$data['ViewHead'] = $this->franchiseesm->ListHead($tbl_nm);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('ListView', $data);
		$this->load->view('includes/footer');
	}

	//Franchisees Add View
	public function franchisees_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
			'Franchisee List' => 'franchiseesc/franchisees_list',
			'Franchisee Add' => 'franchiseesc/franchisees_add',
		);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/franchisees/franchisees_add', $data); 
		$this->load->view('includes/footer');
	}

	//Franchisees Query
	public function franchisees_entry(){ 
		$data = array();
		$data['franchisee_entry'] = $this->franchiseesm->franchisee_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'franchiseesc';
		$this->load->view('QueryPage',$data); 
	}

	//Franchisees Inquiry List
	public function franchisees_inq_list(){

		$tbl_nm = "franchisee_inq_mst";
		$data = array();
		$data['list_title'] = "Inquires List";
		$data['list_url'] = "franchiseesc/franchisees_inq_list";
		$data['tbl_nm'] = "franchisee_inq_mst";
		$data['primary_col'] = "fran_inq_no";
		$data['edit_url'] = "franchiseesc/franchisees_inq_add";
		$data['edit_enable'] = "yes";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Dashboard' => 'franchiseesc',
			'Steps' => 'franchiseesc/franchisees_inq_steps',
			'Inquiry List' => 'franchiseesc/franchisees_inq_list',
		);

		$data['ViewHead'] = $this->franchiseesm->ListHead($tbl_nm);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('ListView', $data);
		$this->load->view('includes/footer');
	}

	//Franchise Inquiry
	public function franchisees_inq_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
			'Franchisee Steps' => 'franchiseesc/franchisees_inq_steps',
			'Franchisee Inquiry List' => 'franchiseesc/franchisees_inq_list',
			'Franchisee Inquiry Add' => 'franchiseesc/franchisees_inq_add',
		);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/franchisees/franchisees_inq_add', $data); 
		$this->load->view('includes/footer');
	}

	//Franchisees Inquiry Query
	public function franchisees_inq_entry(){ 
		$data = array();
		$data['franchisee_inq_entry'] = $this->franchiseesm->franchisee_inq_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		$data['url'] = 'franchiseesc/franchisees_inq_steps';
		$this->load->view('QueryPage',$data); 
	}

	//Franchise Inquiry
	public function franchisees_inq_steps(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
			'Franchisee Steps' => 'franchiseesc/franchisees_inq_steps',
		);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/franchisees/franchisees_inq_steps', $data); 
		$this->load->view('includes/footer');
	}

	//View All Inquires Status Wise
	public function view_all_inq(){
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
			'Franchisee Steps' => 'franchiseesc/franchisees_inq_steps',
			'All Inquires' => 'franchiseesc/view_all_inq',
		);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/franchisees/view_all_inq', $data); 
		$this->load->view('includes/footer');

	}

	//Export All Inquires
	public function franchisees_inq_export(){
		$this->load->view('modules/franchisees/franchisees_inq_export'); 

	}

	//Import Inquires
	//View All Inquires Status Wise
	public function franchisees_import_inq(){
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Franchisee Dashboard' => 'franchiseesc',
			'Franchisee Steps' => 'franchiseesc/franchisees_inq_steps',
			'Import Inquires' => 'franchiseesc/franchisees_import_inq',
		);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/franchisees/franchisees_import_inq', $data); 
		$this->load->view('includes/footer');

	}

	//Import Format
	public function franchisees_imp_for(){
		$this->load->view('modules/franchisees/franchisees_imp_for'); 

	}

	//Inquiry Import Entry
	function inq_import_entry()
	{
		//Excel Import Function Starts
		if(isset($_FILES["import_file"]["name"]))
		{
			$path = $_FILES["import_file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
					$fran_inq_name = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$fran_inq_phone = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$fran_inq_whatsapp = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$fran_inq_email = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$fran_inq_addr1 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$fran_inq_city = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$fran_inq_state = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$fran_inq_pin = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
					$fran_inq_country = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
					$fran_wfm_pref_loc = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
					$fran_model = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
					$fran_lead_source = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
					$fran_status = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
					$fran_remarks = $worksheet->getCellByColumnAndRow(13, $row)->getValue();

					$data[] = array(
						'fran_date'			    =>	date('Y-m-d'),
						'fran_inq_name'		    =>	$fran_inq_name,
						'fran_inq_phone'	    =>	$fran_inq_phone,
						'fran_inq_whatsapp'	    =>	$fran_inq_whatsapp,
						'fran_inq_email'	    =>	$fran_inq_email,
						'fran_inq_addr1'	    =>	$fran_inq_addr1,
						'fran_inq_city'		    =>	$fran_inq_city,
						'fran_inq_state'	    =>	$fran_inq_state,
						'fran_inq_pin'		    =>	$fran_inq_pin,
						'fran_inq_country'	    =>	$fran_inq_country,
						'fran_wfm_pref_loc'	    =>	$fran_wfm_pref_loc,
						'fran_model'		    =>	$fran_model,
						'fran_lead_source'	    =>	$fran_lead_source,
						'fran_status'		    =>	$fran_status,
						'fran_remarks'		    =>	$fran_remarks,
						'fran_inq_created_by'   =>	$this->session->userRowId,
						'fran_inq_created_date'	=>	date('Y-m-d H:i:s'),
						'fran_inq_modified_by'	=>	$this->session->userRowId,
					);
				}
			}

			$this->franchiseesm->excel_insert($data);
			//Excel Import Function Ends

			$data['message'] = 'Data Imported successfully';
			$data['url'] = 'franchiseesc/franchisees_inq_steps';
			$this->load->view('QueryPage',$data); 
		}	
	}
	
}
