<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptProforma_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptproforma_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Proforma Invoice Log')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['parties'] = $this->Rptproforma_model->getPartyList();
			$this->load->view('RptProforma_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptproforma_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/Q.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/Q.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:M2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:M3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Proforma Invoice Log');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:M4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:M5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Party: ' . $this->input->post('party'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:M6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'V.No');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Party');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Letter#');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Total Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Dis.Per');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Dis.Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'VAT Per');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'VAT Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'Net Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Tot. Qty.');
		$objPHPExcel->getActiveSheet()->setCellValue('M7', 'User');

		$objPHPExcel->getActiveSheet()->getStyle('F7:M7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "M" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['partyName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['letterNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discountPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discountAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vatPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vatAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['net']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalQty']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['user']);
		 	$i++;
		}
		$r=$i-1;
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H8:H'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J8:J'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "M" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "M" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// // $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// // $objWriter->save("excelfiles/$acname$branch.xls");
		// $objWriter->save("excelfiles/Q.xls");
		// echo base_url()."excelfiles/Q.xls";

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('excelfiles/Proforma_'. $dt . ' (' . $tm . ') ' .'.xls');
		echo base_url().'excelfiles/Proforma_'. $dt . ' (' . $tm . ') ' .'.xls';
	}


	// public function printToPdf($arg)
	// {

	// 	$this->load->library('Pdf');
	// 	$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	// 	$pdf->SetTitle('Quotations Log');
	// 	// $pdf->SetHeaderMargin(1);
	// 	$pdf->SetPrintHeader(false);
	// 	$pdf->SetTopMargin(15);
	// 	$pdf->setFooterMargin(12);
	// 	$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
	// 	$pdf->SetAuthor('Suri');
	// 	$pdf->SetDisplayMode('real', 'default');
	// 	$pdf->AddPage('L', 'A4');

	// 	$pdf->SetFont('', '', 10, '', true); 
	// 	// $data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
	// 	// $orgImagePath = $data['org'][0]['imagePath'];
	// 	// $html='<table border="0">
	// 	// 			<tr>
	// 	// 				<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/logolt.jpg" /></td>
	// 	// 				<td style="width:90mm;"></td>
	// 	// 				<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
	// 	// 			</tr>
	// 	// 			<tr>
	// 	// 				<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
	// 	// 			</tr>
	// 	// 			<tr>
	// 	// 				<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
	// 	// 			</tr>
	// 	// 			<tr>
	// 	// 				<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
	// 	// 			</tr>
	// 	// 			<tr>
	// 	// 				<td colspan="2">' . $data['org'][0]['tin'] . '</td>
	// 	// 			</tr>
	// 	// 	</table>';

	// 	// $pdf->writeHTML($html, true, false, true, false, '');
	// 	// // Line ($x1, $y1, $x2, $y2, $style=array())
	// 	// $pdf->Line(0, 73, 210, 73);


	// 	$html='<table border="0">
	// 			<tr>
	// 				<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Quotations Log</td>
	// 			</tr>
	// 		</table>';
	// 	$pdf->writeHTML($html, true, false, true, false, '');

	// 	$myTableData = $this->input->post('TableData');
 //        $myTableData = stripcslashes($myTableData);
 //        $myTableData = json_decode($myTableData,TRUE);
 //        $myTableRows = count($myTableData);
	// 	$r = $myTableRows;


	// 	// // $pdf->writeHTML(100,"dd");


	// 	$productRows = "";
	// 	for($k=0; $k < $r-1; $k++)
	// 	{
	// 		$sn=$k+1;
	// 		$productRows .= "<tr nobr=\"true\">";
	// 			$productRows .= "<td>". $sn ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['vType'] . "-". $myTableData[$k]['vNo'] . "</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['vDt'] ."</td>";
	// 			$productRows .= "<td align=\"left\">". $myTableData[$k]['partyName'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['letterNo'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['totalAmt'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['discountPer'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['discountAmt'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['vatPer'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['vatAmt'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['net'] ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['totalQty'] ."</td>";
	// 		$productRows .= "</tr>";
	// 	}
	// 	$html='<table border="1" cellpadding="5">
	// 				<tr>
	// 					<th style="width:12mm; color:white; background-color:black;">S.N.</th>
	// 					<th style="width:15mm; color:white; background-color:black;">V.No.</th>
	// 					<th style="width:25mm; color:white; background-color:black;">Date</th>
	// 					<th style="width:55mm; color:white; background-color:black;">Party</th>
	// 					<th style="width:20mm; color:white; background-color:black;">Letter No</th>
	// 					<th style="width:20mm; color:white; background-color:black;">Total Amt.</th>
	// 					<th style="width:20mm; color:white; background-color:black;">Dis%</th>
	// 					<th style="width:20mm; color:white; background-color:black;">Dis.</th>
	// 					<th style="width:20mm; color:white; background-color:black;">VAT%</th>
	// 					<th style="width:20mm; color:white; background-color:black;">VAT</th>
	// 					<th style="width:20mm; color:white; background-color:black;">Net</th>
	// 					<th style="width:20mm; color:white; background-color:black;">Total Qty</th>
	// 				</tr>'.$productRows.
	// 		'</table>';
	// 	$pdf->writeHTML($html, true, false, true, false, '');



	// 	$dt = date("Y_m_d");
	// 	date_default_timezone_set("Asia/Kolkata");
	// 	$tm = date("H_i_s");
	// 	$pdf->Output(FCPATH . '/downloads/PL_'. $dt . ' (' . $tm . ').pdf', 'F');
	// 	echo base_url()."/downloads/PL_". $dt . " (" . $tm . ").pdf";
	// }
}
