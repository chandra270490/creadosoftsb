<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Despatch_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Despatch_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Despatch')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['godowns'] = $this->Despatch_model->getGodowns();
			$data['records'] = $this->Despatch_model->getDataLimit();
			$data['parties'] = $this->Util_model->getPartyList();
			$data['transporters'] = $this->Despatch_model->getTransporterList();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Despatch_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  


	public function insert()
	{
		$c = $this->Despatch_model->checkAvailability();
		if( $c == 1)
		{
			echo json_encode('Minus');
		}
		else
		{
			$this->Despatch_model->insert();
			$data['records'] = $this->Despatch_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function update()
	{
		$c = $this->Despatch_model->goingPendingInMinus();
		if( $c == 1)
		{
			echo json_encode('Minus');
		}
		else
		{
			$this->Despatch_model->update();
			$data['records'] = $this->Despatch_model->getDataLimit();
			echo json_encode($data);
		}
	}



	public function delete()
	{
		$c = $this->Despatch_model->checkDependency();
		// $c=1;
		// echo json_encode($c);
		// return;
		if( $c == 1)
		{
			echo json_encode('cannot');
		}
		else
		{
			$this->Despatch_model->delete();
			$data['records'] = $this->Despatch_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Despatch_model->getDataAll();
		echo json_encode($data);
	}
	public function loadLimitedRecords()
	{
		$data['records'] = $this->Despatch_model->getDataLimit();
		echo json_encode($data);
	}

	public function getProducts()
	{
		$data['products'] = $this->Despatch_model->getProducts();
		echo json_encode($data);
	}
	public function getProductsFromDespatch()
	{
		$data['products'] = $this->Despatch_model->getProductsFromDespatch();
		echo json_encode($data);
	}
}
