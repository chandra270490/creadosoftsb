<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptInvoice_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptinvoice_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Invoice Report')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['parties'] = $this->Rptinvoice_model->getPartyList();
			$this->load->view('RptInvoice_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptinvoice_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:S1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:S2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:S3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Invoice Report');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:S4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:S5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Party: ' . $this->input->post('party'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:S6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'V.No');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Party');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', '');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Total Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Dis.Per');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Dis.Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'VAT Per');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'VAT Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'SGST Per');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'SGST Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('M7', 'CGST Per');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', 'CGST Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('O7', 'IGST Per');
		$objPHPExcel->getActiveSheet()->setCellValue('P7', 'IGST Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('Q7', 'Net Amt.');
		$objPHPExcel->getActiveSheet()->setCellValue('R7', 'Tot. Qty.');
		$objPHPExcel->getActiveSheet()->setCellValue('S7', 'User');

		$objPHPExcel->getActiveSheet()->getStyle('F7:S7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "S" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['partyName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, '');
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discountPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['discountAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vatPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['vatAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['sgstPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['sgstAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['cgstPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['cgstAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['igstPer']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['igstAmt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['net']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['totalQty']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['user']);
		 	$i++;
		}
		$r=$i-1;
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H8:H'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J8:J'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=SUM(N8:N'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '=SUM(P8:P'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=SUM(Q8:Q'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '=SUM(R8:R'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "S" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "S" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$partyName = '';//$data['purchase'][0]['name'];
		$poNo=0;

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/InvRepo_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/InvRepo_" . $dt . ' (' . $tm . ') ' . ".xls";

	}

	public function getProducts()
	{
		$data['products'] = $this->Rptinvoice_model->getProducts();
		echo json_encode($data);
	}

	public function exportDataDetail()
	{
		$this->printToExcelDetail();
	}

	public function printToExcelDetail()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:N2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:N3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Invoice Detail');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E4:N4');
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Inv#: ' . $this->input->post('globalVno'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:D5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Date: ' . $this->input->post('globalVdt'));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:N5');
		$objPHPExcel->getActiveSheet()->setCellValue('E5', '');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:D6');
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Party: ' . $this->input->post('globalPartyName'));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:N6');
		$objPHPExcel->getActiveSheet()->setCellValue('E6', ' ' );

		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:L6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Category');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Product');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Qty');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Rate');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Amt');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Colour');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Remarks');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'Desp.No.');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'Desp.Dt.');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'Odr.No.');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Odr.Dt.');
		$objPHPExcel->getActiveSheet()->setCellValue('M7', 'Odr.Type');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', 'Commit.Dt.');

		$objPHPExcel->getActiveSheet()->getStyle('F7:L7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "N" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		for($k=0; $k < $r; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['productCategory']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['productName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['qty']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['rate']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['amt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['colour']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['remarks']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['despatchNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['despatchDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['orderNo']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['orderDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['orderType']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['commitDt']);
		 	$i++;
		}
		$r=$i-1;
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '=SUM(D8:D'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "N" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "N" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$partyName = '';//$data['purchase'][0]['name'];
		$poNo=0;

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/InvDetailRepo_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/InvDetailRepo_" . $dt . ' (' . $tm . ') ' . ".xls";

	}

}
