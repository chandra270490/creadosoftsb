<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Colours_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Colours_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Countries')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Colours_model->getDataLimit();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Colours_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function insert()
	{
		if($this->Colours_model->checkDuplicate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Colours_model->insert();
			$data['records'] = $this->Colours_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function update()
	{
		if($this->Colours_model->checkDuplicateOnUpdate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Colours_model->update();
			$data['records'] = $this->Colours_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function delete()
	{
		if($this->Util_model->isDependent('qpodetail', 'colourRowId', $this->input->post('rowId')) == 1)
        {
        	$data['dependent'] = "yes";
        	echo json_encode($data);
        }
        else
        {
			$this->Colours_model->delete();
			$data['records'] = $this->Colours_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Colours_model->getDataAll();
		echo json_encode($data);
	}



	public function getImageName()
	{
		$path = FCPATH . '/bootstrap/images/colours/' . $this->input->post('rowId') . '.jpg';
        $r = file_exists($path);
        if($r == true)
        {
			echo json_encode(base_url()."bootstrap/images/colours/" . $this->input->post('rowId') . '.jpg');
        }
        else
        {
        	echo json_encode("NF"); // not found
        }
	}

	public function uploadImage()
	{
      $rowId = $_REQUEST['txtHiddenRowId']; 
      // $rowId = sprintf("%04d", $rowId);
      $tmp = base_url()."bootstrap/images/colours/";
      $config = array(
        'upload_path' => "bootstrap/images/colours/",
        'file_name' => $rowId . '.jpg',
        'allowed_types' => "jpg|jpeg",
        'overwrite' => TRUE,
        'max_size' => "1024000", // Can be set to particular file size , here it is 2 MB(2048 Kb = 2048000 B)
        // 'max_height' => "600", 
        // 'max_width' => "800"  
      );
      $this->load->library('upload', $config);

      if($_FILES["imageFile"]["name"] != "")
	  {
	      if( $this->upload->do_upload('imageFile') )
	      {
            // echo "Done...";
            $config['image_library'] = 'gd2';
            $config['source_image'] = 'bootstrap/images/colours/' . $rowId . '.jpg';
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']         = 60;
            $config['height']       = 60;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo "Done...";

          }
          else
          {
          	$error = array('error' => $this->upload->display_errors());
          	echo json_encode($error);	
          }
  	  }
  	  else ///in case no file selected but detail store
  	  {
        echo "aaaa";
  	  }
	}

	public function deleteImage()
	{
		$d = "en";
	  	$path = FCPATH . '/bootstrap/images/colours/' . $this->input->post('rowId') . '.jpg';
		unlink($path);

		$path = FCPATH . '/bootstrap/images/colours/' . $this->input->post('rowId') . '_thumb.jpg';
		unlink($path);

		echo $d;
	}
}
