<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptDsr_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptdsr_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'DSR Report')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $this->load->model('Colours_model');
			$data['parties'] = $this->Rptdsr_model->getAbList();
			$data['users'] = $this->Rptdsr_model->getUsers();
			$data['leadstatus'] = $this->Rptdsr_model->getleadstatus();
			// $data['colours'] = $this->Rptdsr_model->getColours();
			// $data['products'] = $this->Rptdsr_model->getProducts4CheckBox();
			// $data['parties'] = $this->Rptdsr_model->getPartyList();
			$this->load->view('RptDsr_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptdsr_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:M2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:M3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'DSR Report');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:M4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));


		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Dt');
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'User');
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Task');
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Type');
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Party');
		$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Party Type');
		$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Remarks');
		$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Further');
		$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Reason');
		$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Dt');
		$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Time');

		$objPHPExcel->getActiveSheet()->getStyle('F8:L8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (6) . ":" . "L" . (6);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$data['records'] = $this->Rptdsr_model->getDataForReport();

		// $myTableData = $this->input->post('TableData');
  //       $myTableData = stripcslashes($myTableData);
  //       $myTableData = json_decode($myTableData,TRUE);
  //       $myTableRows = count($myTableData);
		// $r = $myTableRows;
		$productRows = "";
		$i = 7;
		for($k=0; $k < count($data['records']); $k++)
		// for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, date('d-M-Y', strtotime($data['records'][$k]['dsrDt'])));
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['uid']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['task']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['type']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['name']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['partyType']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['remarks']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['further']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['meetingRemark']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['nextDt']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $data['records'][$k]['nextTm']);
		 	$i++;
		}
		$r=$i-1;
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I8:I'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');

		// $cellRange1 = "A" . ($i) . ":" . "M" . ($i);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "L" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/temp_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/temp_" . $dt . ' (' . $tm . ') ' . ".xls";


	}

	public function getProducts()
	{
		$data['products'] = $this->Rptdsr_model->getProducts();
		echo json_encode($data);
	}

	

}
