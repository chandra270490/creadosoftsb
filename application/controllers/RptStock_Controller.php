<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptStock_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Products_model');
            $this->load->model('Rptstock_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Stock Report')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			// $data['records'] = $this->Products_model->getDataForReport();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('RptStock_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptstock_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:D2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:D3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Stock Report');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "Date: " . date("d_m_Y"));

		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:M5');
		// $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Category: ' . $this->input->post('productCategory'));

		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:M6');
		// $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Colours: ' . $this->input->post('colorNames'));

		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:M7');
		// $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Products: ' . $this->input->post('productNames'));

		$objPHPExcel->getActiveSheet()->setCellValue('A8', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B8', 'Category');
		$objPHPExcel->getActiveSheet()->setCellValue('C8', 'Product Name');
		$objPHPExcel->getActiveSheet()->setCellValue('D8', 'Qty. in Hand');

		$objPHPExcel->getActiveSheet()->getStyle('F8:D8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (8) . ":" . "D" . (8);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 9;
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['category']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['productName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['inHandQty']);
		    // $myCol = $myCol + 1;

		 	$i++;
		}
		$r=$i-1;
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I8:I'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');

		$cellRange1 = "A" . ($i) . ":" . "D" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "D" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/StockRepo_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/StockRepo_" . $dt . ' (' . $tm . ') ' . ".xls";


	}

	// public function printToPdf($arg)
	// {
	// 	$this->load->library('Pdf');
	// 	$pdf = new Pdf('Landscape', 'mm', 'A4', true, 'UTF-8', false);
	// 	$pdf->SetTitle('Stock Report');
	// 	// $pdf->SetHeaderMargin(1);
	// 	$pdf->SetPrintHeader(false);
	// 	$pdf->SetTopMargin(15);
	// 	$pdf->setFooterMargin(12);
	// 	$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
	// 	$pdf->SetAuthor('Suri');
	// 	$pdf->SetDisplayMode('real', 'default');
	// 	$pdf->AddPage();

	// 	$pdf->SetFont('', '', 10, '', true); 
	// 	$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
	// 	$orgImagePath = $data['org'][0]['imagePath'];
	// 	$html='<table border="0">
	// 				<tr>
	// 					<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
	// 					<td style="width:90mm;"></td>
	// 					<td style="width:50mm;"></td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2">GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
	// 				</tr>
	// 		</table>';

	// 	$pdf->writeHTML($html, true, false, true, false, '');
	// 	// Line ($x1, $y1, $x2, $y2, $style=array())
	// 	$pdf->Line(0, 73, 210, 73);


	// 	$html='<table border="0">
	// 				<tr>
	// 					<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Stock Report</td>
	// 				</tr>
	// 				<tr>
	// 					<td colspan="2" align="center" style="font-size: 12pt; font-weight:normal;">Category: '. $this->input->post("productCategory") .'</td>
	// 				</tr>
	// 		</table>';
	// 	$pdf->writeHTML($html, true, false, true, false, '');

	// 	$myTableData = $this->input->post('TableData');
 //        $myTableData = stripcslashes($myTableData);
 //        $myTableData = json_decode($myTableData,TRUE);
 //        $myTableRows = count($myTableData);
	// 	$r = $myTableRows;


	// 	// // $pdf->writeHTML(100,"dd");


	// 	$productRows = "";
	// 	for($k=0; $k < $r-1; $k++)
	// 	{
	// 		$sn=$k+1;
	// 		$productRows .= "<tr nobr=\"true\">";
	// 			$productRows .= "<td>". $sn ."</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['productName'] . "</td>";
	// 			$productRows .= "<td>". $myTableData[$k]['inHandQty'] ."</td>";
	// 		$productRows .= "</tr>";
	// 	}
	// 	$html='<table border="1" cellpadding="10">
	// 				<tr>
	// 					<th style="width:20mm; color:white; background-color:black;">S.N.</th>
	// 					<th style="width:100mm; color:white; background-color:black;">Product Name</th>
	// 					<th style="width:30mm; color:white; background-color:black;">Qty In Hand</th>
	// 				</tr>'.$productRows.
	// 		'</table>';
	// 	$pdf->writeHTML($html, true, false, true, false, '');



	// 	$dt = date("Y_m_d");
	// 	date_default_timezone_set("Asia/Kolkata");
	// 	$tm = date("H_i_s");
	// 	$pdf->Output(FCPATH . '/downloads/SR_'. $dt . ' (' . $tm . ').pdf', 'F');
	// 	echo base_url()."/downloads/SR_". $dt . " (" . $tm . ").pdf";
	// }
}
