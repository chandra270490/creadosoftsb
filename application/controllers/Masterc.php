<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Masterc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('Masterm');
	}

	//Budget List
	public function index(){
		$tbl_nm = "budget_mst";
		$data = array();
		$data['list_title'] = "Budget List";
		$data['list_url'] = "Masterc";
		$data['tbl_nm'] = "budget_mst";
		$data['primary_col'] = "budget_id";
		$data['edit_url'] = "Masterc/budget_form";
		$data['edit_enable'] = "yes";
		$data['where_str'] = " ";

		$data['ViewHead'] = $this->Masterm->ListHead($tbl_nm);

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('ListView', $data); 
		$this->load->view('includes/footer');
	}

	//Budget Form
	public function budget_form(){
		$id = $_REQUEST['id'];

		if($id != ""){
			$data['get_by_id'] = $this->Masterm->get_by_id('budget_mst','budget_id',$id);
		} else {
			$data['get_by_id'] = "";
		}

		$this->load->view('includes/header4all');
		$MenuRights['mr'] = $this->Util_model->getUserRights();
		$this->load->view('includes/menu4admin', $MenuRights);
		$this->load->view('modules/masters/budget_form', $data); 
		$this->load->view('includes/footer');	
	}

	//Budget Entry
	public function budget_entry(){
		$data = array();
		$data['budget_entry'] = $this->Masterm->budget_entry($data);
		$data['url'] = 'Masterc';
		$this->load->view('QueryPage', $data);	
	}
}
