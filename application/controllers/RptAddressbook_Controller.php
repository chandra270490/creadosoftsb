<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptAddressbook_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptaddressbook_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Address Book Report')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $data['parties'] = $this->Rptaddressbook_model->getPartyList();
			$data = 'x';
			$this->load->view('RptAddressbook_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptaddressbook_model->getDataForReport();
		echo json_encode($data);
	}

	public function exportData()
	{
		// $this->printToPdf('Save');
		$this->printToExcel();
	}

	public function printToExcel()
	{
		set_time_limit(0);
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Address Book');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
		//$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:L5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Type: ' . $this->input->post('abType'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:L6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'prefix');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'name');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Type');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Company');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Address');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Town');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'District');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'State');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'PIN');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'Land.L1');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Land.L2');
		$objPHPExcel->getActiveSheet()->setCellValue('M7', 'Mobile1');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', 'Mobile2');
		$objPHPExcel->getActiveSheet()->setCellValue('O7', 'Email1');
		$objPHPExcel->getActiveSheet()->setCellValue('P7', 'Email2');
		$objPHPExcel->getActiveSheet()->setCellValue('Q7', 'PAN');
		$objPHPExcel->getActiveSheet()->setCellValue('R7', 'DIN');
		$objPHPExcel->getActiveSheet()->setCellValue('S7', 'Passport#');
		$objPHPExcel->getActiveSheet()->setCellValue('T7', 'Valid Upto');
		$objPHPExcel->getActiveSheet()->setCellValue('U7', 'Driv.Lic.#');
		$objPHPExcel->getActiveSheet()->setCellValue('V7', 'Valid Upto');
		$objPHPExcel->getActiveSheet()->setCellValue('W7', 'Fam.Card #');
		$objPHPExcel->getActiveSheet()->setCellValue('X7', 'Valid Upto');
		$objPHPExcel->getActiveSheet()->setCellValue('Y7', 'DOB');
		$objPHPExcel->getActiveSheet()->setCellValue('Z7', 'DOM');
		$objPHPExcel->getActiveSheet()->setCellValue('AA7', 'TIN S');
		$objPHPExcel->getActiveSheet()->setCellValue('AB7', 'TIN C');
		$objPHPExcel->getActiveSheet()->setCellValue('AC7', 'Contact Person1');
		$objPHPExcel->getActiveSheet()->setCellValue('AD7', 'Mobile');
		$objPHPExcel->getActiveSheet()->setCellValue('AE7', 'Dept.');
		$objPHPExcel->getActiveSheet()->setCellValue('AF7', 'Desig.');
		$objPHPExcel->getActiveSheet()->setCellValue('AG7', 'Contact Person2');
		$objPHPExcel->getActiveSheet()->setCellValue('AG7', 'Mobile');
		$objPHPExcel->getActiveSheet()->setCellValue('AI7', 'Dept.');
		$objPHPExcel->getActiveSheet()->setCellValue('AJ7', 'Desig.');
		$objPHPExcel->getActiveSheet()->setCellValue('AK7', 'Contact Person3');
		$objPHPExcel->getActiveSheet()->setCellValue('AL7', 'Mobile');
		$objPHPExcel->getActiveSheet()->setCellValue('AM7', 'Dept.');
		$objPHPExcel->getActiveSheet()->setCellValue('AN7', 'Desig.');
		$objPHPExcel->getActiveSheet()->setCellValue('AO7', 'Priority');
		$objPHPExcel->getActiveSheet()->setCellValue('AP7', 'Contact Type');

		$objPHPExcel->getActiveSheet()->getStyle('F7:AP7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "AP" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$productRows = "";
		$i = 8;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $myTableRows);
		for($k=0; $k < $r-1; $k++)
		{
		    $myCol = 0;
		    $sn = $k + 1;

		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['prefix']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, htmlspecialchars($myTableData[$k]['name']));
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['abType']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['labelName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, htmlspecialchars($myTableData[$k]['addr']));
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['townName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['districtName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['stateName']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['pin']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['landLine1']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['landLine2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['mobile1']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['mobile2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['email1']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['email2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['pan']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['din']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['passport']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['passportValidUpto']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['drivingLic']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['drivingLicValidUpto']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['familyCard']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['familyCardValidUpto']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['dob']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['dom']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['tinState']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['tinCentre']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactPerson']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactMobile']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactDepartment']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactDesignation']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactPerson2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactMobile2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactDepartment2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactDesignation2']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactPerson3']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactMobile3']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactDepartment3']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactDesignation3']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactPriority']);
		    $myCol = $myCol + 1;
		 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['contactType']);
		 	$i++;
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);	

		for ($ii = 'I'; $ii != $objPHPExcel->getActiveSheet()->getHighestColumn(); $ii++) 
		{
		    $objPHPExcel->getActiveSheet()->getColumnDimension($ii)->setWidth(13);
		}
		// $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(13);	
		// $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(13);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/AddressBookLimited_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/AddressBookLimited_" . $dt . ' (' . $tm . ') ' . ".xls";


	}

	public function exportCompleteAddressBook()
	{
		// $this->printToPdf('Save');
		$this->printToExcelCompleteAddressBook();
	}
	public function printToExcelCompleteAddressBook()
	{
		set_time_limit(0);
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Address Book');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
		//$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:L5');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Type: ' . $this->input->post('abType'));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:L6');

		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'prefix');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'name');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Type');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Company');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Address');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Town');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'District');
		$objPHPExcel->getActiveSheet()->setCellValue('I7', 'State');
		$objPHPExcel->getActiveSheet()->setCellValue('J7', 'PIN');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', 'Land.L1');
		$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Land.L2');
		$objPHPExcel->getActiveSheet()->setCellValue('M7', 'Mobile1');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', 'Mobile2');
		$objPHPExcel->getActiveSheet()->setCellValue('O7', 'Email1');
		$objPHPExcel->getActiveSheet()->setCellValue('P7', 'Email2');
		$objPHPExcel->getActiveSheet()->setCellValue('Q7', 'PAN');
		$objPHPExcel->getActiveSheet()->setCellValue('R7', 'DIN');
		$objPHPExcel->getActiveSheet()->setCellValue('S7', 'Passport#');
		$objPHPExcel->getActiveSheet()->setCellValue('T7', 'Valid Upto');
		$objPHPExcel->getActiveSheet()->setCellValue('U7', 'Driv.Lic.#');
		$objPHPExcel->getActiveSheet()->setCellValue('V7', 'Valid Upto');
		$objPHPExcel->getActiveSheet()->setCellValue('W7', 'Fam.Card #');
		$objPHPExcel->getActiveSheet()->setCellValue('X7', 'Valid Upto');
		$objPHPExcel->getActiveSheet()->setCellValue('Y7', 'DOB');
		$objPHPExcel->getActiveSheet()->setCellValue('Z7', 'DOM');
		$objPHPExcel->getActiveSheet()->setCellValue('AA7', 'TIN S');
		$objPHPExcel->getActiveSheet()->setCellValue('AB7', 'TIN C');
		$objPHPExcel->getActiveSheet()->setCellValue('AC7', 'Contact Person1');
		$objPHPExcel->getActiveSheet()->setCellValue('AD7', 'Mobile');
		$objPHPExcel->getActiveSheet()->setCellValue('AE7', 'Dept.');
		$objPHPExcel->getActiveSheet()->setCellValue('AF7', 'Desig.');
		$objPHPExcel->getActiveSheet()->setCellValue('AG7', 'Contact Person2');
		$objPHPExcel->getActiveSheet()->setCellValue('AH7', 'Mobile');
		$objPHPExcel->getActiveSheet()->setCellValue('AI7', 'Dept.');
		$objPHPExcel->getActiveSheet()->setCellValue('AJ7', 'Desig.');
		$objPHPExcel->getActiveSheet()->setCellValue('AK7', 'Contact Person3');
		$objPHPExcel->getActiveSheet()->setCellValue('AL7', 'Mobile');
		$objPHPExcel->getActiveSheet()->setCellValue('AM7', 'Dept.');
		$objPHPExcel->getActiveSheet()->setCellValue('AN7', 'Desig.');
		$objPHPExcel->getActiveSheet()->setCellValue('AO7', 'Priority');
		$objPHPExcel->getActiveSheet()->setCellValue('AP7', 'Contact Type');

		$objPHPExcel->getActiveSheet()->getStyle('F7:AP7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (7) . ":" . "AP" . (7);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$data['records'] = $this->Rptaddressbook_model->getCompleteAddressBook();
		$row = 8;
		for ($i=0; $i < count($data['records']) ; $i++) 
		{ 
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i+$row, $i+1);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i+$row, $data['records'][$i]['prefixType']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i+$row, $data['records'][$i]['name']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i+$row, $data['records'][$i]['abType']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i+$row, $data['records'][$i]['labelName']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i+$row, $data['records'][$i]['addr']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i+$row, $data['records'][$i]['townName']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i+$row, $data['records'][$i]['districtName']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i+$row, $data['records'][$i]['stateName']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i+$row, $data['records'][$i]['pin']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i+$row, $data['records'][$i]['landLine1']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $i+$row, $data['records'][$i]['landLine2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $i+$row, $data['records'][$i]['mobile1']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $i+$row, $data['records'][$i]['mobile2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $i+$row, $data['records'][$i]['email1']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $i+$row, $data['records'][$i]['email2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $i+$row, $data['records'][$i]['pan']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $i+$row, $data['records'][$i]['din']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $i+$row, $data['records'][$i]['passport']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29, $i+$row, $data['records'][$i]['passportValidUpto']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $i+$row, $data['records'][$i]['drivingLic']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $i+$row, $data['records'][$i]['drivingLicValidUpto']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $i+$row, $data['records'][$i]['familyCard']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $i+$row, $data['records'][$i]['familyCardValidUpto']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $i+$row, $data['records'][$i]['dob']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $i+$row, $data['records'][$i]['dom']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $i+$row, $data['records'][$i]['tinState']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $i+$row, $data['records'][$i]['tinCentre']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $i+$row, $data['records'][$i]['contactPerson']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29, $i+$row, $data['records'][$i]['contactMobile']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30, $i+$row, $data['records'][$i]['contactDepartment']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31, $i+$row, $data['records'][$i]['contactDesignation']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(32, $i+$row, $data['records'][$i]['contactPerson2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(33, $i+$row, $data['records'][$i]['contactMobile2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(34, $i+$row, $data['records'][$i]['contactDepartment2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(35, $i+$row, $data['records'][$i]['contactDesignation2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(36, $i+$row, $data['records'][$i]['contactPerson3']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(37, $i+$row, $data['records'][$i]['contactMobile3']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(38, $i+$row, $data['records'][$i]['contactDepartment3']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(39, $i+$row, $data['records'][$i]['contactDesignation3']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(40, $i+$row, $data['records'][$i]['contactPriority']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(41, $i+$row, $data['records'][$i]['contactType']);

		}


		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);	

		for ($ii = 'I'; $ii != $objPHPExcel->getActiveSheet()->getHighestColumn(); $ii++) 
		{
		    $objPHPExcel->getActiveSheet()->getColumnDimension($ii)->setWidth(13);
		}
		

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/AddressBookLimited_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/AddressBookLimited_" . $dt . ' (' . $tm . ') ' . ".xls";


	}
}
