<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptSalaryMonthly_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptsalarymonthly_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Salary Register (Monthly)')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $data['parties'] = $this->Rptsalarymonthly_model->getPartyList();
			$this->load->view('RptSalaryMonthly_view');
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptsalarymonthly_model->getDataForReport();
		echo json_encode($data);
	}


	public function exportData()
	{
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Salary Register (Monthly)');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));



		$objPHPExcel->getActiveSheet()->getStyle('A6:AZ6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (6) . ":" . "AZ" . (6);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$noOfDays = $this->input->post('noOfDays');
		$i = 6;
		for($k=0; $k < $r; $k++)
		{
			for($c=0; $c<count($myTableData[$k]); $c++)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $i, $myTableData[$k][$c]);
			}
			$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
		 	$i++;
		}
		$r=$i-1;

		$cellRange1 = "A" . ($i) . ":" . "AZ" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (6) . ":" . "AZ" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);	
		for ($ii = 'D'; $ii != $objPHPExcel->getActiveSheet()->getHighestColumn(); $ii++) 
		{
		    $objPHPExcel->getActiveSheet()->getColumnDimension($ii)->setWidth(9);
		}

		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '=SUM(E7:E'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F7:F'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '=SUM(G7:G'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H7:H'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I7:I'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J7:J'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K7:K'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L7:L'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '=SUM(M7:M'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=SUM(N7:N'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '=SUM(O7:O'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '=SUM(P7:P'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=SUM(Q7:Q'.$r.')');
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '=SUM(R7:R'.$r.')');


	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('excelfiles/Sal_Mon_'. $dt . ' (' . $tm . ') ' .'.xls');
		echo base_url().'excelfiles/Sal_Mon_'. $dt . ' (' . $tm . ') ' .'.xls';
		// $objWriter->save("excelfiles/tmp.xls");
		// echo base_url()."excelfiles/tmp.xls";

	}
}
