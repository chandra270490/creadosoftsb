<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proforma_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Proforma_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Proforma Invoice')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Util_model->getDataLimitQpo('P');
			$data['parties'] = $this->Util_model->getPartyList();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$this->load->model('Placements_model');
			$data['placements'] = $this->Placements_model->getPlacementList();
			$this->load->model('Colours_model');
			$data['colours'] = $this->Colours_model->getColourList();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Proforma_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getProductList()
	{
		$data['products'] = $this->Proforma_model->getProductList();
		echo json_encode($data);
	}

	public function getPlacemetns()
	{
		$this->load->model('Placements_model');
		$data['placements'] = $this->Placements_model->getPlacementListRefresh();
		echo json_encode($data);
	}
	public function getColours()
	{
		$this->load->model('Colours_model');
		$data['colours'] = $this->Colours_model->getColourListRefresh();
		echo json_encode($data);
	}
	
	public function insert()
	{
		$this->Proforma_model->insert();
		if( $this->input->post('exportIn') == "W")
		{
			$this->printData('Save');
		}
		else
		{
			$this->printToPdf('Save');
		}
		// 
		
	}

	public function update()
	{
		$this->Proforma_model->update();
		if( $this->input->post('exportIn') == "W")
		{
			// $this->printData('Update');
		}
		else
		{
			$this->printToPdf('Update');
		}
	}

	public function printToPdf($arg)
	{
		$rowId = -10;
		if($arg == "Update")
		{
			$rowId = $this->input->post('globalrowid');
		}
		else if($arg == "Save")
		{
			$rowId = $this->Proforma_model->getInvNo();
		}
		$data['qpo'] = $this->Proforma_model->getQpo($rowId);

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Proforma Invoice');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg($rowId);
		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, 'CodeIgniter TCPDF Integration');
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/logolt.jpg" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// Line ($x1, $y1, $x2, $y2, $style=array())
		$pdf->Line(0, 73, 210, 73);

		$addr = ($this->input->post('addr'));
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$addrNew = "";
		for($c=0; $c<$cnt; $c++)
		{
			$addrNew .= $textlines[$c] . "<br />";
		}

		$html='<table border="0">
					<tr>
						<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Proforma Invoice</td>
					</tr>
					<tr>
						<td style="width:95mm;height:10mm;">Date: '.$this->input->post('vDt').'</td>
						<td align="right" style="width:90mm;">P.No.: '.$data['qpo'][0]['vNo'].'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;">To,</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 12pt; font-weight:bold;">'.$data['qpo'][0]['prefixType']. ' ' .$data['qpo'][0]['name'].'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:10mm;font-size: 10pt;">'.$addrNew.'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 10pt;">Dear Sir,</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 10pt;">     This acknowledge with thanks the receipt of your NIT/Letter No. '. $this->input->post('letterNo') .'  dt. '. $this->input->post('vDt') .' and as desired we are providing our quotation for the items required by you.</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;


		// $pdf->writeHTML(100,"dd");


		$productRows = "";
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
				$productRows .= "<td>". $sn ."</td>";
				$productRows .= "<td>". $myTableData[$k]['productName'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['productQty'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['productRate'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['productAmt'] ."</td>";

			$productRows .= "</tr>";
		}
		$html='<table border="1" cellpadding="5">
					<tr>
						<th style="width:15mm; color:white; background-color:black;">S.N.</th>
						<th style="width:100mm; color:white; background-color:black;">Product</th>
						<th style="width:25mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="width:25mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="width:25mm; text-align:center; color:white; background-color:black;">Amt.</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$col1="";
		$col2="";
		$col3="";

		$col1 .= 'Total Qty.: '. $this->input->post('totalQty') . '<br>';
		$col2 .= 'Total Amt.:' . '<br>';	
		$col3 .= $this->input->post('totalAmt') . '<br>';	

		if($this->input->post('discountAmt') == 0) 
		{
			$printDiscountTitle="";
			$printDiscountPer="";
			$printDiscountAmt="";
		}
		else
		{
			$printDiscountTitle = "Discount Amt.:";
			$printDiscountPer = $this->input->post('discountAmt').' ('. $this->input->post('discountPer')  .'%)' ;
			$printDiscountAmt = $this->input->post('totalAfterDiscount');
			$col2 .= $printDiscountTitle . ' [' . $printDiscountPer . ']<br>';
			$col3 .= $printDiscountAmt . '<br>';
		}

		if($this->input->post('vatPer') == 0) 
		{
			$printVatTitle="";
			$printVatPer="";
			$printVatAmt="";
		}
		else
		{
			$printVatTitle = "VAT:";
			$printVatPer = $this->input->post('vatPer').'%';
			$printVatAmt = $this->input->post('vatAmt');
			$col2 .= $printVatTitle . ' (' . $printVatPer . ')<br>';
			$col3 .= $printVatAmt . '<br>';			
		}

		if($this->input->post('sgstPer') == 0) 
		{
			$printSgstTitle="";
			$printSgstPer="";
			$printSgstAmt="";
		}
		else
		{
			$printSgstTitle = "SGST:";
			$printSgstPer = $this->input->post('sgstPer').'%';
			$printSgstAmt = $this->input->post('sgstAmt');
			$col2 .= $printSgstTitle . ' (' . $printSgstPer . ')<br>';
			$col3 .= $printSgstAmt . '<br>';			
		}
		if($this->input->post('cgstPer') == 0) 
		{
			$printCgstTitle="";
			$printCgstPer="";
			$printCgstAmt="";
		}
		else
		{
			$printCgstTitle = "CGST:";
			$printCgstPer = $this->input->post('cgstPer').'%';
			$printCgstAmt = $this->input->post('cgstAmt');
			$col2 .= $printCgstTitle . ' (' . $printCgstPer . ')<br>';
			$col3 .= $printCgstAmt . '<br>';			
		}
		if($this->input->post('igstPer') == 0) 
		{
			$printIgstTitle="";
			$printIgstPer="";
			$printIgstAmt="";
		}
		else
		{
			$printIgstTitle = "IGST:";
			$printIgstPer = $this->input->post('igstPer').'%';
			$printIgstAmt = $this->input->post('igstAmt');
			$col2 .= $printIgstTitle . ' (' . $printIgstPer . ')<br>';
			$col3 .= $printIgstAmt . '<br>';			
		}
		$col2 .= 'Net Amt.:';
		$col3 .= $this->input->post('net');			

		$html='<table border="0" cellpadding="5">
					<tr>
						<td align="left"  style="width:60mm;height:7mm;font-size: 10pt;">' . $col1 . '</td>
						<td align="right" style="width:97mm;height:7mm;font-size: 10pt;">' . $col2 . '</td>
						<td align="right" style="width:30mm;height:7mm;font-size: 10pt;">' . $col3 . '</td>
					</tr>

					<tr>
						<td align="right" colspan="3" style="height:7mm;font-size: 10pt;">'. $this->input->post('inWords') .'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		// $html='<table border="0" cellpadding="5">
		// 			<tr>
		// 				<td align="right" colspan="2" style="width:120mm;height:7mm;font-size: 10pt;">Total Amt.:</td>
		// 				<td align="right" colspan="2" style="width:70mm;height:7mm;font-size: 10pt;">'. $this->input->post('totalAmt') .'</td>
		// 			</tr>
		// 			<tr>
		// 				<td align="right" colspan="2" style="height:7mm;font-size: 10pt;">Discount Amt.:</td>
		// 				<td align="right" style="height:7mm;font-size: 10pt;">'. $this->input->post('discountAmt').' ('. $this->input->post('discountPer') .'%)' .'</td>
		// 				<td align="right" style="height:7mm;font-size: 10pt;">'. $this->input->post('totalAfterDiscount') .'</td>
		// 			</tr>
		// 			<tr>
		// 				<td align="right" colspan="2" style="height:7mm;font-size: 10pt;">VAT:</td>
		// 				<td align="right" style="height:7mm;font-size: 10pt;">'. $this->input->post('vatPer').'%' .'</td>
		// 				<td align="right" style="height:7mm;font-size: 10pt;">'. $this->input->post('vatAmt') .'</td>
		// 			</tr>
		// 			<tr>
		// 				<td align="right" colspan="2" style="height:7mm;font-size: 10pt;">Net Amt.:</td>
		// 				<td align="right" style="height:7mm;font-size: 10pt;"></td>
		// 				<td align="right" style="height:7mm;font-size: 10pt;">'. $this->input->post('net') .'</td>
		// 			</tr>
		// 			<tr>
		// 				<td align="right" colspan="4" style="height:7mm;font-size: 10pt;">'. $this->input->post('inWords') .'</td>
		// 			</tr>
		// 	</table>';
		// $pdf->writeHTML($html, true, false, true, false, '');


		$html='<table border="0">
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">1. All prices are for delivery F.O.R./Ajmer/Destination.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">2. VAT and Other duties will be extra.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">3. if the purchaser fails to take the delivery of the goods from the transport he shall be liable to bear all the expenses for damage of goods etc.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">4. ____50___% payment will be received by sending the dispatch documents through Bank and balance at the time of delivery.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">5. All disputes are subject to Ajmer Jurisdiction only.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">6. In case payments are not made within the stipulated period interest at 18% p.a. will be charged.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">7. Prices are based on present market rates of raw material and subject to </td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">For '.$data['org'][0]['orgName'].'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$partyName = $data['qpo'][0]['name'];
		// $pdf->Output($_SERVER['DOCUMENT_ROOT'] . 'sberp/downloads/P_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		$pdf->Output(FCPATH . '/downloads/P_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."downloads/P_". $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}

	public function printData($arg)
	{

		$rowId = -10;
		if($arg == "Update")
		{
			$rowId = $this->input->post('globalrowid');
		}
		else if($arg == "Save")
		{
			$rowId = $this->Proforma_model->getInvNo();
		}

		require_once 'PHPWord-master/src/PhpWord/Autoloader.php';
		\PhpOffice\PhpWord\Autoloader::register();
		// Creating the new document...
		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		$section = $phpWord->addSection(array('orientation'=>'portrait'
																, 'marginTop' => 850
																, 'marginBottom' => 1000
																, 'marginLeft' => 850
																, 'marginRight' => 850
																, 'pageNumberingStart' => 1
																, 'breakType' => 'nextPage'
																));
		$footer = $section->addFooter();
		$footer->addPreserveText('Page {PAGE} of {SECTIONPAGES}.');

		$data['org'] = $this->Util_model->getOrg($rowId);
		$orgImagePath = $data['org'][0]['imagePath'];
		
	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => 'FFFFFF',
						    'borderBottomSize' => 18, 
						    'borderBottomColor' => '000000',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable', $tableStyle);
		$table = $section->addTable('myTable');
		
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false);
		$table->addRow(); // argument is height
		$table->addCell(2000, array('valign'=>'bottom', 'borderRightSize'=>10, 'borderRightColor'=>'FFFFFF'))->addImage(FCPATH.'/bootstrap/images/logolt.jpg', array('width'=>210, 'align'=>'center'));
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 2));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>0));
		$table->addCell(2000, array('valign'=>'bottom', 'halign'=>'right','borderRightSize'=>1, 'borderRightColor'=>'FFFFFF', 'borderLeftColor'=>'FFFFFF'))->addImage(FCPATH.'/bootstrap/images/'.$orgImagePath , array('width'=>140,'align'=>'center'));


		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['orgName'], array('name' => 'Arial', 'size' => 14, 'bold'=>true), array('spaceAfter'=>0));
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['add1'].$data['org'][0]['add2'].$data['org'][0]['add3'].$data['org'][0]['add4'], $fontStyle, array('spaceAfter'=>0));
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['mobile1'].$data['org'][0]['email'], $fontStyle, array('spaceAfter'=>0));
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['org'][0]['tin'], $fontStyle, array('spaceAfter'=>0));


		$section->addText(' ', $fontStyle, array('align' => 'center','spaceBefore'=>10, 'spaceAfter'=>4));
		$fontStyle = array('name' => 'Arial', 'size' => 14, 'bold'=>true);
		$section->addText('Proforma Invoice', $fontStyle, array('align' => 'center','spaceBefore'=>10, 'spaceAfter'=>4));

	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => 'FFFFFF',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable1', $tableStyle);
		$table = $section->addTable('myTable1');
		
		$fontStyle = array('name' => 'Arial', 'size' => 11, 'bold'=>false);
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 2));
		$cell->addText("Date: ".$this->input->post('vDt'), $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(6500, array('valign'=>'top', 'halign'=>'right', 'gridSpan' => 2));
		$cell->addText("P. No.: " . $rowId, $fontStyle, array('align' => 'right','spaceAfter'=>3));
		
		// $data['partyInfo'] = $this->Proforma_model->getPartyInfo();

		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));

		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText("To,", $fontStyle, array('spaceAfter'=>3));

		$data['qpo'] = $this->Proforma_model->getQpo($rowId);
		$table->addRow(); // argument is height
		$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
		$cell->addText($data['qpo'][0]['prefixType'] . ' ' . $data['qpo'][0]['name'], $fontStyle, array('spaceAfter'=>3));
		
		$addr = ($this->input->post('addr'));
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		for($c=0; $c<$cnt; $c++)
		{
			$table->addRow(); // argument is height
			$cell = $table->addCell(6500, array('valign'=>'top', 'gridSpan' => 4));
			$cell->addText($textlines[$c], $fontStyle, array('spaceAfter'=>3));
		}
		

		$section->addText(' ', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));
		$section->addText('Dear Sir,', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));
		$section->addText('     This acknowledge with thanks the receipt of your NIT/Letter No. '. $this->input->post('letterNo') .'  dt. '. $this->input->post('vDt') .' and as desired we are providing our quotation for the items required by you.', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));
		$section->addText(' ', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));

	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => '000000',
						    'borderSize' => 6,
						    'cellMargin' => 80
						);

		$phpWord->addTableStyle('myTable2', $tableStyle);
		$table = $section->addTable('myTable2');
		
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold'=>false);
		$table->addRow(); // argument is height
		$cell = $table->addCell(200, array('valign'=>'top'));
		$cell->addText("S.N.", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(2000, array('valign'=>'top'));
		$cell->addText("Product", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(1500, array('valign'=>'top'));
		$cell->addText("Placement", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(1500, array('valign'=>'top'));
		$cell->addText("Colour", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(500, array('valign'=>'top'));
		$cell->addText("Qty", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(500, array('valign'=>'top'));
		$cell->addText("Rate", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(500, array('valign'=>'top'));
		$cell->addText("Amt.", $fontStyle, array('spaceAfter'=>0));
		$cell = $table->addCell(2000, array('valign'=>'top'));
		$cell->addText("Preview", $fontStyle, array('spaceAfter'=>0));

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;

		$cellStyle = array('valign'=>'center', 'borderSize'=>6, 'borderColor'=>'000000');
		$fontStyle = array('name' => 'Arial', 'size' => 10, 'bold' => false);
		$paraStyleLeft = array('spaceAfter'=>1, 'align' => 'left');
		$paraStyleRight = array('spaceAfter'=>1, 'align' => 'right');

		for($k=0; $k < $r; $k++)
		{
			$table->addRow(); // argument is height
			$table->addCell(200, $cellStyle)->addText($k+1, $fontStyle, $paraStyleLeft);
			$table->addCell(2000, $cellStyle)->addText($myTableData[$k]['productName'], $fontStyle, $paraStyleLeft);
			$table->addCell(1500, $cellStyle)->addText($myTableData[$k]['placement'], $fontStyle, $paraStyleLeft);
			$table->addCell(1500, $cellStyle)->addText($myTableData[$k]['colour'], $fontStyle, $paraStyleLeft);
			$table->addCell(500, $cellStyle)->addText($myTableData[$k]['productQty'], $fontStyle, $paraStyleLeft);
			$table->addCell(500, $cellStyle)->addText($myTableData[$k]['productRate'], $fontStyle, $paraStyleLeft);
			$table->addCell(500, $cellStyle)->addText($myTableData[$k]['productAmt'], $fontStyle, $paraStyleLeft);
			// $table->addCell(2000, $cellStyle)->addText('i', $fontStyle, $paraStyleLeft);
			$productRowId = $myTableData[$k]['productRowId'];
			$data['img'] = $this->Proforma_model->getImageName($productRowId);
			// $productRowId = sprintf("%04d", $productRowId);
          	$fileName = $data['img'][0]['imagePathThumb'];
          	if( $fileName != "N")
          	{
				$table->addCell(2000, array('valign'=>'center'))->addImage(FCPATH.'/bootstrap/images/products/'.$fileName, array('align'=>'center'));
          	}
			else
			{
				$table->addCell(2000, $cellStyle)->addText(' ', $fontStyle, $paraStyleLeft);
			}
		}

		$section->addText(' ', $fontStyle, array('align' => 'justify','spaceBefore'=>10, 'spaceAfter'=>2));

	    /////////// Define table style arrays
		$tableStyle = array(
						    'borderColor' => 'FFFFFF',
						    'borderSize' => 6,
						    'cellMargin' => 10
						);

		$phpWord->addTableStyle('myTable3', $tableStyle);
		$table = $section->addTable('myTable3');
		
		$fontStyle = array('name' => 'Arial', 'size' => 11, 'bold'=>false);
		$table->addRow(); // argument is height
		$cell = $table->addCell(5000, array('valign'=>'top', 'gridSpan' => 3));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText("Total Amt.: ", $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top'));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText($this->input->post('totalAmt'), $fontStyle, array('align' => 'right','spaceAfter'=>3));

		$table->addRow(); // argument is height
		$cell = $table->addCell(3000, array('valign'=>'top', 'gridSpan' => 3));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText("Discount Amt.: ", $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText($this->input->post('discountAmt').' ('. $this->input->post('discountPer') .'%)', $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText($this->input->post('totalAfterDiscount'), $fontStyle, array('align' => 'right','spaceAfter'=>3));

		$table->addRow(); // argument is height
		$cell = $table->addCell(3000, array('valign'=>'top', 'gridSpan' => 3));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText("VAT: ", $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText($this->input->post('vatPer').'%', $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText($this->input->post('vatAmt'), $fontStyle, array('align' => 'right','spaceAfter'=>3));

		$table->addRow(); // argument is height
		$cell = $table->addCell(3000, array('valign'=>'top', 'gridSpan' => 3));
		$cell->addText(" ", $fontStyle, array('spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText("Net Amt.: ", $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText(' ', $fontStyle, array('align' => 'right','spaceAfter'=>3));
		$cell = $table->addCell(1500, array('valign'=>'top', 'halign'=>'right'));
		$cell->addText($this->input->post('net'), $fontStyle, array('align' => 'right','spaceAfter'=>3));


		$section->addText(' ', $fontStyle, array('align' => 'center','spaceBefore'=>10, 'spaceAfter'=>4));
		$fontStyle = array('name' => 'Arial', 'size' => 7, 'bold'=>false);
		$section->addText('1. All prices are for delivery F.O.R./Ajmer/Destination.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('2. VAT and Other duties will be extra.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('3. if the purchaser fails to take the delivery of the goods from the transport he shall be liable to bear all the expenses for damage of goods etc.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('4. ____50___% payment will be received by sending the dispatch documents through Bank and balance at the time of delivery.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('5. All disputes are subject to Ajmer Jurisdiction only.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('6. In case payments are not made within the stipulated period interest at 18% p.a. will be charged.', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));
		$section->addText('7. Prices are based on present market rates of raw material and subject to ', $fontStyle, array('align' => 'left','spaceBefore'=>10, 'spaceAfter'=>4));

		$fontStyle = array('name' => 'Arial', 'size' => 11, 'bold'=>false);
		$section->addText('For Kinny Fibreglass', $fontStyle, array('align' => 'right','spaceBefore'=>10, 'spaceAfter'=>4));


		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$partyName = $data['qpo'][0]['name'];
		// $pdf->Output($_SERVER['DOCUMENT_ROOT'] . 'sberp/downloads/P_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');

		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$fileName = "downloads/P_" . $dt . ' (' . $tm . ') ' . $partyName . ".docx";
		$objWriter->save($fileName);
		echo base_url().$fileName;
		// echo "hello";

		// Get FileName from CMS/Framework/Library or temporary variable:
		// $newDoc = 'downloads/Q.docx';

		// /* Add our HTTP Headers */
		// // http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
		// // http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html

		// // Doc generated on the fly, may change so do not cache it; mark as public or
		// // private to be cached.
		// header('Pragma: no-cache');
		// // Mark file as already expired for cache; mark with RFC 1123 Date Format up to
		// // 1 year ahead for caching (ex. Thu, 01 Dec 1994 16:00:00 GMT)
		// header('Expires: 0');
		// // Forces cache to re-validate with server
		// header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		// // DocX Content Type
		// header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
		// // Tells browser we are sending file
		// header('Content-Disposition: attachment; filename=Sample_02_TabStops.docx;');
		// // Tell proxies and gateways method of file transfer
		// header('Content-Transfer-Encoding: binary');
		// // Indicates the size to receiving browser
		// header('Content-Length: '.filesize($newDoc));

		// // Send the file:
		// readfile($newDoc);		

	}   

	public function delete()
	{
		$this->Proforma_model->delete();
		$data['records'] = $this->Util_model->getDataLimitQpo('P');
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Util_model->getDataAllQpo('P');
		echo json_encode($data);
	}
	public function loadLimitedRecords()
	{
		$data['records'] = $this->Util_model->getDataLimitQpo('P');
		echo json_encode($data);
	}

	public function getProducts()
	{
		$data['products'] = $this->Proforma_model->getProducts();
		echo json_encode($data);
	}
}
