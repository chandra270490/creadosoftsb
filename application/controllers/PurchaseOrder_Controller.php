<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseOrder_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Purchaseorder_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Purchase Order') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Purchaseorder_model->getDataLimit();
			$data['parties'] = $this->Util_model->getPartyList();
			// $data['references'] = $this->Purchaseorder_model->getReferenceList();
			// $data['orderTypes'] = $this->Purchaseorder_model->getOrderTypeList();
			$this->load->model('Productcategories_model');

			//Product Master Category
			$data['productmc'] = $this->Productcategories_model->getProductmc();

			$data['productCategories'] = $this->Productcategories_model->getProductCategories();

			// $this->load->model('Placements_model');
			// $data['placements'] = $this->Placements_model->getPlacementList();
			$this->load->model('Colours_model');
			$data['colours'] = $this->Colours_model->getColourList();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('PurchaseOrder_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}

	public function PurchaseOrder_pc_ajax(){
		$this->load->view('PurchaseOrder_pc_ajax');
	}

	public function PurchaseOrder_prod_ajax(){
		$this->load->view('PurchaseOrder_prod_ajax');
	}
	
	public function getProductCatList()
	{
		$data['ProductCatList'] = $this->Purchaseorder_model->ProductCatList();
		echo json_encode($data);
	}

	public function getProductList()
	{
		$data['products'] = $this->Purchaseorder_model->getProductList();
		echo json_encode($data);
	}

	public function getCurrentQty()
	{
		$data['currentQty'] = $this->Purchaseorder_model->getCurrentQty();
		$data['lastPurchaseInfo'] = $this->Purchaseorder_model->getLastPurchaseInfo();
		echo json_encode($data);
	}

	public function showHistory()
	{
		// $data['currentQty'] = $this->Purchaseorder_model->getCurrentQty();
		$data['historyData'] = $this->Purchaseorder_model->getHistoryData();
		echo json_encode($data);
	}
	
	public function insert()
	{
		if($this->Util_model->isSessionExpired() == 1)
        {
        	$data = "Session out...";
        	echo json_encode($data);
        	// echo $data;
        }
        else
        {
			$this->Purchaseorder_model->insert();
			$data['records'] = $this->Purchaseorder_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function update()
	{
		$this->Purchaseorder_model->update();
		$data['records'] = $this->Purchaseorder_model->getDataLimit();
		echo json_encode($data);
	}


	public function delete()
	{
		$c = $this->Purchaseorder_model->checkDependency();
		// echo json_encode($c);
		// return;
		if( $c == 1)
		{
			echo json_encode('cannot');
		}
		else
		{
			$this->Purchaseorder_model->delete();
			$data['records'] = $this->Purchaseorder_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Purchaseorder_model->getDataAll();
		echo json_encode($data);
	}
	public function loadLimitedRecords()
	{
		$data['records'] = $this->Purchaseorder_model->getDataLimit();
		echo json_encode($data);
	}

	public function getProducts()
	{
		$data['products'] = $this->Purchaseorder_model->getProducts();
		echo json_encode($data);
	}
}
