<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Purchase_model');
            $this->load->helper('form');
            $this->load->helper('url');
	}
	
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Purchase') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Purchase_model->getDataLimit();
			$data['parties'] = $this->Purchase_model->loadParties();
			$this->load->model('Productcategories_model');
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Purchase_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['pendingProducts'] = $this->Purchase_model->showData();
		echo json_encode($data);
	}


	
	public function insert()
	{
		if($this->Util_model->isSessionExpired() == 1)
        {
        	$data = "Session out...";
        	echo json_encode($data);
        	// echo $data;
        }
        else
        {
			$this->Purchase_model->insert();
			$data['records'] = $this->Purchase_model->getDataLimit();
			echo json_encode($data);
		}
	}


	public function delete()
	{
		$this->Purchase_model->delete();
		$data['records'] = $this->Purchase_model->getDataLimit();
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Purchase_model->getDataAll();
		echo json_encode($data);
	}

}
