<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MoveInStage_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Moveinstage_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Move In Stage') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['products'] = $this->Moveinstage_model->getProductList();
			$data['employees'] = $this->Moveinstage_model->getEmpList();
			$data['stagesTo'] = $this->Moveinstage_model->getStagesTo();
			$data['stagesFrom'] = $this->Moveinstage_model->getStagesFrom();
			$data['stages4table'] = $this->Moveinstage_model->getStages4table();
			$this->load->model('Colours_model');
			$data['colours'] = $this->Colours_model->getColourList();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('MoveInStage_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  


	public function getProductStatus()
	{
		$data['ob'] = $this->Moveinstage_model->getProductStatus();
		$data['despStageQty'] = $this->Moveinstage_model->getDespStageQty();
		$data['pendingOrders'] = $this->Moveinstage_model->getPendingOrders();
		echo json_encode($data);
	}


	public function moveData()
	{
		$this->Moveinstage_model->moveData();
		$data['ob'] = $this->Moveinstage_model->getProductStatus();
		$data['despStageQty'] = $this->Moveinstage_model->getDespStageQty();
		$data['pendingOrders'] = $this->Moveinstage_model->getPendingOrders();
		// $data['ob'] = 'ff';
		echo json_encode($data);
	}



	// public function saveChanges()
	// {
	// 	$this->Moveinstage_model->saveChanges();

	// }


}
