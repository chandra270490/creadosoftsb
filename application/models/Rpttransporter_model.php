<?php
class Rpttransporter_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        if( $this->input->post('partyRowId') == "-1" )
        {
             $this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
             $this->db->from('despatch');
             $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
            $this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
             // $this->db->where('qpo.vType', 'Q');
             // $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('despatch.deleted', 'N');
             $this->db->where('despatch.orgRowId', $this->session->orgRowId);
             $this->db->order_by('despatch.despatchRowId');
             $query = $this->db->get();
             return($query->result_array());
        }
        else
        {
             $this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
             $this->db->from('despatch');
             $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
            $this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
             $this->db->where('despatch.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('despatch.deleted', 'N');
             $this->db->where('despatch.orgRowId', $this->session->orgRowId);
             $this->db->order_by('despatch.despatchRowId');
             $query = $this->db->get();
             return($query->result_array());

        }

    }

    public function getDataForReportPrGr()
    {
         $this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
         $this->db->from('despatch');
         $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
         $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
         $this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
        $this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
         $this->db->where('despatch.prGrNo', $this->input->post('prGrNo'));
         $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
         $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
         $this->db->where('despatch.deleted', 'N');
         $this->db->where('despatch.orgRowId', $this->session->orgRowId);
         $this->db->order_by('despatch.despatchRowId');
         $query = $this->db->get();
         return($query->result_array());
    }

    public function getProducts()
    {
        //////creating tmp table
        $this->load->dbforge();
        if($this->db->table_exists('tmp'))
        {
            $this->dbforge->drop_table('tmp');
        }
        $fields = array(
                    'one' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                             
                                ),
                    'two' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                ),
                    'three' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'four' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'five' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'six' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'seven' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'eight' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'nine' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    ),
                    'ten' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '50',
                                    )

            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmp');

        $this->db->distinct();// cidetail.despatchDetailRowId, ci.vType as ciVType, ci.vNo as ciVNo,
        $this->db->select('despatchdetail.*,colours.colourName, qpo.vType, qpo.vNo, qpo.vDt, ordertypes.orderType, productcategories.productCategoryRowId, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('despatchdetail.despatchRowId', $this->input->post('rowid'));
        $this->db->from('despatchdetail');
        // $this->db->join('cidetail','cidetail.despatchDetailRowId = despatchdetail.rowId', 'LEFT');
        // $this->db->join('ci','ci.ciRowId = cidetail.ciRowId AND vType="C"', 'LEFT');
        $this->db->join('qpo','qpo.qpoRowId = despatchdetail.qpoRowId');
        $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->join('ordertypes','ordertypes.ordertypeRowId = qpo.ordertypeRowId');
        $this->db->join('products','products.productRowId = despatchdetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            $challanNo = '-';
            $this->db->select('ci.vType, ci.vNo');
            $this->db->where('cidetail.despatchDetailRowId', $row->rowId);
            $this->db->where('ci.vType', 'C');
            $this->db->from('ci');
            $this->db->join('cidetail','cidetail.ciRowId = ci.ciRowId');
            $query1 = $this->db->get();
            if ($query1->num_rows() > 0)
            {
                $row1 = $query1->row_array();
                $challanNo = $row1['vType'].'-'.$row1['vNo'];
            }

            $invNo = '-';
            $this->db->select('ci.vType, ci.vNo');
            $this->db->where('cidetail.despatchDetailRowId', $row->rowId);
            $this->db->where('ci.vType', 'I');
            $this->db->from('ci');
            $this->db->join('cidetail','cidetail.ciRowId = ci.ciRowId');
            $query1 = $this->db->get();
            if ($query1->num_rows() > 0)
            {
                $row1 = $query1->row_array();
                $invNo = $row1['vType'].'-'.$row1['vNo'];
            }

            $data = array(
                'one' => $row->vType.'-'.$row->vNo,
                'two' => $row->vDt,
                'three' => $row->orderType,
                'four' => $row->productCategory,
                'five' => $row->productName." (".$row->productLength."x".$row->productWidth."x".$row->productHeight.")".$row->uom,
                'six' => $row->colourName,
                'seven' => $row->despatchQty,
                'eight' => $challanNo,
                'nine' => $invNo,
                'ten' => $row->despatchRowId
            );
            $this->db->insert('tmp', $data);
        }      
        $this->db->select('*');
        // $this->db->order_by('productname');
        $query = $this->db->get('tmp');
        return($query->result_array());

    }

    //  public function getInvNo()
    // {
    //     // $this->db->distinct();// inv.vType as ciVTypeInv, inv.vNo as ciVNoInv    
    //     $this->db->select('despatchdetail.*,cidetail.despatchDetailRowId, ci.vType as ciVType, ci.vNo as ciVNo');
    //     $this->db->where('despatchdetail.despatchRowId', $this->input->post('rowid'));
    //     $this->db->from('despatchdetail');
    //     $this->db->join('cidetail','cidetail.despatchDetailRowId = despatchdetail.rowId', 'LEFT');
    //     $this->db->join('ci','ci.ciRowId = cidetail.ciRowId AND vType="c"');

    //     $query = $this->db->get();
    //     return($query->result_array());

    // }
}