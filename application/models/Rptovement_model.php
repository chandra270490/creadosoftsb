<?php
class Rptovement_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmployee4CheckBox()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        // $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
         $this->db->select('movestage.*, addressbook.name, productName, stages.stageName fromStageName, stageTo.stageName toStageName');
         $this->db->from('movestage');
         $this->db->join('employees','employees.empRowId = movestage.empRowId');
         $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
         $this->db->join('products','products.productRowId = movestage.productRowId');
         $this->db->join('stages','stages.stageRowId = movestage.fromStage');
         $this->db->join('stages stageTo','stageTo.stageRowId = movestage.toStage');
         $empRowId = explode(",", $this->input->post('empRowId'));
        $this->db->where_in('movestage.empRowId', $empRowId );
         // $this->db->where('movestage.empRowId', $this->input->post('empRowId'));
         $this->db->where('movestage.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
         $this->db->where('movestage.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
         // $this->db->where('qpo.orgRowId', $this->session->orgRowId);
         $this->db->order_by('movestage.rowId');
         $query = $this->db->get();
         return($query->result_array());
    }

}