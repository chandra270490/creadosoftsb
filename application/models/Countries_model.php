<?php
class Countries_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('countryRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('countries');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('countryRowId');
		$query = $this->db->get('countries');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('countryName');
		$this->db->where('countryName', $this->input->post('countryName'));
		$query = $this->db->get('countries');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('countryRowId');
		$query = $this->db->get('countries');
        $row = $query->row_array();

        $current_row = $row['countryRowId']+1;

		$data = array(
	        'countryRowId' => $current_row
	        , 'countryName' => ucwords($this->input->post('countryName'))
	        , 'isd' => $this->input->post('isd')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('countries', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('countryName');
		$this->db->where('countryName', $this->input->post('countryName'));
		$this->db->where('countryRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('countries');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'countryName' => ucwords($this->input->post('countryName'))
	        , 'isd' => $this->input->post('isd')
		);
		$this->db->where('countryRowId', $this->input->post('globalrowid'));
		$this->db->update('countries', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('countryRowId', $this->input->post('rowId'));
		$this->db->delete('countries');
	}

	public function getCountryList()
	{
		$this->db->select('countryRowId, countryName');
		$this->db->where('deleted', 'N');
		$this->db->order_by('countryName');
		$query = $this->db->get('countries');
		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['countryRowId']]= $row['countryName'];
		}
		return $arr;
	}
}