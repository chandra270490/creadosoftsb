<?php
class Moveingodown_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getProductList()
    {
        $this->db->select('productRowId, productName, productcategories.productCategory');
        $this->db->where('products.deleted', 'N');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('productName');
        $query = $this->db->get('products');

        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productRowId']] = $row['productName'] . '  ---  ['. $row['productCategory'] . ' ]';
        }

        return $arr;
    }

  

    public function getStagesFrom()
    {
        $this->db->select('addressbook.name, addressbook.abRowId');
        $this->db->from('addressbook');
        $this->db->where('addressbook.deleted', 'N');
        $this->db->where('addressbook.godown', 'Y');
        // $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();

        $arr = array();
        $arr["-1"] = '--- Select ---';
        $arr["-2"] = 'Default Stage';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['abRowId']] = $row['name'];
        }

        return $arr;
        // return($query->result_array());
    }

    

    public function getCurrentQty()
    {
        if($this->input->post('stage') == "-2") ///Desp stage se
        {
            $this->db->select_sum('avlQty');
            $this->db->where('productRowId',  $this->input->post('productRowId'));
            $this->db->where('colourRowId',  $this->input->post('colourRowId'));
            $this->db->where('openingbal.stageRowId',  26);
            $this->db->from('openingbal');
            // $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
            $query = $this->db->get();

            return($query->result_array());
        }
        else //// Godown se
        {
            $this->db->select_sum('avlQty');
            $this->db->where('productRowId',  $this->input->post('productRowId'));
            $this->db->where('colourRowId',  $this->input->post('colourRowId'));
            $this->db->where('godownstatus.godownAbRowId',  $this->input->post('stage'));
            $this->db->from('godownstatus');
            // $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
            $query = $this->db->get();

            return($query->result_array());
        }
    }

    

    public function getPendingOrders()
    {
        $this->db->select_sum('pendingQty');
        $this->db->where('productRowId',  $this->input->post('productRowId'));
        $this->db->where('qpo.orgRowId', $this->session->orgRowId);
        $this->db->join('qpo',"qpo.qpoRowId = qpodetail.qpoRowId AND qpo.deleted='N'");
        $this->db->from('qpodetail');

        // $this->db->where('productRowId',  $this->input->post('productRowId'));
        // $this->db->order_by('odr');
        // $this->db->from('qpodetail');
        // $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
        $query = $this->db->get();

        return($query->result_array());
    }

    public function moveData()
    {
        set_time_limit(0);
        $this->db->trans_start();

        $this->db->select_max('rowId');
        $query = $this->db->get('movegodown');
        $row = $query->row_array();

        $current_row = $row['rowId']+1;

        if($this->input->post('dt') == '')
        {
            $dt = null;
        }
        else
        {
            $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        }
        $data = array(
            'rowId' => $current_row
            , 'productRowId' => $this->input->post('productRowId')
            , 'dt' => $dt
            , 'colourRowId' => $this->input->post('colourRowId')
            , 'fromStage' => $this->input->post('fromStage')
            , 'toStage' => $this->input->post('toStage')
            , 'qty' => $this->input->post('qty')
            , 'remarks' => $this->input->post('remarks')
            , 'orgRowId' => $this->session->orgRowId
            , 'createdBy' => $this->session->userRowId
            
        );
        $this->db->set('createdStamp', 'NOW()', FALSE);
        $this->db->insert('movegodown', $data); 



        $this->db->trans_complete();
    }

    public function getDataLimit()
    {
        $this->db->select('movegodown.*, products.productName, colours.colourName, addressbook.name as fromStageName, abTo.name as toStageName');
        $this->db->join('products','products.productRowId = movegodown.productRowId');
        $this->db->join('colours','colours.colourRowId = movegodown.colourRowId');
        $this->db->join('addressbook','addressbook.abRowId = movegodown.fromStage');
        $this->db->join('addressbook as abTo','abTo.abRowId = movegodown.toStage');
        $this->db->where('movegodown.deleted', 'N');
        $this->db->order_by('rowId desc');
        $this->db->limit(10);
        $query = $this->db->get('movegodown');
        return($query->result_array());
    }

    public function getDataAll()
    {
        $this->db->select('movegodown.*, products.productName, colours.colourName, addressbook.name as fromStageName, abTo.name as toStageName');
        $this->db->join('products','products.productRowId = movegodown.productRowId');
        $this->db->join('colours','colours.colourRowId = movegodown.colourRowId');
        $this->db->join('addressbook','addressbook.abRowId = movegodown.fromStage');
        $this->db->join('addressbook as abTo','abTo.abRowId = movegodown.toStage');
        $this->db->where('movegodown.deleted', 'N');
        $this->db->order_by('rowId');
        // $this->db->limit(10);
        $query = $this->db->get('movegodown');
        return($query->result_array());
    }

}