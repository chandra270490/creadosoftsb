<?php
class Dsr_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

   
	public function getDataLimit()
	{
		if( $this->session->userRowId == 1 )	/// for admin
		{
			$this->db->select('dsr.*, addressbook.name, meetingremarks.meetingRemark, users.uid, dsr_lead_stage_mst.dsr_ls_name');
			$this->db->from('dsr');
			// $this->db->join('parties','parties.partyRowId = dsr.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = dsr.abRowId', "LEFT");
			$this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId', "LEFT");
			$this->db->join('users', 'users.rowid = dsr.userRowId');
			$this->db->join('dsr_lead_stage_mst', 'dsr_lead_stage_mst.dsr_ls_id = dsr.cboLeadStage', "LEFT");
			$this->db->where('dsr.deleted', 'N');
			$this->db->where('dsr.orgRowId', $this->session->orgRowId);
			$this->db->order_by('dsr.dsrRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else //// for other users
		{
			$this->db->select('dsr.*, addressbook.name, meetingremarks.meetingRemark, users.uid, dsr_lead_stage_mst.dsr_ls_name');
			$this->db->from('dsr');
			// $this->db->join('parties','parties.partyRowId = dsr.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = dsr.abRowId', "LEFT");
			$this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId', "LEFT");
			$this->db->join('users', 'users.rowid = dsr.userRowId');
			$this->db->join('dsr_lead_stage_mst', 'dsr_lead_stage_mst.dsr_ls_id = dsr.cboLeadStage', "LEFT");
			$this->db->where('dsr.deleted', 'N');
			$this->db->where('dsr.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('dsr.createdBy', $abAccessIn);
			$this->db->order_by('dsr.dsrRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}
	

    public function getDataAll()
	{
		if( $this->session->userRowId == 1 )
		{
			$this->db->select('dsr.*, addressbook.name, meetingremarks.meetingRemark, users.uid, dsr_lead_stage_mst.dsr_ls_name');
			$this->db->from('dsr');
			// $this->db->join('parties','parties.partyRowId = dsr.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = dsr.abRowId', "LEFT");
			$this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId', "LEFT");
			$this->db->join('users', 'users.rowid = dsr.userRowId');
			$this->db->join('dsr_lead_stage_mst', 'dsr_lead_stage_mst.dsr_ls_id = dsr.cboLeadStage', "LEFT");
			$this->db->where('dsr.deleted', 'N');
			$this->db->where('dsr.orgRowId', $this->session->orgRowId);
			$this->db->order_by('dsr.dsrRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else ////if( $this->session->userRowId == "L" )
		{
			$this->db->select('dsr.*, addressbook.name, meetingremarks.meetingRemark, users.uid, dsr_lead_stage_mst.dsr_ls_name');
			$this->db->from('dsr');
			// $this->db->join('parties','parties.partyRowId = dsr.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = dsr.abRowId', "LEFT");
			$this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId', "LEFT");
			$this->db->join('users', 'users.rowid = dsr.userRowId');
			$this->db->join('dsr_lead_stage_mst', 'dsr_lead_stage_mst.dsr_ls_id = dsr.cboLeadStage', "LEFT");
			$this->db->where('dsr.deleted', 'N');
			$this->db->where('dsr.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('dsr.createdBy', $abAccessIn);
			$this->db->order_by('dsr.dsrRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    public function getClosingRemarks()
	{
		$this->db->select('meetingremarks.mrRowId, meetingremarks.meetingRemark');
		$this->db->from('meetingremarks');
		$this->db->where('meetingremarks.deleted', 'N');
		$this->db->order_by('meetingremarks.meetingRemark');
		$query = $this->db->get();

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['mrRowId']]= $row['meetingRemark'];
		}
		return $arr;
	}

	public function getCboLeadStage()
	{
		$this->db->select('dsr_lead_stage_mst.dsr_ls_id, dsr_lead_stage_mst.dsr_ls_name');
		$this->db->from('dsr_lead_stage_mst');
		$this->db->order_by('dsr_lead_stage_mst.dsr_ls_id');
		$query = $this->db->get();

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['dsr_ls_id']]= $row['dsr_ls_name'];
		}
		return $arr;
	}

	public function checkDuplicate()
    {
  //   	$dsrDt = date('Y-m-d', strtotime($this->input->post('dsrDt')));
		// $this->db->select('*');
		// $this->db->where('dsrDt', $dsrDt);
		// $this->db->where('userRowId', $this->session->userRowId );
		// $query = $this->db->get('dsr');

		// if ($query->num_rows() > 0)
		// {
		// 	return 1;
		// }
    }

	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$this->db->select_max('dsrRowId');
		$query = $this->db->get('dsr');
        $row = $query->row_array();

        $current_row = $row['dsrRowId']+1;
        $dsrDt = date('Y-m-d', strtotime($this->input->post('dsrDt')));
        if($this->input->post('nextDt') == '')
        {
        	$nextDt = null;
        }
        else
        {
        	$nextDt = date('Y-m-d', strtotime($this->input->post('nextDt')));
        }

        if( $this->input->post('task') == "No-Meeting" || $this->input->post('task') == "Holiday" )
        {
        	$data = array(
			        'dsrRowId' => $current_row
			        , 'orgRowId' => $this->session->orgRowId
			        , 'dsrDt' => $dsrDt
			        , 'userRowId' => $this->session->userRowId
			        , 'task' => $this->input->post('task')
			        , 'remarks' => $this->input->post('remarks')
					, 'mrRowId' => $this->input->post('mrRowId')
					, 'cboLeadStage' => $this->input->post('cboLeadStage')
			        , 'createdBy' => $this->session->userRowId
				);
        }
        else
        {
	        if( $this->input->post('further') == "Continue" )
	        {
				$data = array(
			        'dsrRowId' => $current_row
			        , 'orgRowId' => $this->session->orgRowId
			        , 'dsrDt' => $dsrDt
			        , 'userRowId' => $this->session->userRowId
			        , 'task' => $this->input->post('task')
			        , 'type' => $this->input->post('type')
			        , 'abRowId' => $this->input->post('abRowId')
			        , 'partyType' => $this->input->post('partyType')
			        , 'remarks' => $this->input->post('remarks')
			        , 'further' => $this->input->post('further')
					, 'mrRowId' => $this->input->post('mrRowId')
					, 'cboLeadStage' => $this->input->post('cboLeadStage')
			        , 'nextDt' => $nextDt
			        , 'nextTm' => $this->input->post('nextTm')
					, 'createdBy' => $this->session->userRowId
				);
			}
			else if( $this->input->post('further') == "Close" )
	        {
				$data = array(
			        'dsrRowId' => $current_row
			        , 'orgRowId' => $this->session->orgRowId
			        , 'dsrDt' => $dsrDt
			        , 'userRowId' => $this->session->userRowId
			        , 'task' => $this->input->post('task')
			        , 'type' => $this->input->post('type')
			        , 'abRowId' => $this->input->post('abRowId')
			        , 'partyType' => $this->input->post('partyType')
			        , 'remarks' => $this->input->post('remarks')
			        , 'further' => $this->input->post('further')
					, 'mrRowId' => $this->input->post('mrRowId')
					, 'cboLeadStage' => $this->input->post('cboLeadStage')
			        , 'createdBy' => $this->session->userRowId
				);
			}
		}
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('dsr', $data);	

		///////////// Marking Attendance
		$this->db->where('userRowId', $this->session->userRowId);
		$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->where('dt', $dsrDt);
		$this->db->delete('attendanceexecutives');

		$this->db->select_max('aeRowId');
		$query = $this->db->get('attendanceexecutives');
        $row = $query->row_array();
        $aeRowId = $row['aeRowId']+1;

        $att = "AB";
        if( $this->input->post('task') == "No-Meeting")
        {
        	$att = "PR";
        }
        else if( $this->input->post('task') == "Holiday")
        {
        	$att = "HO";
        }
        else if( $this->input->post('task') == "Meeting")
        {
        	$att = "PR";
        }
        else if( $this->input->post('task') == "Call")
        {
        	$att = "PR";
        }

        if( $this->input->post('task') == "Action" )
        {

        }
        else
        {
	        $data = array(
			        'aeRowId' => $aeRowId
			        , 'refRowId' => $current_row
			        , 'orgRowId' => $this->session->orgRowId
			        , 'dt' => $dsrDt
			        , 'userRowId' => $this->session->userRowId
			        , 'attendance' => $att
			        , 'createdBy' => $this->session->userRowId
				);
	        $this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('attendanceexecutives', $data);
		}
		///////////// END - Marking Attendance


		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}

	public function checkDuplicateOnUpdate()
    {
  //   	$dsrDt = date('Y-m-d', strtotime($this->input->post('dsrDt')));
		// $this->db->select('*');
		// $this->db->where('dsrDt', $dsrDt);
		// $this->db->where('userRowId', $this->session->userRowId );
		// $this->db->where('dsrRowId !=', $this->input->post('globalrowid'));
		// $query = $this->db->get('dsr');

		// if ($query->num_rows() > 0)
		// {
		// 	return 1;
		// }
    }

	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$dsrDt = date('Y-m-d', strtotime($this->input->post('dsrDt')));
		
		if($this->input->post('nextDt') == '')
        {
        	$nextDt = null;
        }
        else
        {
        	$nextDt = date('Y-m-d', strtotime($this->input->post('nextDt')));
        }

        if( $this->input->post('task') == "No-Meeting"  || $this->input->post('task') == "Holiday" )
        {
        	$data = array(
					 'orgRowId' => $this->session->orgRowId
			        , 'dsrDt' => $dsrDt
			        // , 'userRowId' => $this->session->userRowId
			        , 'task' => $this->input->post('task')
			        , 'remarks' => $this->input->post('remarks')
					, 'mrRowId' => $this->input->post('mrRowId')
					, 'cboLeadStage' => $this->input->post('cboLeadStage')
			        , 'createdBy' => $this->session->userRowId
				);
        }
        else
        {
	        if( $this->input->post('further') == "Continue" )
	        {
				$data = array(
			        'orgRowId' => $this->session->orgRowId
			        , 'dsrDt' => $dsrDt
			        // , 'userRowId' => $this->session->userRowId
			        , 'task' => $this->input->post('task')
			        , 'type' => $this->input->post('type')
			        , 'abRowId' => $this->input->post('abRowId')
			        , 'partyType' => $this->input->post('partyType')
			        , 'remarks' => $this->input->post('remarks')
			        , 'further' => $this->input->post('further')
			        , 'mrRowId' => $this->input->post('mrRowId')
			        , 'nextDt' => $nextDt
					, 'nextTm' => $this->input->post('nextTm')
					, 'cboLeadStage' => $this->input->post('cboLeadStage')
				);
			}
			else if( $this->input->post('further') == "Close" )
	        {
				$data = array(
			        'orgRowId' => $this->session->orgRowId
			        , 'dsrDt' => $dsrDt
			        // , 'userRowId' => $this->session->userRowId
			        , 'task' => $this->input->post('task')
			        , 'type' => $this->input->post('type')
			        , 'abRowId' => $this->input->post('abRowId')
			        , 'partyType' => $this->input->post('partyType')
			        , 'remarks' => $this->input->post('remarks')
			        , 'further' => $this->input->post('further')
					, 'mrRowId' => $this->input->post('mrRowId')
					, 'cboLeadStage' => $this->input->post('cboLeadStage')
				);
			}
		}
		$this->db->where('dsrRowId', $this->input->post('globalrowid'));
		$this->db->update('dsr', $data);	


		///////////// Marking Attendance
		// $this->db->select_max('aeRowId');
		// $query = $this->db->get('attendanceexecutives');
  //       $row = $query->row_array();
  //       $aeRowId = $row['aeRowId']+1;

        $att = "AB";
        if( $this->input->post('task') == "No-Meeting")
        {
        	$att = "PR";
        }
        else if( $this->input->post('task') == "Holiday")
        {
        	$att = "HO";
        }
        else if( $this->input->post('task') == "Meeting")
        {
        	$att = "PR";
        }
        else if( $this->input->post('task') == "Call")
        {
        	$att = "PR";
        }

        $data = array(
			        'attendance' => $att
				);
        $this->db->where('refRowId', $this->input->post('globalrowid'));
		$this->db->update('attendanceexecutives', $data);
		///////////// END - Marking Attendance


		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }	
	}

	public function delete()
	{
		$data = array(
		        'deleted' => 'Y',
		        'deletedBy' => $this->session->userRowId

		);
		$this->db->set('deletedStamp', 'NOW()', FALSE);
		$this->db->where('dsrRowId', $this->input->post('rowId'));
		$this->db->update('dsr', $data);

	}


	public function dsr_srm_entry(){
		$mrRowId = $this->input->post("dsr_sr_id");
		$mrRowId1 = $this->input->post("dsr_sr_id");
		$meetingRemark = $this->input->post("dsr_short_rmks");
		$createdBy  = $this->session->userRowId;
		$createdStamp  = date('Y-m-d H:i:s');

		if($mrRowId1 == ""){
			$sql_ins = $this->db->query("insert into meetingremarks(meetingRemark, createdBy, createdStamp) 
			values('".$meetingRemark."', '".$createdBy."', '".$createdStamp."')");
		} else {
			$sql_updt = $this->db->query("update meetingremarks 
			set meetingRemark = '".$meetingRemark."'
			where mrRowId = '".$mrRowId."'");
		}
	}

	public function dsr_lead_stage_entry(){
		$dsr_ls_id = $this->input->post("dsr_ls_id");
		$dsr_ls_id1 = $this->input->post("dsr_ls_id");
		$dsr_ls_name = $this->input->post("dsr_ls_name");
		$dsr_ls_created_by  = $this->session->userRowId;

		if($dsr_ls_id1 == ""){
			$sql_ins = $this->db->query("insert into dsr_lead_stage_mst(dsr_ls_name, dsr_ls_created_by) 
			values('".$dsr_ls_name."', '".$dsr_ls_created_by."')");
		} else {
			$sql_updt = $this->db->query("update dsr_lead_stage_mst 
			set dsr_ls_name = '".$dsr_ls_name."', dsr_ls_created_by = '".$dsr_ls_created_by."' 
			where dsr_ls_id = '".$dsr_ls_id."'");
		}
	}


}