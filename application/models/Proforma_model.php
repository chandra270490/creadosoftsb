<?php
class Proforma_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

 //    public function getPartyList()
	// {
	// 	$this->db->select('addressbook.*, towns.townName, districts.districtName, states.stateName, countries.countryName, parties.partyRowId');
	// 	$this->db->from('addressbook');
	// 	$this->db->join('parties','parties.abRowId = addressbook.abRowId');
	// 	$this->db->join('towns','towns.townRowId = addressbook.townRowId');
	// 	$this->db->join('districts','districts.districtRowId = towns.districtRowId');
	// 	$this->db->join('states','states.stateRowId = districts.stateRowId');
	// 	$this->db->join('countries','countries.countryRowId = states.countryRowId');
	// 	$this->db->where('parties.deleted', 'N');
	// 	$this->db->where('parties.orgRowId', $this->session->orgRowId);		
	// 	$this->db->order_by('addressbook.name');
	// 	$query = $this->db->get();

	// 	return($query->result_array());
	// }

	public function getProductList()
    {
    	set_time_limit(0);
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

 
	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$this->db->select_max('qpoRowId');
		$query = $this->db->get('qpo');
        $row = $query->row_array();

        $current_row = $row['qpoRowId']+1;
		$this->globalInvNo = $current_row;
		
        $this->db->select_max('vNo');
        $this->db->where('vType', 'P');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('qpo');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        if($this->input->post('vDt') == '')
        {
        	$vDt = null;
        }
        else
        {
        	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));
        }

		$data = array(
	        'qpoRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => 'P'
	        , 'vNo' => $vNo 
	        , 'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'letterNo' => $this->input->post('letterNo')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	        , 'vatPer' => (float)$this->input->post('vatPer')
	        , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
	        , 'net' => (float)$this->input->post('net')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('qpo', $data);	


		/////Saving Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('rowId');
			$query = $this->db->get('qpodetail');
	        $row = $query->row_array();
	        $rowId = $row['rowId']+1;

			$data = array(
			        'rowId' => $rowId
			        , 'qpoRowId' => $current_row
			        , 'productRowId' => $TableData[$i]['productRowId']
			        , 'rate' => (float) $TableData[$i]['productRate']
			        , 'qty' => (float) $TableData[$i]['productQty']
			        , 'amt' => (float) $TableData[$i]['productAmt']
			        , 'placementRowId' => $TableData[$i]['placementRowId']
			        , 'colourRowId' => $TableData[$i]['colourRowId']
			);
			$this->db->insert('qpodetail', $data);	  
        }
        /////END - Saving Products

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}


	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

    	if($this->input->post('vDt') == '')
        {
        	$vDt = null;
        }
        else
        {
        	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));
        }

		$data = array(
	        'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'letterNo' => $this->input->post('letterNo')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	        , 'vatPer' => (float)$this->input->post('vatPer')
	        , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
	        , 'net' => (float)$this->input->post('net')
		);
		$this->db->where('qpoRowId', $this->input->post('globalrowid'));
		$this->db->update('qpo', $data);	

		/////Saving Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        $this->db->where('qpoRowId', $this->input->post('globalrowid'));
        $this->db->delete('qpodetail');

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('rowId');
			$query = $this->db->get('qpodetail');
	        $row = $query->row_array();
	        $rowId = $row['rowId']+1;

			$data = array(
			        'rowId' => $rowId
			        , 'qpoRowId' => $this->input->post('globalrowid')
			        , 'productRowId' => $TableData[$i]['productRowId']
			        , 'rate' => (float) $TableData[$i]['productRate']
			        , 'qty' => (float) $TableData[$i]['productQty']
			        , 'amt' => (float) $TableData[$i]['productAmt']
			        , 'placementRowId' => $TableData[$i]['placementRowId']
			        , 'colourRowId' => $TableData[$i]['colourRowId']
			);
			$this->db->insert('qpodetail', $data);	  
        }
        /////END - Saving Products


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}


	public function delete()
	{
		set_time_limit(0);
		$data = array(
		    'deleted' => 'Y'
		);
		$this->db->where('qpoRowId',  $this->input->post('rowId'));
		$this->db->update('qpo', $data);
		
		// $this->db->where('qpoRowId', $this->input->post('rowId'));
		// $this->db->delete('qpo');

		// $this->db->where('qpoRowId', $this->input->post('rowId'));
		// $this->db->delete('qpodetail');
	}


	public function getProducts()
    {
    	set_time_limit(0);
        $this->db->select('qpodetail.*, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight, placements.placement, colours.colourName');
        $this->db->where('qpodetail.qpoRowId', $this->input->post('rowid'));
        $this->db->from('qpodetail');
        $this->db->join('products','products.productRowId = qpodetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->join('placements','placements.placementRowId = qpodetail.placementRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getInvNo()
    {
        return $this->globalInvNo;
    }

	public function getImageName($rowId)
    {
    	set_time_limit(0);
        $this->db->select('products.productRowId, products.imagePathThumb');
		$this->db->from('products');
		$this->db->where('products.productRowId', $rowId);
		// $this->db->order_by('qpo.qpoRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function getQpo($rowId)
    {
    	set_time_limit(0);
        $this->db->select('qpo.*, addressbook.name, addressbook.addr, addressbook.pin, prefixtypes.prefixType, towns.townName');
		$this->db->from('qpo');
		$this->db->join('parties','parties.partyRowId = qpo.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('qpo.qpoRowId', $rowId);
		$this->db->order_by('qpo.qpoRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());

    }

    

}