<?php
class Rptesi_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }


    public function getDataForReport()
    {
         set_time_limit(0);

        $this->db->select('salarymonthly.*, addressbook.name as empName');
        $this->db->order_by('empName');
        $this->db->where('salarymonthly.dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('salarymonthly.dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('salarymonthly.orgRowId', $this->session->orgRowId);
        // $this->db->join('employees','employees.empRowId = salarymonthly.empRowId');
        $this->db->join('employees','employees.empRowId = salarymonthly.empRowId AND employees.deductEsi=\'Y\' ');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->from('salarymonthly');
        $query = $this->db->get();
        return($query->result_array());

        
    }

    public function getDataForReportExecutives()
    {
         set_time_limit(0);

        $this->db->select('salaryexecutives.*, addressbook.name as empName');
        $this->db->order_by('empName');
        $this->db->where('salaryexecutives.dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('salaryexecutives.dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('salaryexecutives.esi>', 0);
        $this->db->where('salaryexecutives.orgRowId', $this->session->orgRowId);
        $this->db->join('employees','employees.empRowId = salaryexecutives.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->from('salaryexecutives');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getEmployerContri()
    {
         set_time_limit(0);

        $this->db->select('baselimits.*');
        $this->db->from('baselimits');
        $query = $this->db->get();
        return($query->result_array());
    }

  
}