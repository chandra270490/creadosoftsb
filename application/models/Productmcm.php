<?php
class Productmcm extends CI_Model 
{
    function __construct(){   
		parent::__construct();  
	}

	function ListHead($tbl_nm){
		$query = $this->db->query("SHOW columns FROM $tbl_nm 
		where Field not in('orgRowId','deleted','createdBy','createdStamp','deletedBy','deletedStamp')");
		
		return $query;
	}

	//Id's Column
	public function get_by_id($tbl_nm, $id_col, $id){
		$query = $this->db->query("select * from $tbl_nm where $id_col = '".$id."'");
		return $query;
	}

	//Ticket Entry
	public function product_mc_entry($data){   
		$ProductMCId = $this->input->post("ProductMCId");
		$ProductMCId1 = $this->input->post("ProductMCId");
		$productMCName = $this->input->post("productMCName");
		$productCategory = $this->input->post("productCategory");
		//$orgRowId = $this->input->post("orgRowId");
		$orgRowId = 2;
		$productCategoryArr = implode(',', $productCategory);
		
		//echo $productCategoryArr; die;

		//User Details
		$createdBy = $this->session->userRowId;
		$createdStamp = date("Y-m-d h:i:s");
		
		//Transaction Start
		$this->db->trans_start();

		if($ProductMCId1 == ""){
			$sql = $this->db->query("insert into productmcc
			(productMCName, productCategory, orgRowId, createdBy, createdStamp) 
			values 
			('".$productMCName."', '".$productCategoryArr."', '".$orgRowId."', '".$createdBy."', '".$createdStamp."')");

		} else {

			$sql = $this->db->query("update productmcc 
			set productMCName = '".$productMCName."', productCategory = '".$productCategoryArr."', 
			orgRowId = '".$orgRowId."', createdBy = '".$createdBy."', createdStamp = '".$createdStamp."'
			where ProductMCId = '".$ProductMCId."'");
		}

		$this->db->trans_complete();
		//Transanction Complete

	}
}