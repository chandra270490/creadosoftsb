<?php
class Incometaxslabs_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataAll()
    {
      $this->db->select('*');
      $this->db->order_by('lowerRange');
      $query = $this->db->get('incometaxslabs');

      return($query->result_array());
    }


    public function insert()
    {
        $this->db->where("1","1");
        $this->db->delete('incometaxslabs');    

          $TableData = $this->input->post('TableData');
          $TableData = stripcslashes($TableData);
          $TableData = json_decode($TableData,TRUE);
          $myTableRows = count($TableData);

          for ($i=0; $i < $myTableRows; $i++) 
          {
              $this->db->select_max('itSlabRowId');
              $query = $this->db->get('incometaxslabs');
              $row = $query->row_array();
              $itSlabRowId = $row['itSlabRowId']+1;

              $data = array(
                      'itSlabRowId' => $itSlabRowId
                      , 'lowerRange' => (float) $TableData[$i]['lowerRange']
                      , 'upperRange' => (float) $TableData[$i]['upperRange']
                      , 'rate' => (float) $TableData[$i]['rate']
                      , 'calculatedFigure' => (float) $TableData[$i]['calculatedFigure']
                      , 'surcharge' => $this->input->post('surcharge')
                      , 'cess' => $this->input->post('cess')
                      , 'createdBy' => $this->session->userRowId
              );
              $this->db->set('createdStamp', 'NOW()', FALSE);
              $this->db->insert('incometaxslabs', $data);    
          }
          
    }
}