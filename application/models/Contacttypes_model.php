<?php
class Contacttypes_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('contactTypeRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('contacttypes');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('contactTypeRowId');
		$query = $this->db->get('contacttypes');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('contactType');
		$this->db->where('contactType', $this->input->post('contactType'));
		$query = $this->db->get('contacttypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('contactTypeRowId');
		$query = $this->db->get('contacttypes');
        $row = $query->row_array();

        $current_row = $row['contactTypeRowId']+1;

		$data = array(
	        'contactTypeRowId' => $current_row
	        , 'contactType' => ucwords($this->input->post('contactType'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('contacttypes', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('contactType');
		$this->db->where('contactType', $this->input->post('contactType'));
		$this->db->where('contactTypeRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('contacttypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'contactType' => ucwords($this->input->post('contactType'))
		);
		$this->db->where('contactTypeRowId', $this->input->post('globalrowid'));
		$this->db->update('contacttypes', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('contactTypeRowId', $this->input->post('rowId'));
		$this->db->delete('contacttypes');
	}

	public function getContactTypes()
	{
		$this->db->select('contactTypeRowId ,contactType');
		$this->db->where('deleted', 'N');
		$this->db->order_by('contactType');
		$query = $this->db->get('contacttypes');
		$arr = array();
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['contactTypeRowId']]= $row['contactType'];
		}

		return $arr;
	}
}