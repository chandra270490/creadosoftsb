<?php
class Util_model extends CI_Model 
{
    public function __construct()
    {
       $this->load->database('');
       $this->db->query("SET time_zone='+5:30'");
    }

    public function getUserRights()
    {
        $this->db->select('menuoption');
        $this->db->where('userrowid', $this->session->userRowId);
        $this->db->order_by('');
        $query = $this->db->get('userrights');
        return($query->result_array());
    }
    
    public function isSessionExpired()
    {
        $orgRowId = $this->session->orgRowId;
        $userRowId = $this->session->userRowId;

        // return $orgRowId;
        if ($orgRowId <= 0 || $userRowId <= 0)
        {
            return 1;
        }
    }

    public function getRight($uid,$right)
    {
        $query = $this->db->query("SELECT count(*) AS `rowcount` FROM userrights WHERE userrowid=".$uid." AND menuoption='".$right."'");

        return $query->row()->rowcount;
    }
	
    public function isDependent($tableName, $fieldName, $val)
    {
        $this->db->select($fieldName);
        $this->db->where($fieldName, $val);
        $query = $this->db->get($tableName);
        if ($query->num_rows() > 0)
        {
            return 1;
        }
    } 
      
    public function getVisitor()
    {
        $no=0;
        $this->db->select_max('cnt');
        $query = $this->db->get('visitors');
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            $no = $row->cnt+1;
        }
        $data = array(
                'cnt' => $no
        );
        $this->db->update('visitors', $data);               
        return $no;
    } 


    public function getOrg()
    {
        $this->db->select('organisations.*');
        $this->db->from('organisations');
        $this->db->where('organisations.orgRowId', $this->session->orgRowId);
        // $this->db->order_by('qpo.qpoRowId');
        // $this->db->limit(5);
        $query = $this->db->get();

        return($query->result_array());
    }

    public function getAbList()
    {
        if( $this->session->abAccess == "C" )
        {
            $this->db->select('addressbook.*, towns.townName, districts.districtName, states.stateName, countries.countryName');
            $this->db->from('addressbook');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            $this->db->join('states','states.stateRowId = districts.stateRowId');
            $this->db->join('countries','countries.countryRowId = states.countryRowId');
            $this->db->where('addressbook.deleted', 'N');
            // $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('addressbook.*, towns.townName, districts.districtName, states.stateName, countries.countryName');
            $this->db->from('addressbook');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            $this->db->join('states','states.stateRowId = districts.stateRowId');
            $this->db->join('countries','countries.countryRowId = states.countryRowId');
            $this->db->where('addressbook.deleted', 'N');
            // $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
            $abAccessIn = explode(",", $this->session->abAccessIn);
            // $this->db->where_in('addressbook.createdBy', $abAccessIn);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();

            return($query->result_array());
        }
    }

    public function getPartyList()
    {
        if( $this->session->abAccess == "C" )
        {
            $this->db->select('addressbook.*, towns.townName, districts.districtName, states.stateName, countries.countryName, parties.partyRowId');
            $this->db->from('addressbook');
            $this->db->join('parties','parties.abRowId = addressbook.abRowId');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            $this->db->join('states','states.stateRowId = districts.stateRowId');
            $this->db->join('countries','countries.countryRowId = states.countryRowId');
            $this->db->where('parties.deleted', 'N');
            // $this->db->where('parties.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('addressbook.*, towns.townName, districts.districtName, states.stateName, countries.countryName, parties.partyRowId');
            $this->db->from('addressbook');
            $this->db->join('parties','parties.abRowId = addressbook.abRowId');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            $this->db->join('states','states.stateRowId = districts.stateRowId');
            $this->db->join('countries','countries.countryRowId = states.countryRowId');
            $this->db->where('parties.deleted', 'N');
            // $this->db->where('parties.orgRowId', $this->session->orgRowId);
            $abAccessIn = explode(",", $this->session->abAccessIn);
            // $this->db->where_in('parties.createdBy', $abAccessIn);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();

            return($query->result_array());
        }
    }

    public function getDataLimitQpo($vType)
    {
        if( $this->session->abAccess == "C" )
        {
             $this->db->select('qpo.*, addressbook.name');
             $this->db->from('qpo');
             $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('qpo.vType', $vType);
             $this->db->where('qpo.deleted', 'N');
             $this->db->where('qpo.orgRowId', $this->session->orgRowId);
             $this->db->order_by('qpo.qpoRowId desc');
             $this->db->limit(5);
             $query = $this->db->get();

             return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
             $this->db->select('qpo.*, addressbook.name');
             $this->db->from('qpo');
             $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('qpo.vType', $vType);
             $this->db->where('qpo.deleted', 'N');
             $this->db->where('qpo.orgRowId', $this->session->orgRowId);
             // $abAccessIn = explode(",", $this->session->abAccessIn);
             // $this->db->where_in('qpo.createdBy', $abAccessIn);
             $this->db->where_in('qpo.createdBy', $this->session->userRowId);
             $this->db->order_by('qpo.qpoRowId desc');
             $this->db->limit(5);
             $query = $this->db->get();
            return($query->result_array());
        }
    }

    public function getDataAllQpo($vType)
    {
        if( $this->session->abAccess == "C" )
        {
             $this->db->select('qpo.*, addressbook.name');
             $this->db->from('qpo');
             $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('qpo.vType', $vType);
             $this->db->where('qpo.deleted', 'N');
             $this->db->where('qpo.orgRowId', $this->session->orgRowId);
             $this->db->order_by('qpo.qpoRowId');
             // $this->db->limit(5);
             $query = $this->db->get();

             return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
             $this->db->select('qpo.*, addressbook.name');
             $this->db->from('qpo');
             $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('qpo.vType', $vType);
             $this->db->where('qpo.deleted', 'N');
             $this->db->where('qpo.orgRowId', $this->session->orgRowId);
             // $abAccessIn = explode(",", $this->session->abAccessIn);
             // $this->db->where_in('qpo.createdBy', $abAccessIn);
             $this->db->where_in('qpo.createdBy', $this->session->userRowId);
             $this->db->order_by('qpo.qpoRowId');
             // $this->db->limit(5);
             $query = $this->db->get();
            return($query->result_array());
        }
    }

    public function getDataLimitCi($vType)
    {
        if( $this->session->abAccess == "C" )
        {
             $this->db->select('ci.*, addressbook.name');
             $this->db->from('ci');
             $this->db->join('parties','parties.partyRowId = ci.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('ci.vType', $vType);
             // $this->db->where('ci.deleted', 'N');
             $this->db->where('ci.orgRowId', $this->session->orgRowId);
             $this->db->order_by('ci.ciRowId desc');
             $this->db->limit(5);
             $query = $this->db->get();

             return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
             $this->db->select('ci.*, addressbook.name');
             $this->db->from('ci');
             $this->db->join('parties','parties.partyRowId = ci.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('ci.vType', $vType);
             // $this->db->where('ci.deleted', 'N');
             $this->db->where('ci.orgRowId', $this->session->orgRowId);
             // $abAccessIn = explode(",", $this->session->abAccessIn);
             // $this->db->where_in('ci.createdBy', $abAccessIn);
             $this->db->where_in('ci.createdBy', $this->session->userRowId);
             $this->db->order_by('ci.ciRowId desc');
             $this->db->limit(5);
             $query = $this->db->get();
            return($query->result_array());
        }
    }


    public function getDataAllCi($vType)
    {
        if( $this->session->abAccess == "C" )
        {
             $this->db->select('ci.*, addressbook.name');
             $this->db->from('ci');
             $this->db->join('parties','parties.partyRowId = ci.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('ci.vType', $vType);
             // $this->db->where('ci.deleted', 'N');
             $this->db->where('ci.orgRowId', $this->session->orgRowId);
             $this->db->order_by('ci.ciRowId');
             // $this->db->limit(5);
             $query = $this->db->get();

             return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
             $this->db->select('ci.*, addressbook.name');
             $this->db->from('ci');
             $this->db->join('parties','parties.partyRowId = ci.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->where('ci.vType', $vType);
             // $this->db->where('ci.deleted', 'N');
             $this->db->where('ci.orgRowId', $this->session->orgRowId);
             // $abAccessIn = explode(",", $this->session->abAccessIn);
             // $this->db->where_in('ci.createdBy', $abAccessIn);
             $this->db->where_in('ci.createdBy', $this->session->userRowId);
             $this->db->order_by('ci.ciRowId');
             // $this->db->limit(5);
             $query = $this->db->get();
            return($query->result_array());
        }
    }
}