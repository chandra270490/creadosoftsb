<?php
class Purchaseorder_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

	public function getProductList()
    {
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PO');
			$this->db->where('po.approved', 'N');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			$this->db->order_by('po.poRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PO');
			$this->db->where('po.approved', 'N');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('po.createdBy', $abAccessIn);
			$this->db->where_in('po.createdBy', $this->session->userRowId);
			$this->db->order_by('po.poRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PO');
			$this->db->where('po.approved', 'N');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			$this->db->order_by('po.poRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PO');
			$this->db->where('po.approved', 'N');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('po.createdBy', $abAccessIn);
			$this->db->where_in('po.createdBy', $this->session->userRowId);
			$this->db->order_by('po.poRowId');
			// $this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}


	public function getCurrentQty()
    {
    	$q = "Select sum(receive)-sum(Issue) as currentQty from ledgerp where productRowId=".$this->input->post('productRowId'). " AND orgRowId=".$this->session->orgRowId;
        $query = $this->db->query($q);
        return($query->result_array());
    }

	public function getLastPurchaseInfo()
    {
        // $this->db->select('ledgerp.*');
        // $this->db->from('ledgerp');
        // $this->db->where('productRowId', $this->input->post('productRowId'));
        // $this->db->where('vType', 'PI');
        // $this->db->order_by('vNo desc');
        // $query = $this->db->get();
        // return($query->result_array());

        $this->db->select('po.poRowId, po.vType, podetail.receivedQty as receive, podetail.billDt as dt');
        $this->db->from('podetail');
        $this->db->where('productRowId', $this->input->post('productRowId'));
        $this->db->where('vType', 'PI');
        $this->db->join('po','po.poRowId = podetail.poRowId');
        $this->db->order_by('vNo desc');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getHistoryData()
    {
        $this->db->select('po.*, podetail.*, addressbook.name');
        $this->db->from('podetail');
        $this->db->where('productRowId', $this->input->post('productRowId'));
        $this->db->where('vType', 'PI');
        $this->db->join('po','po.poRowId = podetail.poRowId');
        $this->db->join('parties','parties.partyRowId = po.partyRowId');
        $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
        $this->db->order_by('vNo desc');
        $query = $this->db->get();
        return($query->result_array());
    }
    
	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE po WRITE, podetail WRITE, notificationhierarchy WRITE,notifications WRITE');

		$this->db->select_max('poRowId');
		$query = $this->db->get('po');
        $row = $query->row_array();

        $current_row = $row['poRowId']+1;
		$this->globalInvNo = $current_row;
		
        $this->db->select_max('vNo');
        $this->db->where('vType', 'PO');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('po');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        if($this->input->post('vDt') == '')
        {
        	$vDt = null;
        }
        else
        {
        	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));
        }

		$data = array(
	        'poRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => 'PO'
	        , 'vNo' => $vNo 
	        , 'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'orderValidity' => $this->input->post('orderValidity')
	        , 'paymentTerms' => $this->input->post('paymentTerms')
	        , 'freight' => $this->input->post('freight')
	        , 'inspection' => $this->input->post('inspection')
	        , 'delayClause' => $this->input->post('delayClause')
	        , 'modeOfDespatch' => $this->input->post('modeOfDespatch')
	        , 'guarantee' => $this->input->post('guarantee')
	        , 'insurance' => $this->input->post('insurance')
	        , 'specialNote' => $this->input->post('specialNote')
	        , 'createdBy' => $this->session->userRowId
	        , 'purchaseOrExpense' => $this->input->post('task')
	        , 'PayType' => $this->input->post('PayType')
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('po', $data);	


		/////Saving Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('poDetailRowId');
			$query = $this->db->get('podetail');
	        $row = $query->row_array();
	        $poDetailRowId = $row['poDetailRowId']+1;

		    if($TableData[$i]['deliveryDt'] == '')
	        {
	        	$deliveryDt = null;
	        }
	        else
	        {
	        	$deliveryDt = date('Y-m-d', strtotime($TableData[$i]['deliveryDt']));
	        }

			$data = array(
			        'poDetailRowId' => $poDetailRowId
			        , 'poRowId' => $current_row
			        , 'productRowId' => $TableData[$i]['productRowId']
			        , 'rate' => (float) $TableData[$i]['productRate']
			        , 'orderQty' => (float) $TableData[$i]['productQty']
			        , 'pendingQty' => (float) $TableData[$i]['productQty']
			        , 'amt' => (float) $TableData[$i]['productAmt']
			        , 'discount' => (float) $TableData[$i]['productDiscount']
			        , 'amtAfterDiscount' => (float) $TableData[$i]['amtAfterDiscount']
			        , 'colour' => $TableData[$i]['colour']
			        , 'deliveryDt' => $deliveryDt
			        , 'remarks' => $TableData[$i]['remarks']
			        , 'poVno' => $vNo
			        , 'againstPoDetailRowId' => $poDetailRowId
			        , 'lastPurchaseInfo' => $TableData[$i]['lastPurchaseInfo']
			);
			$this->db->insert('podetail', $data);	  
        }
        /////END - Saving Products


		////////////////////Notification
		$this->db->select('userRowIdMgr');
		$this->db->where('userRowId', $this->session->userRowId );
		$this->db->where('forModule', 'Purchase' );
		$queryNh = $this->db->get('notificationhierarchy');
		foreach ($queryNh->result() as $rowNh)
        {
        	$userRowIdMgr = $rowNh->userRowIdMgr;


			$this->db->select_max('notificationRowId');
			$queryN = $this->db->get('notifications');
	        $rowN = $queryN->row_array();

	        $notificationRowId = $rowN['notificationRowId']+1;
		    $data = array(
		        'notificationRowId' => $notificationRowId
		        , 'orgRowId' => $this->session->orgRowId
		        , 'vType' => 'PO'
		        , 'vNo' => $current_row
		        , 'notification' => $this->session->userid . " Punched a PO.(". $this->input->post('partyName') .")"
		        , 'userRowIdFor' => $userRowIdMgr
		        , 'createdBy' => $this->session->userRowId
			);
			$this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('notifications', $data);	
		}        

        $this->db->query('UNLOCK TABLES');

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}


	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE po WRITE, podetail WRITE');

    	if($this->input->post('vDt') == '')
        {
        	$vDt = null;
        }
        else
        {
        	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));
        }

		$data = array(
	        'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'orderValidity' => $this->input->post('orderValidity')
	        , 'paymentTerms' => $this->input->post('paymentTerms')
	        , 'freight' => $this->input->post('freight')
	        , 'inspection' => $this->input->post('inspection')
	        , 'delayClause' => $this->input->post('delayClause')
	        , 'modeOfDespatch' => $this->input->post('modeOfDespatch')
	        , 'guarantee' => $this->input->post('guarantee')
	        , 'insurance' => $this->input->post('insurance')
	        , 'specialNote' => $this->input->post('specialNote')
		);
		$this->db->where('poRowId', $this->input->post('globalrowid'));
		$this->db->update('po', $data);	

		/////Saving Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
   //      	////////// Pending Qty Arrangment
   //      	$this->db->select('qty, pendingQty');
   //      	$this->db->where('po',  $this->input->post('globalrowid'));
			// $this->db->where('productRowId', $TableData[$i]['productRowId']);
			// $query = $this->db->get('podetail');
	  //       $row = $query->row_array();
	  //       $qty = $row['qty'];
	  //       $pendingQty = $row['pendingQty'];

	  //       $qtyDifference = $qty - $TableData[$i]['productQty'];
	  //       $newPendingQty = $pendingQty - $qtyDifference;
   //      	//////////END - Pending Qty Arrangment
        	if($TableData[$i]['deliveryDt'] == '')
	        {
	        	$deliveryDt = null;
	        }
	        else
	        {
	        	$deliveryDt = date('Y-m-d', strtotime($TableData[$i]['deliveryDt']));
	        }

			$data = array(
			        'rate' => (float) $TableData[$i]['productRate']
			        , 'orderQty' => (float) $TableData[$i]['productQty']
			        , 'pendingQty' => (float) $TableData[$i]['productQty']
			        , 'amt' => (float) $TableData[$i]['productAmt']
			        , 'discount' => (float) $TableData[$i]['productDiscount']
			        , 'amtAfterDiscount' => (float) $TableData[$i]['amtAfterDiscount']
			        , 'colour' => $TableData[$i]['colour']
			        , 'deliveryDt' => $deliveryDt
			        , 'remarks' => $TableData[$i]['remarks']
			);
			$this->db->where('poRowId',  $this->input->post('globalrowid'));
			$this->db->where('productRowId', $TableData[$i]['productRowId']);
			$this->db->update('podetail', $data);			
        }
        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}

	public function checkDependency()
	{
		// $this->db->select('podetail.rowId');
  //       $this->db->where('podetail.po', $this->input->post('rowId'));
  //       $this->db->from('podetail');
  //       $query = $this->db->get();
  //       $qpoDetailRowId='';
  //       foreach ($query->result() as $row)
		// {
		// 	$qpoDetailRowId = $qpoDetailRowId .$row->rowId.",";
		// }
		// $qpoDetailRowId = substr($qpoDetailRowId, 0, strlen($qpoDetailRowId)-1);
  //    	// return $qpoDetailRowId;

  //    	$this->db->select('despatchdetail.rowId');
  //       $this->db->where_in('despatchdetail.qpoDetailRowId', $qpoDetailRowId);
  //       // $this->db->where('cidetail.despatchDetailRowId', $despathDetailRowId);
  //       $this->db->from('despatchdetail');
  //       $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId AND despatch.deleted="N"');
  //       // $this->db->where('despatch.deleted','N');
  //       $query = $this->db->get();
  //       if ($query->num_rows() > 0)
		// {
		// 	return 1;
		// }
	}

	public function delete()
	{
		$data = array(
		    'deleted' => 'Y'
		);
		$this->db->where('poRowId',  $this->input->post('rowId'));
		$this->db->update('po', $data);
	}


	public function getProducts()
    {
        $this->db->select('podetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('podetail.poRowId', $this->input->post('rowid'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getInvNo()
    {
        return $this->globalInvNo;
    }

	public function getImageName($rowId)
    {
        $this->db->select('products.productRowId, products.imagePathThumb');
		$this->db->from('products');
		$this->db->where('products.productRowId', $rowId);
		// $this->db->order_by('po.po');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function getQpo($rowId)
    {
        $this->db->select('po.*, addressbook.name, addressbook.addr, addressbook.pin, prefixtypes.prefixType, towns.townName');
		$this->db->from('po');
		$this->db->join('parties','parties.partyRowId = po.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('po.po', $rowId);
		$this->db->order_by('po.po');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());

    }


    
}