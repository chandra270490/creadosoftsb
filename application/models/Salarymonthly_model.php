<?php
class Salarymonthly_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function checkDuplicate()
    {
        //////// checking if already calculated
        $this->db->select('salarymonthly.salary');
        $this->db->where('dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->from('salarymonthly');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return 1;
        }
        //////// END - checking if already calculated
    }

    public function PooreMahineKiAttendanceMarkHoGayi()
    {
        $days = date('d', strtotime($this->input->post('dtTo'))) - date('d', strtotime($this->input->post('dtFrom'))) + 1;
        $this->db->distinct();
        $this->db->select('attendance.dt');
        $this->db->from('attendance');
        $this->db->where('attendance.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('attendance.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            if( $query->num_rows() == $days )
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        //////// END - checking if already calculated
    }


    public function getDataForReport()
    {
         set_time_limit(0);
         ////// ESI base limit and emp contri
        $this->db->select('baselimits.*');
        $this->db->from('baselimits');
        $queryBaseLimits = $this->db->get();
        if ($queryBaseLimits->num_rows() > 0)
        {
            $rowBaseLimits = $queryBaseLimits->row(); 
            $esiBase = $rowBaseLimits->esiBase;
            $esiEmployee = $rowBaseLimits->esiEmployee;
        }
        ////// END - ESI base limit and emp contri

        //////creating tmpsalm table
        $this->load->dbforge();
        if($this->db->table_exists('tmpsalm'))
        {
            $this->dbforge->drop_table('tmpsalm');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'designationRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'mins' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'extraMins' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'basicSal' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'daysinmonth' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'salPerMin' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,5',
                                     'default' => '0',
                                ),
                    'salary' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'night' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'tour' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'attendance' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'nashta' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'absent' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'other' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'gross' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'esi' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'due' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'netSal' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'netPayble' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'discontinue' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'discontinueDt' => array(
                                     'type' => 'DATE',
                                     'default' => NULL,
                                )
            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmpsalm');

        $userRowId = "(employees.userRowId <= 0 OR employees.userRowId is NULL)";
        $this->db->select('employees.*, addressbook.name');
        $this->db->from('employees');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.salType', 'M' );
        $this->db->where('employees.discontinue', 'N');     ///this line added on 06 march 2019
        $this->db->where($userRowId);
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('name');
        $query = $this->db->get();
        
        //print_r($this->db->last_query()); die;

        $rowId = 1;
        foreach ($query->result() as $row)
        {
            $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'designationRowId' => $row->designationRowId,
                        'basicSal' => $row->basicSal,
                        'discontinue' => $row->discontinue,
                        'discontinueDt' => $row->discontinueDt,
                );
            $this->db->insert('tmpsalm', $data);               
            $rowId++;

            ///// calculatins MINUTES worked
            $this->db->select('tmIn, tmOut, extraHours, duty');
            $this->db->from('attendance');
            $this->db->where('attendance.empRowId', $row->empRowId );
            $this->db->where('attendance.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('attendance.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            // $this->db->where('attendance.orgRowId', $this->session->orgRowId);
            $queryMins = $this->db->get();
            $mins = 0;
            $extraMins = 0;
            //////////////////////// fetch Allownces desig wise
            $this->db->select('allowances.*');
            $this->db->from('allowances');
            $this->db->where('allowances.designationRowId', $row->designationRowId );
            $queryAllow = $this->db->get();
            if ($queryAllow->num_rows() > 0)
            {
               $rowAllow = $queryAllow->row(); 
                $nightAll = $rowAllow->nightAllow;
                $midNightAll = $rowAllow->midNightAllow;
                $tourAll = $rowAllow->tourAllow;
                $attendanceAll = $rowAllow->attendanceAllow;
                $breakfastAll = $rowAllow->breakfastAllow;
                $lunchAll = $rowAllow->lunchAllow;
            }
            else
            {
                $nightAll = 0;
                $midNightAll = 0;
                $tourAll = 0;
                $attendanceAll = 0;
                $breakfastAll = 0;
                $lunchAll = 0;
            }
            ///////////////////////
            $tourTot = 0;
            $attendanceTot = 0;
            $nightTot = 0;
            $midNightTot = 0;
            $absentTot = 0;
            $presentCount = 0;
            $breakfastTot = 0;
            $lunchTot = 0;
            $nashta = 0;
            foreach ($queryMins->result() as $rowMins)
        	{
                $duty = $rowMins->duty;
                if( $duty == "Present" ||  $duty == "Night Shift" ||  $duty == "Mid-Night" ||  $duty == "Tour" ||  $duty == "Holiday" )
                {
            		$tmIn = "2016-12-13 " . $rowMins->tmIn;
            		$tmOut = "2016-12-13 " . $rowMins->tmOut;
                    $extraHrs = $rowMins->extraHours;
                    $h = substr($extraHrs, 0, 2);
                    $m = substr($extraHrs, 3);
                    $extraMinsOfThatDay = ($h * 60) + $m;
                    $extraMins += ($h * 60) + $m;
            		$to_time = strtotime($tmIn);
    				$from_time = strtotime($tmOut);
    				$minDiff = round(abs($to_time - $from_time) / 60,2); ///mins worked in a day
    				$mins += $minDiff;

                    
                    if( $duty == "Night Shift" )
                    {
                        $nightTot += $nightAll;
                        $presentCount++;
                    }
                    else if( $duty == "Holiday" )
                    {
                        $presentCount++;
                        if ( $extraMinsOfThatDay > 0 && $extraMinsOfThatDay <= 240 )   ///less than 4hrs
                        {
                            $breakfastTot += $breakfastAll;
                            $nashta += $breakfastAll;
                        }
                        else if ( $extraMinsOfThatDay > 240 && $extraMinsOfThatDay <= 510 ) ///more than 4 and less than 8.30
                        {
                            $lunchTot += $lunchAll;
                            $nashta += $lunchAll;
                        }
                        
                    }
                    else if( $duty == "Mid-Night" )
                    {
                        $midNightTot += $midNightAll;
                        $presentCount++;
                    }
                    else if( $duty == "Tour" )
                    {
                        $tourTot += $tourAll;
                        $presentCount++;
                    }
                    else if( $duty == "Present" )
                    {
                        $presentCount++;
                    }
                }
                else if( $duty == "Absent" )
                {
                    $absentTot++;
                }
        	}

            $date1 = $this->input->post('dtFrom');
            $date2 = $this->input->post('dtTo');
            $dpm = (abs(strtotime($date2) - strtotime($date1))) / (60*60*24)  +  1;
            $salPerMin = number_format($row->basicSal / $dpm / 8.5 / 60, 5);
            $salary = ($mins + $extraMins) * $salPerMin;
            $salPerDay = number_format($row->basicSal / $dpm, 2);
            $absetDeduction = $absentTot * $salPerDay * 0.5;

            /////////// Attendance incentive
            // $attendanceTot = $queryMins->num_rows();
            if( $dpm == $presentCount )
            {
                $attendanceTot = $attendanceAll;
            }
            /////////// END - Attendance incentive

            //////  Other Incentive
            $this->db->select_sum('salaryinc.amt');
            $this->db->from('salaryinc');
            $this->db->where('salaryinc.empRowId', $row->empRowId );
            $this->db->where('salaryinc.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('salaryinc.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('salaryinc.orgRowId', $this->session->orgRowId);
            $queryOther = $this->db->get();
            if ($queryOther->num_rows() > 0)
            {
                $rowOther = $queryOther->row_array();
                $other = $rowOther['amt'];
            }
            else
            {
                $other = 0;
            }
             ////// END - Other Incentive

            $gross = $salary + $nightTot + $midNightTot + $tourTot + $attendanceTot + $nashta - $absetDeduction + $other;
            ////////////// Calc. Esi
            if( $row->deductEsi == 'Y' && $row->basicSal <= $esiBase )
            {
                $esi = number_format($gross * $esiEmployee /100 , 2);
            }
            else
            {
                $esi = 0;   
            }
            ////////////// END - Calc. Esi      

            ////// Storing already paid
            $this->db->select_sum('payments.amt');
            $this->db->from('payments');
            $this->db->where('payments.empRowId', $row->empRowId );
            // $this->db->where('payments.calculated', 'N' );
            $queryPaid = $this->db->get();
            if ($queryPaid->num_rows() > 0)
            {
                $rowPaid = $queryPaid->row_array();
                $paid = $rowPaid['amt'];
            }
            else
            {
                $paid = 0;
            }
             ////// END - Storing already paid


            //////  Prev. Dues
            $oldSalarySum = 0; 
            // $this->db->select_sum('salarymonthly.dueNow');
            $this->db->select_sum('salarymonthly.net');
            $this->db->from('salarymonthly');
            $this->db->where('salarymonthly.empRowId', $row->empRowId );
            // $this->db->where('salarymonthly.calculated', 'N' );
            $queryDue = $this->db->get();
            if ($queryDue->num_rows() > 0)
            {
                $rowDue = $queryDue->row_array();
                $oldSalarySum = $rowDue['net'];
            }
            $prevDue = $oldSalarySum - $paid;
             ////// END - Prev. Dues
            $netSal = $gross - $esi;
            $netPayble = $gross - $esi + $prevDue;

	        $data = array(
                    'mins' => $mins
                    , 'extraMins' => $extraMins
                    , 'daysinmonth' => $dpm
                    , 'salPerMin' => $salPerMin
                    , 'salary' => $salary
                    , 'night' => $nightTot + $midNightTot
                    , 'tour' => $tourTot
                    , 'attendance' => $attendanceTot
                    , 'nashta' => $nashta
                    , 'absent' => $absetDeduction
                    , 'other' => $other
                    , 'gross' => $gross
                    , 'esi' => $esi
                    , 'due' => $prevDue
                    , 'netSal' => $netSal
                    , 'netPayble' => $netPayble
                    );
                $this->db->where('empRowId', $row->empRowId);
                $this->db->update('tmpsalm', $data);
        }


        /// now sending data to client
        // $this->db->select_sum('tmpsalm.amt');
        $this->db->select('tmpsalm.*');
        $this->db->order_by('tmpsalm.empName');
        // $this->db->where('tmpsalm.discontinue', 'N');  //Correctin on 16 nov 17
        $this->db->where('tmpsalm.discontinueDt ', null);
        $this->db->or_where('tmpsalm.discontinueDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->from('tmpsalm');
        $query = $this->db->get();
        return($query->result_array());
    }

    
    public function saveData()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        // $this->db->query('LOCK TABLE qpo WRITE, qpodetail WRITE');

        


        $dtFrom = date('Y-m-d', strtotime($this->input->post('dtFrom')));
        $dtTo = date('Y-m-d', strtotime($this->input->post('dtTo')));
        $dt = date('Y-m-d', strtotime($this->input->post('dt')));

        /////Saving salry contract
        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
            /////updateing calculated Y in Payments
            $data = array(
                        'calculated' => 'Y'
                );
                $this->db->where('empRowId', $TableData[$i]['empRowId']);
                $this->db->update('payments', $data);    
            /////END - updateing calculated Y in Payments

            /////updateing calculated Y in SalaryMonthly
            $data = array(
                        'calculated' => 'Y'
                );
                $this->db->where('empRowId', $TableData[$i]['empRowId']);
                // $this->db->where('salarymonthly.dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                // $this->db->where('salarymonthly.dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                $this->db->update('salarymonthly', $data);    
            /////END - updateing calculated Y in SalaryContract


            /////////// Inserting Current Data
            $this->db->select_max('smRowId');
            $query = $this->db->get('salarymonthly');
            $row = $query->row_array();
            $smRowId = $row['smRowId'] + 1;

            $data = array(
                    'smRowId' => $smRowId
                    , 'dt' => $dt
                    , 'dtFrom' => $dtFrom
                    , 'dtTo' => $dtTo
                    , 'orgRowId' => $this->session->orgRowId
                    , 'empRowId' => $TableData[$i]['empRowId']
                    , 'salary' => (float) $TableData[$i]['salPerMonth']
                    , 'salaryCalculated' => (float) $TableData[$i]['salCalculated']
                    , 'nightAllow' => (float) $TableData[$i]['nightAllow']
                    , 'tourAllow' => (float) $TableData[$i]['tourAllow']
                    , 'attendanceAllow' => (float) $TableData[$i]['attendanceAllow']
                    , 'breakfastAllow' => (float) $TableData[$i]['breakfastAllow']
                    , 'absentDeduct' => (float) $TableData[$i]['absent']
                    , 'otherInc' => (float) $TableData[$i]['otherInc']
                    , 'gross' => (float) $TableData[$i]['gross']
                    , 'esi' => (float) $TableData[$i]['esi']
                    , 'previousDues' => (float) $TableData[$i]['prevDues']
                    , 'net' => (float) $TableData[$i]['net']
                    , 'netPayble' => (float) $TableData[$i]['netPayble']
                    , 'payingNow' => (float) $TableData[$i]['payingNow']
                    , 'dueNow' => (float) $TableData[$i]['dueNow']
                    , 'mins' => (float) $TableData[$i]['mins']
                    , 'extraMins' => (float) $TableData[$i]['extraMins']
                    , 'salPerMin' => (float) $TableData[$i]['salPerMin']
                    , 'createdBy' => $this->session->userRowId
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('salarymonthly', $data);    
        }
        /////END - salry contract



        // $this->db->query('UNLOCK TABLES');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}