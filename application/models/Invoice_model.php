<?php
class Invoice_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }


    public function getGstIn()
    {
        $this->db->select('addressbook.name, addressbook.gstIn, parties.partyRowId, prefixtypes.prefixType');
        $this->db->from('addressbook');
        $this->db->where('parties.partyRowId', $this->input->post('partyRowId'));
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
        $query = $this->db->get();

        return($query->result_array());     
    }
    
	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        

        $this->db->query('LOCK TABLE ci WRITE, cidetail WRITE, despatchdetail WRITE');

		$this->db->select_max('ciRowId');
		$query = $this->db->get('ci');
        $row = $query->row_array();

        $current_row = $row['ciRowId']+1;
		$this->globalInvNo = $current_row;
		
		$this->db->select_max('vNo');
        $this->db->where('vType', 'I');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('ci');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        $ciDt = date('Y-m-d', strtotime($this->input->post('ciDt')));

		$data = array(
	        'ciRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => 'I'
	        , 'vNo' => $vNo 
	        , 'ciDt' => $ciDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
            , 'vatPer' => (float)$this->input->post('vatPer')
            , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
            , 'otherExpName' => $this->input->post('otherExpName')
            , 'otherExpAmt' => (float)$this->input->post('otherExpAmt')
	        , 'roundAmt' => (float)$this->input->post('roundAmt')
	        , 'net' => (float)$this->input->post('net')
	        , 'cpe' => $this->input->post('cpe')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('ci', $data);	

		

		/////Saving ciDetail
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        // echo $myTableRows;
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('ciDetailRowId');
			$query = $this->db->get('cidetail');
	        $row = $query->row_array();
	        $ciDetailRowId = $row['ciDetailRowId']+1;

			$data = array(
			        'ciDetailRowId' => $ciDetailRowId
			        , 'ciRowId' => $current_row
			        , 'despatchDetailRowId' => $TableData[$i]['despatchDetailRowId']
			        , 'rate' => (float) $TableData[$i]['rate']
			        , 'qty' => (float) $TableData[$i]['qty']
                    , 'amt' => (float) $TableData[$i]['amt']
			        , 'letterNo' => $TableData[$i]['letterNo']
			);
			$this->db->insert('cidetail', $data);	  

            // sleep(6);

			////////////updating despatchDetail ChallanDone YES

			$data = array(
			        'invoiceDone' =>  'Y'
			);
			$this->db->where('rowId',  $TableData[$i]['despatchDetailRowId']);
			$this->db->update('despatchdetail', $data);
			////////////END - updating despatchDetail ChallanDone YES

        }
       /////END - ciDetail

        $this->db->query('UNLOCK TABLES');
        
		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return "error";
        }
        else
        {
            $this->db->trans_commit();
            return "noError";
        }
	}


	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

         $this->db->query('LOCK TABLE ci WRITE, cidetail WRITE, despatchdetail WRITE');

        $ciDt = date('Y-m-d', strtotime($this->input->post('ciDt')));

		$data = array(
	         'ciDt' => $ciDt
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	        , 'vatPer' => (float)$this->input->post('vatPer')
	        , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
            , 'otherExpName' => $this->input->post('otherExpName')
            , 'otherExpAmt' => (float)$this->input->post('otherExpAmt')
	        , 'roundAmt' => (float)$this->input->post('roundAmt')
	        , 'net' => (float)$this->input->post('net')
	        , 'cpe' => $this->input->post('cpe')
		);
		$this->db->where('ciRowId', $this->input->post('globalrowid'));
		$this->db->update('ci', $data);	

		
		/////Updating DespatchDetail
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
			$data = array(
			        'rate' => (float) $TableData[$i]['rate']
			        , 'amt' => (float) $TableData[$i]['amt']
			);
			$this->db->where('ciDetailRowId',  $TableData[$i]['ciDetailRowId']);
			$this->db->update('cidetail', $data);			

        }
        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}


	public function delete()
	{
        $data = array(
            'deleted' => 'Y'
        );
        $this->db->where('ciRowId',  $this->input->post('rowId'));
        $this->db->update('ci', $data);
        
        ////////////updating despatchDetail invoiceDone No
        $this->db->select('despatchDetailRowId');
        $this->db->where('ciRowId',  $this->input->post('rowId'));
        $query = $this->db->get('cidetail');
        $row = $query->result();
        foreach($row as $rec)
        {
            $data = array(
                    'invoiceDone' =>  'N'
            );
            $this->db->where('rowId',  $rec->despatchDetailRowId);
            $this->db->update('despatchdetail', $data);
        }
        ////////////END - updating despatchDetail ChallanDone YES

		// $this->db->where('qpoRowId', $this->input->post('rowId'));
		// $this->db->delete('qpo');

		// $this->db->where('qpoRowId', $this->input->post('rowId'));
		// $this->db->delete('qpodetail');
	}


	public function getProducts()
    {	//
        $this->db->select('despatchdetail.rowId as despatchDetailRowId, despatchdetail.despatchRowId, despatchdetail.productRowId, despatchdetail.despatchQty, qpodetail.rowId as qpoDetailRowId, qpodetail.qpoRowId, qpodetail.qty as odrQty, qpodetail.rate, qpodetail.amt, qpodetail.remarks, qpo.vNo as odrNo, qpo.deliveryAddress, qpo.vDt as odrDt, qpo.commitmentDate, qpo.discountPer, qpo.letterNo, productcategories.productCategoryRowId, productcategories.productCategory, products.productName, products.hsn, ordertypes.orderType, colours.colourRowId, colours.colourName');
        $this->db->where('despatch.partyRowId', $this->input->post('partyRowId'));
        // $this->db->where('qpo.vType', 'O');
        $this->db->where('despatchdetail.invoiceDone', 'N');
        $this->db->from('despatchdetail');
        $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId AND despatch.deleted="N"');
        $this->db->join('qpodetail','qpodetail.rowId = despatchdetail.qpoDetailRowId');
        $this->db->join('qpo','qpo.qpoRowId = qpodetail.qpoRowId');
        $this->db->join('ordertypes','ordertypes.orderTypeRowId = qpo.orderTypeRowId');
        $this->db->join('products','products.productRowId = qpodetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->order_by('despatchdetail.rowId');

        $query = $this->db->get();
        return($query->result_array());
    }


	public function getProductsFromCi()
    {	//
        $this->db->select('cidetail.*, despatchdetail.rowId as despatchDetailRowId, despatchdetail.productRowId, products.productName, products.hsn, qpo.qpoRowId, qpo.vDt, qpo.vNo as odrNo, qpo.deliveryAddress');
        $this->db->where('cidetail.ciRowId', $this->input->post('rowid'));
        $this->db->from('cidetail');
        $this->db->join('despatchdetail','despatchdetail.rowId = cidetail.despatchDetailRowId');
        $this->db->join('qpo','qpo.qpoRowId = despatchdetail.qpoRowId');
        $this->db->join('products','products.productRowId = despatchdetail.productRowId');
        // $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->join('colours','colours.colourRowId = despatchdetail.colourRowId');
        $this->db->order_by('ciDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getInvNo()
    {
        return $this->globalInvNo;
    }

    public function getCi($rowId)
    {
        $this->db->select('ci.*, addressbook.name, addressbook.addr, addressbook.pin, addressbook.tinCentre, prefixtypes.prefixType, towns.townName, addressbook.gstIn');
		$this->db->from('ci');
		$this->db->join('parties','parties.partyRowId = ci.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('ci.ciRowId', $rowId);
		$this->db->order_by('ci.ciRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function despInfo($despatchdetailRowId)
    {	//
        $this->db->select('despatchdetail.*, despatch.prGrNo, despatch.despatchDt, addressbook.name, ordertypes.orderType');
        $this->db->where('despatchdetail.rowId', $despatchdetailRowId);
        $this->db->from('despatchdetail');
        $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId');
        $this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
        $this->db->join('addressbook','addressbook.abRowId = transporters.abRowId');
        $this->db->join('qpo','qpo.qpoRowId = despatchdetail.qpoRowId');
        $this->db->join('ordertypes','ordertypes.orderTypeRowId = qpo.orderTypeRowId');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function challanInfo($despatchdetailRowId)
    {	//
        $despatchdetailRowId = substr($despatchdetailRowId, 0, strlen($despatchdetailRowId)-1);
        // $despatchdetailRowId = implode(",", $despatchdetailRowId);
        // return $despatchdetailRowId;
  //   	$this->db->select('challanDone, rowId');
  //   	$this->db->where_in('despatchdetail.rowId', $despatchdetailRowId);
		// $query = $this->db->get('despatchdetail');
  //       $row = $query->row_array();

  //       $challanDone = $row['challanDone'];
  //       // $despatchDetailRowId = $row['rowId'];
  //       if($challanDone == "Y")
  //       {
            $this->db->distinct();
        	$this->db->select('ci.vNo'); //, ci.vNo
            $x = explode(",", $despatchdetailRowId);
	        $this->db->where_in('cidetail.despatchDetailRowId', $x);
            $this->db->where('vType', 'C');
	        // $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId');
	        // $this->db->join('cidetail','cidetail.despatchDetailRowId = despatchdetail.rowId');
	        $this->db->join('ci','ci.ciRowId = cidetail.ciRowId');
	        $this->db->from('cidetail');
	        $query = $this->db->get();
            $challanNo = '';
            foreach ($query->result() as $row)
            {
               $challanNo .=  $row->vNo . ',';
            }
	        // $row = $query->row_array();
	        // $challanNo = $row['vNo'];
            $challanNo = substr($challanNo, 0, strlen($challanNo)-1);
	        return $challanNo;
	        // return($query->result_array());
        // }
        // else
        // {
        // 	return " ";
        // }


        // $this->db->select('despatchdetail.*');
        // $this->db->where('despatchdetail.rowId', $despatchdetailRowId);
        // $this->db->from('despatchdetail');
        // $query = $this->db->get();
        // return($query->result_array());
    }

}