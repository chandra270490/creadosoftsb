<?php
class Maillog_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getMailTypes()
    {
        $this->db->select('mailtypes.mailType, mailtypes.mailTypeRowId');
        $this->db->from('mailtypes');
        // $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('mailtypes.deleted', 'N');
        $this->db->order_by('mailtypes.mailType');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['mailTypeRowId']]= $row['mailType'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
         $refRowId = explode(",", $this->input->post('refRowId'));
         $contactTypeRowId = explode(",", $this->input->post('contactTypeRowId'));
         $this->db->distinct();
         $this->db->select('addressbook.abRowId, addressbook.name, addressbook.email1');
         $this->db->from('addressbook');
         $this->db->where_in('addressbook.referenceRowId', $refRowId );
         $this->db->join('addressbookcontacttypes','addressbookcontacttypes.abRowId = addressbook.abRowId');
         $this->db->where('addressbook.deleted', 'N');
         $this->db->where_in('addressbookcontacttypes.contactTypeRowId', $contactTypeRowId );

         $this->db->order_by('addressbook.name');
         $query = $this->db->get();
         // return($query->result_array());
        $rows = array();
        foreach ($query->result_array() as $row)
        {
            $row['prev1'] = '-';
            $row['prev2'] = '-';
            $row['prev3'] = '-';
            $this->db->select('maillog.mailLogDt, maillogdetail.remarks, mailtypes.mailType');
            $this->db->from('maillog');
            $this->db->where('maillog.abRowId', $row['abRowId'] );
            $this->db->join('maillogdetail','maillogdetail.mailLogDetailRowId = maillog.mailLogDetailRowId');
            $this->db->join('mailtypes','mailtypes.mailTypeRowId = maillogdetail.mailTypeRowId');
            $this->db->limit(3);
            $this->db->order_by('maillog.mailLogRowId desc');
            $queryInner = $this->db->get();
            if ($queryInner->num_rows() > 0)       
            {
                // $rowInner = $queryInner->row_array();
                $rowInner = $queryInner->first_row('array');
                $row['prev1'] = $rowInner['mailLogDt'] . " [" . $rowInner['mailType'] . " - " . $rowInner['remarks'] . "]";
                $rowInner = $queryInner->next_row('array');
                $row['prev2'] = $rowInner['mailLogDt'] . " [" . $rowInner['mailType'] . " - " . $rowInner['remarks'] . "]";
                $rowInner = $queryInner->next_row('array');
                $row['prev3'] = $rowInner['mailLogDt'] . " [" . $rowInner['mailType'] . " - " . $rowInner['remarks'] . "]";
            }


            $rows[] = $row;                 //// adding updated row to array
        }   //// Main outer for each loop ends here
        return $rows;

    }



    public function getContactTypes()
    {
        $this->db->select('contactTypeRowId, contactType');
        $this->db->where('deleted', 'N');
        $this->db->where('length(contactType)>', 0);
        $this->db->order_by('contactType');
        $query = $this->db->get('contacttypes');
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['contactTypeRowId']]= $row['contactType'];
        }

        return $arr;
    }


    public function getReferencers()
    {
        $this->db->distinct();
        $this->db->select('addressbook.referenceRowId, AB.name as refName');
        $this->db->from('addressbook');
        $this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId');
        $this->db->where('addressbook.deleted', 'N');
        $this->db->order_by('refName');
        $query = $this->db->get();
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['referenceRowId']]= $row['refName'];
        }

        return $arr;
    }
    
    public function saveData()
    {



        $this->db->select_max('mailLogDetailRowId');
        $query = $this->db->get('maillogdetail');
        $row = $query->row_array();
        $mailLogDetailRowId = $row['mailLogDetailRowId']+1;
        $data = array(
            'mailLogDetailRowId' => $mailLogDetailRowId
            , 'mailTypeRowId' => $this->input->post('mailTypeRowId')
            , 'remarks' => $this->input->post('remarks')
        );
        $this->db->insert('maillogdetail', $data);


        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);
        $mobile="";
        for ($i=0; $i < $myTableRows; $i++) 
        {
            //// pichle 3 se mail se purane record delete
            $this->db->where('mailLogDt <', $TableData[$i]['prev3']);
            $this->db->where('abRowId', $TableData[$i]['abRowId']);
            $this->db->delete('maillog');

            
            //// END - pichle 3 se mail se purane record delete

            $this->db->select_max('mailLogRowId');
            $query = $this->db->get('maillog');
            $row = $query->row_array();

            $current_row = $row['mailLogRowId']+1;

            $data = array(
                'mailLogRowId' => $current_row
                , 'mailLogDt' => date('Y-m-d', strtotime($this->input->post('dt')))
                , 'abRowId' => $TableData[$i]['abRowId']
                , 'mailLogDetailRowId' => $mailLogDetailRowId
                , 'createdBy' => $this->session->userRowId
            );
            
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('maillog', $data); 


            $this->db->query('DELETE FROM maillogdetail WHERE mailLogDetailRowId NOT IN (Select mailLogDetailRowId from maillog)'); 
        }

                    
    }
}