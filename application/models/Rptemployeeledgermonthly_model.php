<?php
class Rptemployeeledgermonthly_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmpList()
    {
        $userRowId = "(employees.userRowId <= 0 OR employees.userRowId is NULL)";
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.discontinue', 'N');
        $this->db->where('employees.salType', 'M');
        $this->db->where($userRowId);
        // $this->db->where('employees.salType', 'C');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'].' ('. $row['empRowId'] .')';
        }
        return $arr;
    }

    public function getOpeningBalPayments()
    {
     $this->db->select_Sum('amt');
     $this->db->from('payments');
     $this->db->where('payments.empRowId', $this->input->post('empRowId'));
     $this->db->where('payments.dt <', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
     $query = $this->db->get();
     return($query->result_array());
    }

    public function getOpeningBalDues()
    {
     $this->db->select_Sum('net');
     $this->db->from('salarymonthly');
     $this->db->where('salarymonthly.empRowId', $this->input->post('empRowId'));
     $this->db->where('salarymonthly.dt <', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
     $query = $this->db->get();
     return($query->result_array());
    }

    public function getFromPaymentTable()
    {
         $this->db->select('payments.*, addressbook.name');
        $this->db->from('payments');
        $this->db->where('payments.empRowId', $this->input->post('empRowId'));
        $this->db->where('payments.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('payments.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->join('employees','employees.empRowId = payments.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('payments.paymentRowId');
        $query = $this->db->get();
        return($query->result_array());
    }


    public function getFromSalaryTable()
    {
         $this->db->select('salarymonthly.*, addressbook.name');
        $this->db->from('salarymonthly');
        $this->db->where('salarymonthly.empRowId', $this->input->post('empRowId'));
        $this->db->where('salarymonthly.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('salarymonthly.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->join('employees','employees.empRowId = salarymonthly.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('salarymonthly.smRowId');
        // $this->db->limit(5);
        $query = $this->db->get();

        return($query->result_array());
    }


}