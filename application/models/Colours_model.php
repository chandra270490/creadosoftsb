<?php
class Colours_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->where('active', '1');
		$this->db->order_by('colourRowId desc');
		// $this->db->limit(5);
		$query = $this->db->get('colours');

		// return($query->result_array());
		$rows = array();
        foreach ($query->result_array() as $row)
        {
            $row['imgExists']="N";
            
            $path = FCPATH . '/bootstrap/images/colours/' . $row['colourRowId'] . '.jpg';
            $r = file_exists($path);
            if($r == true)
            {
            	 $row['imgExists']="Y";
            }

            $rows[] = $row;                 //// adding updated row to array
        }   //// Main outer for each loop ends here

        return $rows;
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->where('active', '1');
		$this->db->order_by('colourRowId');
		$query = $this->db->get('colours');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('colourName');
		$this->db->where('colourName', $this->input->post('colourName'));
		$query = $this->db->get('colours');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('colourRowId');
		$query = $this->db->get('colours');
        $row = $query->row_array();

        $current_row = $row['colourRowId']+1;

		$data = array(
			'colourRowId' => $current_row, 
			'colourName' => ucwords($this->input->post('colourName')), 
			'remarks' => $this->input->post('remarks'), 
			'hike' => $this->input->post('hike'), 
			'createdBy' => $this->session->userRowId,
			'active' => $this->input->post('active')
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('colours', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('colourName');
		$this->db->where('colourName', $this->input->post('colourName'));
		$this->db->where('colourRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('colours');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
			'colourName' => ucwords($this->input->post('colourName')), 
			'remarks' => $this->input->post('remarks'), 
			'hike' => $this->input->post('hike'),
			'active' => $this->input->post('active')
		);

		//print_r($data); die;
		$this->db->where('colourRowId', $this->input->post('globalrowid'));
		$this->db->update('colours', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('colourRowId', $this->input->post('rowId'));
		$this->db->delete('colours');
	}

	public function getColourList()
	{
		$this->db->select('colourRowId, colourName');
		$this->db->where('deleted', 'N');
		$this->db->where('active', '1');
		$this->db->order_by('colourName');
		$query = $this->db->get('colours');
		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['colourRowId']]= $row['colourName'];
		}
		return $arr;
	}
	public function getColourListWithHike()
	{
		$this->db->select('colours.*');
		$this->db->where('deleted', 'N');
		$this->db->where('active', '1');
		$this->db->order_by('colourName');
		$query = $this->db->get('colours');
		return $query->result_array();
	}
	public function getColourListRefresh()
	{
		$this->db->select('colourRowId, colourName');
		$this->db->where('deleted', 'N');
		$this->db->where('active', '1');
		$this->db->order_by('colourName');
		$query = $this->db->get('colours');
		return $query->result_array();
	}
}