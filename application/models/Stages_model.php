<?php
class Stages_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

 //    public function getDataLimit()
	// {
	// 	$this->db->select('stages.*, districts.districtName, states.stateRowId, states.stateName, countries.countryRowId, countries.countryName');
	// 	$this->db->from('stages');
	// 	$this->db->join('districts','districts.districtRowId = stages.districtRowId');
	// 	$this->db->join('states','states.stateRowId = districts.stateRowId');
	// 	$this->db->join('countries','countries.countryRowId = states.countryRowId');
	// 	$this->db->where('stages.deleted', 'N');
	// 	$this->db->order_by('stages.stageRowId desc');
	// 	$this->db->limit(5);
	// 	$query = $this->db->get();

	// 	return($query->result_array());
	// }

    public function getDataAll()
	{
		$this->db->select('stages.*');
		$this->db->from('stages');
		$this->db->where('stages.deleted', 'N');
		$this->db->where('stages.orgRowId', $this->session->orgRowId);
		$this->db->order_by('stages.odr');
		$query = $this->db->get();

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('stageName');
		$this->db->where('stageName', $this->input->post('stageName'));
		$query = $this->db->get('stages');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('stageRowId');
		$query = $this->db->get('stages');
        $row = $query->row_array();

        $current_row = $row['stageRowId']+1;
		$data = array(
	        'stageRowId' => $current_row
	        , 'stageName' => ucwords($this->input->post('stageName'))
	        , 'weightage' => $this->input->post('weightage')
	        , 'remarks' => $this->input->post('remarks')
	        , 'odr' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('stages', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('stageName');
		$this->db->where('stageName', $this->input->post('stageName'));
		$this->db->where('stageRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('stages');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'stageName' => ucwords($this->input->post('stageName'))
	        , 'weightage' => $this->input->post('weightage')	        
	        , 'remarks' => $this->input->post('remarks')
		);
		$this->db->where('stageRowId', $this->input->post('globalrowid'));
		$this->db->update('stages', $data);			
	}

	public function delete()
	{
		$data = array(
		        'deleted' => 'Y',
		        'deletedBy' => $this->session->userRowId

		);
		$this->db->set('deletedStamp', 'NOW()', FALSE);
		$this->db->where('stageRowId', $this->input->post('rowId'));
		$this->db->update('stages', $data);
	}


}