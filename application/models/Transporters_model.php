<?php
class Transporters_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('transporters.*, addressbook.name');
			$this->db->from('transporters');
			$this->db->join('addressbook','addressbook.abRowId = transporters.abRowId');
			$this->db->where('transporters.deleted', 'N');
			// $this->db->where('transporters.orgRowId', $this->session->orgRowId);
			$this->db->order_by('transporters.transporterRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('transporters.*, addressbook.name');
			$this->db->from('transporters');
			$this->db->join('addressbook','addressbook.abRowId = transporters.abRowId');
			$this->db->where('transporters.deleted', 'N');
			// $this->db->where('transporters.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('transporters.createdBy', $abAccessIn);
			$this->db->order_by('transporters.transporterRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('transporters.*, addressbook.name');
			$this->db->from('transporters');
			$this->db->join('addressbook','addressbook.abRowId = transporters.abRowId');
			$this->db->where('transporters.deleted', 'N');
			$this->db->where('transporters.orgRowId', $this->session->orgRowId);
			$this->db->order_by('transporters.transporterRowId');
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('transporters.*, addressbook.name');
			$this->db->from('transporters');
			$this->db->join('addressbook','addressbook.abRowId = transporters.abRowId');
			$this->db->where('transporters.deleted', 'N');
			$this->db->where('transporters.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('transporters.createdBy', $abAccessIn);
			$this->db->order_by('transporters.transporterRowId');
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    

	public function checkDuplicate()
    {
		$this->db->select('abRowId');
		$this->db->where('abRowId', $this->input->post('abRowId'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('transporters');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('transporterRowId');
		$query = $this->db->get('transporters');
        $row = $query->row_array();

        $current_row = $row['transporterRowId']+1;

		$data = array(
	        'transporterRowId' => $current_row
	        , 'abRowId' => $this->input->post('abRowId')
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('transporters', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('abRowId');
		$this->db->where('abRowId', $this->input->post('abRowId'));
		$this->db->where('transporterRowId !=', $this->input->post('globalrowid'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('transporters');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'abRowId' => $this->input->post('abRowId')
	        , 'orgRowId' => $this->session->orgRowId
		);
		$this->db->where('transporterRowId', $this->input->post('globalrowid'));
		$this->db->update('transporters', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('transporterRowId', $this->input->post('rowId'));
		$this->db->delete('transporters');
	}
}