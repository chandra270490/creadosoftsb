<?php
class Rptsalaryexecutives_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }


    public function getDataForReport()
    {
         set_time_limit(0);

        $this->db->select('salaryexecutives.*, addressbook.name, users.uid');
        $this->db->where('dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('salaryexecutives.orgRowId', $this->session->orgRowId);
        $this->db->join('employees','employees.empRowId = salaryexecutives.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->join('users','users.rowid = salaryexecutives.userRowId');
        $this->db->order_by('name');
        $this->db->from('salaryexecutives');
        $query = $this->db->get();
        return($query->result_array());
    }


}