<?php
class Masterm extends CI_Model 
{
    function __construct(){   
		parent::__construct();  
	}

	function ListHead($tbl_nm){
		$query = $this->db->query("SHOW columns FROM $tbl_nm 
		where Field not in('orgRowId','deleted','createdBy','createdStamp','deletedBy','deletedStamp')");
		
		return $query;
	}

	//Id's Column
	public function get_by_id($tbl_nm, $id_col, $id){
		$query = $this->db->query("select * from $tbl_nm where $id_col = '".$id."'");
		return $query;
	}

	//Ticket Entry
	public function budget_entry($data){   
		$budget_id = $this->input->post("budget_id");
		$budget_id1 = $this->input->post("budget_id");
		$budget_from_dt = $this->input->post("budget_from_dt");
		$budget_to_dt = $this->input->post("budget_to_dt");
		//$orgRowId = $this->input->post("orgRowId");
		$orgRowId = 2;

		$category_id = $this->input->post("category_id");
		$annual_budget = $this->input->post("annual_budget");
		$monthy_budget = $this->input->post("monthy_budget");
		$so_value_perc = $this->input->post("so_value_perc");
		$annual_bud_exc_act = $this->input->post("annual_bud_exc_act");
		$monthly_bud_exc_act = $this->input->post("monthly_bud_exc_act");

		$category_arr_cnt = count($category_id);

		//User Details
		$created_id = $this->session->userRowId;
		$created_date = date("Y-m-d h:i:s");
		$modified_id = $this->session->userRowId;
		
		//Transaction Start
		$this->db->trans_start();

		if($budget_id1 == ""){
			$sql = $this->db->query("insert into budget_mst
			(budget_from_dt, budget_to_dt, created_id, created_date, modified_id) 
			values 
			('".$budget_from_dt."', '".$budget_to_dt."', '".$created_id."', '".$created_date."', '".$modified_id."')");

			//Getting Max Budget Id
			$sql_max_id = "select max(budget_id) max_budget_id from budget_mst";
			$qry_max_id = $this->db->query($sql_max_id)->row();
			$max_budget_id = $qry_max_id->max_budget_id;

			//Getting Previous Entries Of Budget
			$sql_cnt = "select count(*) as cnt from budget_det where budget_id = '".$max_budget_id."'";
			$qry_cnt = $this->db->query($sql_cnt)->row();
			$cnt = $qry_cnt->cnt;

			if($cnt > 0){
				//Delete Previous Entries
				$sql_del = $this->db->query("delete from budget_det where budget_id = '".$max_budget_id."'");

				for($i=0; $i<$category_arr_cnt; $i++){

					$sql2 = $this->db->query("insert into budget_det
					(budget_id, category_id, annual_budget, 
					monthy_budget, so_value_perc, annual_bud_exc_act, monthly_bud_exc_act) 
					values 
					('".$max_budget_id."', '".$category_id[$i]."', '".$annual_budget[$i]."', 
					'".$monthy_budget[$i]."', '".$so_value_perc[$i]."', '".$annual_bud_exc_act[$i]."', '".$monthly_bud_exc_act[$i]."')");

				}

			} else {

				for($i=0; $i<$category_arr_cnt; $i++){

					$sql2 = $this->db->query("insert into budget_det
					(budget_id, category_id, annual_budget, 
					monthy_budget, so_value_perc, annual_bud_exc_act, monthly_bud_exc_act) 
					values 
					('".$max_budget_id."', '".$category_id[$i]."', '".$annual_budget[$i]."', 
					'".$monthy_budget[$i]."', '".$so_value_perc[$i]."', '".$annual_bud_exc_act[$i]."', '".$monthly_bud_exc_act[$i]."')");

				}

			}

		} else {

			$sql = $this->db->query("update budget_mst 
			set budget_from_dt = '".$budget_from_dt."', budget_to_dt = '".$budget_to_dt."', 
			created_id = '".$created_id."', created_date = '".$created_date."',
			modified_id = '".$modified_id."'
			where budget_id = '".$budget_id."'");

			//Getting Previous Entries Of Budget
			$sql_cnt = "select count(*) as cnt from budget_det where budget_id = '".$budget_id."'";
			$qry_cnt = $this->db->query($sql_cnt)->row();
			$cnt = $qry_cnt->cnt;

			if($cnt > 0){
				//Delete Previous Entries
				$sql_del = $this->db->query("delete from budget_det where budget_id = '".$budget_id."'");

				for($i=0; $i<$category_arr_cnt; $i++){

					$sql2 = $this->db->query("insert into budget_det
					(budget_id, category_id, annual_budget, 
					monthy_budget, so_value_perc, annual_bud_exc_act, monthly_bud_exc_act) 
					values 
					('".$budget_id."', '".$category_id[$i]."', '".$annual_budget[$i]."', 
					'".$monthy_budget[$i]."', '".$so_value_perc[$i]."', '".$annual_bud_exc_act[$i]."', '".$monthly_bud_exc_act[$i]."')");

				}

			} else {

				for($i=0; $i<$category_arr_cnt; $i++){

					$sql2 = $this->db->query("insert into budget_det
					(budget_id, category_id, annual_budget, 
					monthy_budget, so_value_perc, annual_bud_exc_act, monthly_bud_exc_act) 
					values 
					('".$budget_id."', '".$category_id[$i]."', '".$annual_budget[$i]."', 
					'".$monthy_budget[$i]."', '".$so_value_perc[$i]."', '".$annual_bud_exc_act[$i]."', '".$monthly_bud_exc_act[$i]."')");

				}

			}
		}

		$this->db->trans_complete();
		//Transanction Complete

	}
}