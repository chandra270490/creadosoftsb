<?php
class Attendanceexecutive_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function showData()
    {
        //////Checking if already stored on that date
        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        $this->db->select('dt');
        $this->db->where('dt', date('Y-m-d', strtotime($this->input->post('dt'))));
        $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
        $query = $this->db->get('attendanceexecutives');
        if ($query->num_rows() > 0) /////////Already Marked
        {
            // $this->db->distinct();
            // $dt = date('Y-m-d', strtotime($this->input->post('dt')));  //, addressbook.name
            // $this->db->select('ziew_empuser.*, attendanceexecutives.attendance');
            // $this->db->from('ziew_empuser');
            // $this->db->where('ziew_empuser.orgRowId', $this->session->orgRowId);
            // $this->db->join('attendanceexecutives','attendanceexecutives.userRowId = ziew_empuser.userRowId AND  attendanceexecutives.dt = "'.$dt.'"', 'left');
            // $this->db->join('employees','employees.userRowId = ziew_empuser.userRowid AND employees.showInAttendance=\'Y\'');
            // $this->db->where('employees.doj <=', $dt);
            // $this->db->order_by('name');
            // $query = $this->db->get();
            // return($query->result_array());

            // $this->db->distinct();/// this row on 07 june 2019
            $dt = date('Y-m-d', strtotime($this->input->post('dt')));  //, addressbook.name, attendanceexecutives.attendance
            $this->db->select('users.uid, users.rowid as userRowId, addressbook.name, attendanceexecutives.attendance');
            $this->db->from('users');
            // $this->db->where('attendanceexecutives.dt ', $dt);
            // $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
            $this->db->where('users.deleted', 'N');
            $this->db->where('users.uid !=', 'admin');
            // $this->db->where('employees.doj <=', $dt);
            $exp = "employees.discontinueDt > '" . $dt ."' OR employees.discontinueDt is NULL";
            $this->db->where($exp);

            $designationRowId = "employees.designationRowId != 1";
            $this->db->where($designationRowId);

            $this->db->join('attendanceexecutives','attendanceexecutives.userRowId = users.rowid AND attendanceexecutives.orgRowId='. $this->session->orgRowId .' AND  attendanceexecutives.dt = "'.$dt.'"', 'LEFT');
            $this->db->join('employees','employees.userRowId = users.rowid AND employees.orgRowId = '. $this->session->orgRowId . ' AND employees.deleted=\'N\' AND employees.showInAttendance=\'Y\' AND employees.doj <= \''. $dt . '\'' );
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            $this->db->order_by('name');
            $query = $this->db->get();
            return($query->result_array());
        }
        else ////if not marked earlier (new)
        {
            // $this->db->select('users.uid, users.rowid as userRowId, addressbook.name, employees.discontinueDt');
            // $this->db->from('users');
            // $this->db->where('users.deleted', 'N');
            // $this->db->where('users.uid !=', 'admin');
            // $this->db->where('employees.doj <=', $dt);
            // $this->db->join('employees','employees.userRowId = users.rowid AND employees.orgRowId = '. $this->session->orgRowId . ' AND employees.discontinue=\'N\' AND employees.deleted=\'N\' AND employees.showInAttendance=\'Y\'');
            // $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            // $this->db->order_by('name');
            // $query = $this->db->get();
            // return($query->result_array());

            $this->db->select('users.uid, users.rowid as userRowId, addressbook.name, employees.discontinueDt');
            $this->db->from('users');
            $this->db->where('users.deleted', 'N');
            $this->db->where('users.uid !=', 'admin');
            // $this->db->where('employees.doj <=', $dt);
            // $this->db->where('employees.doj <=', '2020-11-01');
            $exp = "employees.discontinueDt > '" . $dt ."' OR employees.discontinueDt is NULL";
            $this->db->where($exp);

            $designationRowId = "employees.designationRowId != 1";
            $this->db->where($designationRowId);
            // $this->db->where('employees.discontinueDt >', $dt);
            $this->db->join('employees','employees.userRowId = users.rowid AND employees.orgRowId = '. $this->session->orgRowId . ' AND employees.deleted=\'N\' AND employees.showInAttendance=\'Y\' AND employees.doj <= \''. $dt . '\'' );
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            $this->db->order_by('name');
            $query = $this->db->get();
            return($query->result_array());

        }

    }

    public function getIsHoliday()
    {
        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        $this->db->select('holidays.*');
        $this->db->from('holidays');
        $this->db->where('holidays.holidayDt', $dt);
        $query = $this->db->get();
        return($query->result_array());
    }

	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        // echo $myTableRows;

        $this->db->where('dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
        $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
        $this->db->delete('attendanceexecutives');	
        
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('aeRowId');
			$query = $this->db->get('attendanceexecutives');
	        $row = $query->row_array();
	        $aeRowId = $row['aeRowId']+1;

			$data = array(
		        'aeRowId' => $aeRowId
		        , 'dt' => $dt
		        , 'userRowId' => $TableData[$i]['userRowId']
		        , 'attendance' => $TableData[$i]['attendance']
                , 'orgRowId' => $this->session->orgRowId
                , 'refRowId' => '-3'
                , 'createdBy' => $this->session->userRowId
			);
            $this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('attendanceexecutives', $data);	
		}

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}

}