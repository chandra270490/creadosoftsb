<?php
class Home_model extends CI_Model {

        public function __construct()
        {
                // $this->load->database('');
        }


		public function searchAjax()
		{
			// echo "model";
			// echo $this->input->post('bloodGroup');
			$this->db->select('name,mobile1,locality,bloodGroup');
			$this->db->where('deleted', 'N');
			$this->db->where('authorised', 'Y');
			$this->db->where('bloodGroup', $this->input->post('bloodGroup'));
			$this->db->order_by('locality');
			$query = $this->db->get('donors');
			
			// return($query->num_rows());
			return($query->result_array());
		}

		public function checkDuplicate()
        {

			$this->db->select('mobile1');
			$this->db->where('mobile1', $this->input->post('mobile1'));
			$query = $this->db->get('donors');
			if ($query->num_rows() > 0)
			{
				return 1;
			}
        }
		public function insert()
        {
        	// echo $this->input->post('name');
			$this->db->select_max('rowId');
			$query = $this->db->get('donors');
			if ($query->num_rows() > 0)
			{
			        $row = $query->row_array();
			        $current_row = $row['rowId'] + 1;

				
					$this->load->helper('date');
					$datetime = unix_to_human(now('asia/kolkata'));

					$data = array(
					        'rowId' => $current_row,
					        'name' => $this->input->post('name'),
					        'fName' => $this->input->post('fname'),
					        'mobile1' => $this->input->post('mobile1'),
					        'mobile2' => $this->input->post('mobile2'),
					        'email' => $this->input->post('email'),
					        'houseNo' => $this->input->post('houseNo'),
					        'street' => $this->input->post('street'),
					        'locality' => $this->input->post('locality'),
					        'city' => $this->input->post('city'),
					        'bloodGroup' => $this->input->post('bloodGroup'),
					        'alreadyDonatedTimes' => (int)$this->input->post('already'),
					        'gender' => $this->input->post('gender'),
					        'dataSource' => $this->input->post('dataSource'),
					        'remarks' => $this->input->post('remarks'),
					        'createdBy' => $this->session->userRowId
					);
					$this->db->set('dob', 'STR_TO_DATE("'.$this->input->post('dob').'","%d-%M-%Y")', FALSE);
					$this->db->set('createdStamp', 'NOW()', FALSE);
					$this->db->insert('donors', $data);				
			}
		}
}