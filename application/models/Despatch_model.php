<?php
class Despatch_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

    public function getTransporterList()
	{
		$this->db->select('addressbook.*,  transporters.transporterRowId');
		$this->db->from('addressbook');
		$this->db->join('transporters','transporters.abRowId = addressbook.abRowId');
		$this->db->where('transporters.deleted', 'N');
		$this->db->order_by('addressbook.name');
		$query = $this->db->get();
		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['transporterRowId']]= $row['name'];
		}
		return $arr;
	}

	public function getGodowns()
    {
        $this->db->select('addressbook.name, addressbook.abRowId');
        $this->db->from('addressbook');
        $this->db->where('addressbook.deleted', 'N');
        $this->db->where('addressbook.godown', 'Y');
        // $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();

        // $arr = array();
        // $arr["-1"] = '--- Select ---';
        // $arr["-2"] = 'Default Stage';
        // foreach ($query->result_array() as $row)
        // {
        //     $arr[$row['abRowId']] = $row['name'];
        // }

        // return $arr;
        return($query->result_array());
    }

 	public function getDataLimit()
	{
		set_time_limit(0);
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
			$this->db->from('despatch');
			$this->db->join('parties','parties.partyRowId = despatch.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
			$this->db->where('despatch.orgRowId', $this->session->orgRowId);
			$this->db->where('despatch.deleted', 'N');
			$this->db->order_by('despatch.despatchRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
			$this->db->from('despatch');
			$this->db->join('parties','parties.partyRowId = despatch.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
			$this->db->where('despatch.orgRowId', $this->session->orgRowId);
			$this->db->where('despatch.deleted', 'N');
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('despatch.createdBy', $abAccessIn);
			$this->db->where_in('despatch.createdBy', $this->session->userRowId);
			$this->db->order_by('despatch.despatchRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
			$this->db->from('despatch');
			$this->db->join('parties','parties.partyRowId = despatch.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
			$this->db->where('despatch.orgRowId', $this->session->orgRowId);
			$this->db->where('despatch.deleted', 'N');
			$this->db->order_by('despatch.despatchRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter');
			$this->db->from('despatch');
			$this->db->join('parties','parties.partyRowId = despatch.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
			$this->db->where('despatch.orgRowId', $this->session->orgRowId);
			$this->db->where('despatch.deleted', 'N');
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('despatch.createdBy', $abAccessIn);
			$this->db->where_in('despatch.createdBy', $this->session->userRowId);
			$this->db->order_by('despatch.despatchRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    public function checkAvailability()
	{
		set_time_limit(0);
        $this->db->trans_start();

        $flag = 0;
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);
        
        if($this->session->orgRowId == 2)  /////If org KINNY
        {
	        for ($i=0; $i < $myTableRows; $i++) 
	        {
	        	$this->db->select('ourProduct');
				$this->db->where('productRowId', $TableData[$i]['productRowId']);
				$query = $this->db->get('products');
		        $row = $query->row_array();
		        $ourProduct = $row['ourProduct'];
		        if( $ourProduct == "Y" )
		        {

		        	if($TableData[$i]['godownAbRowId'] == "-2") ///Agar Desp stage se bhej rahe h
		        	{
			        	$this->db->select_max('stages.stageRowId');
				        $this->db->from('stages');
				        $this->db->where('stages.deleted', 'N');
				        // $this->db->order_by('stages.odr');
				        $query = $this->db->get();
				        $row = $query->row_array();
				        $lastStageRowId = $row['stageRowId'];

				        $this->db->select('avlQty');
				        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
				        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
				        $this->db->where('stageRowId',  $lastStageRowId);
				        $this->db->where('orgRowId',  $this->session->orgRowId);
				        $query = $this->db->get('openingbal');
				        $row = $query->row_array();
				        $abhiKiQty = $row['avlQty'];
				        $despatchQty = $TableData[$i]['despatchQty'];
				        $newQty = $abhiKiQty - $despatchQty;
				        if ($newQty < 0)
				        {
				        	$flag=1;
				        }
		        	}
		        	else /// Agar kisi godown se bhej rahe h
		        	{
				        $this->db->select('avlQty');
				        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
				        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
				        $this->db->where('godownAbRowId',  $TableData[$i]['godownAbRowId']);
				        $this->db->where('orgRowId',  $this->session->orgRowId);
				        $query = $this->db->get('godownstatus');
				        $row = $query->row_array();
				        $abhiKiQty = $row['avlQty'];
				        $despatchQty = $TableData[$i]['despatchQty'];
				        $newQty = $abhiKiQty - $despatchQty;
				        if ($newQty < 0)
				        {
				        	$flag=1;
				        }
			        }		        	
			        
		        }
				else if( $ourProduct == "N" ) ///readymade products
				{
					$str = $TableData[$i]['productRowId'];
					$q = "Select sum(receive)-sum(Issue) as currentQty, ledgerp.productRowId, productName, productCategory from ledgerp, products, productcategories where ledgerp.orgRowId=". $this->session->orgRowId ." AND ledgerp.productRowId=products.productRowId AND products.productCategoryRowId=productcategories.productCategoryRowId AND ledgerp.productRowId IN (".$str.") group by ledgerp.productRowId,productName";
	        			$query = $this->db->query($q);
	        			$row = $query->row_array();
	        			$abhiKiQty = $row['currentQty'];
	        			$despatchQty = $TableData[$i]['despatchQty'];
				        $newQty = $abhiKiQty - $despatchQty;
				        if ($newQty < 0)
				        {
				        	$flag=1;
				        }
				}

			}
		}
		else/// if org credo
		{
			for ($i=0; $i < $myTableRows; $i++) 
	        {
				$str = $TableData[$i]['productRowId'];
				$q = "Select sum(receive)-sum(Issue) as currentQty, ledgerp.productRowId, productName, productCategory from ledgerp, products, productcategories where ledgerp.orgRowId=". $this->session->orgRowId ." AND ledgerp.productRowId=products.productRowId AND products.productCategoryRowId=productcategories.productCategoryRowId AND ledgerp.productRowId IN (".$str.") group by ledgerp.productRowId,productName";
        			$query = $this->db->query($q);
        			$row = $query->row_array();
        			$abhiKiQty = $row['currentQty'];
        			$despatchQty = $TableData[$i]['despatchQty'];
			        $newQty = $abhiKiQty - $despatchQty;
			        if ($newQty < 0)
			        {
			        	$flag=1;
			        }
			}
		}
        if ($flag == 1)
        {
        	return 1;
        }
        else
        {
        	return 0;
        }

        $this->db->trans_complete();

	}

	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_start();

        $this->db->query('LOCK TABLE despatch WRITE, despatchdetail WRITE, qpodetail WRITE, stages WRITE, openingbal WRITE, ledgerp WRITE, despatchlaststagetracker WRITE, godownstatus WRITE');

		$this->db->select_max('despatchRowId');
		$query = $this->db->get('despatch');
        $row = $query->row_array();

        $current_row = $row['despatchRowId']+1;
		$this->globalInvNo = $current_row;

		$this->db->select_max('vNo');
        $this->db->where('vType', 'D');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('despatch');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;
		
        $despatchDt = date('Y-m-d', strtotime($this->input->post('despatchDt')));

		$data = array(
	        'despatchRowId' => $current_row
	        , 'vType' => 'D'
	        , 'vNo' => $vNo 
	        , 'orgRowId' => $this->session->orgRowId
	        , 'despatchDt' => $despatchDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'transporterRowId' => $this->input->post('transporterRowId')
	        , 'prGrNo' => $this->input->post('prGrNo')
	        , 'remarks' => $this->input->post('remarks')
	        , 'vehicleNo' => $this->input->post('vehicleNo')
	        , 'driverName' => $this->input->post('driverName')
	        , 'driverContactNo' => $this->input->post('driverContactNo')
	        , 'driverLicenceNo' => $this->input->post('driverLicenceNo')
	        , 'vehicleInsuranceNo' => $this->input->post('vehicleInsuranceNo')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('despatch', $data);	

		

		/////Saving DespatchDetail
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        // echo $myTableRows;
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('rowId');
			$query = $this->db->get('despatchdetail');
	        $row = $query->row_array();
	        $rowId = $row['rowId']+1;

			$data = array(
			        'rowId' => $rowId
			        , 'despatchRowId' => $current_row
			        , 'qpoRowId' => $TableData[$i]['qpoRowId']
			        , 'qpoDetailRowId' => $TableData[$i]['qpoDetailRowId']
			        , 'productRowId' => $TableData[$i]['productRowId']
			        , 'despatchQty' => $TableData[$i]['despatchQty']
			        , 'colourRowId' => $TableData[$i]['colourRowId']
			        , 'godownAbRowId' => $TableData[$i]['godownAbRowId']
			);
			$this->db->insert('despatchdetail', $data);	  


			////////////updating pending qty in orders(qpoDetail)
			$this->db->select('pendingQty');
        	$this->db->where('rowId',  $TableData[$i]['qpoDetailRowId']);
			$query = $this->db->get('qpodetail');
	        $row = $query->row_array();
	        $pendingQty = $row['pendingQty'];
	        $despatchQty = $TableData[$i]['despatchQty'];
	        $newPendingQty = $pendingQty - $despatchQty;

			$data = array(
			        'pendingQty' =>  $newPendingQty
			);
			$this->db->where('rowId',  $TableData[$i]['qpoDetailRowId']);
			$this->db->update('qpodetail', $data);
			////////////END - updating pending qty


			////////////updating qty in LAST stage
			if($TableData[$i]['godownAbRowId'] == "-2") ///Agar Desp stage se bhej rahe h
		    {
				$this->db->select_max('stages.stageRowId');
		        $this->db->from('stages');
		        $this->db->where('stages.deleted', 'N');
		        // $this->db->order_by('stages.odr');
		        $query = $this->db->get();
		        $row = $query->row_array();
		        $lastStageRowId = $row['stageRowId'];

		        $this->db->select('avlQty');
		        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
		        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
		        $this->db->where('stageRowId',  $lastStageRowId);
		        $query = $this->db->get('openingbal');
		        $row = $query->row_array();
		        $abhiKiQty = $row['avlQty'];
		        $data = array(
		            'avlQty' => (float)$abhiKiQty - (float)$TableData[$i]['despatchQty']
		        );
		        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
		        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
		        $this->db->where('stageRowId',  $lastStageRowId);
		        $this->db->where('orgRowId',  $this->session->orgRowId);
		        $this->db->update('openingbal', $data); 
		    }
		    else  //Agar godown se bhej rahe h...
		    {
		    	$q="Update godownstatus set avlQty = avlQty - " . $TableData[$i]['despatchQty'] . " WHERE productRowId=" . $TableData[$i]['productRowId'] . " AND colourRowId=". $TableData[$i]['colourRowId']. " AND godownAbRowId=". $TableData[$i]['godownAbRowId']. " AND orgRowId=". $this->session->orgRowId;
                $this->db->query($q);
		    }
			////////////END - updating qty in LAST stage	

	        // ////// Tracking wether last stage updaing or not (30-nov-2017)
         //    $data = array(
         //        'productRowId' => $TableData[$i]['productRowId']
         //        , 'stageRowId' => $lastStageRowId
         //        , 'orgRowId' => $this->session->orgRowId
         //        , 'pahleKiQty' => (float)$abhiKiQty 
         //        , 'despatchQty' => (float)$TableData[$i]['despatchQty']
         //        , 'avlQty' => (float)$abhiKiQty - (float)$TableData[$i]['despatchQty']
         //        , 'insertYaUpdate' => 'Insert'
         //        , 'vNo' => 'D-' . $vNo 
         //        , 'dt' => $despatchDt
         //    );
         //    $this->db->set('createdStamp', 'NOW()', FALSE);
         //    $this->db->insert('despatchlaststagetracker', $data);
	        // ////// END - Tracking wether last stage updaing or not

			// ////////////inserting in ledgerP
            $this->db->select_max('ledgerPRowId');
            $query = $this->db->get('ledgerp');
            $row = $query->row_array();
            $ledgerPRowId = $row['ledgerPRowId']+1;

            $data = array(
                'ledgerPRowId' => $ledgerPRowId
                , 'orgRowId' => $this->session->orgRowId
                , 'vType' => 'D'
                , 'vNo' => $vNo 
                , 'dt' => $despatchDt
                , 'productRowId' => $TableData[$i]['productRowId']
                , 'issue' => (float)$TableData[$i]['despatchQty']
                , 'issueBal' => (float)$TableData[$i]['despatchQty']
                , 'createdBy' => $this->session->userRowId
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('ledgerp', $data);


        }
  //       /////END - Saving in DespatchDetail

        $this->db->query('UNLOCK TABLES');
        
		$this->db->trans_complete();
	}


	public function goingPendingInMinus()
	{
		set_time_limit(0);
        $this->db->trans_start();
        $flag = 0;
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);
        $previousDespatchQty = 0;
        for ($i=0; $i < $myTableRows; $i++) 
        {
    		///////Previous desp qty
        	$this->db->select('despatchQty');
			$this->db->where('qpoDetailRowId',  $TableData[$i]['qpoDetailRowId']);
			$this->db->where('productRowId', $TableData[$i]['productRowId']);
			$this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
			$this->db->where('rowId', $TableData[$i]['despatchDetailRowId']);
			$query = $this->db->get('despatchdetail');
	        $row = $query->row_array();
	        $previousDespatchQty = $row['despatchQty'];
	        /////// END - Previous desp qty
	        if($this->session->orgRowId == 2) /// Agar Kinny h to stage ya godown check (20-Nov-2018)
			{
	        	if($TableData[$i]['godownAbRowId'] == "-2") ///Agar Desp stage se bhej rahe h
			    {
			    	$this->db->select('avlQty');
			        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
			        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
			        $this->db->where('stageRowId',  26); /// last Stage
			        $this->db->where('orgRowId',  $this->session->orgRowId);
			        $query = $this->db->get('openingbal');
			        $row = $query->row_array();
			        $avlQty = $row['avlQty'];
			        $despatchQty = $TableData[$i]['despatchQty'];
			        $newPendingQty = $avlQty + $previousDespatchQty - $despatchQty;		        

			        if ($newPendingQty < 0)
			        {
			        	$flag=1;
			        }
			    }
			    else /// Agar kisi godown se bhej rahe h
	        	{
			        $this->db->select('avlQty');
			        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
			        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
			        $this->db->where('godownAbRowId',  $TableData[$i]['godownAbRowId']);
			        $this->db->where('orgRowId',  $this->session->orgRowId);
			        $query = $this->db->get('godownstatus');
			        $row = $query->row_array();
			        $abhiKiQty = $row['avlQty'];
			        $despatchQty = $TableData[$i]['despatchQty'];
			        $newQty = $abhiKiQty + $previousDespatchQty - $despatchQty;
			        if ($newQty < 0)
			        {
			        	$flag=1;
			        }
		        }	
		    }

	        if($this->session->orgRowId == 1) /// Agar Credo h to ledger check (20-Nov-2018)
			{
				$q = "Select sum(receive)-sum(Issue) as currentQty from ledgerp where productRowId=".$TableData[$i]['productRowId']. " AND orgRowId=".$this->session->orgRowId;
	        	$query = $this->db->query($q);
	        	$row = $query->row_array();
		        $abhiKiQty = $row['currentQty'];
		        $despatchQty = $TableData[$i]['despatchQty'];
		        $newQty = $abhiKiQty + $previousDespatchQty - $despatchQty;
		        if ($newQty < 0)
		        {
		        	$flag=1;
		        }
			}	
		}



        if ($flag == 1)
        {
        	return 1;
        }
        else
        {
        	return 0;
        }

        $this->db->trans_complete();

	}

	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_start();

        $this->db->query('LOCK TABLE despatch WRITE, despatchdetail WRITE, qpodetail WRITE, stages WRITE, openingbal WRITE, ledgerp WRITE, despatchlaststagetracker WRITE, godownstatus WRITE');

        $despatchDt = date('Y-m-d', strtotime($this->input->post('despatchDt')));

		$data = array(
	        'despatchDt' => $despatchDt
	        , 'transporterRowId' => $this->input->post('transporterRowId')
	        , 'prGrNo' => $this->input->post('prGrNo')
	        , 'remarks' => $this->input->post('remarks')
	        , 'vehicleNo' => $this->input->post('vehicleNo')
	        , 'driverName' => $this->input->post('driverName')
	        , 'driverContactNo' => $this->input->post('driverContactNo')
	        , 'driverLicenceNo' => $this->input->post('driverLicenceNo')
	        , 'vehicleInsuranceNo' => $this->input->post('vehicleInsuranceNo')	        
		);
		$this->db->where('despatchRowId', $this->input->post('globalrowid'));
		$this->db->update('despatch', $data);	

		
		/////Updating DespatchDetail
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);
        $previousDespatchQty = 0;
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	///////Previous desp qty
        	$this->db->select('despatchQty');
			$this->db->where('qpoDetailRowId',  $TableData[$i]['qpoDetailRowId']);
			$this->db->where('productRowId', $TableData[$i]['productRowId']);
			$this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
			$this->db->where('rowId', $TableData[$i]['despatchDetailRowId']);
			$query = $this->db->get('despatchdetail');
	        $row = $query->row_array();
	        $previousDespatchQty = $row['despatchQty'];
	        /////// END - Previous desp qty

			$data = array(
			        'despatchQty' => (float) $TableData[$i]['despatchQty']
			);
			$this->db->where('qpoDetailRowId',  $TableData[$i]['qpoDetailRowId']);
			$this->db->where('productRowId', $TableData[$i]['productRowId']);
			$this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
			$this->db->where('rowId', $TableData[$i]['despatchDetailRowId']);
			$this->db->update('despatchdetail', $data);			


			////////////updating pending qty in orders(qpoDetail)
			$this->db->select('pendingQty');
        	$this->db->where('rowId',  $TableData[$i]['qpoDetailRowId']);
			$query = $this->db->get('qpodetail');
	        $row = $query->row_array();
	        $pendingQty = $row['pendingQty'];
	        $despatchQty = $TableData[$i]['despatchQty'];
	        $newPendingQty = $pendingQty + $previousDespatchQty - $despatchQty;

			$data = array(
			        'pendingQty' =>  $newPendingQty
			);
			$this->db->where('rowId',  $TableData[$i]['qpoDetailRowId']);
			$this->db->update('qpodetail', $data);
			////////////END - updating pending qty

			////////////updating qty in LAST stage on 02 jan 17
			if($TableData[$i]['godownAbRowId'] == "-2") ///Agar Desp stage se bhej rahe h
		    {
				$this->db->select_max('stages.stageRowId');
		        $this->db->from('stages');
		        $this->db->where('stages.deleted', 'N');
		        $this->db->order_by('stages.odr');
		        $query = $this->db->get();
		        $row = $query->row_array();
		        $lastStageRowId = $row['stageRowId'];

		        $this->db->select('avlQty');
		        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
		        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
		        $this->db->where('stageRowId',  $lastStageRowId);
		        $query = $this->db->get('openingbal');
		        $row = $query->row_array();
		        $abhiKiQty = $row['avlQty'];
		        $data = array(
		            'avlQty' => $abhiKiQty + $previousDespatchQty - $despatchQty
		        );
		        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
		        $this->db->where('colourRowId',  $TableData[$i]['colourRowId']);
		        $this->db->where('stageRowId',  $lastStageRowId);
		        $this->db->where('orgRowId',  $this->session->orgRowId);
		        $this->db->update('openingbal', $data);
		    }
		    else  //Agar godown se bhej rahe h...
		    {
		    	$qty = $previousDespatchQty - $TableData[$i]['despatchQty'];
		    	$q="Update godownstatus set avlQty = avlQty + " . $qty . " WHERE productRowId=" . $TableData[$i]['productRowId'] . " AND colourRowId=". $TableData[$i]['colourRowId']. " AND godownAbRowId=". $TableData[$i]['godownAbRowId']. " AND orgRowId=". $this->session->orgRowId;
                $this->db->query($q);
		    }
			////////////END - updating qty in LAST stage

	        ////// Tracking wether last stage updaing or not (30-nov-2017)
            $data = array(
                'productRowId' => $TableData[$i]['productRowId']
                , 'stageRowId' => $lastStageRowId
                , 'orgRowId' => $this->session->orgRowId
                , 'pahleKiQty' => (float)$abhiKiQty 
                , 'previousDespatchQty' => (float)$previousDespatchQty 
                , 'despatchQty' => (float)$TableData[$i]['despatchQty']
                , 'avlQty' => $abhiKiQty + $previousDespatchQty - $despatchQty
                , 'insertYaUpdate' => 'Update'
                , 'vNo' => 'D-' . $this->input->post('globalDespatchVno') 
                , 'dt' => $despatchDt
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('despatchlaststagetracker', $data);
	        ////// END - Tracking wether last stage updaing or not

			// ////////////updating in ledgerP
            $data = array(
                'dt' => $despatchDt
                , 'issue' => (float)$TableData[$i]['despatchQty']
                , 'issueBal' => (float)$TableData[$i]['despatchQty']
            );
	        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
	        $this->db->where('vType', 'D');
	        $this->db->where('vNo',  $this->input->post('globalDespatchVno'));
	        $this->db->where('orgRowId',  $this->session->orgRowId);
	        $this->db->update('ledgerp', $data); 


        }
        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');

        $this->db->trans_complete();

	}

	public function checkDependency()
	{
		$this->db->select('despatchdetail.rowId');
        $this->db->where('despatchdetail.despatchRowId', $this->input->post('rowId'));
        $where = '(despatchdetail.challanDone = "Y" OR despatchdetail.invoiceDone = "Y")';
        $this->db->where($where);
        // $this->db->where('despatchdetail.challanDone', 'Y');
        // $this->db->where('despatchdetail.invoiceDone', 'Y');
        $this->db->from('despatchdetail');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
		{
			return 1;
		}
	}

	public function delete()
	{
		set_time_limit(0);
        $this->db->trans_start();

        $this->db->query('LOCK TABLE despatch WRITE, despatchdetail WRITE, qpodetail WRITE, stages WRITE, openingbal WRITE, despatchlaststagetracker WRITE, ledgerp WRITE');

		////////////updating pending qty in orders(qpoDetail)
		$this->db->select('qpoDetailRowId, despatchQty, productRowId, colourRowId');
    	$this->db->where('despatchRowId',  $this->input->post('rowId'));
		$query = $this->db->get('despatchdetail');

        foreach ($query->result() as $row)
		{
        	$qpoDetailRowId = $row->qpoDetailRowId;
			$this->db->select('pendingQty');
        	$this->db->where('rowId',  $row->qpoDetailRowId);
			$query1 = $this->db->get('qpodetail');
	        $row1 = $query1->row_array();
	        $pendingQty = $row1['pendingQty'];
	        $despatchQty = $row->despatchQty;
	        $newPendingQty = $pendingQty + $despatchQty;

			$data = array(
			        'pendingQty' =>  $newPendingQty
			);
			$this->db->where('rowId',  $row->qpoDetailRowId);
			$this->db->update('qpodetail', $data);


			////////////updating qty in LAST stage on 02 jan 17
			$this->db->select_max('stages.stageRowId');
	        $this->db->from('stages');
	        $this->db->where('stages.deleted', 'N');
	        $this->db->order_by('stages.odr');
	        $query1 = $this->db->get();
	        $row1 = $query1->row_array();
	        $lastStageRowId = $row1['stageRowId'];

	        $this->db->select('avlQty');
	        $this->db->where('productRowId',  $row->productRowId);
	        $this->db->where('colourRowId',  $row->colourRowId);
	        $this->db->where('stageRowId',  $lastStageRowId);
	        $query2 = $this->db->get('openingbal');
	        $row2 = $query2->row_array();
	        $abhiKiQty = $row2['avlQty'];
	        $data = array(
	            'avlQty' => $abhiKiQty + $despatchQty
	        );
	        $this->db->where('productRowId',  $row->productRowId);
	        $this->db->where('colourRowId',  $row->colourRowId);
	        $this->db->where('stageRowId',  $lastStageRowId);
	        $this->db->where('orgRowId',  $this->session->orgRowId);
	        $this->db->update('openingbal', $data); 
			////////////END - updating qty in LAST stage	

	        ////// Tracking wether last stage updaing or not (30-nov-2017)
            $data = array(
                'productRowId' => $row->productRowId
                , 'stageRowId' => $lastStageRowId
                , 'orgRowId' => $this->session->orgRowId
                , 'pahleKiQty' => (float)$abhiKiQty 
                , 'despatchQty' => (float)$despatchQty
                , 'avlQty' => $abhiKiQty + $despatchQty
                , 'insertYaUpdate' => 'Delete'
                , 'vNo' => 'D-'.$this->input->post('globalDespatchVno')
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('despatchlaststagetracker', $data);
	        ////// END - Tracking wether last stage updaing or not			


			// ////////////updating in ledgerP
            $data = array(
                 'issue' => 0
                , 'issueBal' => 0
            );
	        $this->db->where('productRowId',  $row->productRowId);
	        $this->db->where('vType', 'D');
	        $this->db->where('vNo',  $this->input->post('globalDespatchVno'));
	        $this->db->where('orgRowId',  $this->session->orgRowId);
	        $this->db->update('ledgerp', $data); 
        }
        ////////////END - updating pending qty

		$data = array(
		    'deleted' => 'Y'
		);
		$this->db->where('despatchRowId',  $this->input->post('rowId'));
		$this->db->update('despatch', $data);



		$this->db->query('UNLOCK TABLES');

        $this->db->trans_complete();
	}


	public function getProducts()
    {	//
        $this->db->select('qpodetail.*,qpo.qpoRowId, qpo.vType, qpo.vNo, qpo.vDt, qpo.commitmentDate, productcategories.productCategoryRowId, productcategories.productCategory, products.productName, ordertypes.orderType, colours.colourRowId, colours.colourName');
        $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
        $this->db->where('qpo.vType', 'O');
        $this->db->where('qpo.deleted', 'N');
        $this->db->where('qpodetail.pendingQty >', 0);
        $this->db->from('qpodetail');
        $this->db->join('qpo','qpo.qpoRowId = qpodetail.qpoRowId AND orgRowId = ' . $this->session->orgRowId);
        $this->db->join('ordertypes','ordertypes.orderTypeRowId = qpo.orderTypeRowId');
        $this->db->join('products','products.productRowId = qpodetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }


	public function getProductsFromDespatch()
    {	
        // $this->db->select('despatchdetail.*, products.productName, qpodetail.qty, qpodetail.pendingQty ');
        // $this->db->where('despatchdetail.despatchRowId', $this->input->post('rowid'));
        // $this->db->from('despatchdetail');
        // $this->db->join('qpodetail','qpodetail.rowId = despatchdetail.qpoDetailRowId');
        // $this->db->join('products','products.productRowId = despatchdetail.productRowId');
        // $this->db->order_by('rowId');
        // $query = $this->db->get();
        // return($query->result_array());

        $this->db->select('despatchdetail.*, products.productName, qpodetail.qty, qpodetail.pendingQty, qpo.vType, qpo.vNo ');
        $this->db->where('despatchdetail.despatchRowId', $this->input->post('rowid'));
        $this->db->from('despatchdetail');
        $this->db->join('qpodetail','qpodetail.rowId = despatchdetail.qpoDetailRowId');
        $this->db->join('qpo','qpo.qpoRowId = qpodetail.qpoRowId AND orgRowId = ' . $this->session->orgRowId);
        $this->db->join('products','products.productRowId = despatchdetail.productRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }
   

}