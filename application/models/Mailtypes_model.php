<?php
class Mailtypes_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('mailTypeRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('mailtypes');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('mailTypeRowId');
		$query = $this->db->get('mailtypes');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('mailType');
		$this->db->where('mailType', $this->input->post('mailType'));
		$query = $this->db->get('mailtypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('mailTypeRowId');
		$query = $this->db->get('mailtypes');
        $row = $query->row_array();

        $current_row = $row['mailTypeRowId']+1;

		$data = array(
	        'mailTypeRowId' => $current_row
	        , 'mailType' => ucwords($this->input->post('mailType'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('mailtypes', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('mailType');
		$this->db->where('mailType', $this->input->post('mailType'));
		$this->db->where('mailTypeRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('mailtypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'mailType' => ucwords($this->input->post('mailType'))
		);
		$this->db->where('mailTypeRowId', $this->input->post('globalrowid'));
		$this->db->update('mailtypes', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('mailTypeRowId', $this->input->post('rowId'));
		$this->db->delete('mailtypes');
	}

	public function getContactTypes()
	{
		$this->db->select('mailTypeRowId ,mailType');
		$this->db->where('deleted', 'N');
		$this->db->order_by('mailType');
		$query = $this->db->get('mailtypes');
		$arr = array();
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['mailTypeRowId']]= $row['mailType'];
		}

		return $arr;
	}
}