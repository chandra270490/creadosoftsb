<?php
class Parties_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('parties.*, addressbook.name');
			$this->db->from('parties');
			$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
			$this->db->where('parties.deleted', 'N');
			// $this->db->where('parties.orgRowId', $this->session->orgRowId);
			$this->db->order_by('parties.partyRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('parties.*, addressbook.name');
			$this->db->from('parties');
			$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
			$this->db->where('parties.deleted', 'N');
			// $this->db->where('parties.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('parties.createdBy', $abAccessIn);
			$this->db->order_by('parties.partyRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('parties.*, addressbook.name');
			$this->db->from('parties');
			$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
			$this->db->where('parties.deleted', 'N');
			$this->db->where('parties.orgRowId', $this->session->orgRowId);
			$this->db->order_by('parties.partyRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('parties.*, addressbook.name');
			$this->db->from('parties');
			$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
			$this->db->where('parties.deleted', 'N');
			$this->db->where('parties.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('parties.createdBy', $abAccessIn);
			$this->db->order_by('parties.partyRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    

	public function checkDuplicate()
    {
		$this->db->select('abRowId');
		$this->db->where('abRowId', $this->input->post('abRowId'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->where('createdBy', $this->session->userRowId);
		$query = $this->db->get('parties');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('partyRowId');
		$query = $this->db->get('parties');
        $row = $query->row_array();

        $current_row = $row['partyRowId']+1;

		$data = array(
	        'partyRowId' => $current_row
	        , 'abRowId' => $this->input->post('abRowId')
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('parties', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('abRowId');
		$this->db->where('abRowId', $this->input->post('abRowId'));
		$this->db->where('partyRowId !=', $this->input->post('globalrowid'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->where('createdBy', $this->session->userRowId);

		$query = $this->db->get('parties');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'abRowId' => $this->input->post('abRowId')
          , 'orgRowId' => $this->session->orgRowId

		);
		$this->db->where('partyRowId', $this->input->post('globalrowid'));
		$this->db->update('parties', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('partyRowId', $this->input->post('rowId'));
		$this->db->delete('parties');
	}
}