<?php
class Purchaseordersaveandprint_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPurchaseOrders()
	{
		$this->db->select('po.*, addressbook.name, users.uid');
		$this->db->from('po');
		$this->db->join('parties','parties.partyRowId = po.partyRowId');
		$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
		$this->db->join('users', 'users.rowid = po.createdBy');
		$this->db->where('po.vType', 'PO');
		$this->db->where('po.deleted', 'N');
        $this->db->where('po.approved', 'Y');
        $this->db->where('po.saved', 'N');
		$this->db->where('po.orgRowId', $this->session->orgRowId);
		$this->db->order_by('po.poRowId');
		$query = $this->db->get();
		return($query->result_array());
	}

    public function getPurchaseOrdersSaved()
    {
        $this->db->select('po.*, addressbook.name, users.uid');
        $this->db->from('po');
        $this->db->join('parties','parties.partyRowId = po.partyRowId');
        $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
        $this->db->join('users', 'users.rowid = po.createdBy');
        $this->db->where('po.vType', 'PO');
        $this->db->where('po.deleted', 'N');
        $this->db->where('po.approved', 'Y');
        $this->db->where('po.saved', 'Y');
        $this->db->where('po.orgRowId', $this->session->orgRowId);
        $this->db->order_by('po.poRowId desc');
        $this->db->limit(50);
        $query = $this->db->get();
        return($query->result_array());
    }
	public function getProducts()
    {
        $this->db->select('podetail.*, products.productName');
        $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        // $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }


    public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE po WRITE');

		$data = array(
	        'saved' => 'Y'
	        , 'saveDt' => date('Y-m-d')
		);
		$this->db->where('poRowId', $this->input->post('poRowId'));
		$this->db->update('po', $data);	


   		$this->db->query('UNLOCK TABLES');
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}

    public function checkDependency()
    {
        $this->db->select('podetail.poVno');
        $this->db->where('podetail.poVno', $this->input->post('poVno'));
        $this->db->where('podetail.poRowId',  $this->input->post('poRowId'));
        $this->db->where('podetail.receivedQty >', 0);
        $this->db->where('po.orgRowId', $this->session->orgRowId);
        $this->db->from('podetail');
        $this->db->join('po','po.poRowId = podetail.poRowId');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0)
        {
         return 1;
        }
    }

    public function delete()
    {
        $data = array(
            'saved' => 'N'
        );
        $this->db->where('poRowId',  $this->input->post('poRowId'));
        $this->db->update('po', $data);
    }

    public function partyInfo($partyRowId)
    {
        $this->db->select('parties.partyRowId, addressbook.*');
        $this->db->from('parties');
        $this->db->where('parties.partyRowId', $partyRowId);
        $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
        // $this->db->where('parties.deleted', 'N');
        // $this->db->where('parties.orgRowId', $this->session->orgRowId);
        // $this->db->order_by('parties.partyRowId desc');
        // $this->db->limit(5);
        $query = $this->db->get();

        return($query->result_array());
    }
}