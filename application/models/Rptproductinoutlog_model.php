<?php
class Rptproductinoutlog_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

	public function getProductList()
    {
        set_time_limit(0);
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getData()
    {
        set_time_limit(0);
        //////creating tmp table
        $this->load->dbforge();
        if($this->db->table_exists('tmp'))
        {
            $this->dbforge->drop_table('tmp');
        }
        $fields = array(
                    'ledgerPRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'dt' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '15',
                                             
                                ),
                    'vType' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '10',
                                ),
                    'vNo' => array(
                                     'type' => 'INT',
                                     'constraint' => '10',     
                                ),
                    'qtyIn' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,3',     
                                ),
                    'qtyOut' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,3',     
                                ),
                    'toName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'user' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '40',     
                                )
            );  
        $this->dbforge->add_field($fields);
        /////// adding stages
        $this->db->select('ledgerp.*, users.uid');
        // $this->db->where('deleted', 'N');
        $this->db->where('ledgerp.orgRowId', $this->session->orgRowId);
        $this->db->where('ledgerp.productRowId', $this->input->post('productRowId'));
         $this->db->where('ledgerp.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
         $this->db->where('ledgerp.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->join('users','users.rowid = ledgerp.createdBy');
        $this->db->order_by('ledgerPRowId');
        $query = $this->db->get('ledgerp');
        $this->dbforge->create_table('tmp');
        ////// END - creating tmp table

        foreach ($query->result() as $row)
        {
            $data = array(
                    'ledgerPRowId' => $row->ledgerPRowId,
                    'productRowId' => $row->productRowId,
                    'dt' => $row->dt,
                    'vType' => $row->vType,
                    'vNo' => $row->vNo,
                    'qtyIn' => $row->receive,
                    'qtyOut' => $row->Issue,
                    'user' => $row->uid
            );
            $this->db->insert('tmp', $data); 

            if ($row->vType == "OB")              
            {
            	$data = array(
                    'toName' => 'Opening Balance',
	            );
	            $this->db->where('ledgerPRowId',  $row->ledgerPRowId);
                $this->db->update('tmp', $data);
            }
            else if ($row->vType == "Issue")              
            {
            	$this->db->select('issuereceive.irRowId, addressbook.name');
                $this->db->where('vNo',  $row->vNo);
                $this->db->where('vType',  'Issue');
                $this->db->where('issuereceive.orgRowId', $this->session->orgRowId);
                $this->db->join('employees',"employees.empRowId = issuereceive.empRowId");
	            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
      
                $this->db->from('issuereceive');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $toName = $row1->name;
                }
                else
                {
                    $toName='';
                }

            	$data = array(
                    'toName' => $toName,
	            );
	            $this->db->where('ledgerPRowId',  $row->ledgerPRowId);
                $this->db->update('tmp', $data);
            }
            else if ($row->vType == "Receive")              
            {
            	$this->db->select('issuereceive.irRowId, addressbook.name');
                $this->db->where('vNo',  $row->vNo);
                $this->db->where('vType',  'Receive');
                $this->db->where('issuereceive.orgRowId', $this->session->orgRowId);
                $this->db->join('employees',"employees.empRowId = issuereceive.empRowId");
	            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
      
                $this->db->from('issuereceive');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $toName = $row1->name;
                }
                else
                {
                    $toName='';
                }

            	$data = array(
                    'toName' => $toName,
	            );
	            $this->db->where('ledgerPRowId',  $row->ledgerPRowId);
                $this->db->update('tmp', $data);
            }
            else if ($row->vType == "PI")              
            {
            	$this->db->select('po.poRowId, addressbook.name');
                $this->db->where('vNo',  $row->vNo);
                $this->db->where('vType',  'PI');
                $this->db->where('po.orgRowId', $this->session->orgRowId);
                $this->db->join('parties',"parties.partyRowId = po.partyRowId");
	            $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
      
                $this->db->from('po');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $toName = $row1->name;
                }
                else
                {
                    $toName='';
                }

            	$data = array(
                    'toName' => $toName,
	            );
	            $this->db->where('ledgerPRowId',  $row->ledgerPRowId);
                $this->db->update('tmp', $data);
            }
            else if ($row->vType == "D")              
            {
            	$this->db->select('despatch.despatchRowId, addressbook.name');
                $this->db->where('vNo',  $row->vNo);
                $this->db->where('vType',  'D');
                $this->db->where('despatch.orgRowId', $this->session->orgRowId);
                $this->db->join('parties',"parties.partyRowId = despatch.partyRowId");
	            $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
      
                $this->db->from('despatch');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $toName = $row1->name;
                }
                else
                {
                    $toName='';
                }

            	$data = array(
                    'toName' => $toName,
	            );
	            $this->db->where('ledgerPRowId',  $row->ledgerPRowId);
                $this->db->update('tmp', $data);
            }            
        }   ////outer for loop ends here


        /// now sending data to client
        $this->db->select('tmp.*');
        $this->db->order_by('ledgerPRowId');
        $this->db->from('tmp');
        $query = $this->db->get();
        return($query->result_array());
    }
}