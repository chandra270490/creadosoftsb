<?php
class Rptproforma_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        if( $this->input->post('partyRowId') == "-1" )
        {
             $this->db->select('qpo.*, addressbook.name, users.uid');
             $this->db->from('qpo');
             $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('users','users.rowid = qpo.createdBy');
             $this->db->where('qpo.vType', 'P');
             // $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('qpo.deleted', 'N');
             $this->db->where('qpo.orgRowId', $this->session->orgRowId);
             $this->db->order_by('qpo.qpoRowId');
             $query = $this->db->get();
             return($query->result_array());
        }
        else
        {
             $this->db->select('qpo.*, addressbook.name, users.uid');
             $this->db->from('qpo');
             $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('users','users.rowid = qpo.createdBy');
             $this->db->where('qpo.vType', 'P');
             $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('qpo.deleted', 'N');
             $this->db->where('qpo.orgRowId', $this->session->orgRowId);
             $this->db->order_by('qpo.qpoRowId');
             $query = $this->db->get();
             return($query->result_array());
        }
    }
}