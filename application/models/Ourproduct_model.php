<?php
class Ourproduct_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
        $this->db->select('products.*, ledgerp.receive');
        $this->db->from('products');
        $this->db->where('products.deleted', 'N');
        $this->db->where('products.productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->join('ledgerp','ledgerp.productRowId = products.productRowId AND vType="OB" AND ledgerp.orgRowId='. $this->session->orgRowId, 'left');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function insert()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);


        for ($i=0; $i < $myTableRows; $i++) 
        {
                $data = array(
                    'ourProduct' => $TableData[$i]['ourProduct']
                );
                $this->db->where('productRowId', $TableData[$i]['productRowId']);
                $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
                $this->db->update('products', $data);  
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}