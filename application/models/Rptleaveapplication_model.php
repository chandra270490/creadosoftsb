<?php
class Rptleaveapplication_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getUsers()
    {
        $this->db->distinct();
        $this->db->select('leaveapplication.userRowId, users.uid');
        $this->db->from('leaveapplication');
        $this->db->join('users', 'users.rowid = leaveapplication.userRowId');
        $this->db->where('leaveapplication.orgRowId', $this->session->orgRowId);
        $this->db->where('leaveapplication.approved', 'Y');
        $this->db->order_by('uid');
        // $this->db->limit(5);
        $query = $this->db->get();

        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['userRowId']]= $row['uid'];
        }
        return $arr;

    }
    public function getDataForReport()
    {
        if ( $this->input->post('userRowId') == "-1")
        {
            $this->db->distinct();
            $this->db->select('leaveapplication.userRowId, users.uid');
            $this->db->from('leaveapplication');
            $this->db->where('leaveapplication.dtFrom >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('leaveapplication.dtTo <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('leaveapplication.orgRowId', $this->session->orgRowId);
            $this->db->join('users', 'users.rowid = leaveapplication.userRowId');
            // $this->db->order_by('name');
            $query = $this->db->get();
        }
        else
        {
            $this->db->distinct();
            $this->db->select('leaveapplication.userRowId, users.uid');
            $this->db->from('leaveapplication');
            $this->db->where('leaveapplication.dtFrom >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('leaveapplication.dtTo <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('leaveapplication.orgRowId', $this->session->orgRowId);
            $this->db->where('leaveapplication.userRowId', $this->input->post('userRowId'));
            $this->db->join('users', 'users.rowid = leaveapplication.userRowId');
            // $this->db->order_by('name');
            $query = $this->db->get();
        }

        // return($query->result_array());

        //////creating tmpla table
        $this->load->dbforge();
        if($this->db->table_exists('tmpla'))
        {
            $this->dbforge->drop_table('tmpla');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'userRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'user' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                )
            );  
        $this->dbforge->add_field($fields);


        $begin = new DateTime( $this->input->post('dtFrom') );
        $end = new DateTime( $this->input->post('dtTo') );
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $totalDays = 0;
        foreach ( $period as $dt )
        {
          $f = $dt->format( "Y-m-d" );
          $fields = array(
                        'd-'.$f => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => '100', 
                                          )
                                    );
                    $this->dbforge->add_field($fields);
                    $totalDays++;
        }
        $this->dbforge->create_table('tmpla');

        $rowId = 1;
        foreach ($query->result() as $row)
        {

            $data = array(
                        'rowId' => $rowId,
                        'userRowId' => $row->userRowId,
                        'user' => $row->uid
                    );
            $this->db->insert('tmpla', $data);               

            //// fetching duty
            foreach ( $period as $dt )
            {
                $f = $dt->format( "Y-m-d" );
                $this->db->select('dtFrom');
                $this->db->from('leaveapplication');
                $this->db->where('leaveapplication.userRowId', $row->userRowId );
                $this->db->where('leaveapplication.dtFrom <=', $f );
                $this->db->where('leaveapplication.dtTo >=', $f );

                // $this->db->where('leaveapplication.dt', $f);
                $queryLeaves = $this->db->get();        
                if ($queryLeaves->num_rows() > 0)
                {
                    $rowDuty = $queryLeaves->row_array();
                    $applied = 'LE';
                }    
                else
                {
                    $applied = '';
                }
                $data = array(
                        'd-'.$f => $applied
                    );
                $this->db->where('userRowId', $row->userRowId );
                $this->db->update('tmpla', $data);
            }

            $rowId++;
        }

        ///////// returning data
        $this->db->select('tmpla.*');
        // $this->db->order_by('tmpla.empName');
        $this->db->from('tmpla');
        $query = $this->db->get();
        return($query->result_array());
    }

}