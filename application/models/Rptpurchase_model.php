<?php
class Rptpurchase_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        if( $this->input->post('partyRowId') == "-1" )
        {
            // $this->db->distinct();
            //  $this->db->select('po.*, addressbook.name, podetail.billNo, podetail.billDt');
            // $this->db->from('po');
            // $this->db->join('parties','parties.partyRowId = po.partyRowId');
            // $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            // $this->db->join('podetail', 'podetail.poRowId = po.poRowId');
            // $this->db->where('po.vType', 'PI');
            // $this->db->where('po.deleted', 'N');
            // $this->db->where('po.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            //  $this->db->where('po.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            // $this->db->where('po.orgRowId', $this->session->orgRowId);
            // $this->db->order_by('po.poRowId desc');
            // // $this->db->limit(5);
            // $query = $this->db->get();

            // return($query->result_array());
            $this->db->distinct();
             $this->db->select('po.*, addressbook.name');
            $this->db->from('po');
            $this->db->join('parties','parties.partyRowId = po.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            // $this->db->join('podetail', 'podetail.poRowId = po.poRowId');
            $this->db->where('po.vType', 'PI');
            $this->db->where('po.deleted', 'N');
            $this->db->where('po.billDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('po.billDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('po.orgRowId', $this->session->orgRowId);
            $this->db->order_by('po.billDt');
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
        else
        {
            // $this->db->distinct();
            // $this->db->select('po.*, addressbook.name, podetail.billNo, podetail.billDt');
            // $this->db->from('po');
            // $this->db->join('parties','parties.partyRowId = po.partyRowId');
            // $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            // $this->db->join('podetail', 'podetail.poRowId = po.poRowId');
            // $this->db->where('po.vType', 'PI');
            // $this->db->where('po.deleted', 'N');
            //  $this->db->where('po.partyRowId', $this->input->post('partyRowId'));
            // $this->db->where('po.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            //  $this->db->where('po.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            // $this->db->where('po.orgRowId', $this->session->orgRowId);
            // $this->db->order_by('po.poRowId desc');
            // // $this->db->limit(5);
            // $query = $this->db->get();

            // return($query->result_array());

            $this->db->distinct();
            $this->db->select('po.*, addressbook.name');
            $this->db->from('po');
            $this->db->join('parties','parties.partyRowId = po.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            // $this->db->join('podetail', 'podetail.poRowId = po.poRowId');
            $this->db->where('po.vType', 'PI');
            $this->db->where('po.deleted', 'N');
             $this->db->where('po.partyRowId', $this->input->post('partyRowId'));
            $this->db->where('po.billDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('po.billDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('po.orgRowId', $this->session->orgRowId);
            $this->db->order_by('po.billDt');
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        } 
    }



    public function getProducts()
    {   //, colours.colourName
        $this->db->select('podetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }
}