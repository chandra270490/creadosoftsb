<?php
class Rptdsr_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
             $this->db->select('dsr.*, users.uid, addressbook.name, meetingremarks.meetingRemark, dsr_lead_stage_mst.dsr_ls_name');
             $this->db->from('dsr');
             $this->db->join('users','users.rowid = dsr.userRowId');
             $this->db->join('addressbook','addressbook.abRowId = dsr.abRowId');
             $this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId');
             $this->db->join('dsr_lead_stage_mst','dsr_lead_stage_mst.dsr_ls_id = dsr.cboLeadStage');
             
             $abRowId = explode(",", $this->input->post('abRowId'));
             // $this->db->where_in('dsr.abRowId', $abRowId );

             $this->db->group_start();
             $chunk = array_chunk($abRowId,25);
             foreach($chunk as $ab_ids)
             {
                $this->db->or_where_in('dsr.abRowId', $ab_ids);
             }
             $this->db->group_end();

             $userRowId = explode(",", $this->input->post('userRowId'));
             $this->db->where_in('dsr.userRowId', $userRowId );

             $chkleadstatus = explode(",", $this->input->post('chkleadstatus'));
             $this->db->where_in('dsr.cboLeadStage', $chkleadstatus );

             $this->db->where('dsr.dsrDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('dsr.dsrDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('dsr.orgRowId', $this->session->orgRowId);
             $this->db->order_by('dsr.dsrRowId');
             $query = $this->db->get();
             return($query->result_array());

    }




    public function getAbList()
    {
        $this->db->select('addressbook.abRowId, addressbook.name');
            $this->db->from('addressbook');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            // $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            // $this->db->join('states','states.stateRowId = districts.stateRowId');
            // $this->db->join('countries','countries.countryRowId = states.countryRowId');
            $this->db->where('addressbook.deleted', 'N');
            // $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();

            // return($query->result_array());
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['abRowId']]= $row['name'];
        }

        return $arr;
    }

    public function getUsers()
    {
        $this->db->select('users.rowid, users.uid');
            $this->db->from('users');
            $this->db->where('users.deleted', 'N');
            $this->db->order_by('users.uid');
            $query = $this->db->get();

            // return($query->result_array());
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['rowid']]= $row['uid'];
        }

        return $arr;
    }

    public function getleadstatus()
    {
        $this->db->select('dsr_lead_stage_mst.dsr_ls_id, dsr_lead_stage_mst.dsr_ls_name');
            $this->db->from('dsr_lead_stage_mst');
            $this->db->order_by('dsr_lead_stage_mst.dsr_ls_id');
            $query = $this->db->get();

            // return($query->result_array());
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['dsr_ls_id']]= $row['dsr_ls_name'];
        }

        return $arr;
    }
}