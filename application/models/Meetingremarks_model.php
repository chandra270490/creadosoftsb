<?php
class Meetingremarks_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('mrRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('meetingremarks');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('mrRowId');
		$query = $this->db->get('meetingremarks');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('meetingRemark');
		$this->db->where('meetingRemark', $this->input->post('meetingRemark'));
		$query = $this->db->get('meetingremarks');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('mrRowId');
		$query = $this->db->get('meetingremarks');
        $row = $query->row_array();

        $current_row = $row['mrRowId']+1;

		$data = array(
	        'mrRowId' => $current_row
	        , 'meetingRemark' => ucwords($this->input->post('meetingRemark'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('meetingremarks', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('meetingRemark');
		$this->db->where('meetingRemark', $this->input->post('meetingRemark'));
		$this->db->where('mrRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('meetingremarks');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'meetingRemark' => ucwords($this->input->post('meetingRemark'))
		);
		$this->db->where('mrRowId', $this->input->post('globalrowid'));
		$this->db->update('meetingremarks', $data);			
	}

	public function delete()
	{
		$data = array(
		        'deleted' => 'Y',
		        'deletedBy' => $this->session->userRowId

		);
		$this->db->set('deletedStamp', 'NOW()', FALSE);
		$this->db->where('mrRowId', $this->input->post('rowId'));
		$this->db->update('meetingremarks', $data);

		// $this->db->where('mrRowId', $this->input->post('rowId'));
		// $this->db->delete('meetingremarks');
	}

	public function getPrefixes()
	{
		$this->db->select('mrRowId, meetingRemark');
		$this->db->where('deleted', 'N');
		$this->db->order_by('meetingRemark');
		$query = $this->db->get('meetingremarks');

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['mrRowId']]= $row['meetingRemark'];
		}

		return $arr;
	}
}