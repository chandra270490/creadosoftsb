<?php
class Franchiseesm extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}

	//View List
	function ListHead($tbl_nm){
        $query = $this->db->query("SHOW columns FROM $tbl_nm where Field not in('password','admin_pass')");

        return $query;
    }
	
	public function franchisee_entry($data){ 
		$sql_franchisee_cnt = "select count(*) as count from franchisee_mst";
		$qry_franchisee_cnt = $this->db->query($sql_franchisee_cnt)->row();
		$count = $qry_franchisee_cnt->count;

		if($count == 0){
			//SVIPL-FRA-2020-00001;
			$franchisee_no = "SVIPL-FRA-".date("Y")."-".sprintf('%05d', 1);
		} else {
			$sql_franchisee_max = "select max(substring(franchisee_no,16,5)) as prev_no from franchisee_mst";
			$qry_franchisee_max = $this->db->query($sql_franchisee_max)->row();
			$prev_no = $qry_franchisee_max->prev_no;
			$new_no = $prev_no+1;
			
			$franchisee_no = "SVIPL-FRA-".date("Y")."-".sprintf('%05d', $new_no);
		}

		$franchisee_name = $this->input->post("franchisee_name");
		$franchisee_email = $this->input->post("franchisee_email");
		$franchisee_phone = $this->input->post("franchisee_phone");
		$franchisee_addr1 = $this->input->post("franchisee_addr1");
		$franchisee_addr2 = $this->input->post("franchisee_addr2");
		$franchisee_city = $this->input->post("franchisee_city");
		$franchisee_state = $this->input->post("franchisee_state");
		$franchisee_postcode = $this->input->post("franchisee_postcode");
		$franchisee_country  = $this->input->post("franchisee_country");
		$franchisee_created_by  = $this->session->userRowId;
		$franchisee_created_date = date('Y-m-d H:i:s');
		$franchisee_modified_by  = $this->session->userRowId;
		$franchisee_modified_date = date('Y-m-d H:i:s');

		//Trim
		$franchisee_name = trim($franchisee_name);
		$franchisee_email = trim($franchisee_email);
		$franchisee_phone = trim($franchisee_phone);
		$franchisee_addr1 = trim($franchisee_addr1);
		$franchisee_addr2 = trim($franchisee_addr2);
		$franchisee_city = trim($franchisee_city);
		$franchisee_state = trim($franchisee_state);
		$franchisee_postcode = trim($franchisee_postcode);
		$franchisee_country  = trim($franchisee_country);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into franchisee_mst(franchisee_no, franchisee_name, franchisee_email, franchisee_phone, franchisee_addr1, 
		franchisee_addr2, franchisee_city, franchisee_state, franchisee_postcode, 
		franchisee_country, franchisee_created_by, franchisee_created_date, franchisee_modified_by, 
		franchisee_modified_date) 
		values 
		('".$franchisee_no."', '".$franchisee_name."', '".$franchisee_email."', '".$franchisee_phone."', '".$franchisee_addr1."', 
		'".$franchisee_addr2."', '".$franchisee_city."', '".$franchisee_state."', '".$franchisee_postcode."', 
		'".$franchisee_country."', '".$franchisee_created_by."', '".$franchisee_created_date."', '".$franchisee_modified_by."', 
		'".$franchisee_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	}

	//Franchise Enquiry Entry
	public function franchisee_inq_entry($data){ 
		$sql_franchisee_cnt = "select count(*) as count from franchisee_inq_mst";
		$qry_franchisee_cnt = $this->db->query($sql_franchisee_cnt)->row();
		$count = $qry_franchisee_cnt->count;

		$fran_inq_no1 = $this->input->post("fran_inq_no");

		if($fran_inq_no1 == ""){

			if($count == 0){
				//SVIPL-FRA-2020-00001;
				$fran_inq_no = "SVIPL-INQ-".date("Y")."-".sprintf('%05d', 1);
			} else {
				$sql_franchisee_max = "select max(substring(fran_inq_no,16,5)) as prev_no from franchisee_inq_mst";
				$qry_franchisee_max = $this->db->query($sql_franchisee_max)->row();
				$prev_no = $qry_franchisee_max->prev_no;
				$new_no = $prev_no+1;
				
				$fran_inq_no = "SVIPL-INQ-".date("Y")."-".sprintf('%05d', $new_no);
			}

		} else {
			$fran_inq_no = $fran_inq_no1;
		}

		$fran_date = $this->input->post("fran_date");
		$fran_inq_name = $this->input->post("fran_inq_name");
		$fran_inq_phone = $this->input->post("fran_inq_phone");
		$fran_inq_whatsapp = $this->input->post("fran_inq_whatsapp");
		$fran_inq_email = $this->input->post("fran_inq_email");
		$fran_inq_addr1 = $this->input->post("fran_inq_addr1");
		$fran_inq_city = $this->input->post("fran_inq_city");
		$fran_inq_state = $this->input->post("fran_inq_state");
		$fran_inq_pin = $this->input->post("fran_inq_pin");
		$fran_inq_country  = $this->input->post("fran_inq_country");
		$fran_wfm_pref_loc = $this->input->post("fran_wfm_pref_loc");
		$fran_model = $this->input->post("fran_model");
		$fran_lead_source = $this->input->post("fran_lead_source");
		$fran_status = $this->input->post("fran_status");
		$fran_remarks = $this->input->post("fran_remarks");

		$fran_inq_created_by  = $this->session->userRowId;
		$fran_inq_created_date = date('Y-m-d H:i:s');
		$fran_inq_modified_by  = $this->session->userRowId;
		$fran_inq_modified_date = date('Y-m-d H:i:s');

		//Trim
		$fran_inq_name = trim($fran_inq_name);
		$fran_inq_phone = trim($fran_inq_phone);
		$fran_inq_whatsapp = trim($fran_inq_whatsapp);
		$fran_inq_email = trim($fran_inq_email);
		$fran_inq_addr1 = trim($fran_inq_addr1);
		$fran_inq_city = trim($fran_inq_city);
		$fran_inq_state = trim($fran_inq_state);
		$fran_inq_pin = trim($fran_inq_pin);
		$fran_inq_country = trim($fran_inq_country);
		$fran_wfm_pref_loc = trim($fran_wfm_pref_loc);
		$fran_model = trim($fran_wfm_pref_loc);
		$fran_lead_source = trim($fran_lead_source);
		$fran_status = trim($fran_status);
		$fran_remarks = trim($fran_remarks);

		//Transaction Start
		$this->db->trans_start();

		if($fran_inq_no1 == ""){
			//New Entry
			$sql = "insert into franchisee_inq_mst(fran_date, fran_inq_no, fran_inq_name, fran_inq_phone, fran_inq_whatsapp, fran_inq_email, 
			fran_inq_addr1, fran_inq_city, fran_inq_state, 
			fran_inq_pin, fran_inq_country, fran_wfm_pref_loc, fran_model, 
			fran_lead_source, fran_status, fran_remarks,fran_inq_created_by, 
			fran_inq_created_date, fran_inq_modified_by, fran_inq_modified_date) 
			values 
			('".$fran_date."','".$fran_inq_no."', '".$fran_inq_name."', '".$fran_inq_phone."', '".$fran_inq_whatsapp."', '".$fran_inq_email."', 
			'".$fran_inq_addr1."', '".$fran_inq_city."', '".$fran_inq_state."', 
			'".$fran_inq_pin."', '".$fran_inq_country."', '".$fran_wfm_pref_loc."', '".$fran_model."', 
			'".$fran_lead_source."', '".$fran_status."', '".$fran_remarks."', '".$fran_inq_created_by."',
			'".$fran_inq_created_date."', '".$fran_inq_modified_by."', '".$fran_inq_modified_date."')";

			$this->db->query($sql);

			//Remark History
			$sql_rmk_hist = "insert into franchisee_rmk_hist(fran_inq_no, fran_remarks, created_by) 
			values ('".$fran_inq_no."', '".$fran_remarks."', '".$fran_inq_created_by."')";

			$this->db->query($sql_rmk_hist);

		} else {
			//Update Entry
			$sql = "update franchisee_inq_mst set fran_date='".$fran_date."', fran_inq_name='".$fran_inq_name."',
			fran_inq_phone='".$fran_inq_phone."', fran_inq_whatsapp='".$fran_inq_whatsapp."', fran_inq_email='".$fran_inq_email."', fran_inq_addr1='".$fran_inq_addr1."', 
			fran_inq_city='".$fran_inq_city."', fran_inq_state='".$fran_inq_state."', fran_inq_pin='".$fran_inq_pin."', 
			fran_inq_country='".$fran_inq_country."', fran_wfm_pref_loc='".$fran_wfm_pref_loc."', fran_model='".$fran_model."', 
			fran_lead_source='".$fran_lead_source."', fran_status='".$fran_status."', fran_remarks='".$fran_remarks."', 
			fran_inq_modified_by='".$fran_inq_modified_by."', fran_inq_modified_date='".$fran_inq_modified_date."'
			where fran_inq_no = '".$fran_inq_no."'";

			$this->db->query($sql);

			//Remark History
			$sql_rmk_hist = "insert into franchisee_rmk_hist(fran_inq_no, fran_remarks, created_by) 
			values ('".$fran_inq_no."', '".$fran_remarks."', '".$fran_inq_created_by."')";

			$this->db->query($sql_rmk_hist);
		}

		$this->db->trans_complete();
		//Transanction Complete
	}

	//inquires entries excel
	function excel_insert($data){

		$row_cnt = count($data);
		$sql_franchisee_max = "select max(substring(fran_inq_no,16,5)) as prev_no from franchisee_inq_mst";
		$qry_franchisee_max = $this->db->query($sql_franchisee_max)->row();
		$prev_no = $qry_franchisee_max->prev_no;

		$new_no = $prev_no;
		for($i=0;$i<$row_cnt;$i++){
			$new_no++;

			$fran_inq_no = "SVIPL-INQ-".date("Y")."-".sprintf('%05d', $new_no);

			$sql = "insert into franchisee_inq_mst
			(fran_date, fran_inq_no, fran_inq_name, fran_inq_phone, fran_inq_whatsapp,
			fran_inq_email, fran_inq_addr1, fran_inq_city, fran_inq_state,
			fran_inq_pin, fran_inq_country, fran_wfm_pref_loc, fran_model,
			fran_lead_source, fran_status, fran_remarks, fran_inq_created_by,
			fran_inq_created_date, fran_inq_modified_by) 
			values
			('".$data[$i]['fran_date']."','".$fran_inq_no."','".$data[$i]['fran_inq_name']."','".$data[$i]['fran_inq_phone']."','".$data[$i]['fran_inq_whatsapp']."',
			'".$data[$i]['fran_inq_email']."','".$data[$i]['fran_inq_addr1']."','".$data[$i]['fran_inq_city']."','".$data[$i]['fran_inq_state']."',
			'".$data[$i]['fran_inq_pin']."','".$data[$i]['fran_inq_country']."','".$data[$i]['fran_wfm_pref_loc']."','".$data[$i]['fran_model']."',
			'".$data[$i]['fran_lead_source']."','".$data[$i]['fran_status']."','".$data[$i]['fran_remarks']."','".$data[$i]['fran_inq_created_by']."',
			'".$data[$i]['fran_inq_created_date']."','".$data[$i]['fran_inq_modified_by']."')";

			$this->db->query($sql);
		}

		//$this->db->insert_batch('franchisee_inq_mst', $data);
	}

	//Franchisee Inquiry Count
	function fran_inq_count(){
		$query = $this->db->query("select count(*) as count from franchisee_inq_mst");
		return $query;
	}

	//Franchisee Max Number
	function fran_max_no(){
		$query = $this->db->query("select max(substring(fran_inq_no,16,5)) as prev_no from franchisee_inq_mst");
		return $query;
	}
	 
   }  
?>