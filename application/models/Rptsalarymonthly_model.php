<?php
class Rptsalarymonthly_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }


    public function getDataForReport()
    {
         set_time_limit(0);

        $this->db->select('salarymonthly.*, addressbook.name as empName');
        $this->db->order_by('empName');
        $this->db->where('salarymonthly.dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('salarymonthly.dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('salarymonthly.orgRowId', $this->session->orgRowId);
        $this->db->join('employees','employees.empRowId = salarymonthly.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->from('salarymonthly');
        $query = $this->db->get();
        return($query->result_array());
    }

  
}