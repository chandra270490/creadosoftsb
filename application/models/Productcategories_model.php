<?php
class Productcategories_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('productCategoryRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('productcategories');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('productCategoryRowId');
		$query = $this->db->get('productcategories');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('productCategory');
		$this->db->where('productCategory', $this->input->post('productCategory'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('productcategories');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('productCategoryRowId');
		$query = $this->db->get('productcategories');
        $row = $query->row_array();

        $current_row = $row['productCategoryRowId']+1;

		$data = array(
	        'productCategoryRowId' => $current_row
	        , 'productCategory' => ucwords($this->input->post('productCategory'))
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('productcategories', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('productCategory');
		$this->db->where('productCategory', $this->input->post('productCategory'));
		$this->db->where('productCategoryRowId !=', $this->input->post('globalrowid'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('productcategories');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'productCategory' => ucwords($this->input->post('productCategory'))
	        , 'orgRowId' => $this->session->orgRowId
		);
		$this->db->where('productCategoryRowId', $this->input->post('globalrowid'));
		$this->db->update('productcategories', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('productCategoryRowId', $this->input->post('rowId'));
		$this->db->delete('productcategories');
	}

	public function getProductCategories()
	{
		$this->db->select('productCategoryRowId, productCategory');
		$this->db->where('deleted', 'N');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('productCategory');
		$query = $this->db->get('productcategories');

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['productCategoryRowId']]= $row['productCategory'];
		}

		return $arr;
	}

	public function getProductmc()
	{
		$this->db->select('ProductMCId, productMCName');
		$this->db->where('deleted', 'N');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('productMCName');
		$query = $this->db->get('productmcc');

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['ProductMCId']]= $row['productMCName'];
		}

		return $arr;
	}

	
}