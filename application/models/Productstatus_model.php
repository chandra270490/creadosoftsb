<?php
class Productstatus_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getProductList()
    {
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }
    
    public function getStages4table()
    {
        $this->db->select('*');
        $this->db->where('deleted', 'N');
        $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('odr');
        $query = $this->db->get('stages');

        return($query->result_array());
    }

    public function getProducts4CheckBox()
    {
        $this->db->select('productRowId, productName');
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get('products');
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productRowId']]= $row['productName'];
        }

        return $arr;
    }

    public function getDataFromPhpArray()
    {
        set_time_limit(0);
        $this->db->select('products.*, productcategories.productCategory');
        $this->db->from('products');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $productRowId = explode(",", $this->input->post('productRowId'));
        $this->db->where_in('products.productRowId', $productRowId );
        $this->db->order_by('productName');
        $query = $this->db->get();

        $rows = array();
        foreach ($query->result_array() as $row)
        {
            $row['stageRowId']="";
            $this->db->select('stageRowId, stageName');
            $this->db->where('deleted', 'N');
            $this->db->where('stages.orgRowId', $this->session->orgRowId);
            $this->db->order_by('odr');
            // $this->db->limit(1);
            $queryStages = $this->db->get('stages');
            $x=1;
            foreach ($queryStages->result_array() as $rowStages)
            {
                ///// pending qty from orders
                $this->db->select_sum('pendingQty');
                $this->db->where('productRowId',  $row['productRowId']);
                $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                $this->db->join('qpo',"qpo.qpoRowId = qpodetail.qpoRowId AND qpo.deleted='N'");
                $this->db->from('qpodetail');
                $pQty=0;
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $pQty = $row1->pendingQty;
                }
                $row['pendingQty'] = $pQty;


                ///// avalable qty EXCEPT Desp Stage
                $this->db->select('openingbal.*, odr');
                $this->db->where('productRowId',  $row['productRowId']);
                $this->db->where('openingbal.stageRowId', $rowStages['stageRowId']);
                $this->db->where('openingbal.stageRowId<',  26);
                $this->db->order_by('odr');
                $this->db->from('openingbal');
                $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
                $query1 = $this->db->get();
                $avlQty=0;
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $avlQty = $row1->avlQty;
                }
                $row['S'.$rowStages['stageRowId']] = $avlQty;

                ///// avalable qty Desp Stage only Sum at desp stage of all colours
                $this->db->select_sum('avlQty');
                $this->db->where('productRowId', $row['productRowId']);
                $this->db->where('openingbal.stageRowId',  26);
                // $this->db->where('openingbal.avlQty>', 0); //// this line on 04-Jan-2020
                $this->db->order_by('odr');
                $this->db->from('openingbal');
                $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
                $query1 = $this->db->get();
                $avlQty=0;
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $avlQty = $row1->avlQty;
                }
                $row['S26'] = $avlQty;
                
                // $data = array(
                //     'S26' => $avlQty
                // );
                // $this->db->where('productRowId',  $row->productRowId);
                
                // $this->db->update('tmp', $data);
            }

            // $rowStages = $queryStages->row_array();
            // if (isset($rowStages))
            // {
            //     $row['stageRowId'] = $rowStages['stageRowId'];
            // }
            
            $rows[] = $row;
        }

        return $rows;
    }

    public function getData()
    {
        set_time_limit(0);
        //////creating tmp table
        $this->load->dbforge();
        if($this->db->table_exists('tmp'))
        {
            $this->dbforge->drop_table('tmp');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                             
                                ),
                    'productName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                             
                                ),
                    'pendingQty' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'imagePathThumb' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',     
                                )
            );  
        $this->dbforge->add_field($fields);
        /////// adding stages
        $this->db->select('stageRowId, stageName');
        $this->db->where('deleted', 'N');
        // $this->db->where('stageRowId <', 26); ///Desp Stages
        $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('odr');
        $query = $this->db->get('stages');

        $stages = array();
        foreach ($query->result() as $row)
        {
            array_push($stages, $row->stageRowId);
            $fields = array(
                        'S'.$row->stageRowId => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => '5', 
                                                 'default' => '0'
                                          )
                );  

            $this->dbforge->add_field($fields);     
        }

        $this->dbforge->create_table('tmp');
        ////// END - creating tmp table

        ////// Creating array of stage (like 1,2,3,4)
        $fields = $this->db->list_fields('tmp');
        $totalstages = count($fields)-5;    //coz intial 5 fields are fix
        $stagesArray = array('');
        foreach ($fields as $field)
        {
            array_push($stagesArray, substr($field, 1, strlen($field)));
        }
        array_shift($stagesArray);  //removing first 5 fields (rowId, proRowId, ProName)
        array_shift($stagesArray);
        array_shift($stagesArray);
        array_shift($stagesArray);
        array_shift($stagesArray);
        array_shift($stagesArray);
        // return $stagesArray;

        ///////////////////// loading products in tmp table
        $this->db->select('products.*, productcategories.productCategory');
        $this->db->from('products');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $productRowId = explode(",", $this->input->post('productRowId'));
        $this->db->where_in('products.productRowId', $productRowId );
        $this->db->order_by('productName');
 // $this->db->limit(10);
        $query = $this->db->get();
        $rowId = 1;
        foreach ($query->result() as $row)
        {
            $data = array(
                    'rowId' => $rowId,
                    'productRowId' => $row->productRowId,
                    'imagePathThumb' => $row->imagePathThumb,
                    'productName' => $row->productName . ' - ' . $row->productCategory . ' [' . $row->productLength . 'x' . $row->productWidth . 'x' . $row->productHeight . ' ' . $row->uom . ']'
            );
            $this->db->insert('tmp', $data);               
            $rowId++;


            
            for($i=0; $i<count($stagesArray)-1; $i++)
            {
                ///// pending qty from orders
                $this->db->select_sum('pendingQty');
                $this->db->where('productRowId',  $row->productRowId);
                $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                $this->db->join('qpo',"qpo.qpoRowId = qpodetail.qpoRowId AND qpo.deleted='N'");
      
                $this->db->from('qpodetail');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $pQty = $row1->pendingQty;
                }
                else
                {
                    $pQty=0;
                }

                $data = array(
                    'pendingQty' => $pQty
                );
                $this->db->where('productRowId',  $row->productRowId);
                $this->db->update('tmp', $data);

                ///// avalable qty EXCEPT Desp Stage
                $this->db->select('openingbal.*, odr');
                $this->db->where('productRowId',  $row->productRowId);
                $this->db->where('openingbal.stageRowId', $stagesArray[$i]);
                $this->db->order_by('odr');
                $this->db->from('openingbal');
                $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $avlQty = $row1->avlQty;
                }
                else
                {
                    $avlQty=0;
                }

                $data = array(
                    'S'.$stagesArray[$i] => $avlQty
                );
                $this->db->where('productRowId',  $row->productRowId);
                
                $this->db->update('tmp', $data);
            }   ////inner for loop ends here

            ///// avalable qty Desp Stage only
            $this->db->select_sum('avlQty');
            $this->db->where('productRowId',  $row->productRowId);
            $this->db->where('openingbal.stageRowId',  26);
            $this->db->order_by('odr');
            $this->db->from('openingbal');
            $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
            $query1 = $this->db->get();
            if ($query1->num_rows() > 0)
            {
                $row1 = $query1->row(); 
                $avlQty = $row1->avlQty;
            }
            else
            {
                $avlQty=0;
            }

            $data = array(
                'S26' => $avlQty
            );
            $this->db->where('productRowId',  $row->productRowId);
            
            $this->db->update('tmp', $data);
        }   ////outer for loop ends here


        /// now sending data to client
        $this->db->select('tmp.*');
        $this->db->order_by('rowId');
        $this->db->from('tmp');
        $query = $this->db->get();
        return($query->result_array());
    }



    // public function getPendingOrders()
    // {
    //     $this->db->select_sum('pendingQty');
    //     $this->db->where('productRowId',  $this->input->post('productRowId'));
    //     // $this->db->order_by('odr');
    //     $this->db->from('qpodetail');
    //     // $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
    //     $query = $this->db->get();

    //     return($query->result_array());
    // }

}