<?php
class Rptpurchaseproductwise_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getProducts()
    {
        $this->db->select('productRowId, productName');
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get('products');
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productRowId']]= $row['productName'];
        }
        return $arr;
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        $this->db->select('podetail.*, productcategories.productCategory, products.productName, addressbook.name, po.vType, po.vNo, po.billDt, po.partyRowId');
        // $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
        $this->db->where('po.billDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('po.billDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $productRowId = explode(",", $this->input->post('productRowId'));
        $this->db->where_in('podetail.productRowId', $productRowId );
        $partyRowId = explode(",", $this->input->post('partyRowId'));
        $this->db->where_in('po.partyRowId', $partyRowId );
        $this->db->from('podetail');
        $this->db->join('po','po.poRowId = podetail.poRowId');
        $this->db->join('parties','parties.partyRowId = po.partyRowId');
        $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('po.billDt, po.poRowId, podetail.poDetailRowId');
        
        $query = $this->db->get();

        return($query->result_array());
        
    }



    // public function getProducts()
    // {   //, colours.colourName
    //     $this->db->select('podetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
    //     $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
    //     $this->db->from('podetail');
    //     $this->db->join('products','products.productRowId = podetail.productRowId');
    //     $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
    //     $this->db->order_by('poDetailRowId');
    //     $query = $this->db->get();
    //     return($query->result_array());
    // }
}