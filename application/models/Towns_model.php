<?php
class Towns_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('towns.*, districts.districtName, states.stateRowId, states.stateName, countries.countryRowId, countries.countryName');
		$this->db->from('towns');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('towns.deleted', 'N');
		$this->db->order_by('towns.townRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('towns.*, districts.districtName, states.stateRowId, states.stateName, countries.countryRowId, countries.countryName');
		$this->db->from('towns');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('towns.deleted', 'N');
		$this->db->order_by('towns.townRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('townName');
		$this->db->where('townName', $this->input->post('townName'));
		$query = $this->db->get('towns');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('townRowId');
		$query = $this->db->get('towns');
        $row = $query->row_array();

        $current_row = $row['townRowId']+1;
		$data = array(
	        'townRowId' => $current_row
	        , 'townName' => ucwords($this->input->post('townName'))
	        , 'districtRowId' => $this->input->post('districtRowId')
	        , 'pin' => $this->input->post('pin')
	        , 'std' => $this->input->post('std')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('towns', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('townName');
		$this->db->where('townName', $this->input->post('townName'));
		$this->db->where('townRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('towns');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'townName' => ucwords($this->input->post('townName'))
	        , 'districtRowId' => $this->input->post('districtRowId')	        
	        , 'pin' => $this->input->post('pin')
	        , 'std' => $this->input->post('std')
		);
		$this->db->where('townRowId', $this->input->post('globalrowid'));
		$this->db->update('towns', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('townRowId', $this->input->post('rowId'));
		$this->db->delete('towns');
	}

	public function getTownList()
	{
		$this->db->select('towns.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('towns');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('towns.deleted', 'N');
		$this->db->order_by('towns.townName');
		$query = $this->db->get();

		return($query->result_array());
	}

	public function getTownListRefresh()
	{
		$this->db->select('towns.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('towns');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('towns.deleted', 'N');
		$this->db->order_by('towns.townName');
		$query = $this->db->get();

		return($query->result_array());
	}
}