<?php
class Login_model extends CI_Model {

        public function __construct()
        {
                $this->load->database('');
                $this->load->model('Loghash_model');
        }
        
		// public function checkuser($uid, $pwd)
		// {
		// 		// How to secure from SQL injections and other threat ...???
		// 		// SHOULD we use md5(..) encryption..??
				
		//         $query = $this->db->get_where('users', array('uid' => $uid, 'pwd' => $pwd));
		//         return $query->row_array();
		// }

		public function checkuser($uid, $pwd, $org)
		{

		        $query = $this->db->get_where('users', array('uid' => $uid));
		        // if(count($query)==1)
		        {
		        	$row = $query->row(); 
		        	if ($query->num_rows()>0)
		        	{
		        		if( $org == 1 ) ///if credo selected
		        		{
		        			$dbPwd = $row->pwd;
		        		}
		        		else if( $org == 2 )  ///if kinny selected
		        		{
		        			$dbPwd = $row->pwd2;
		        		}
			        	// passing password typed by user ($pwd) and db password. 
			        	$res = $this->Loghash_model->validate_password($pwd, $dbPwd);
			        	if($res==1)	// authenticates successfully
			        	{
			        		return $query->row_array();
			        	}
		        	}
		        }
		        // return();
		}
		public function insert_session($uid)
		{
			$query = $this->db->query('SELECT max(rowid) as rowid FROM sessionlog ORDER BY rowid');
			if ($query->num_rows() > 0)
			{
			        $row = $query->row_array();

			        $current_row = $row['rowid']+1;		// getting next rowid

					// Getting rowid of logged in user
					$query = $this->db->query("SELECT rowid  FROM users WHERE uid='".$uid."'");
					$user_row = $query->row_array();
					// END - Getting rowid of logged in user
					
					// $this->load->helper('date');
					// $datetime = unix_to_human(now('asia/kolkata'));

					$session_id = $this->session->session_id;

					// $sqlStr = 'INSERT INTO sessionlog (`rowid`,`userrowid`,`sessionid`,`loginstamp`) VALUES(' . $current_row . ', ' . $user_row['rowid'] . ',"' . $session_id . '", NOW())' ;
					// $this->db->query($sqlStr);

					$data = array(
					        'rowid' => $current_row,
					        'userrowid' => $user_row['rowid'],
					        'orgrowid' => $_POST['cboOrg'],
					        'sessionid' => $session_id
					);
					$this->db->set('loginstamp', 'NOW()', FALSE);
					$this->db->insert('sessionlog', $data);				

			}
		}

		public function logout_session($sid)
		{
			$sqlStr = "update sessionlog set logoutstamp=NOW() where sessionid='".$sid."'";
			$this->db->query($sqlStr);
			// unset($_SESSION['userid']);			
		}


	public function getOrgList()
	{

		$this->db->select('organisations.*');
		$this->db->from('organisations');
		$this->db->where('active', 1 );
		$this->db->order_by('organisations.orgName');
		$query = $this->db->get();

		$arr = array();
		//$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['orgRowId']]= $row['orgName'];
		}
		return $arr;

	}

	public function getOrgDetail($rowId)
	{

		$this->db->select('organisations.*');
		$this->db->from('organisations');
		$this->db->where('orgRowId', $rowId);
		$this->db->order_by('organisations.orgName');
		$query = $this->db->get();

		return($query->result_array());

	}

	public function getImageFile()
	{

		$this->db->select('organisations.imagePath');
		$this->db->from('organisations');
		$this->db->where('orgRowId', $_POST['cboOrg']);
		$query = $this->db->get();

		return $query->result_array();
	}


	public function getNotifications()
	{
		$this->db->select('notifications.*, users.uid');
		$this->db->from('notifications');
		$this->db->where('padhLiya', 'N');
		$this->db->where('userRowIdFor', $this->session->userRowId);
		$this->db->join('users', 'users.rowid = notifications.createdBy');
		$this->db->order_by('notifications.notificationRowId desc');
		$query = $this->db->get();

		return $query->result_array();

	}

	public function markPadhLiya()
	{
		$data = array(
	        'padhLiya' => 'Y'
		);
		$this->db->where('notificationRowId', $this->input->post('notificationRowId'));
		$this->db->update('notifications', $data);		
	}

}