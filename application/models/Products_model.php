<?php
class Products_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getRowId()
    {
		$this->db->select_max('productRowId');
		$query = $this->db->get('products');
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
		    $current_row = $row['productRowId'] + 1;
		    return $current_row;
		}
	}

    public function getDataLimit()
	{
		$this->db->select('products.*, productcategories.productCategory');
		$this->db->from('products');
		$this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
		$this->db->where('products.deleted', 'N');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('products.productRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

	public function getDataLimitDel()
	{
		$this->db->select('products.*, productcategories.productCategory');
		$this->db->from('products');
		$this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
		$this->db->where('products.deleted', 'Y');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('products.productRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('products.*, productcategories.productCategory');
		$this->db->from('products');
		$this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
		$this->db->where('products.deleted', 'N');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('products.productRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

	public function getDataAllDel()
	{
		$this->db->select('products.*, productcategories.productCategory');
		$this->db->from('products');
		$this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
		$this->db->where('products.deleted', 'Y');
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('products.productRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('productName');
		$this->db->where('productName', $_REQUEST['txtProductName']);
		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
			$this->db->select_max('productRowId');
			$query = $this->db->get('products');
			if ($query->num_rows() > 0)
			{
			        $row = $query->row_array();
			        $current_row = $row['productRowId'] + 1;

				

					if($_FILES["imageFile"]["name"] != "")
					{
          				$rowNum = sprintf("%04d", $current_row);
          				$fileName = $rowNum . '.jpg';
          				$fileNameThumb = $rowNum . '_thumb' . '.jpg';
          			}
          			else
	  				{
	  					$fileName = "N";
	  					$fileNameThumb = "N";
	  				}

					        // 
					$data = array(
					        'productRowId' => $current_row,
					        'productCategoryRowId' => $_REQUEST['cboProductCategories'],
					        'productName' => ucwords($_REQUEST['txtProductName']),
					        'weightage' => (float)$_REQUEST['txtWeightage'],
					        'productLength' => (float)$_REQUEST['txtLength'],
					        'productHeight' => (float)$_REQUEST['txtHeight'],
					        'productWidth' => (float)$_REQUEST['txtWidth'],
					        'uom' => $_REQUEST['cboUom'],
					        'roLevel' => (float)$_REQUEST['txtRoLevel'],
					        'hsn' => $_REQUEST['txtHsn'],
					        'productRate' => (float)$_REQUEST['txtRate'],
					        'imagePath' => $fileName,
					        'imagePathThumb' => $fileNameThumb,
					        'createdBy' => $this->session->userRowId
					);
					$this->db->set('createdStamp', 'NOW()', FALSE);
					$this->db->insert('products', $data);				
			}


	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('productName');
		$this->db->where('productName', $_REQUEST['txtProductName']);
		$this->db->where('productRowId !=', $_REQUEST['txtHiddenRowId']);
		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		if($_REQUEST['txtPhotoChange'] == "yes")
		{
			if($_FILES["imageFile"]["name"] != "")
			{
				$rowNum = sprintf("%04d", $_REQUEST['txtHiddenRowId']);
				$fileName = $rowNum . '.jpg';
				$fileNameThumb = $rowNum . '_thumb' . '.jpg';
			}
			else
			{
				$fileName = "N";
				$fileNameThumb = "N";
			}
			
			$data = array(
			        'productCategoryRowId' => $_REQUEST['cboProductCategories'],
			        'productName' => ucwords($_REQUEST['txtProductName']),
			        'weightage' => (float)$_REQUEST['txtWeightage'],
			        'productLength' => (float)$_REQUEST['txtLength'],
			        'productHeight' => (float)$_REQUEST['txtHeight'],
			        'productWidth' => (float)$_REQUEST['txtWidth'],
					'uom' => $_REQUEST['cboUom'],
					'roLevel' => (float)$_REQUEST['txtRoLevel'],
					'hsn' => $_REQUEST['txtHsn'],
			        'productRate' => (float)$_REQUEST['txtRate'],
			        'imagePath' => $fileName,
			        'imagePathThumb' => $fileNameThumb,
			        'createdBy' => $this->session->userRowId,
			        'deleted' => $_REQUEST['deleted']
			);
		}
		else // if no change in photo
		{
			$data = array(
			        'productCategoryRowId' => $_REQUEST['cboProductCategories'],
			        'productName' => $_REQUEST['txtProductName'],
			        'weightage' => ($_REQUEST['txtWeightage']),
			        'productLength' => (float)$_REQUEST['txtLength'],
			        'productHeight' => (float)$_REQUEST['txtHeight'],
			        'productWidth' => (float)$_REQUEST['txtWidth'],
			        'productRate' => (float)$_REQUEST['txtRate'],
			        'roLevel' => (float)$_REQUEST['txtRoLevel'],
			        'hsn' => $_REQUEST['txtHsn'],
					'createdBy' => $this->session->userRowId,
					'deleted' => $_REQUEST['deleted']
			);
		}
		// $this->db->where('rowId', $this->input->post('rowid'));
		$this->db->where('productRowId', $_REQUEST['txtHiddenRowId']);
		$this->db->update('products', $data);		
	}

	public function delete()
	{
		if ( $this->input->post('globalImageSrc') != "" )
		{
			$tmp = $this->input->post('rowId');
			$rowId = sprintf("%04d", $tmp);
			$tmp = FCPATH ."/bootstrap/images/products/".$rowId.".jpg";
			unlink($tmp); //to delete image file
			$tmp = FCPATH ."/bootstrap/images/products/".$rowId."_thumb.jpg";
			unlink($tmp); //to delete image file
		}

		$this->db->where('productRowId', $this->input->post('rowId'));
		$this->db->delete('products');
	}

	public function getTownList()
	{
		$this->db->select('products.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('products');
		$this->db->join('districts','districts.productCategoryRowId = products.productCategoryRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('products.deleted', 'N');
		$this->db->order_by('products.productName');
		$query = $this->db->get();

		return($query->result_array());
	}


	public function getDataForReport()
	{
		$this->db->select('products.*, productcategories.productCategory');
		$this->db->from('products');
		$this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
		$this->db->where('products.deleted', 'N');
		if ($this->input->post('productCategoryRowId') == "ALL")
		{

		}
		else
		{
			$this->db->where('products.productCategoryRowId', $this->input->post('productCategoryRowId'));
			// $this->db->where('products.productCategoryRowId', 1);
		}
		// $this->db->where('productcategories.orgRowId', $this->session->orgRowId);
		$this->db->order_by('products.productName');
		$query = $this->db->get();

		return($query->result_array());
	}	

	public function prod_uom_entry(){
		$prod_uom_id = $this->input->post("prod_uom_id");
		$prod_uom_id1 = $this->input->post("prod_uom_id");
		$prod_uom_name = $this->input->post("prod_uom_name");
		$created_by  = $this->session->userRowId;

		if($prod_uom_id1 == ""){
			$sql_ins = $this->db->query("insert into prod_uom_mst(prod_uom_name, created_by) 
			values('".$prod_uom_name."', '".$created_by."')");
		} else {
			$sql_updt = $this->db->query("update prod_uom_mst 
			set prod_uom_name = '".$prod_uom_name."', created_by = '".$created_by."' 
			where prod_uom_id = '".$prod_uom_id."'");
		}
	}
}