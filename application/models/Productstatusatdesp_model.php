<?php
class Productstatusatdesp_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getProductList()
    {
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }
    
    public function getGodowns4table()
    {
        // $this->db->select('*');
        // $this->db->where('deleted', 'N');
        // $this->db->where('stages.orgRowId', $this->session->orgRowId);
        // $this->db->order_by('odr');
        // $query = $this->db->get('stages');

        $this->db->select('addressbook.name, addressbook.abRowId');
        $this->db->from('addressbook');
        $this->db->where('addressbook.deleted', 'N');
        $this->db->where('addressbook.godown', 'Y');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();


        return($query->result_array());
    }

    public function getProducts4CheckBox()
    {
        $this->db->select('productRowId, productName');
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get('products');
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productRowId']]= $row['productName'];
        }

        return $arr;
    }

    public function getData()
    {
        set_time_limit(0);
        //////creating tmp table
        $this->load->dbforge();
        if($this->db->table_exists('tmp'))
        {
            $this->dbforge->drop_table('tmp');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                             
                                ),
                    'productName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                             
                                ),
                    'colourRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                             
                                ),
                    'colourName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                             
                                ),
                    'pendingQty' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                     'null' => TRUE,
                                ),
                    'despStageQty' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                )
            );  
        $this->dbforge->add_field($fields);
        /////// adding Godowns
        $this->db->select('addressbook.name, addressbook.abRowId');
        $this->db->from('addressbook');
        $this->db->where('addressbook.deleted', 'N');
        $this->db->where('addressbook.godown', 'Y');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();

        $godowns = array();
        foreach ($query->result() as $row)
        {
            array_push($godowns, $row->abRowId);
            $fields = array(
                        'G'.$row->abRowId => array(
                             'type' => 'VARCHAR',
                             'constraint' => '5', 
                             'default' => '0'
                          )
                );  

            $this->dbforge->add_field($fields);     
        }

        $this->dbforge->create_table('tmp');
        ////// END - creating tmp table
        ////// Creating array of godowns (like 1,2,3,4)
        $fields = $this->db->list_fields('tmp');
        $totalGodowns = count($fields)-7;    //coz intial 5 fields are fix
        $godownArray = array('');
        foreach ($fields as $field)
        {
            array_push($godownArray, substr($field, 1, strlen($field)));
        }
        array_shift($godownArray);  //removing first 7 fields (rowId, proRowId, ProName)
        array_shift($godownArray);
        array_shift($godownArray);
        array_shift($godownArray);
        array_shift($godownArray);
        array_shift($godownArray);
        array_shift($godownArray);
        array_shift($godownArray);


        // ///////////////////// loading products in tmp table
         $this->db->select('openingbal.productRowId, openingbal.colourRowId, openingbal.avlQty, products.productName, colours.colourName, products.productLength, products.productWidth, products.productHeight, products.uom');
        $this->db->from('openingbal');
        $this->db->where('stageRowId', 26); ///Desp Stages
        $this->db->where('openingbal.orgRowId', $this->session->orgRowId);
        $this->db->join('products','products.productRowId = openingbal.productRowId');
        $this->db->join('colours','colours.colourRowId = openingbal.colourRowId');
        
        $productRowId = explode(",", $this->input->post('productRowId'));
        $this->db->where_in('products.productRowId', $productRowId );
        $this->db->order_by('productName');

        $query = $this->db->get();
        $rowId = 1;
        foreach ($query->result() as $row)
        {
            $data = array(
                    'rowId' => $rowId,
                    'productRowId' => $row->productRowId,
                    'productName' => $row->productName . ' - <i>' . $row->colourName . '</i> [' . $row->productLength . 'x' . $row->productWidth . 'x' . $row->productHeight . ' ' . $row->uom . ']',
                    'colourRowId' => $row->colourRowId,
                    'colourName' => $row->colourName,
                    'despStageQty' => $row->avlQty
            );
            $this->db->insert('tmp', $data);               
            $rowId++;

            ////Godown Qty
            for($i=0; $i<count($godownArray); $i++)
            {
                $this->db->select('avlQty');
                $this->db->where('productRowId',  $row->productRowId);
                $this->db->where('colourRowId', $row->colourRowId);
                $this->db->where('godownAbRowId', $godownArray[$i]);
                $this->db->where('orgRowId', $this->session->orgRowId);
      
                $this->db->from('godownstatus');
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $aQty = $row1->avlQty;
                }
                else
                {
                    $aQty=0;
                }

                $data = array(
                    'G'.$godownArray[$i] => $aQty

                );
                $this->db->where('productRowId',  $row->productRowId);
                $this->db->where('colourRowId', $row->colourRowId);
                // $this->db->where('productRowId',  62);
                // $this->db->where('colourRowId', 19);
                $this->db->update('tmp', $data);
            }

            ///// pending qty from orders
            $this->db->select_sum('pendingQty');
            $this->db->where('productRowId',  $row->productRowId);
            $this->db->where('colourRowId',  $row->colourRowId);
            $this->db->where('qpo.orgRowId', $this->session->orgRowId);
            $this->db->join('qpo',"qpo.qpoRowId = qpodetail.qpoRowId AND qpo.deleted='N'");
            $this->db->from('qpodetail');
            $query1 = $this->db->get();
            if ($query1->num_rows() > 0)
            {
                $row1 = $query1->row(); 
                $pQty = $row1->pendingQty;
            }
            else
            {
                $pQty=0;
            }

            $data = array(
                'pendingQty' => $pQty
            );
            $this->db->where('productRowId',  $row->productRowId);
            $this->db->where('colourRowId',  $row->colourRowId);
            $this->db->update('tmp', $data);


        }   ////outer for loop ends here


        /// now sending data to client
        $this->db->select('tmp.*');
        $this->db->order_by('rowId');
        $this->db->from('tmp');
        $query = $this->db->get();

        return($query->result_array());
    }

    // public function getPendingOrders()
    // {
    //     $this->db->select_sum('pendingQty');
    //     $this->db->where('productRowId',  $this->input->post('productRowId'));
    //     // $this->db->order_by('odr');
    //     $this->db->from('qpodetail');
    //     // $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
    //     $query = $this->db->get();

    //     return($query->result_array());
    // }

}