<?php
class Openingbalparties_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
        $this->db->select('parties.*, addressbook.name');
        $this->db->from('parties');
        $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->where('parties.orgRowId', $this->session->orgRowId);
        $this->db->order_by('name');
        // $this->db->limit(5);
        $query = $this->db->get();

        return($query->result_array());
    }

    public function insert()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);


        for ($i=0; $i < $myTableRows; $i++) 
        {
            $this->db->select('partyRowId');
            $this->db->from('ledgera');
            $this->db->where('partyRowId', $TableData[$i]['partyRowId']);
            $this->db->where('orgRowId', $this->session->orgRowId);
            $this->db->where('ledgera.vType', 'OB');
            $query = $this->db->get();
            if ($query->num_rows() > 0) ////if product OB is already in Ledger
            {
                $this->db->select_max('ledgerARowId');
                $query = $this->db->get('ledgera');
                $row = $query->row_array();
                $ledgerARowId = $row['ledgerARowId']+1;

                $data = array(
                    'recd' => (float)$TableData[$i]['recd']
                    , 'recdBal' => (float)$TableData[$i]['recd']
                    , 'paid' => (float)$TableData[$i]['paid']
                    , 'paidBal' => (float)$TableData[$i]['paid']
                    , 'dt' => date('Y-m-d')
                );
                $this->db->where('vType', 'OB');
                $this->db->where('partyRowId', $TableData[$i]['partyRowId']);
                $this->db->where('orgRowId', $this->session->orgRowId);
                $this->db->update('ledgera', $data);
            }
            else /// product first time in ledger
            {
                $this->db->select_max('ledgerARowId');
                $query = $this->db->get('ledgera');
                $row = $query->row_array();
                $ledgerARowId = $row['ledgerARowId']+1;

                $data = array(
                    'ledgerARowId' => $ledgerARowId
                    , 'orgRowId' => $this->session->orgRowId
                    , 'vType' => 'OB'
                    , 'vNo' => 1
                    , 'dt' => date('Y-m-d')
                    , 'partyRowId' => $TableData[$i]['partyRowId']
                    , 'recd' => (float)$TableData[$i]['recd']
                    , 'recdBal' => (float)$TableData[$i]['recd']
                    , 'paid' => (float)$TableData[$i]['paid']
                    , 'paidBal' => (float)$TableData[$i]['paid']
                    , 'createdBy' => $this->session->userRowId
                );
                $this->db->set('createdStamp', 'NOW()', FALSE);
                $this->db->insert('ledgera', $data);
            }

            ////Updating Party Master
             $data = array(
                    'recd' => (float)$TableData[$i]['recd']
                    , 'paid' => (float)$TableData[$i]['paid']
                );
                $this->db->where('partyRowId', $TableData[$i]['partyRowId']);
                $this->db->where('orgRowId', $this->session->orgRowId);
                $this->db->update('parties', $data);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}