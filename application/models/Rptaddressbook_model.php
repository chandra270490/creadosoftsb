<?php
class Rptaddressbook_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getCompleteAddressBook()
    {
        $this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, districts.districtName, states.stateName, contactType, AB.name as refName');
        $this->db->from('addressbook');
        $this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
        $this->db->join('towns','towns.townRowId = addressbook.townRowId');
        $this->db->join('districts','districts.districtRowId = towns.districtRowId');
        $this->db->join('states','states.stateRowId = districts.stateRowId');
        $this->db->join('addressbookcontacttypes','addressbookcontacttypes.abRowId = addressbook.abRowId');
        $this->db->join('contacttypes','contacttypes.contactTypeRowId = addressbookcontacttypes.contactTypeRowId');
        $this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');

        $this->db->where('addressbook.deleted', 'N');
        // $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.abRowId');
        $query = $this->db->get();
        return($query->result_array());
    }
   
    public function getDataForReport()
    {
        if( $this->input->post('abType') == "-1" )
        {
            $this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, districts.districtName, states.stateName, contactType, AB.name as refName');
            $this->db->from('addressbook');
            $this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            $this->db->join('states','states.stateRowId = districts.stateRowId');
            $this->db->join('addressbookcontacttypes','addressbookcontacttypes.abRowId = addressbook.abRowId');
            $this->db->join('contacttypes','contacttypes.contactTypeRowId = addressbookcontacttypes.contactTypeRowId');
            $this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');

            $this->db->where('addressbook.deleted', 'N');
            // $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.abRowId');
            $query = $this->db->get();
            return($query->result_array());
        }
        else
        {
            $this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, districts.districtName, states.stateName, contactType, AB.name as refName');
            $this->db->from('addressbook');
            $this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
            $this->db->join('towns','towns.townRowId = addressbook.townRowId');
            $this->db->join('districts','districts.districtRowId = towns.districtRowId');
            $this->db->join('states','states.stateRowId = districts.stateRowId');
            $this->db->join('addressbookcontacttypes','addressbookcontacttypes.abRowId = addressbook.abRowId');
            $this->db->join('contacttypes','contacttypes.contactTypeRowId = addressbookcontacttypes.contactTypeRowId');
            $this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');

            $this->db->where('addressbook.deleted', 'N');
            $this->db->where('addressbook.abType', $this->input->post('abType'));
            // $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.abRowId');
            $query = $this->db->get();
            return($query->result_array());
        }
    }
}