<?php
class Rptattendance_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
        $this->db->distinct();
        $this->db->select('attendance.empRowId, addressbook.name');
        $this->db->from('attendance');
        $this->db->where('attendance.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('attendance.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('attendance.orgRowId', $this->session->orgRowId);
        $this->db->join('employees',"employees.empRowId = attendance.empRowId");
        // $this->db->where('employees.discontinue', 'N'); employees.discontinue
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('name');
        $query = $this->db->get();
        // return($query->result_array());

        //////creating tmpatt table
        $this->load->dbforge();
        if($this->db->table_exists('tmpatt'))
        {
            $this->dbforge->drop_table('tmpatt');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                )
            );  
        $this->dbforge->add_field($fields);


        $begin = new DateTime( $this->input->post('dtFrom') );
        $end = new DateTime( $this->input->post('dtTo') );
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $totalDays = 0;
        foreach ( $period as $dt )
        {
          $f = $dt->format( "Y-m-d" );
          $fields = array(
                        'd-'.$f => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => '100', 
                                          )
                                    );
                    $this->dbforge->add_field($fields);
                    $totalDays++;
        }


        $this->dbforge->create_table('tmpatt');

        $rowId = 1;
        foreach ($query->result() as $row)
        {

            $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name
                    );
            $this->db->insert('tmpatt', $data);               

            //// fetching duty
            foreach ( $period as $dt )
            {
                $f = $dt->format( "Y-m-d" );
                $this->db->select('tmIn, tmOut, extraHours, duty');
                $this->db->from('attendance');
                $this->db->where('attendance.empRowId', $row->empRowId );
                $this->db->where('attendance.dt', $f);
                // $this->db->where('attendance.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                $queryDuty = $this->db->get();        
                if ($queryDuty->num_rows() > 0)
                {
                    $rowDuty = $queryDuty->row_array();
                    $duty = $rowDuty['duty'] . '<br /> [' . $rowDuty['tmIn'] . ', '. $rowDuty['tmOut'] . ', ' . $rowDuty['extraHours'] . ']';
                }    
                else
                {
                    $duty = 'Not Applicable';
                }
                $data = array(
                        'd-'.$f => $duty
                    );
                $this->db->where('empRowId', $row->empRowId );
                $this->db->update('tmpatt', $data);
            }

            $rowId++;
        }

        ///////// returning data
        $this->db->select('tmpatt.*');
        $this->db->order_by('tmpatt.empName');
        $this->db->from('tmpatt');
        $query = $this->db->get();
        return($query->result_array());
    }

}