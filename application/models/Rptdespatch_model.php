<?php
class Rptdespatch_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        if( $this->input->post('partyRowId') == "-1" )
        {
             $this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter, users.uid');
             $this->db->from('despatch');
             $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
            $this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
            $this->db->join('users','users.rowid = despatch.createdBy');
             // $this->db->where('qpo.vType', 'Q');
             // $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('despatch.deleted', 'N');
             $this->db->where('despatch.orgRowId', $this->session->orgRowId);
             $this->db->order_by('despatch.despatchRowId');
             $query = $this->db->get();
             return($query->result_array());
        }
        else
        {
             $this->db->select('despatch.*, addressbook.name, transporters.transporterRowId, ab.name as transporter, users.uid');
             $this->db->from('despatch');
             $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('transporters','transporters.transporterRowId = despatch.transporterRowId');
            $this->db->join('addressbook as ab', 'ab.abRowId = transporters.abRowId');
            $this->db->join('users','users.rowid = despatch.createdBy');
             $this->db->where('despatch.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('despatch.deleted', 'N');
             $this->db->where('despatch.orgRowId', $this->session->orgRowId);
             $this->db->order_by('despatch.despatchRowId');
             $query = $this->db->get();
             return($query->result_array());

        }

    }



    public function getProducts()
    {
        $this->db->select('despatchdetail.*, productcategories.productCategoryRowId, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('despatchdetail.despatchRowId', $this->input->post('rowid'));
        $this->db->from('despatchdetail');
        $this->db->join('products','products.productRowId = despatchdetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());

    }
}