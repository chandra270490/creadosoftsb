<?php
class Scheduledappointments_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }


    public function getDataForReport()
    {
         set_time_limit(0);

        if( $this->session->abAccess == "C" )
        {
            $this->db->select('dsr.*, addressbook.name, addressbook.addr, addressbook.mobile1, meetingremarks.meetingRemark, users.uid');
            $this->db->from('dsr');
            // $this->db->join('parties','parties.partyRowId = dsr.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = dsr.abRowId', "LEFT");
            $this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId', "LEFT");
            $this->db->join('users', 'users.rowid = dsr.userRowId');
            $this->db->where('dsr.deleted', 'N');
            $this->db->where('dsr.nextDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('dsr.nextDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        
            $this->db->where('dsr.orgRowId', $this->session->orgRowId);
            $this->db->order_by('dsr.dsrRowId');
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('dsr.*, addressbook.name, addressbook.addr, addressbook.mobile1, meetingremarks.meetingRemark, users.uid');
            $this->db->from('dsr');
            // $this->db->join('parties','parties.partyRowId = dsr.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = dsr.abRowId', "LEFT");
            $this->db->join('meetingremarks','meetingremarks.mrRowId = dsr.mrRowId', "LEFT");
            $this->db->join('users', 'users.rowid = dsr.userRowId');
            $this->db->where('dsr.deleted', 'N');
            $this->db->where('dsr.nextDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('dsr.nextDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('dsr.orgRowId', $this->session->orgRowId);
            $abAccessIn = explode(",", $this->session->abAccessIn);
            $this->db->where_in('dsr.createdBy', $abAccessIn);
            $this->db->order_by('dsr.dsrRowId');
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
    }

    public function getEmployerContri()
    {
         set_time_limit(0);

        $this->db->select('baselimits.*');
        $this->db->from('baselimits');
        $query = $this->db->get();
        return($query->result_array());
    }

  
}