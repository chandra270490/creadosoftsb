<?php
class Rptcolourdespatch_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        // $this->db->select('despatch.*');
        // $this->db->from('despatch');
         
        //  $query = $this->db->get();
        //  return($query->result_array());
        if( $this->input->post('partyRowId') == "-1" )
        {
             // $this->db->distinct();
             // $this->db->select('despatch.*');
             $this->db->select('despatch.*, despatchdetail.*, products.productName, productcategories.productCategory, colours.colourName, addressbook.name, addressbook.addr');
             $this->db->from('despatch');
             $this->db->join('despatchdetail','despatchdetail.despatchRowId = despatch.despatchRowId');
             $this->db->join('products','products.productRowId = despatchdetail.productRowId');
             $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
             $this->db->join('colours','colours.colourRowId = despatchdetail.colourRowId');
             $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $colourRowId = explode(",", $this->input->post('colorRowId'));
             $this->db->where_in('colours.colourRowId', $colourRowId );
             $productRowId = explode(",", $this->input->post('productRowId'));
             $this->db->where_in('products.productRowId', $productRowId );
             $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('despatch.deleted', 'N');
             $this->db->where('despatch.orgRowId', $this->session->orgRowId);
             $this->db->order_by('despatch.despatchRowId');
             $query = $this->db->get();
             return($query->result_array());
        }
        else
        {
           $this->db->select('despatch.*, despatchdetail.*, products.productName, productcategories.productCategory, colours.colourName, addressbook.name, addressbook.addr');
             $this->db->from('despatch');
             $this->db->join('despatchdetail','despatchdetail.despatchRowId = despatch.despatchRowId');
             $this->db->join('products','products.productRowId = despatchdetail.productRowId');
             $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
             $this->db->join('colours','colours.colourRowId = despatchdetail.colourRowId');
             $this->db->join('parties','parties.partyRowId = despatch.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $colourRowId = explode(",", $this->input->post('colorRowId'));
             $this->db->where_in('colours.colourRowId', $colourRowId );
             $productRowId = explode(",", $this->input->post('productRowId'));
             $this->db->where_in('products.productRowId', $productRowId );
             $this->db->where('despatch.despatchDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('despatch.despatchDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('despatch.deleted', 'N');
             $this->db->where('despatch.orgRowId', $this->session->orgRowId);
               $this->db->where('despatch.partyRowId', $this->input->post('partyRowId'));
             $this->db->order_by('despatch.despatchRowId');
             $query = $this->db->get();
             return($query->result_array());
            
        } 
    }



    // public function getProducts()
    // {   //, colours.colourName
    //     $this->db->select('cidetail.*, ordertypes.orderType, qpo.vType, qpo.vNo, qpo.vDt, qpo.commitmentDate, despatch.despatchRowId, despatch.despatchDt, qpodetail.remarks, colours.colourName, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
    //     $this->db->where('cidetail.ciRowId', $this->input->post('rowid'));
    //     $this->db->from('cidetail');
    //     $this->db->join('despatchdetail','despatchdetail.rowId = cidetail.despatchDetailRowId');
    //     $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId');
    //     $this->db->join('qpodetail','qpodetail.rowId = despatchdetail.qpoDetailRowId');
    //     $this->db->join('qpo','qpo.qpoRowId = qpodetail.qpoRowId');
    //     $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
    //     $this->db->join('ordertypes','ordertypes.orderTypeRowId = qpo.orderTypeRowId');
    //     $this->db->join('products','products.productRowId = despatchdetail.productRowId');
    //     $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
    //     // $this->db->join('colours','colours.colourRowId = cidetail.colourRowId');
    //     // $this->db->order_by('ciDetailRowId');
    //     $query = $this->db->get();
    //     return($query->result_array());
    // }

    public function getColours()
    {
        $this->db->select('colourRowId, colourName');
        $this->db->where('deleted', 'N');
        $this->db->order_by('colourName');
        $query = $this->db->get('colours');
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['colourRowId']]= $row['colourName'];
        }

        return $arr;
    }

    public function getProducts4CheckBox()
    {
        $this->db->select('productRowId, productName');
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get('products');
        $arr = array();
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productRowId']]= $row['productName'];
        }

        return $arr;
    }
}