<?php
class Issuereceive_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
    	$this->load->database();
    }

	public function getProductList()
    {
    	set_time_limit(0);
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getEmpList()
    {
    	set_time_limit(0);
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees',"employees.abRowId = addressbook.abRowId AND employees.discontinue='N'");
        $this->db->where('employees.deleted', 'N');
        // $this->db->where('employees.discontinue', 'N');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataLimit()
	{
		set_time_limit(0);
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('issuereceive.*, addressbook.name, productName');
			$this->db->from('issuereceive');
        	$this->db->join('employees',"employees.empRowId = issuereceive.empRowId AND employees.discontinue='N'");
			$this->db->join('addressbook', 'addressbook.abRowId = employees.abRowId');
			$this->db->join('products', 'products.productRowId = issuereceive.productRowId');
			$this->db->where('issuereceive.deleted', 'N');
			$this->db->where('issuereceive.orgRowId', $this->session->orgRowId);
			$this->db->order_by('issuereceive.irRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('issuereceive.*, addressbook.name, productName');
			$this->db->from('issuereceive');
        	$this->db->join('employees',"employees.empRowId = issuereceive.empRowId AND employees.discontinue='N'");
			$this->db->join('addressbook', 'addressbook.abRowId = employees.abRowId');
			$this->db->join('products', 'products.productRowId = issuereceive.productRowId');
			$this->db->where('issuereceive.deleted', 'N');
			$this->db->where('issuereceive.orgRowId', $this->session->orgRowId);
			$this->db->where_in('issuereceive.createdBy', $this->session->userRowId);
			$this->db->order_by('issuereceive.irRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		set_time_limit(0);
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('issuereceive.*, addressbook.name, productName');
			$this->db->from('issuereceive');
        	$this->db->join('employees',"employees.empRowId = issuereceive.empRowId AND employees.discontinue='N'");
			$this->db->join('addressbook', 'addressbook.abRowId = employees.abRowId');
			$this->db->join('products', 'products.productRowId = issuereceive.productRowId');
			$this->db->where('issuereceive.deleted', 'N');
			$this->db->where('issuereceive.orgRowId', $this->session->orgRowId);
			$this->db->order_by('issuereceive.irRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());

		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('issuereceive.*, addressbook.name, productName');
			$this->db->from('issuereceive');
        	$this->db->join('employees',"employees.empRowId = issuereceive.empRowId AND employees.discontinue='N'");
			$this->db->join('addressbook', 'addressbook.abRowId = employees.abRowId');
			$this->db->join('products', 'products.productRowId = issuereceive.productRowId');
			$this->db->where('issuereceive.deleted', 'N');
			$this->db->where('issuereceive.orgRowId', $this->session->orgRowId);
			$this->db->where_in('issuereceive.createdBy', $this->session->userRowId);
			$this->db->order_by('issuereceive.irRowId');
			// $this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}


	public function getCurrentQty()
    {
    	set_time_limit(0);
    	$q = "Select sum(receive)-sum(Issue) as currentQty from ledgerp where productRowId=".$this->input->post('productRowId');
        $query = $this->db->query($q);
        return($query->result_array());
    }
    
	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE issuereceive WRITE, ledgerp WRITE');

		$this->db->select_max('irRowId');
		$query = $this->db->get('issuereceive');
        $row = $query->row_array();

        $current_row = $row['irRowId']+1;
		// $this->globalInvNo = $current_row;
		
        $this->db->select_max('vNo');
        $this->db->where('vType', $this->input->post('vType'));
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('issuereceive');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        $vDt = date('Y-m-d', strtotime($this->input->post('vDt')));



		$data = array(
	        'irRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => $this->input->post('vType')
	        , 'vNo' => $vNo 
	        , 'dt' => $vDt
	        , 'empRowId' => $this->input->post('empRowId')
	        , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
	        , 'productRowId' => $this->input->post('productRowId')
	        , 'qty' => $this->input->post('qty')
	        , 'remarks' => $this->input->post('remarks')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('issuereceive', $data);	

		// ////////////inserting in ledgerP
        $this->db->select_max('ledgerPRowId');
        $query = $this->db->get('ledgerp');
        $row = $query->row_array();
        $ledgerPRowId = $row['ledgerPRowId']+1;

        if($this->input->post('vType') == "Issue")
        {
	        $data = array(
	            'ledgerPRowId' => $ledgerPRowId
	            , 'orgRowId' => $this->session->orgRowId
	            , 'vType' => $this->input->post('vType')
	            , 'vNo' => $vNo  
	            , 'dt' => $vDt
	            , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
	            , 'productRowId' => $this->input->post('productRowId')
	            , 'issue' => $this->input->post('qty')
	            , 'issueBal' => $this->input->post('qty')
	            , 'createdBy' => $this->session->userRowId
	        );
    	}
    	else
    	{
	        $data = array(
	            'ledgerPRowId' => $ledgerPRowId
	            , 'orgRowId' => $this->session->orgRowId
	            , 'vType' => $this->input->post('vType')
	            , 'vNo' => $vNo  
	            , 'dt' => $vDt
	            , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
	            , 'productRowId' => $this->input->post('productRowId')
	            , 'receive' => $this->input->post('qty')
	            , 'receiveBal' => $this->input->post('qty')
	            , 'createdBy' => $this->session->userRowId
	        );    		
    	}
        $this->db->set('createdStamp', 'NOW()', FALSE);
        $this->db->insert('ledgerp', $data);



        $this->db->query('UNLOCK TABLES');

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}


	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE issuereceive WRITE, ledgerp WRITE');

    	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));

		$data = array(
	        'dt' => $vDt
	        , 'empRowId' => $this->input->post('empRowId')
	        , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
	        , 'productRowId' => $this->input->post('productRowId')
	        , 'qty' => $this->input->post('qty')
	        , 'remarks' => $this->input->post('remarks')

		);
		$this->db->where('irRowId', $this->input->post('globalrowid'));
		$this->db->update('issuereceive', $data);	

		////Updaing in Ledger
		if($this->input->post('vType') == "Issue")
        {
	        $data = array(
	            'dt' => $vDt
	            , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
	            , 'productRowId' => $this->input->post('productRowId')
	            , 'issue' => $this->input->post('qty')
	            , 'issueBal' => $this->input->post('qty')
	        );
    	}
    	else
    	{
	        $data = array(
	             'dt' => $vDt
	            , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
	            , 'productRowId' => $this->input->post('productRowId')
	            , 'receive' => $this->input->post('qty')
	            , 'receiveBal' => $this->input->post('qty')
	        );    		
    	}		
    	$this->db->where('vType', $this->input->post('vType'));
    	$this->db->where('vNo', $this->input->post('vNo'));
    	$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->update('ledgerp', $data);	


        $this->db->query('UNLOCK TABLES');
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}


	public function delete()
	{
		set_time_limit(0);
    	$this->db->where('vType', $this->input->post('vType'));
    	$this->db->where('vNo', $this->input->post('vNo'));
    	$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->delete('issuereceive');

    	$this->db->where('vType', $this->input->post('vType'));
    	$this->db->where('vNo', $this->input->post('vNo'));
    	$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->delete('ledgerp');
	}


	public function getProducts()
    {
    	set_time_limit(0);
        $this->db->select('podetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('podetail.poRowId', $this->input->post('rowid'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getInvNo()
    {
        return $this->globalInvNo;
    }

	public function getImageName($rowId)
    {
    	set_time_limit(0);
        $this->db->select('products.productRowId, products.imagePathThumb');
		$this->db->from('products');
		$this->db->where('products.productRowId', $rowId);
		// $this->db->order_by('po.po');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function getQpo($rowId)
    {
    	set_time_limit(0);
        $this->db->select('po.*, addressbook.name, addressbook.addr, addressbook.pin, prefixtypes.prefixType, towns.townName');
		$this->db->from('po');
		$this->db->join('parties','parties.partyRowId = po.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('po.po', $rowId);
		$this->db->order_by('po.po');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());

    }


    
}