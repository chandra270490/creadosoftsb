<?php
class Employeecontract_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmpList()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.salType', 'C');
        $this->db->where('employees.discontinue', 'N');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getData()
    {
        //////creating tmp table
        $this->load->dbforge();
        if($this->db->table_exists('tmp'))
        {
            $this->dbforge->drop_table('tmp');
        }
        $fields = array(
                    'catRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => NULL,
                                             
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => NULL,
                                             
                                ),
                    'contractRate' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => 0
                                )

            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmp');
        ////// END - creating tmp table

        //// storing contractRate 0 for all products
        $this->db->select('productCategoryRowId, productRowId');
        $this->db->where('deleted', 'N');
        $this->db->where('productCategoryRowId', $this->input->post('productCategory'));
        $this->db->order_by('productRowId');
        $query = $this->db->get('products');
        foreach ($query->result() as $row)
        {
            $data = array(
                'catRowId' => $row->productCategoryRowId
                , 'productRowId' => $row->productRowId
                );
            $this->db->insert('tmp', $data);    
        }
        ////END - storing contractRate 0 for all products

        //// Now setting already saved contractRate
        $this->db->select('tmp.*');
        $this->db->order_by('productRowId');
        $this->db->from('tmp');
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            $this->db->select('contractRate');
            $this->db->where('empRowId',  $this->input->post('empRowId'));
            $this->db->where('productRowId', $row->productRowId);
            $this->db->from('employeecontract');
            $query1 = $this->db->get();
            if ($query1->num_rows() > 0)
            {
                $row1 = $query1->row(); 
                
                $data = array(
                    'contractRate' => $row1->contractRate
                    );
                $this->db->where('productRowId', $row->productRowId);
                $this->db->update('tmp', $data);   
            }
        }
        //// END - Now setting already saved contractRate

        /// now sending data to client
        $this->db->select('tmp.*, products.productName, productcategories.productCategory');
        $this->db->order_by('productName');
        $this->db->from('tmp');
        $this->db->join('products','products.productRowId = tmp.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $query = $this->db->get();

        return($query->result_array());
    }

    public function saveChanges()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);
        for ($i=0; $i < $myTableRows; $i++) 
        {
            $this->db->where('empRowId', $this->input->post('empRowId'));
            $this->db->where('productRowId', $TableData[$i]['productRowId']);
            $this->db->delete('employeecontract');


            $this->db->select_max('rowId');
            $query = $this->db->get('employeecontract');
            $row = $query->row_array();

            $current_row = $row['rowId']+1;

            $data = array(
                'rowId' => $current_row
                , 'empRowId' => $this->input->post('empRowId')
                , 'productRowId' => $TableData[$i]['productRowId']
                , 'contractRate' => $TableData[$i]['contractRate']
            );
            $this->db->insert('employeecontract', $data);    
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}