<?php
class Employees_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getUsers()
	{
		$this->db->distinct();
		$this->db->select('users.*');
		$this->db->from('users');
		$this->db->where('users.uid !=', 'admin');
		$this->db->where('users.deleted', 'N');
		$this->db->order_by('uid');
		// $this->db->limit(5);
		$query = $this->db->get();

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['rowid']]= $row['uid'];
		}
		return $arr;

	}

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('employees.*, addressbook.name, ab.name as reference, abRep.name as reportingManagerName, departmenttypes.departmentType, designationtypes.designationType');
			$this->db->from('employees');
			$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = employees.referencerRowId');
			$this->db->join('addressbook as abRep', 'abRep.abRowId = employees.reportingManager');
			$this->db->join('departmenttypes', 'departmenttypes.departmentTypeRowId = employees.departmentRowId');
			$this->db->join('designationtypes', 'designationtypes.designationTypeRowId = employees.designationRowId');
			$this->db->where('employees.deleted', 'N');
			$this->db->where('employees.discontinue', 'N');
			$this->db->where('employees.orgRowId', $this->session->orgRowId);
			$this->db->order_by('employees.empRowId desc');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('employees.*, addressbook.name, ab.name as reference, abRep.name as reportingManagerName, departmenttypes.departmentType, designationtypes.designationType');
			$this->db->from('employees');
			$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = employees.referencerRowId');
			$this->db->join('addressbook as abRep', 'abRep.abRowId = employees.reportingManager');			
			$this->db->join('departmenttypes', 'departmenttypes.departmentTypeRowId = employees.departmentRowId');
			$this->db->join('designationtypes', 'designationtypes.designationTypeRowId = employees.designationRowId');
			$this->db->where('employees.deleted', 'N');
			$this->db->where('employees.discontinue', 'N');
			$this->db->where('employees.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('employees.createdBy', $abAccessIn);
			$this->db->order_by('employees.empRowId desc');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('employees.*, addressbook.name, ab.name as reference, abRep.name as reportingManagerName, departmenttypes.departmentType, designationtypes.designationType');
			$this->db->from('employees');
			$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = employees.referencerRowId');
			$this->db->join('addressbook as abRep', 'abRep.abRowId = employees.reportingManager');			
			$this->db->join('departmenttypes', 'departmenttypes.departmentTypeRowId = employees.departmentRowId');
			$this->db->join('designationtypes', 'designationtypes.designationTypeRowId = employees.designationRowId');
			$this->db->where('employees.deleted', 'N');
			$this->db->where('employees.discontinue', 'N');
			$this->db->where('employees.orgRowId', $this->session->orgRowId);
			$this->db->order_by('employees.empRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('employees.*, addressbook.name, ab.name as reference, abRep.name as reportingManagerName, departmenttypes.departmentType, designationtypes.designationType');
			$this->db->from('employees');
			$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = employees.referencerRowId');
			$this->db->join('addressbook as abRep', 'abRep.abRowId = employees.reportingManager');			
			$this->db->join('departmenttypes', 'departmenttypes.departmentTypeRowId = employees.departmentRowId');
			$this->db->join('designationtypes', 'designationtypes.designationTypeRowId = employees.designationRowId');
			$this->db->where('employees.deleted', 'N');
			$this->db->where('employees.discontinue', 'N');
			$this->db->where('employees.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('employees.createdBy', $abAccessIn);
			$this->db->order_by('employees.empRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}

    

	public function checkDuplicate()
    {
		$this->db->select('abRowId');
		$this->db->where('abRowId', $_REQUEST['cboAbList']);
		$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->where('createdBy', $this->session->userRowId);
		$query = $this->db->get('employees');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

    public function checkDuplicateUser()
    {
		$this->db->select('userRowId');
		$this->db->where('userRowId', $_REQUEST['cboUser']);
		$this->db->where('userRowId>', 0); /// kyu k -1 b h... matlb jo emp users nahi h
		// $this->db->where('orgRowId', $this->session->orgRowId);
		// $this->db->where('createdBy', $this->session->userRowId);
		$query = $this->db->get('employees');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

    public function getRowId()
    {
		$this->db->select_max('empRowId');
		$query = $this->db->get('employees');
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
		    $current_row = $row['empRowId'] + 1;
		    return $current_row;
		}
	}

	public function insert()
    {
		$this->db->select_max('empRowId');
		$query = $this->db->get('employees');
        $row = $query->row_array();

        $current_row = $row['empRowId']+1;
        
        $doj = date('Y-m-d', strtotime($_REQUEST['txtDoj']));
		$data = array(
	        'empRowId' => $current_row
	        , 'abRowId' => $_REQUEST['cboAbList']
	        , 'fName' => $_REQUEST['txtFather']
	        , 'fOccupation' => $_REQUEST['txtFatherOccupation']
	        , 'gender' => $_REQUEST['cboGender']
	        , 'referencerRowId' => $_REQUEST['cboReference']
	        , 'departmentRowId' => $_REQUEST['cboDepartment']
	        , 'designationRowId' => $_REQUEST['cboDesignation']
	        , 'doj' => $doj
	        , 'basicSal' => $_REQUEST['txtBasicSalary']
	        , 'hra' => $_REQUEST['txtHra']
	        , 'ta' => $_REQUEST['txtTa']
	        , 'ca' => $_REQUEST['txtCa']
	        , 'ma' => $_REQUEST['txtMa']
	        , 'deductPf' => $_REQUEST['cboPf']
	        , 'deductEsi' => $_REQUEST['cboEsi']
	        , 'salType' => $_REQUEST['cboSalaryType']
	        , 'mobileProfessional' => $_REQUEST['txtMobileProfessional']
	        , 'emailProfessional' => $_REQUEST['txtEmailProfessional']
	        , 'correspondenceAddress' => $_REQUEST['txtCorrespondenceAddress']
	        , 'aadharNo' => $_REQUEST['txtAadhar']
	        , 'jobFunction' => $_REQUEST['txtJobFunction']
	        , 'reportingManager' => $_REQUEST['cboReportingManager']
	        , 'emergencyContactName' => $_REQUEST['txtEmergencyContactName']
	        , 'emergencyContactNumber' => $_REQUEST['txtEmergencyContactNumber']
	        , 'emergencyContactAddress' => $_REQUEST['txtEmergencyContactAddress']
	        , 'leavesOn' => $_REQUEST['txtLeavesOn']
	        , 'userRowId' => $_REQUEST['cboUser']
	        , 'showInAttendance' => $_REQUEST['cboShowInAttendance']
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('employees', $data);	


		//////////// Saving Enp Docs Info
		$rowId = $current_row;
        $rowId = sprintf("%04d", $rowId); 
		$cnt = count(array_filter( $_FILES['images']['name'] ));
		
		for($i=0; $i<$cnt; $i++)
		{
			// $ext = substr($_FILES["images"]["name"][$i],-3,3);
			$ext = pathinfo($_FILES["images"]["name"][$i])['extension'];

			$target =  $rowId . '_'.$i. '.' . $ext;
	        // $tmp=$_FILES['images']['tmp_name'][$i];
	        // move_uploaded_file($tmp,$target);
			$this->db->select_max('rowId');
			$query = $this->db->get('employeesdocs');
	        $row = $query->row_array();

	        $docsRowId = $row['rowId']+1;
	        
			$data = array(
		        'rowId' => $docsRowId
		        , 'empRowId' => $current_row
		        , 'docFileName' => $target
			);
			$this->db->insert('employeesdocs', $data);	
		}	
		//////////// END - Saving Enp Docs Info

	}

	public function getEmpDocs()
    {
		$this->db->select('docFileName');
		$this->db->where('empRowId', $this->input->post('globalrowid'));
		$this->db->order_by('rowId');
		$query = $this->db->get('employeesdocs');
		return($query->result_array());
	}

	public function getEmpDocs4Deletion($code)
    {
		$this->db->select('docFileName');
		$this->db->where('empRowId', $code);
		$this->db->order_by('rowId');
		$query = $this->db->get('employeesdocs');
		return($query->result_array());
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('abRowId');
		$this->db->where('abRowId', $_REQUEST['cboAbList']);
		$this->db->where('empRowId !=', $_REQUEST['txtHiddenRowId']);
		$this->db->where('orgRowId', $this->session->orgRowId);
		$this->db->where('createdBy', $this->session->userRowId);

		$query = $this->db->get('employees');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }
    public function checkDuplicateOnUpdateUser()
    {
    	// echo "chk";
		$this->db->select('userRowId');
		$this->db->where('userRowId', $_REQUEST['cboUser']);
		$this->db->where('empRowId !=', $_REQUEST['txtHiddenRowId']);
		$this->db->where('userRowId>', 0); /// kyu k -1 b h... matlb jo emp users nahi h
		// $this->db->where('orgRowId', $this->session->orgRowId);
		// $this->db->where('createdBy', $this->session->userRowId);

		$query = $this->db->get('employees');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$doj = date('Y-m-d', strtotime($_REQUEST['txtDoj']));
		$data = array(
	        'abRowId' => $_REQUEST['cboAbList']
	        , 'fName' => $_REQUEST['txtFather']
	        , 'fOccupation' => $_REQUEST['txtFatherOccupation']
	        , 'gender' => $_REQUEST['cboGender']
	        , 'referencerRowId' => $_REQUEST['cboReference']
	        , 'departmentRowId' => $_REQUEST['cboDepartment']
	        , 'designationRowId' => $_REQUEST['cboDesignation']
	        , 'doj' => $doj
	        , 'basicSal' => $_REQUEST['txtBasicSalary']
	        , 'hra' => $_REQUEST['txtHra']
	        , 'ta' => $_REQUEST['txtTa']
	        , 'ca' => $_REQUEST['txtCa']
	        , 'ma' => $_REQUEST['txtMa']
	        , 'deductPf' => $_REQUEST['cboPf']
	        , 'deductEsi' => $_REQUEST['cboEsi']
	        , 'salType' => $_REQUEST['cboSalaryType']
	        , 'mobileProfessional' => $_REQUEST['txtMobileProfessional']
	        , 'emailProfessional' => $_REQUEST['txtEmailProfessional']
	        , 'correspondenceAddress' => $_REQUEST['txtCorrespondenceAddress']
	        , 'aadharNo' => $_REQUEST['txtAadhar']
	        , 'jobFunction' => $_REQUEST['txtJobFunction']
	        , 'reportingManager' => $_REQUEST['cboReportingManager']
	        , 'emergencyContactName' => $_REQUEST['txtEmergencyContactName']
	        , 'emergencyContactNumber' => $_REQUEST['txtEmergencyContactNumber']
	        , 'emergencyContactAddress' => $_REQUEST['txtEmergencyContactAddress']
	        , 'leavesOn' => $_REQUEST['txtLeavesOn']
	        , 'userRowId' => $_REQUEST['cboUser']
	        , 'showInAttendance' => $_REQUEST['cboShowInAttendance']
	        , 'orgRowId' => $this->session->orgRowId
		);
		$this->db->where('empRowId', $_REQUEST['txtHiddenRowId']);
		$this->db->update('employees', $data);	

		//////////// Saving Enp Docs Info
		if($_REQUEST['txtPhotoChange'] == "yes")
        {
			$this->db->where('empRowId', $_REQUEST['txtHiddenRowId']);
			$this->db->delete('employeesdocs');

			$rowId = $_REQUEST['txtHiddenRowId'];
	        $rowId = sprintf("%04d", $rowId); 
			$cnt = count(array_filter( $_FILES['images']['name'] ));
			
			for($i=0; $i<$cnt; $i++)
			{
				// $ext = substr($_FILES["images"]["name"][$i],-3,3);
				$ext = pathinfo($_FILES["images"]["name"][$i])['extension'];

				$target =  $rowId . '_'.$i. '.' . $ext;
		        // $tmp=$_FILES['images']['tmp_name'][$i];
		        // move_uploaded_file($tmp,$target);
				$this->db->select_max('rowId');
				$query = $this->db->get('employeesdocs');
		        $row = $query->row_array();

		        $docsRowId = $row['rowId']+1;
		        
				$data = array(
			        'rowId' => $docsRowId
			        , 'empRowId' => $_REQUEST['txtHiddenRowId']
			        , 'docFileName' => $target
				);
				$this->db->insert('employeesdocs', $data);	
			}	
		}
		//////////// END - Saving Enp Docs Info	
	}

	public function delete()
	{
		$data = array(
		        'deleted' => 'Y',
		        'deletedBy' => $this->session->userRowId

		);
		$this->db->set('deletedStamp', 'NOW()', FALSE);
		$this->db->where('empRowId', $this->input->post('rowId'));
		$this->db->update('employees', $data);

		// $this->db->where('empRowId', $this->input->post('rowId'));
		// $this->db->delete('employees');
	}
}