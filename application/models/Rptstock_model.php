<?php
class Rptstock_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

  public function getDataForReport()
    {
        $this->db->select('products.productRowId');
        $this->db->from('products');
        //$this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $productCategoryRowId = explode(",", $this->input->post('productCategoryRowId'));
        $this->db->where_in('products.productCategoryRowId', $productCategoryRowId );
        $query = $this->db->get();
        $str = "";
        foreach ($query->result_array() as $row)
        {
           $str .= $row['productRowId']. ",";
        }
        $str = substr($str, 0, strlen($str)-1);

        // $q = "Select sum(receive)-sum(Issue) as currentQty, ledgerp.productRowId, productName 
        //from ledgerp, products where ledgerp.orgRowId=". $this->session->orgRowId ." 
        //AND ledgerp.productRowId=products.productRowId AND ledgerp.productRowId IN (".$str.") 
        //group by ledgerp.productRowId,productName";
        
        
        $q = "Select sum(receive)-sum(Issue) as currentQty, ledgerp.productRowId, productName, productCategory 
        from ledgerp, products, productcategories where ledgerp.orgRowId=". $this->session->orgRowId ." 
        AND ledgerp.productRowId=products.productRowId AND products.productCategoryRowId=productcategories.productCategoryRowId 
        AND ledgerp.productRowId IN (".$str.") and products.deleted = 'N' group by ledgerp.productRowId,productName";

        $query = $this->db->query($q);

        return($query->result_array());
    }
}