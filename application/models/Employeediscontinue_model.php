<?php
class Employeediscontinue_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
    {
        if( $this->session->abAccess == "C" )
        {
            $this->db->select('employees.*, addressbook.name, ');
            $this->db->from('employees');
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            // $this->db->where('employees.deleted', 'N');
            $this->db->where('employees.discontinue', 'Y');
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('name');
            $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('employees.*, addressbook.name, ');
            $this->db->from('employees');
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            // $this->db->where('employees.deleted', 'N');
            $this->db->where('employees.discontinue', 'Y');
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('name');
            $abAccessIn = explode(",", $this->session->abAccessIn);
            $this->db->where_in('employees.createdBy', $abAccessIn);
            $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
    }

    public function getDataAll()
    {
        if( $this->session->abAccess == "C" )
        {
            $this->db->select('employees.*, addressbook.name, ');
            $this->db->from('employees');
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            // $this->db->where('employees.deleted', 'N');
            $this->db->where('employees.discontinue', 'Y');
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('name');
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('employees.*, addressbook.name, ');
            $this->db->from('employees');
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            // $this->db->where('employees.deleted', 'N');
            $this->db->where('employees.discontinue', 'Y');
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('name');
            $abAccessIn = explode(",", $this->session->abAccessIn);
            $this->db->where_in('employees.createdBy', $abAccessIn);
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
    }

    public function attendanceLagaiHaiKya()
    {
        $dtDOL = date('Y-m-d', strtotime($this->input->post('dtDOL')));
        $this->db->select('attendance.empRowId');
        $this->db->where('attendance.empRowId', $this->input->post('empRowId'));
        $this->db->where('attendance.dt > ', $dtDOL);
        $query = $this->db->get('attendance');
        if ($query->num_rows() > 0)
        {
            return 1;
        }
    } 

    public function getEmpList()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.discontinue', 'N');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }

 

    public function saveData()
    {
        $dtDOL = date('Y-m-d', strtotime($this->input->post('dtDOL')));
        $data = array(
            'discontinue' => 'Y'
            , 'discontinueDt' => $dtDOL
            , 'discontinueReason' => $this->input->post('reason')
        );
        $this->db->where('empRowId', $this->input->post('empRowId'));
        $this->db->update('employees', $data);            
    }

    public function delete()
    {
        $dtDOL = date('Y-m-d', strtotime($this->input->post('dtDOL')));
        $data = array(
            'discontinue' => 'N'
            , 'discontinueDt' => ""
            , 'discontinueReason' => ""
        );
        $this->db->where('empRowId', $this->input->post('rowId'));
        $this->db->update('employees', $data);            
    }


    public function update()
    {
        $dtDOL = date('Y-m-d', strtotime($this->input->post('dtDOL')));
        $data = array(
            'discontinueDt' => $dtDOL
            , 'discontinueReason' => $this->input->post('reason')
        );
        $this->db->where('empRowId', $this->input->post('globalrowid'));
        $this->db->update('employees', $data);            
    }
}