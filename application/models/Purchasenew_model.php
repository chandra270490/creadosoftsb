<?php
class Purchasenew_model extends CI_Model 
{
	public $globalPurchaseNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

	public function getProductList()
    {
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PI');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			$this->db->order_by('po.poRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PI');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('po.createdBy', $abAccessIn);
			$this->db->where_in('po.createdBy', $this->session->userRowId);
			$this->db->order_by('po.poRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PI');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			$this->db->order_by('po.poRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('po.*, addressbook.name');
			$this->db->from('po');
			$this->db->join('parties','parties.partyRowId = po.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('po.vType', 'PI');
			$this->db->where('po.deleted', 'N');
			$this->db->where('po.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('po.createdBy', $abAccessIn);
			$this->db->where_in('po.createdBy', $this->session->userRowId);
			$this->db->order_by('po.poRowId');
			// $this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

	public function loadParties()
    {
    	set_time_limit(0);
    		$this->db->distinct();
            $this->db->select('addressbook.name, parties.partyRowId');
            $this->db->from('addressbook');
            $this->db->join('parties','parties.abRowId = addressbook.abRowId');
            $this->db->join('ziew_podetail','ziew_podetail.partyRowId = parties.partyRowId');
            $this->db->join('po','po.vNO = ziew_podetail.vNo');
            $this->db->group_by('productRowId, poVno, againstPoDetailRowId'); 
            $this->db->having('sum(orderQty)-sum(receivedQty)>0'); 
            $this->db->where('parties.deleted', 'N');
            $this->db->where('ziew_podetail.orgRowId', $this->session->orgRowId);
            $this->db->where('po.status', NULL);
            // $this->db->where('parties.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
			$query = $this->db->get();

            return($query->result_array());
    }

	public function showData()
    {
    	set_time_limit(0);
		$q = "Select sum(orderQty)-sum(receivedQty) as pendingQty, 
		poDetailRowId, vType, vNo, vDt, productRowId, orderQty, 
		productName, poVno, againstPoDetailRowId,rate,discount,amtAfterDiscount, 
		purchaseOrExpense 
		from ziew_podetail
		where partyRowId=".$this->input->post('partyRowId')." 
		and vNO not in(select distinct vNo from po where status = 'SC')
		group by 
		productRowId, poVno, againstPoDetailRowId 
		having (sum(orderQty)-sum(receivedQty))>0";

        $query = $this->db->query($q);
        return($query->result_array());
    }


    
	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE po WRITE, podetail WRITE, ledgerp WRITE');

		$this->db->select_max('poRowId');
		$query = $this->db->get('po');
        $row = $query->row_array();

        $current_row = $row['poRowId']+1;
		$this->globalPurchaseNo = $current_row;
		
        $this->db->select_max('vNo');
        $this->db->where('vType', 'PI');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('po');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        if($this->input->post('refBillDt') == '')
        {
        	$refBillDt = null;
        }
        else
        {
        	$refBillDt = date('Y-m-d', strtotime($this->input->post('refBillDt')));
        }
        $vDt = date('Y-m-d');


        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

		$data = array(
	        'poRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => 'PI'
	        , 'vNo' => $vNo 
	        , 'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'approved' => 'Y'
	        , 'saved' => 'Y'
	        , 'purchaseOrExpense' => $TableData[0]['purchaseOrExpense']
	        , 'avgDiscountPer' => (float)$this->input->post('avgDiscountPer')
	        , 'totalDiscountAmt' => (float)$this->input->post('totalDisAmt')
	        , 'totalAmtAfterDiscount' => (float)$this->input->post('totalAmtAfterDis')
	        , 'totalNetAmt' => (float)$this->input->post('totalNetAmt')
	        , 'gst5TotalAmt' => (float)$this->input->post('gst5TotalAmt')
	        , 'gst5TaxAmt' => (float)$this->input->post('gst5TaxAmt')
	        , 'gst12TotalAmt' => (float)$this->input->post('gst12TotalAmt')
	        , 'gst12TaxAmt' => (float)$this->input->post('gst12TaxAmt')
	        , 'gst18TotalAmt' => (float)$this->input->post('gst18TotalAmt')
	        , 'gst18TaxAmt' => (float)$this->input->post('gst18TaxAmt')
	        , 'gst28TotalAmt' => (float)$this->input->post('gst28TotalAmt')
	        , 'gst28TaxAmt' => (float)$this->input->post('gst28TaxAmt')
	        , 'billNo' => $this->input->post('refBillNo')
	        , 'billDt' => $refBillDt
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('po', $data);	


		/////Saving Products
		

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('poDetailRowId');
			$query = $this->db->get('podetail');
	        $row = $query->row_array();
	        $poDetailRowId = $row['poDetailRowId']+1;

			$data = array(
			        'poDetailRowId' => $poDetailRowId
			        , 'poRowId' => $current_row
			        , 'productRowId' => $TableData[$i]['productRowId']
			        , 'receivedQty' => (float) $TableData[$i]['receivedQty']
			        , 'poVno' => $TableData[$i]['poVno']
			        , 'againstPoDetailRowId' => $TableData[$i]['poDetailRowId']
                    , 'billNo' => $TableData[$i]['billNo']
                    , 'billDt' => $TableData[$i]['billDt']
                    , 'rate' => $TableData[$i]['rate']
                    , 'amt' => $TableData[$i]['amt']
                    , 'discount' => $TableData[$i]['disPer']
                    , 'amtAfterDiscount' => $TableData[$i]['amtAfterDiscount']
                    , 'gstRate' => $TableData[$i]['gstRate']
                    , 'gstAmt' => $TableData[$i]['gstAmt']
                    , 'netAmt' => $TableData[$i]['netAmt']
			);
			$this->db->insert('podetail', $data);	  

			// ////////////inserting in ledgerP
			if ($TableData[$i]['purchaseOrExpense'] == "E")//Agar exp h to ledger m entry nahi
			{

			}
			else ///Agar P ya x hua to
			{
                $this->db->select_max('ledgerPRowId');
                $query = $this->db->get('ledgerp');
                $row = $query->row_array();
                $ledgerPRowId = $row['ledgerPRowId']+1;

                $data = array(
                    'ledgerPRowId' => $ledgerPRowId
                    , 'orgRowId' => $this->session->orgRowId
                    , 'vType' => 'PI'
                    , 'vNo' => $vNo 
                    , 'dt' => $vDt
                    , 'poRowId' => $current_row
                    , 'productRowId' => $TableData[$i]['productRowId']
                    , 'receive' => (float)$TableData[$i]['receivedQty']
                    , 'receiveBal' => (float)$TableData[$i]['receivedQty']
                    , 'createdBy' => $this->session->userRowId
                );
                $this->db->set('createdStamp', 'NOW()', FALSE);
                $this->db->insert('ledgerp', $data);
            }

        }
        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}

	public function getPurchaseNo()
    {
        return $this->globalPurchaseNo;
	}
	
	//ShortClose PO
	public function ShortClose($vNo)
	{
		//Transaction Start
		$this->db->trans_start();

		$this->db->query("update po set status = 'SC' where vNo = '".$vNo."' and vType = 'PO'");

		$this->db->trans_complete();
		//Transanction Complete
	}

	public function delete()
	{
		// $data = array(
		//     'deleted' => 'Y'
		// );
		// $this->db->where('poRowId',  $this->input->post('rowId'));
		// $this->db->update('po', $data);

		//Transaction Start
		$this->db->trans_start();

		$sql_po = $this->db->query("insert into po_del_hist 
		select * from po where poRowId = '".$this->input->post('rowId')."'");

		$sql_podetail = $this->db->query("insert into podetail_del_hist 
		select * from podetail where poRowId = '".$this->input->post('rowId')."'");

		$sql_ledgerp = $this->db->query("insert into ledgerp_del_hist 
		select * from ledgerp where vType = 'PI' and vNo = '".$this->input->post('piRowId')."'
		and dt = (select max(dt) from ledgerp where vType = 'PI' and vNo = '".$this->input->post('piRowId')."')");

		//Deleted From PO
		$this->db->where('poRowId',  $this->input->post('rowId'));
		$this->db->delete('po');
		
		//Deleted From PO details
		$this->db->where('poRowId',  $this->input->post('rowId'));
		$this->db->delete('podetail');

		$sql_check = "select count(*) as cnt from ledgerp where vType = 'PI' 
		and vNo ='".$this->input->post('piRowId')."' and poRowId = 0";

		//echo $sql_check; die;

		$qry_check = $this->db->query($sql_check)->row();
		$cnt = $qry_check->cnt;
		
		if($cnt > 0){
			//Deleted From Ledger
			//$this->db->where('vType',  'PI');
			//$this->db->where('vNo',  $this->input->post('piRowId'));
			//$this->db->delete('ledgerp');

			$this->db->query("delete from ledgerp where vType = 'PI' and vNo = '".$this->input->post('piRowId')."' 
			and dt = (select max(dt) from ledgerp where vType = 'PI' and vNo = '".$this->input->post('piRowId')."')");

		} else {
			//Deleted From Ledger
			//$this->db->where('vType',  'PI');
			//$this->db->where('vNo',  $this->input->post('piRowId'));
			//$this->db->where('poRowId',  $this->input->post('rowId'));
			//$this->db->delete('ledgerp');

			$this->db->query("delete from ledgerp where vType = 'PI' and vNo = '".$this->input->post('piRowId')."' and poRowId = '".$this->input->post('rowId')."'
			and dt = (select max(dt) from ledgerp where vType = 'PI' and vNo = '".$this->input->post('piRowId')."'  and poRowId = '".$this->input->post('rowId')."')");
		}

		$this->db->trans_complete();
		//Transanction Complete
	}


	public function getProducts()
    {
        $this->db->select('podetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('podetail.poRowId', $this->input->post('rowid'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }


    public function getPurchase($rowId)
    {
        $this->db->select('po.*, addressbook.name, addressbook.addr, addressbook.pin, addressbook.tinCentre, prefixtypes.prefixType, towns.townName, addressbook.gstIn');
		$this->db->from('po');
		$this->db->join('parties','parties.partyRowId = po.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('po.poRowId', $rowId);
		$this->db->order_by('po.poRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function getPurchaseDetail()
    {
    	$this->db->select('podetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }
}