<?php
class Rptrolevel_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

  public function getDataForReport()
    {
        if($this->input->post('productCategoryRowId') == "ALL") ///All Categories
        {
            $this->db->select('products.productRowId');
            $this->db->from('products');
            // $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
            $query = $this->db->get();
            $str = "";
            foreach ($query->result_array() as $row)
            {
               $str .= $row['productRowId']. ",";
            }
            $str = substr($str, 0, strlen($str)-1);
            $q = "Select sum(receive)-sum(Issue) as currentQty, ledgerp.productRowId, productName, roLevel 
            from ledgerp, products where ledgerp.orgRowId=". $this->session->orgRowId ." 
            AND ledgerp.productRowId=products.productRowId AND ledgerp.productRowId IN (".$str.") 
            AND products.deleted = 'N' AND roLevel > 0
            group by ledgerp.productRowId,productName,roLevel";

            $query = $this->db->query($q);
            return($query->result_array());
        }
        else
        {
            $this->db->select('products.productRowId');
            $this->db->from('products');
            $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
            $query = $this->db->get();
            $str = "";
            foreach ($query->result_array() as $row)
            {
               $str .= $row['productRowId']. ",";
            }
            $str = substr($str, 0, strlen($str)-1);
            $q = "Select sum(receive)-sum(Issue) as currentQty, ledgerp.productRowId, productName,roLevel 
            from ledgerp, products where ledgerp.orgRowId=". $this->session->orgRowId ." 
            AND ledgerp.productRowId=products.productRowId AND ledgerp.productRowId IN (".$str.")
            AND products.deleted = 'N' AND roLevel > 0
            group by ledgerp.productRowId,productName,roLevel";
            
            $query = $this->db->query($q);
            return($query->result_array());
        }
    }

    public function orderedProducts()
    {
        $q = "Select sum(orderQty)-sum(receivedQty) as pendingQty, poDetailRowId, vType, 
        vNo, vDt, productRowId, orderQty, productName, poVno, againstPoDetailRowId 
        from ziew_podetail where orgRowId=". $this->session->orgRowId ."   
        group by productRowId, poVno, againstPoDetailRowId having (sum(orderQty)-sum(receivedQty))>0";
        
        $query = $this->db->query($q);
        return($query->result_array());
    }
}