<?php
class Attendance_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function showData()
    {
        //////Checking if already stored on that date
        $this->db->select('dt');
        $this->db->where('dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
        $query = $this->db->get('attendance');
        if ($query->num_rows() > 0) /////////Already Marked
        {
            $this->db->select('addressbook.name, attendance.*');
            $this->db->from('attendance');
            $this->db->join('employees','employees.empRowId = attendance.empRowId');
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            $this->db->where('attendance.dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->where('employees.designationRowId', '1');
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();
            return($query->result_array());
        }
        else
        {
            //$userRowId = "(employees.userRowId <= 0 OR employees.userRowId is NULL)";
            $this->db->select('addressbook.name, employees.empRowId');
            $this->db->from('addressbook');
            $this->db->join('employees','employees.abRowId = addressbook.abRowId');
            $this->db->where('employees.deleted', 'N');
            $this->db->where('employees.salType', 'M');
            $this->db->where('employees.discontinue', 'N');
            $this->db->where('employees.designationRowId', '1');
            $this->db->where('employees.doj<=', date('Y-m-d', strtotime($this->input->post('dt'))));
            //$this->db->where($userRowId);
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();

            //print_r($this->db->last_query()); die;

            return($query->result_array());
        }
    }


	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        // echo $myTableRows;

        $this->db->where('dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
        $this->db->delete('attendance');	
        
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('attendanceRowId');
			$query = $this->db->get('attendance');
	        $row = $query->row_array();
	        $attendanceRowId = $row['attendanceRowId']+1;

			$data = array(
		        'attendanceRowId' => $attendanceRowId
		        , 'dt' => $dt
		        , 'empRowId' => $TableData[$i]['empRowId']
		        , 'tmIn' => $TableData[$i]['tmIn']
		        , 'tmOut' => $TableData[$i]['tmOut']
		        , 'extraHours' => $TableData[$i]['extraHours']
		        , 'duty' => $TableData[$i]['duty']
		        , 'remarks' => $TableData[$i]['remarks']
		        , 'orgRowId' => $this->session->orgRowId
                , 'createdBy' => $this->session->userRowId
			);
            $this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('attendance', $data);	
		}

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}

}