<?php
class Purchaseorderapprove_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPurchaseOrders()
	{
		$this->db->select('po.*, addressbook.name, users.uid');
		$this->db->from('po');
		$this->db->join('parties','parties.partyRowId = po.partyRowId');
		$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
		$this->db->join('users', 'users.rowid = po.createdBy');
		$this->db->where('po.vType', 'PO');
		$this->db->where('po.deleted', 'N');
		$this->db->where('po.approved', 'N');
		$this->db->where('po.orgRowId', $this->session->orgRowId);
		$this->db->order_by('po.poRowId');
		$query = $this->db->get();
		return($query->result_array());
	}

	public function getProducts()
    {
        // $this->db->select('podetail.*, products.productName, products.roLevel');
        // $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
        // $this->db->from('podetail');
        // $this->db->join('products','products.productRowId = podetail.productRowId');
        // // $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->order_by('poDetailRowId');
        // $query = $this->db->get();
        // return($query->result_array());

        $this->db->select('podetail.*, products.productName, products.roLevel');
        $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
        $this->db->from('podetail');
        $this->db->join('products','products.productRowId = podetail.productRowId');
        $this->db->order_by('poDetailRowId');
        $query = $this->db->get();
        // return($query->result_array());
        $rows = array();
        foreach ($query->result_array() as $row)
        {
            $row['stock'] = 0;
            $q = "Select sum(receive)-sum(Issue) as currentQty from ledgerp where productRowId=".$row['productRowId']. " AND orgRowId=".$this->session->orgRowId;
            $queryStock = $this->db->query($q);
            if ($queryStock->num_rows() > 0)
            {
                $rowStock = $queryStock->row(); 
                $stock = $rowStock->currentQty;
            }
            $row['stock'] = $stock;
            $rows[] = $row;
        }
        
        return $rows;
    }


    public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE po WRITE, podetail WRITE, notifications WRITE');

		$data = array(
	        'approved' => 'Y'
	        , 'approvedDt' => date('Y-m-d')
            , 'totalQty' => $this->input->post('totalQty')
            , 'totalAmt' => $this->input->post('totalAmt')
		);
		$this->db->where('poRowId', $this->input->post('poRowId'));
		$this->db->update('po', $data);	

		/////Updating Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
			$data = array(
			        'orderQty' => (float)$TableData[$i]['orderQty']
                    , 'amt' => (float) $TableData[$i]['amt']
                    , 'amtAfterDiscount' => (float) $TableData[$i]['amtAfterDiscount']
			);
			$this->db->where('poDetailRowId',  $TableData[$i]['poDetailRowId']);
			$this->db->update('podetail', $data);			
        }
        /////END - Updating Products

        ////////////////////Notification
            $this->db->select_max('notificationRowId');
            $queryN = $this->db->get('notifications');
            $rowN = $queryN->row_array();

            $notificationRowId = $rowN['notificationRowId']+1;
            $data = array(
                'notificationRowId' => $notificationRowId
                , 'orgRowId' => $this->session->orgRowId
                , 'vType' => 'PO'
                , 'vNo' => $this->input->post('poRowId')
                , 'notification' => "PO of " . $this->input->post('globalPartyName') . " APPROVED"
                , 'userRowIdFor' => $this->input->post('globalUserRowId')
                , 'createdBy' => $this->session->userRowId
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('notifications', $data);  

   		$this->db->query('UNLOCK TABLES');
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}

    public function reject()
    {
        $data = array(
            'deleted' => 'Y'
            , 'rejectReason' => $this->input->post('rejectReason')
        );
        $this->db->where('poRowId',  $this->input->post('poRowId'));
        $this->db->update('po', $data);


        ////////////////////Notification
            $this->db->select_max('notificationRowId');
            $queryN = $this->db->get('notifications');
            $rowN = $queryN->row_array();

            $notificationRowId = $rowN['notificationRowId']+1;
            $data = array(
                'notificationRowId' => $notificationRowId
                , 'orgRowId' => $this->session->orgRowId
                , 'vType' => 'PO'
                , 'vNo' => $this->input->post('poRowId')
                , 'notification' => "PO of " . $this->input->post('globalPartyName') . " REJECTED (" . $this->input->post('rejectReason') . ")"
                , 'userRowIdFor' => $this->input->post('globalUserRowId')
                , 'createdBy' => $this->session->userRowId
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('notifications', $data);  
    }


    public function getDataLimit()
    {
        if( $this->session->abAccess == "C" )
        {
            $this->db->select('po.*, addressbook.name');
            $this->db->from('po');
            $this->db->join('parties','parties.partyRowId = po.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            $this->db->where('po.vType', 'PO');
            $this->db->where('po.deleted', 'N');
            $this->db->where('po.approved', 'Y');
            $this->db->where('po.orgRowId', $this->session->orgRowId);
            $this->db->order_by('po.poRowId desc');
            $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('po.*, addressbook.name');
            $this->db->from('po');
            $this->db->join('parties','parties.partyRowId = po.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            $this->db->where('po.vType', 'PO');
            $this->db->where('po.deleted', 'N');
            $this->db->where('po.approved', 'Y');
            $this->db->where('po.orgRowId', $this->session->orgRowId);
            // $abAccessIn = explode(",", $this->session->abAccessIn);
            // $this->db->where_in('po.createdBy', $abAccessIn);
            $this->db->where_in('po.createdBy', $this->session->userRowId);
            $this->db->order_by('po.poRowId desc');
            $this->db->limit(5);
            $query = $this->db->get();
            return($query->result_array());
        }
    }

    public function getDataAll()
    {
        if( $this->session->abAccess == "C" )
        {
            $this->db->select('po.*, addressbook.name');
            $this->db->from('po');
            $this->db->join('parties','parties.partyRowId = po.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            $this->db->where('po.vType', 'PO');
            $this->db->where('po.deleted', 'N');
            // $this->db->where('po.approved', 'Y');
            $this->db->where('po.orgRowId', $this->session->orgRowId);
            $this->db->order_by('po.poRowId');
            // $this->db->limit(5);
            $query = $this->db->get();

            return($query->result_array());
        }
        else if( $this->session->abAccess == "L" )
        {
            $this->db->select('po.*, addressbook.name');
            $this->db->from('po');
            $this->db->join('parties','parties.partyRowId = po.partyRowId');
            $this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
            $this->db->where('po.vType', 'PO');
            $this->db->where('po.deleted', 'N');
            $this->db->where('po.approved', 'Y');
            $this->db->where('po.orgRowId', $this->session->orgRowId);
            // $abAccessIn = explode(",", $this->session->abAccessIn);
            // $this->db->where_in('po.createdBy', $abAccessIn);
            $this->db->where_in('po.createdBy', $this->session->userRowId);
            $this->db->order_by('po.poRowId');
            // $this->db->limit(5);
            $query = $this->db->get();
            return($query->result_array());
        }
    }


    public function checkDependency()
    {
        $this->db->select('po.saved');
        $this->db->where('po.poRowId', $this->input->post('rowId'));
        $this->db->where('po.vType', 'PO');
        $this->db->from('po');
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
         $saved = $row->saved;
        }
        if ($saved == 'Y')
        {
         return 1;
        }
    }

    public function delete()
    {
        $data = array(
            'approved' => 'N'
        );
        $this->db->where('poRowId',  $this->input->post('rowId'));
        $this->db->update('po', $data);
        // $this->db->where('poRowId',  $this->input->post('poRowId'));
        // $this->db->delete('po');

    }
}