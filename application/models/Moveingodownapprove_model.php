<?php
class Moveingodownapprove_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPending()
	{
		$this->db->select('movegodown.*, products.productName, colours.colourName, addressbook.name as fromStageName, abTo.name as toStageName');
        $this->db->join('products','products.productRowId = movegodown.productRowId');
        $this->db->join('colours','colours.colourRowId = movegodown.colourRowId');
        $this->db->join('addressbook','addressbook.abRowId = movegodown.fromStage');
        $this->db->join('addressbook as abTo','abTo.abRowId = movegodown.toStage');
        $this->db->where('movegodown.deleted', 'N');
        $this->db->where('movegodown.authorised', 'N');
        $this->db->order_by('rowId');
        // $this->db->limit(10);
        $query = $this->db->get('movegodown');
        return($query->result_array());
	}

	// public function getProducts()
 //    {
 //        $this->db->select('podetail.*, products.productName');
 //        $this->db->where('podetail.poRowId', $this->input->post('poRowId'));
 //        $this->db->from('podetail');
 //        $this->db->join('products','products.productRowId = podetail.productRowId');
 //        // $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
 //        $this->db->order_by('poDetailRowId');
 //        $query = $this->db->get();
 //        return($query->result_array());
 //    }


    public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE movegodown WRITE, godownstatus WRITE, openingbal WRITE');

		$data = array(
	        'authorised' => 'Y'
		);
		$this->db->where('rowId', $this->input->post('rowId'));
		$this->db->update('movegodown', $data);	


        if($this->input->post('fromStage') == "-2")    ///despatch stage se godown m
        {
            $this->db->select('colourRowId');
            $this->db->from('godownstatus');
            $this->db->where('godownstatus.productRowId', $this->input->post('productRowId'));
            $this->db->where('godownstatus.colourRowId', $this->input->post('colourRowId'));
            $this->db->where('godownstatus.godownAbRowId', $this->input->post('toStage'));
            $this->db->where('godownstatus.orgRowId', $this->session->orgRowId);
            $query = $this->db->get();
            if ($query->num_rows() > 0) ///this product and color in this godown already exists or not
            {
                $found = 1;
            }
            else
            {
                $found = 0;
            }
            if( $found == 1 )
            {
                $q="Update godownstatus set avlQty = avlQty + " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND colourRowId=". $this->input->post('colourRowId'). " AND godownAbRowId=". $this->input->post('toStage'). " AND orgRowId=". $this->session->orgRowId;
                $this->db->query($q);
            }   
            else /// agar godown m pahle se nahi h product
            {
                $data = array(
                    'godownAbRowId' => $this->input->post('toStage')
                    , 'productRowId' => $this->input->post('productRowId')
                    , 'avlQty' => $this->input->post('qty')
                    , 'orgRowId' => $this->session->orgRowId
                    , 'colourRowId' => $this->input->post('colourRowId')
                );
                $this->db->insert('godownstatus', $data); 
            } 


            //////// Ab Desp stage m se minus
            $q="Update openingbal set avlQty = avlQty - " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND stageRowId=26 AND colourRowId=". $this->input->post('colourRowId'). " AND orgRowId=". $this->session->orgRowId;
            $this->db->query($q);
        }
        else /// agar godown se shift kr rahe h
        {
            if($this->input->post('toStage') == "-2")    ///agar godown se desp stage m bhej rahe h
            {
                ////Godown ki qty minus
                $q="Update godownstatus set avlQty = avlQty - " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND colourRowId=". $this->input->post('colourRowId'). " AND godownAbRowId=". $this->input->post('fromStage'). " AND orgRowId=". $this->session->orgRowId;
                $this->db->query($q);

                //// Desp stage m qty PLUS
                $q="Update openingbal set avlQty = avlQty + " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND stageRowId=26 AND colourRowId=". $this->input->post('colourRowId'). " AND orgRowId=". $this->session->orgRowId;
                $this->db->query($q);
            }
            else ////// Agar ek godown se dusre godown m bhej rahe h
            {
                ///// from godown m MINUS
                $q="Update godownstatus set avlQty = avlQty - " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND colourRowId=". $this->input->post('colourRowId'). " AND godownAbRowId=". $this->input->post('fromStage'). " AND orgRowId=". $this->session->orgRowId;
                $this->db->query($q);

                ///// To Godown m PLUS
                $this->db->select('colourRowId');
                $this->db->from('godownstatus');
                $this->db->where('godownstatus.productRowId', $this->input->post('productRowId'));
                $this->db->where('godownstatus.colourRowId', $this->input->post('colourRowId'));
                $this->db->where('godownstatus.godownAbRowId', $this->input->post('toStage'));
                $this->db->where('godownstatus.orgRowId', $this->session->orgRowId);
                $query = $this->db->get();
                if ($query->num_rows() > 0) ///this product and color in this godown already exists or not
                {
                    $found = 1;
                }
                else
                {
                    $found = 0;
                }
                if( $found == 1 )     /// agar godownStatus m wo godown pahle se h
                {
                    $q="Update godownstatus set avlQty = avlQty + " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND colourRowId=". $this->input->post('colourRowId'). " AND godownAbRowId=". $this->input->post('toStage'). " AND orgRowId=". $this->session->orgRowId;
                    $this->db->query($q);
                }
                else     ////// agar godownStatus m wo godown NAHI h
                {
                    $data = array(
                        'godownAbRowId' => $this->input->post('toStage')
                        , 'productRowId' => $this->input->post('productRowId')
                        , 'avlQty' => $this->input->post('qty')
                        , 'orgRowId' => $this->session->orgRowId
                        , 'colourRowId' => $this->input->post('colourRowId')
                    );
                    $this->db->insert('godownstatus', $data); 
                }
            }
        } 

   		$this->db->query('UNLOCK TABLES');
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

	}

    public function reject()
    {
        $data = array(
            'authorised' => 'R'
            , 'rejectReason' => $this->input->post('rejectReason')
        );
        $this->db->where('rowId',  $this->input->post('rowId'));
        $this->db->update('movegodown', $data);
    }
}