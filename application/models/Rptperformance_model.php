<?php
class Rptperformance_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmpList()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        // $this->db->where('employees.salType', 'C');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']] = $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        set_time_limit(0);
        //////creating tmp table
        $this->load->dbforge();
        if($this->db->table_exists('tmp'))
        {
            $this->dbforge->drop_table('tmp');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'dt' => array(
                                     'type' => 'DATE',
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'productName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'productWeightage' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,2',
                                ),
                    'stageRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'stageWeight' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,2',
                                ),
                    'moveQty' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'direction' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '1',
                                ),
                    'remarks' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '255',
                                ),
                    'points' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                )
            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmp');

        ///////////////////// loading moveStage data
        $this->db->select('movestage.*, addressbook.name, products.productName, products.weightage productWeightage');
        $this->db->from('movestage');
        $this->db->where('movestage.empRowId', $this->input->post('empRowId') );
        $this->db->where('movestage.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('movestage.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->join('products','products.productRowId = movestage.productRowId');
        $this->db->join('employees','employees.empRowId = movestage.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('productRowId');
        $query = $this->db->get();
        $rowId = 1;
        foreach ($query->result() as $row)
        {
            if( $row->direction == "F" ) ////Fwd Stage movement
            {
                $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'dt' => $row->dt,
                        'productRowId' => $row->productRowId,
                        'productName' => $row->productName,
                        'productWeightage' => $row->productWeightage,
                        'stageRowId' => $row->fromStage,
                        'stageWeight' => $row->weightage,
                        'moveQty' => $row->qty,
                        'direction' => $row->direction,
                        'points' => ($row->productWeightage * $row->weightage * $row->qty),
                        'remarks' => 'From Movement Table'
                );
            }
            else if( $row->direction == "B" ) ////Back Stage movement
            {
                $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'dt' => $row->dt,
                        'productRowId' => $row->productRowId,
                        'productName' => $row->productName,
                        'productWeightage' => $row->productWeightage,
                        'stageRowId' => $row->toStage,
                        'stageWeight' => $row->weightage,
                        'moveQty' => $row->qty,
                        'direction' => $row->direction,
                        'points' => ($row->productWeightage * $row->weightage * $row->qty) * -1,
                        'remarks' => 'From Movement Table'
                );
            }
            $this->db->insert('tmp', $data);               
            $rowId++;
        }

        ///////////////////// loading Direct Points Earned
        $this->db->select('pointstoemployee.*, addressbook.name');
        $this->db->from('pointstoemployee');
        $this->db->where('pointstoemployee.empRowId', $this->input->post('empRowId') );
        $this->db->where('pointstoemployee.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('pointstoemployee.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        // $this->db->join('products','products.productRowId = movestage.productRowId');
        $this->db->join('employees','employees.empRowId = pointstoemployee.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('dt');
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            $data = array(
                    'rowId' => $rowId,
                    'empRowId' => $row->empRowId,
                    'empName' => $row->name,
                    'dt' => $row->dt,
                    'productWeightage' => 0,
                    'stageWeight' => 0,
                    'moveQty' => 0,
                    'points' => $row->points,
                    'remarks' => 'From Direct Points Table'
            );
            $this->db->insert('tmp', $data);               
            $rowId++;
        }

        /// now sending data to client
        $this->db->select_sum('tmp.points');
        $this->db->select('tmp.empRowId, tmp.empName, tmp.dt');
        // $this->db->order_by('rowId');
        $this->db->group_by('tmp.dt');
        $this->db->from('tmp');
        $query = $this->db->get();
        return($query->result_array());
    }
}